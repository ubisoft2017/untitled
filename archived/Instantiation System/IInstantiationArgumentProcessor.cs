﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The actual behaviour (i.e RouteFollow) implements this.
/// </summary>
/// <typeparam name="T">The type of instantiation argument this processes.</typeparam>
public interface IInstantiationArgumentProcessor<T>
{
	void ProcessArgument ( T argument );
}
