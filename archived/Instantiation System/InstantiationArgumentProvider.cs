﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Lives on the instantiator object (i.e spawn point).
/// </summary>
/// <typeparam name="T">The type of instantiation argument this provides.</typeparam>
public abstract class InstantiationArgumentProvider<T> : MonoBehaviour
{
	[SerializeField]
	private T argument;

	public T Argument
	{
		get
		{
			return argument;
		}
	}
}
