﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Lives 
	/// </summary>
	[RequireComponent( typeof( RouteFollowProvider ))]
	public class RouteFollowInstantiable : Instantiable<RouteFollowArgument> { }
}