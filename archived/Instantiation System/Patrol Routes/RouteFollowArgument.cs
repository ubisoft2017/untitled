﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RouteFollowArgument
{
	[Tooltip( "Which route to patrol." )]
	[SerializeField]
	private PatrolRoute route;

	public PatrolRoute Route
	{
		get
		{
			return route;
		}
	}

	[Tooltip( "Whether to start the patrol route in forward or reverse order.")]
	[SerializeField]
	private Traversal traversal;

	public Traversal Traversal
	{
		get
		{
			return traversal;
		}
	}

	[Tooltip( "Which patrol point to start at.")]
	[SerializeField]
	private int first;

	public int First
	{
		get
		{
			return first;
		}
	}
}
