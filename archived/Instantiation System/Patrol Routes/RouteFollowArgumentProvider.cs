﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	public class RouteFollowArgumentProvider : InstantiationArgumentProvider<RouteFollowArgument> { }
}