﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class InstantiatorManager : MonoBehaviour
{
	[SerializeField]
	[HideInInspector]
	private List<Instantiator> list;

	private static InstantiatorManager instance;

	public static void Register ( Instantiator instantiator )
	{
		if ( Application.isPlaying )
		{
			Debug.LogError( "Cannot register Instantiator at runtime." );
			Debug.Break();
			return;
		}

		if ( instance == null )
		{
			instance = FindObjectOfType<InstantiatorManager>();

			if ( instance == null )
			{
#if UNITY_EDITOR
				EditorUtility.DisplayDialog( "InstantiatorManager", "Make sure sce_core is loaded first", "Ok" );
#endif
			}
		}

		if ( instance.list == null )
		{
			instance.list = new List<Instantiator>();
		}

		if ( !instance.list.Contains( instantiator ) )
		{
			instance.list.Add( instantiator );
		}
	}

	private void Awake ()
	{
		if ( instance != null )
		{
#if UNITY_EDITOR
			EditorUtility.DisplayDialog( "InstantiatorManager", "Too many InstantiatorManager instances in the scene. Please make sure there is exactly one.", "Ok" );
			EditorGUIUtility.PingObject( this );
			Debug.Break();
#else
			Destroy( this );		
#endif
			return;
		}

		instance = this;
	}

	void Start ()
	{
		print( list.Count );
	}

	public static int GetIndexOf ( Instantiator instantiator )
	{
		if ( !Application.isPlaying )
		{
			Debug.LogError( "Cannot query InstantiationManager in edit mode." );
			return -1;
		}

		if ( instance == null )
		{
			Debug.LogError( "Missing InstantiationManager." );
			return -1;
		}

		return instance.list.IndexOf( instantiator );
	}

	public static Instantiator GetInstantiator ( int index )
	{
		if ( !Application.isPlaying )
		{
			Debug.LogError( "Cannot query InstantiationManager in edit mode." );
			return null;
		}

		if ( instance == null )
		{
			Debug.LogError( "Missing InstantiationManager." );
			return null;
		}

		if ( instance.list[index] != null )
		{
			return instance.list[index];
		}

		return null;
	}
}
