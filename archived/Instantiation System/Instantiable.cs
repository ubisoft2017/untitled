﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Photon;

/// <summary>
/// These live on the prefab (i.e guard asset).
/// </summary>
/// <typeparam name="T">The type of instantiation argument to look for.</typeparam>
public abstract class Instantiable<T> : Photon.PunBehaviour
{
	public override void OnPhotonInstantiate ( PhotonMessageInfo info )
	{
		if ( photonView.instantiationData.Length != 1 )
		{
			Debug.LogError( "Missing or extra instantiation args. Length is " + photonView.instantiationData.Length + ", expected 1.", this );

			return;
		}

		if ( !(photonView.instantiationData[0] is int) )
		{
			Debug.LogError( "Missing instantiation args. Argument was has type " + photonView.instantiationData[0].GetType() + ", expected int.", this );

			return;
		}

		print ("this");

		int index = (int) photonView.instantiationData [0];

		// The object that instantiated this. (i.e the spawn point)
		Instantiator instantiator = InstantiatorManager.GetInstantiator( index );

		// Look for a provider on the instantiator object who provides the type of argument we want to use.
		var provider = instantiator.GetComponent<InstantiationArgumentProvider<T>>();

		// Look for a processor on the instantiated prefab which will accept the type of argument we want to use.
		var processor = GetComponent<IInstantiationArgumentProcessor<T>>();

		// Set the parent
		transform.parent = instantiator.transform.parent;

		if ( provider != null && processor != null )
		{
			processor.ProcessArgument( provider.Argument );
		}
	}
}
