﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Instantiator : MonoBehaviour
{
	[SerializeField]
	private GameObject prefab;

	private int index = -1;

	private object[] data;

	public GameObject Prefab
	{
		get
		{
			return prefab;
		}
	}

	private void Start ()
	{
		if ( Application.isPlaying )
		{
			index = InstantiatorManager.GetIndexOf( this );
			data = new object[] { index };
		}
	}

	public virtual void Instantiate ()
	{
		PhotonNetwork.InstantiateSceneObject( prefab.name, transform.position, transform.rotation, 0, data );
	}

#if UNITY_EDITOR
	private void OnEnable ()
	{
		if ( Application.isPlaying )
		{
			InstantiatorManager.Register( this );
		}
	}

	private void OnDrawGizmos ()
	{
		if ( prefab != null )
		{
			var meshFilter = prefab.GetComponentInChildren<MeshFilter>();

			if ( meshFilter != null )
			{
				var mesh = meshFilter.sharedMesh;

				if ( mesh != null )
				{
					// Uncomment this to re-enable mesh drawing
					// I disabled it because it does not always work as intended
					//Gizmos.DrawMesh( mesh, transform.position, transform.rotation );
				}
			}
		}
	}
#endif
}
