﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextFinderSphere : ContextFinder
{
    Ray ray;
    RaycastHit hit;

    void Update() {
        //should be moved to controller
        if (Input.GetKeyDown(KeyCode.X))
        {
            LookForContext();
        }
    }
    /*
     *  we look for all objects with colliders within a range, from that list we get the ones that are contextual objects
     *  if we find a contextual object, we check to see if its within our line of sight. If it is, we compare its priority to that of the largest priority 
     *  contextual object we have already found, if we found one. then we pass along its message.
     */
    public override void LookForContext()
    {
        ContextualDescription largestPriorityObject = null;
        float largestPriorityDistance = 0;
        ContextualDescription tempObject = null;

        Collider[] contextObjects = Physics.OverlapSphere(transform.position,range);
        for (int i = 0; i < contextObjects.Length; i++)
        {
            tempObject = contextObjects[i].GetComponent<ContextualDescription>();
            if (tempObject != null) {

                ray.direction = tempObject.transform.position - transform.position;
                ray.origin = transform.position;
                if (Physics.Raycast(ray, out hit, range))
                {

                    if(tempObject== hit.collider.gameObject.GetComponent<ContextualDescription>())
                    {
                        if (largestPriorityObject == null || tempObject.getPriority(hit.distance) > largestPriorityObject.getPriority(largestPriorityDistance)) {
                            largestPriorityDistance = hit.distance;
                            largestPriorityObject = tempObject;
                        }
                        if (tempObject.getPriority(hit.distance) == largestPriorityObject.getPriority(largestPriorityDistance)){

                            if (hit.distance < largestPriorityDistance)
                            {
                                largestPriorityDistance = hit.distance;
                                largestPriorityObject = tempObject;
                            }
                        }
                        

                    }

                }
            }
        }
        if (largestPriorityObject != null) {
            passContextualMessage(largestPriorityObject.getDescription());
        }
    }


}
