﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextFinder : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The maximum distance an object can be for agent to mention it")]
    protected float range;


    public virtual void LookForContext() { }

    protected void passContextualMessage(string message) {
        //send message to other player
        Debug.Log(message);
    }
}
