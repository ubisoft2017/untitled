﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextFinderRay : ContextFinder
{
    private RaycastHit hit;
    private Ray ray;

    private Vector3 forward;

    void Start() // remove this, this is just so game tells you what controlls are if you didnt read this yet
    {
        Debug.Log("Press C to describe what you are looing at, X to describe your surroundings");
    }
    void Update() {
        //should be moved to controller
        if (Input.GetKeyDown(KeyCode.C))
        {
            LookForContext();
        }
    }
    public override void LookForContext()
    {
        ray.direction = transform.forward;
        ray.origin = transform.position;
        

        if (Physics.Raycast(ray, out hit,range))
        {
            ContextualDescription d = hit.collider.gameObject.GetComponent<ContextualDescription>();
            if (d != null) {
                d.getDescription(hit.distance);
                passContextualMessage(d.getDescription(hit.distance));
            }
        }
    }
}
