﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Attach this script to objects that we want to add contextual messages to
 * 
 * I implemented this contextual system in such a way that the user looks at objects and can check it out, or 
 * can simply just be near a contextual object within line of sight.  
 * Its easily changeable to a form where contextual objects that the player can see are highligted, and 
 * player chooses what contextual message to send by looking at the right one.
 */
[RequireComponent(typeof(Collider))]
public class ContextualDescription : MonoBehaviour {


    [SerializeField]
    [Tooltip("The description the agent will give when they see object from close up")]
    private string description_near_sight = "";
    [SerializeField]
    [Tooltip("The description the agent will give when they see object from far away")]
    private string description_far_sight = "";
    [SerializeField]
    [Tooltip("The description the agent will give when they are around the object")]
    private string description_around = "";

    [SerializeField]
    [Tooltip("The distance from object for alternative message (I'm in front of a xxx / I see a xxx)")]
    private float proximityRadius = 0;

    [SerializeField]
    [Tooltip("When near multiple contextual objects, we return the use this when near to determine which one is most important (larger = more important)")]
    private int priority_near = 0;
    [SerializeField]
    [Tooltip("When near multiple contextual objects, we return the use this when far to determine which one is most important (larger = more important)")]
    private int priority_far = 0;

    public float getProximity() {
        return proximityRadius;
    }
    public int getPriority(float dist) {
        return dist<=proximityRadius?priority_near:priority_far;
    }

    public string getDescription(float dist) {
            return dist <= proximityRadius ? description_near_sight : description_far_sight;
    }
    public string getDescription()
    {
        return description_around;
    }
    // will prompt in UI
    public void showDescription() {

    }
}
