﻿using UnityEngine;

using Controls;

[RequireComponent( typeof( InteractionRaycaster ), typeof( Eye ) )]
public class ContextualCommsController : InteractionController<Landmark>
{
	[SerializeField] private ButtonAction relayAction;

	//TODO: more ButtonActions for "hold to expand" and using expanded options

	[SerializeField][HideInInspector] private Eye eye;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		eye = GetComponent<Eye>();
	}

	//TODO: Get rid of this (it's redundant)
	[SerializeField][HideInInspector] private Landmark landmark;

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		//TODO: Clean up these if-statements
		//TODO: Find a better way to hide a prompt that is no longer needed

		if ( HasResult )
		{
			// Make sure we hide any prompts that have gone out of scope
			if ( landmark != null && Result != landmark )
			{
				landmark.PromptUI.Hide();
			}

			landmark = Result;

			// Make the prompt face the player
			landmark.PromptUI.Face( eye );

			// Set the prompt's text
			landmark.PromptUI.ShowText( relayAction.Prompt );

			// Set the prompt's icon
			landmark.PromptUI.ShowIcon( Player.Profile[relayAction].mapping );

			if ( GamePad.Query( relayAction, Player.Profile ) )
			{
				//TODO: comms networking / HUD systems
				Debug.Log( "I'm by the " + landmark.label + "!" );
			}
		}
		else if ( landmark != null )
		{
			landmark.PromptUI.Hide();
		}
	}
}
