﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UI;

[RequireComponent( typeof( PromptAnchor ))]
public class Landmark : MonoBehaviour
{
	[Tooltip( "The name used in the contextual communication message for this landmark." )]
	public string label;

	[SerializeField][HideInInspector] private PromptUI promptUI;

	public PromptUI PromptUI
	{
		get
		{
			return promptUI;
		}
	}

	private void Start ()
	{
		promptUI = GetComponentInChildren<PromptUI>();
	}
}
