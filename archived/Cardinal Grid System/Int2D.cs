﻿using System;
using UnityEngine;

[Serializable]
public struct Int2D
{
	public int x, y;

	public Int2D ( int x, int y )
	{
		this.x = x;
		this.y = y;
	}

	public override string ToString ()
	{
		return "(" + x + ", " + y + ")";
	}

	public override bool Equals ( object obj )
	{
		if ( obj is Int2D )
		{
			Int2D int2D = (Int2D) obj;

			return int2D.x == x && int2D.y == y;
		}

		return base.Equals( obj );
	}

	public static implicit operator Vector2 ( Int2D int2D )
	{
		return new Vector2( int2D.x, int2D.y );
	}

	public static implicit operator Vector3 ( Int2D int2D )
	{
		return new Vector3( int2D.x, int2D.y );
	}

	public static implicit operator Int2D ( Vector2 vector )
	{
		return new Int2D( (int) vector.x, (int) vector.y );
	}

	public static implicit operator Int2D ( CardinalDirection direction )
	{
		switch ( direction )
		{
		case CardinalDirection.East:
			return new Int2D( -1, 0 );

		case CardinalDirection.West:
			return new Int2D( 1, 0 );

		case CardinalDirection.North:
			return new Int2D( 0, 1 );

		case CardinalDirection.South:
			return new Int2D( 0, -1 );
		}

		throw new InvalidOperationException();
	}

	public static implicit operator Int2D ( CardinalQuadrant quadrant )
	{
		switch ( quadrant )
		{
		case CardinalQuadrant.NorthEast:
			return new Int2D( -1, 1 );

		case CardinalQuadrant.NorthWest:
			return new Int2D( 1, 1 );

		case CardinalQuadrant.SouthEast:
			return new Int2D( -1, -1 );

		case CardinalQuadrant.SouthWest:
			return new Int2D( 1, -1 );
		}

		throw new InvalidOperationException();
	}

	public static implicit operator Int2D ( Vector3 vector )
	{
		return new Int2D( (int) vector.x, (int) vector.y );
	}

	public static Int2D operator + ( Int2D lhs, Int2D rhs )
	{
		return new Int2D( lhs.x + rhs.x, lhs.y + rhs.y );
	}

	public static Int2D operator - ( Int2D lhs, Int2D rhs )
	{
		return new Int2D( lhs.x - rhs.x, lhs.y - rhs.y );
	}

	public static Int2D operator % ( Int2D lhs, Int2D rhs )
	{
		return new Int2D( lhs.x % rhs.x, lhs.y % rhs.y );
	}

	public static Vector2 operator + ( Int2D lhs, Vector2 rhs )
	{
		return new Vector2( lhs.x + rhs.x, lhs.y + rhs.y );
	}

	public static Vector2 operator - ( Int2D lhs, Vector2 rhs )
	{
		return new Vector2( lhs.x - rhs.x, lhs.y - rhs.y );
	}

	public static Vector2 operator + ( Vector2 lhs, Int2D rhs )
	{
		return new Vector2( lhs.x + rhs.x, lhs.y + rhs.y );
	}

	public static Vector2 operator - ( Vector2 lhs, Int2D rhs )
	{
		return new Vector2( lhs.x - rhs.x, lhs.y - rhs.y );
	}

	public static Vector3 operator + ( Int2D lhs, Vector3 rhs )
	{
		return new Vector3( lhs.x + rhs.x, lhs.y + rhs.y );
	}

	public static Vector3 operator - ( Int2D lhs, Vector3 rhs )
	{
		return new Vector3( lhs.x - rhs.x, lhs.y - rhs.y );
	}

	public static Vector3 operator + ( Vector3 lhs, Int2D rhs )
	{
		return new Vector3( lhs.x + rhs.x, lhs.y + rhs.y );
	}

	public static Vector3 operator - ( Vector3 lhs, Int2D rhs )
	{
		return new Vector3( lhs.x - rhs.x, lhs.y - rhs.y );
	}

	public static Int2D operator * ( Int2D lhs, int rhs )
	{
		return new Int2D( lhs.x * rhs, lhs.y * rhs );
	}

	public static Int2D operator / ( Int2D lhs, int rhs )
	{
		return new Int2D( lhs.x / rhs, lhs.y / rhs );
	}

	public static Int2D operator % ( Int2D lhs, int rhs )
	{
		return new Int2D( lhs.x % rhs, lhs.y % rhs );
	}

	public static Vector2 operator * ( Int2D lhs, float rhs )
	{
		return new Vector2( lhs.x * rhs, lhs.y * rhs );
	}

	public static Vector2 operator / ( Int2D lhs, float rhs )
	{
		return new Vector2( lhs.x / rhs, lhs.y / rhs );
	}

	public static Vector2 operator % ( Int2D lhs, float rhs )
	{
		return new Vector2( lhs.x % rhs, lhs.y % rhs );
	}

	public static Int2D operator * ( int lhs, Int2D rhs )
	{
		return new Int2D( lhs * rhs.x, lhs * rhs.y );
	}

	public static Int2D operator / ( int lhs, Int2D rhs )
	{
		return new Int2D( lhs / rhs.x, lhs / rhs.y );
	}

	public static Int2D operator % ( int lhs, Int2D rhs )
	{
		return new Int2D( lhs % rhs.x, lhs % rhs.y );
	}

	public static Vector2 operator * ( float lhs, Int2D rhs )
	{
		return new Vector2( lhs * rhs.x, lhs * rhs.y );
	}

	public static Vector2 operator / ( float lhs, Int2D rhs )
	{
		return new Vector2( lhs / rhs.x, lhs / rhs.y );
	}

	public static Vector2 operator % ( float lhs, Int2D rhs )
	{
		return new Vector2( lhs % rhs.x, lhs % rhs.y );
	}
}