﻿using System;

public enum Direction
{
	Left = 1,
	Right = 2,
	Up = 4,
	Down = 8
}