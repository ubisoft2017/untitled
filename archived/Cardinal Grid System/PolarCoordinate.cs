﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PolarCoordinate
{
	private const float HalfPI = Mathf.PI / 2.0f;
	private const float ThreeOverTwoPI = Mathf.PI * 1.5f;

	public float angle, radius;

	public PolarCoordinate ( float angle, float radius )
	{
		this.angle = angle;
		this.radius = radius;
	}

	public PolarCoordinate ( Vector2 vector )
	{
		angle = vector.x == 0.0f ? (vector.y < 0.0f ? ThreeOverTwoPI : HalfPI) : Mathf.Atan( vector.y / vector.x );

		radius = vector.magnitude;
	}
}