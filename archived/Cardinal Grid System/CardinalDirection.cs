﻿using System;
using UnityEngine;

[Flags]
public enum CardinalDirection
{
	East = 1,
	West = 2,
	North = 4,
	South = 8
}

public static class CardinalDirectionExtensions
{
	/// <summary>
	/// Interpret this CardinalDirection as a rotation about the Y axis.
	/// 
	/// West	= -0 degrees;
	/// North	= -90 degrees;
	/// East	= -180 degrees;
	/// South	= -270 degrees.
	/// 
	/// </summary>
	/// <param name="direction">Direction to interpret.</param>
	/// <returns>Rotation as a Quaternion.</returns>
	public static Quaternion AsRotation ( this CardinalDirection direction )
	{
		switch ( direction )
		{
		case CardinalDirection.East:
			return Quaternion.AngleAxis( 0.0f, Vector3.up );

		case CardinalDirection.West:
			return Quaternion.AngleAxis( 180.0f, Vector3.up );

		case CardinalDirection.North:
			return Quaternion.AngleAxis( 270.0f, Vector3.up );

		case CardinalDirection.South:
			return Quaternion.AngleAxis( 90.0f, Vector3.up );
		}

		throw new InvalidOperationException();
	}

	/// <summary>
	/// Interpret this CardinalDirection as a direction vector.
	/// </summary>
	/// <param name="direction">Direction to interpret.</param>
	/// <returns>Direction as a Vector3.</returns>
	public static Vector3 AsVector ( this CardinalDirection direction )
	{
		return direction.AsRotation() * Vector3.right;
	}
}