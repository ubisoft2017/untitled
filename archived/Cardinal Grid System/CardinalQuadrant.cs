﻿using System;

public enum CardinalQuadrant
{
	NorthEast = 1,
	NorthWest = 2,
	SouthEast = 4,
	SouthWest = 8
}