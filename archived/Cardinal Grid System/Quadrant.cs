﻿using System;

public enum Quadrant
{
	UpperLeft = 1,
	UpperRight = 2,
	LowerLeft = 4,
	LowerRight = 8
}