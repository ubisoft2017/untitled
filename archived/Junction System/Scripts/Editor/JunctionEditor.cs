﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor( typeof( Junction ), true )]
public class JunctionEditor : Editor
{
	private const float SNAP_TOLERANCE = 0.75f;

	[InitializeOnLoadMethod]
	private static void Init ()
	{
		SceneView.onSceneGUIDelegate += Poll;
	}

	private static bool interacting;

	private static void Poll ( SceneView sceneView )
	{
		if ( Event.current.button == 0 )
		{
			if ( Event.current.type == EventType.MouseDrag )
			{
				interacting = true;
			}
			else if ( Event.current.type == EventType.MouseUp || Event.current.type == EventType.MouseMove )
			{
				interacting = false;
			}
		}
	}

	SerializedProperty direction;

	private void OnEnable ()
	{
		direction = serializedObject.FindProperty( "initialDirection" );
	}

	public void OnSceneGUI ()
	{
		return;

		var selection = target as Junction;

		// Check whether Alt is being held
		bool suppress = (Event.current.modifiers & EventModifiers.Alt) > 0;

		serializedObject.Update();

		switch ( Tools.current )
		{
		case Tool.Move:

			// Snap to other existing Junction objects
			if ( interacting && !suppress )
			{
				float minx = float.PositiveInfinity;
				float minz = float.PositiveInfinity;
				
				float x = selection.transform.position.x;
				float z = selection.transform.position.z;

				Junction snapx = null;
				Junction snapz = null;

				// Get all of the Junction objects in the scene
				var junctions = Object.FindObjectsOfType<Junction>();

				foreach ( var junction in junctions )
				{
					// Avoid snapping the selection to itself
					if ( junction == selection )
					{
						continue;
					}

					Vector3 junctionPosition = junction.transform.rotation * junction.transform.position;
					Vector3 selectionPosition = junction.transform.rotation * selection.transform.position;

					// Compute per-axis absolute difference of positions
					float absx = Mathf.Abs( junctionPosition.x - selectionPosition.x );
					float absz = Mathf.Abs( junctionPosition.z - selectionPosition.z );

					// If the difference along either axis is within the snap tolerance,
					// (and is the closest match so far) then snap to this new position on that axis

					if ( absx < minx && absx <= SNAP_TOLERANCE )
					{
						snapx = junction;
						minx = absx;
						x = junction.transform.position.x;
					}

					if ( absz < minz && absz <= SNAP_TOLERANCE )
					{
						snapz = junction;
						minz = absz;
						z = junction.transform.position.z;
					}
				}

				// Apply the snap
				selection.transform.position = new Vector3( x, 0.0f, z );

				// Visualize snapped axes

				Handles.color = new Color( 0, 1.0f, 1.0f, 0.5f );

				if ( snapx != null )
				{
					Handles.DrawLine( selection.transform.position, snapx.transform.position );
				}

				if ( snapz != null )
				{
					Handles.DrawLine( selection.transform.position, snapz.transform.position );
				}

				Handles.color = Color.white;
			}

			break;

		case Tool.Rotate:

			break;
		}

		//Handles.Label( selection.transform.position + Vector3.up * 2.0f, direction.enumNames[direction.enumValueIndex] );

		Handles.color = Color.cyan;

		//if ( Handles.Button( selection.transform.position, Quaternion.LookRotation( Vector3.up, Vector3.forward ), 1.5f, 2.0f, Handles.CircleHandleCap ) )
		{
			//direction.enumValueIndex = (direction.enumValueIndex + 1) % 4;
		}

		serializedObject.ApplyModifiedProperties();

		selection.OnDrawHandles();

		Handles.color = Color.white;

		Handles.BeginGUI();

		GUIStyle style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreLabel" );

		GUILayout.Label( interacting ? "Junction Snapping = " + (suppress ? "OFF" : "ON") : "", style );
		GUILayout.Label( interacting ? "(hold Alt to disable)" : "", style );

		Handles.EndGUI();
	}
}
#endif
