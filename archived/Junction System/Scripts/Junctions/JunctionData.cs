﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "New JunctionType.asset", menuName = "Junction Type" )]
public abstract class JunctionData : ScriptableObject
{
	[SerializeField] private GameObject virtualPrefab;

	public GameObject VirtualPrefab
	{
		get
		{
			return virtualPrefab;
		}
	}

#if UNITY_EDITOR
	[SerializeField] private Mesh gizmo;

	public virtual void DrawGizmo ( Vector3 position, CardinalDirection direction )
	{
		if ( gizmo != null )
		{
			Gizmos.color = new Color ( 0, 1, 1, 0.5f );

			Gizmos.DrawMesh( gizmo, position, direction.AsRotation() );

			Gizmos.color = Color.white;
		}
	}
#endif
}
