﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JunctionUI : MonoBehaviour
{
	[SerializeField] private Image image;

	[SerializeField] private RectTransform container;

	public Image Image
	{
		get
		{
			return image;
		}
	}

	[SerializeField] private AudioClip clipCycleSuccess;
	[SerializeField] private AudioClip clipCycleFailure;

	[SerializeField][HideInInspector] private Junction junction;

	private void Start ()
	{
		container.SetParent( GameObject.FindGameObjectWithTag( "World Space Canvas" ).transform, true );
	}

	public void Bind ( Junction junction )
	{
		this.junction = junction;
	}

	public void OnClick ()
	{
		if ( junction.TryCycle() )
		{
			// Play success SFX
		}
		else
		{
			// Play failure SFX
		}
	}
}
