﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( PatrolPoint ) )]
[RequireComponent( typeof( JunctionUI ) )]
public abstract class Junction : MonoBehaviour
{
	[SerializeField][HideInInspector] protected JunctionUI ui;

	private void Awake ()
	{
		Reset();
	}

	private void Start ()
	{
		ui = GetComponent<JunctionUI>();
	} 

	public abstract void Reset ();

	public abstract bool TryCycle ();

	public abstract CardinalDirection TransformHeading ( CardinalDirection heading );

#if UNITY_EDITOR
	public abstract void OnDrawHandles ();
#endif
}
