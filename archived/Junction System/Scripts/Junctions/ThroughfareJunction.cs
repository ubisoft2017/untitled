﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ThroughfarJunction : Junction
{
	public event Action<Junction> OnChangeDirection;

	[SerializeField] private CardinalDirection initialDirection = CardinalDirection.North;

	[SerializeField][HideInInspector] protected CardinalDirection currentDirection;

	public CardinalDirection CurrentDirection
	{
		get
		{
			return currentDirection;
		}

		private set
		{
			if ( value != currentDirection )
			{
				currentDirection = value;

				if ( OnChangeDirection != null )
				{
					OnChangeDirection( this );
				}
			}
		}
	}

	public bool TrySetDirection ( CardinalDirection direction )
	{
		CurrentDirection = direction;

		return true;
	}

	public override bool TryCycle ()
	{
		CardinalDirection previous = currentDirection;

		CardinalDirection next;

		int i = 0;

		do
		{
			next = (CardinalDirection) (1 << i);

			i = (i + 1) % 4;
		}
		while ( next != currentDirection );

		currentDirection = (CardinalDirection) (1 << i);

		return true;
	}

	public override CardinalDirection TransformHeading ( CardinalDirection heading )
	{
		return currentDirection;
	}

	public override void Reset ()
	{
		currentDirection = initialDirection;
	}

#if UNITY_EDITOR
	private static Vector3 L = Vector3.Lerp( Vector3.forward, Vector3.left, 0.5f ).normalized * 0.25f;
	private static Vector3 R = Vector3.Lerp( Vector3.back, Vector3.left, 0.5f ).normalized * 0.25f;

	private void OnDrawGizmos ()
	{
		Gizmos.color = Color.cyan;

		Vector3 center = transform.position + Vector3.up * 0.1f;

		for ( int i = 0; i < 4; i++ )
		{
			Gizmos.DrawRay( center, ((CardinalDirection) (1 << i)).AsVector() );
		}

		Quaternion rotation = Application.isPlaying ? currentDirection.AsRotation() : initialDirection.AsRotation();

		center += rotation * Vector3.right;

		Gizmos.DrawRay( center, rotation * L );
		Gizmos.DrawRay( center, rotation * R );

		Gizmos.color = Color.white;
	}

	public override void OnDrawHandles ()
	{
		Vector3 center = transform.position + Vector3.up * 0.1f;

		for ( int i = 0; i < 4; i++ )
		{
			CardinalDirection direction = ((CardinalDirection) (1 << i));

			Handles.Label( center + direction.AsVector(), direction.ToString().Substring( 0, 1 ) );
		}
	}
#endif
}
