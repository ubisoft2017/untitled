﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototypes.DontBeLate
{
	public class Car : MonoBehaviour
	{
		public WheelCollider leftWheel;
		public WheelCollider rightWheel;

		public float torque  = 90;
		public float brakeStrength;
		public float maxSteer;

		private new Rigidbody rigidbody;

		void Awake ()
		{
			rigidbody = GetComponent<Rigidbody>();
		}

		void Update ()
		{
			leftWheel.transform.GetChild( 0 ).Rotate( Vector3.right, leftWheel.rpm * (360 / 60) * Time.deltaTime );
			rightWheel.transform.GetChild( 0 ).Rotate( Vector3.right, rightWheel.rpm * (360 / 60) * Time.deltaTime );
		}

		void OnGUI ()
		{
			GUI.Label( new Rect( 10, 80, 200, 20 ), "Left Torque: " + leftWheel.motorTorque.ToString() );
			GUI.Label( new Rect( 10, 100, 200, 20 ), "Right Torque: " + rightWheel.motorTorque.ToString() );
			GUI.Label( new Rect( 10, 120, 200, 20 ), "Left Brake: " + leftWheel.brakeTorque.ToString() );
			GUI.Label( new Rect( 10, 140, 200, 20 ), "Right Brake: " + rightWheel.brakeTorque.ToString() );
			GUI.Label( new Rect( 10, 10, 200, 20 ), "RPM: " + leftWheel.rpm.ToString() );
			GUI.Label( new Rect( 10, 30, 200, 20 ), "Velocity: " + rigidbody.velocity.magnitude.ToString() );
			GUI.Label( new Rect( 10, 50, 200, 20 ), "Ratio: " + (leftWheel.rpm / rigidbody.velocity.magnitude).ToString() );
		}

		void Accel ( float amount )
		{
			leftWheel.motorTorque = amount * torque;
			rightWheel.motorTorque = amount * torque;
		}

		void Brake ( float amount )
		{
			leftWheel.brakeTorque = amount * brakeStrength;
			rightWheel.brakeTorque = amount * brakeStrength;
		}

		void Steer ( float amount )
		{
			leftWheel.steerAngle = amount * maxSteer;
			rightWheel.steerAngle = amount * maxSteer;
		}
	}
}
