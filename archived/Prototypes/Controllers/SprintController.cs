﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    [RequireComponent( typeof( ISpeedMultipliers ) )]
    public class SprintController : MonoBehaviour    // NetworkBehaviour
    {
        [Range( 1.0f, 4.0f )] public float multiplier;

        private ISpeedMultipliers locomotion;

        private void Start ()
        {
            locomotion = GetComponent<ISpeedMultipliers>();
        } 

        void Update ()
        {
            if ( Input.GetKey( KeyCode.LeftShift ) )
            {
                locomotion.ForwardSpeedMult = Input.GetKey( KeyCode.LeftShift ) ? multiplier : 1.0f;
            }
        }
    }
}