﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    [RequireComponent( typeof( Rigidbody ) )]
    [RequireComponent( typeof( IGroundInfo ) )]
    public class JumpController : MonoBehaviour
    {
        public float force;

        private new Rigidbody rigidbody;
        private IGroundInfo groundInfo;

        // Used to prevent "bunny hopping"
        private bool canJump;

        void Awake ()
        {
            rigidbody = GetComponent<Rigidbody>();
            groundInfo = GetComponent<IGroundInfo>();
        }

        void Update ()
        {
            //if ( !isLocalPlayer )
            //{
            //    return;
            //}

            if ( groundInfo.IsGrounded )
            {
                // Jumping

                if ( canJump && Input.GetAxis( "Jump" ) == 1.0f )
                {
                    rigidbody.AddForce( Vector3.up * force );

                    // Disable jumping until the Jump button has been released
                    canJump = false;
                }
                else
                {
                    // Re-enable jumping
                    canJump = true;
                }
            }
        }
    }
}