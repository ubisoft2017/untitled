﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    public abstract class LocomotionController : MonoBehaviour /*NetworkBehaviour*/, ISpeedMultipliers
    {
        public virtual float ForwardSpeedMult { get; set; }
        public virtual float LateralSpeedMult { get; set; }
        public virtual float BackwardSpeedMult { get; set; }
    }
}