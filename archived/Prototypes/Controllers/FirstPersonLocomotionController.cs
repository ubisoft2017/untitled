﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    [RequireComponent( typeof( Rigidbody ) )]
    [RequireComponent( typeof( IGroundInfo ) )]
    public class FirstPersonLocomotionController : LocomotionController
    {
        [Tooltip( "Force applied to cancel-out velocity when no input is being received." )]
        public float brakeAccel;

        public float forwardAccel;
        public float backwardAccel;
        public float lateralAccel;

        public float maxForwardSpeed;
        public float maxBackwardSpeed;
        public float maxLateralSpeed;

        private new Rigidbody rigidbody;
        private IGroundInfo groundInfo;

        void Awake ()
        {
            rigidbody = GetComponent<Rigidbody>();
            groundInfo = GetComponent<IGroundInfo>();

            ForwardSpeedMult = 1.0f;
            LateralSpeedMult = 1.0f;
            BackwardSpeedMult = 1.0f;
        }

        void Update ()
        {
            //if ( !isLocalPlayer )
            //{
            //    return;
            //}

            var speed = rigidbody.velocity.magnitude;

            if ( groundInfo.IsGrounded )
            {
                // Negate gravity
                rigidbody.AddForce( -Physics.gravity, ForceMode.Acceleration );

                // Read input axes

                var x = Input.GetAxis( "Horizontal" );
                var z = Input.GetAxis( "Vertical" );

                Debug.DrawRay( transform.position, groundInfo.Forward, Color.blue );
                Debug.DrawRay( transform.position, groundInfo.Right, Color.red );
                Debug.DrawRay( transform.position, groundInfo.Surface.normal, Color.green );

                // Decompose velocity

                var lateralDot = Vector3.Dot( rigidbody.velocity, groundInfo.Right );
                var alignedDot = Vector3.Dot( rigidbody.velocity, groundInfo.Forward );

                var lateralSpeed = speed * lateralDot;
                var alignedSpeed = speed * alignedDot;

                // Apply lateral locomotion forces (strafing)

                if ( x > 0.0f )
                {
                    if ( lateralSpeed < maxLateralSpeed * LateralSpeedMult )
                    {
                        rigidbody.AddForce( groundInfo.Right * x * lateralAccel * Time.deltaTime );
                    }
                }
                else
                {
                    if ( -lateralSpeed < maxLateralSpeed * LateralSpeedMult )
                    {
                        rigidbody.AddForce( groundInfo.Right * x * lateralAccel * Time.deltaTime );
                    }
                }

                // Apply aligned locomotion forces (running forward / backward)

                if ( z > 0.0f )
                {
                    if ( alignedSpeed < maxForwardSpeed * ForwardSpeedMult )
                    {
                        rigidbody.AddForce( groundInfo.Forward * z * forwardAccel * Time.deltaTime );
                    }
                }
                else
                {
                    if ( -alignedSpeed < maxBackwardSpeed * BackwardSpeedMult )
                    {
                        rigidbody.AddForce( groundInfo.Forward * z * backwardAccel * Time.deltaTime );
                    }
                }

                // Apply lateral braking forces

                if ( x == 0.0f )
                {
                    rigidbody.AddForce( groundInfo.Right * (- lateralSpeed / maxLateralSpeed) * brakeAccel * Time.deltaTime );
                }

                // Apply in-line braking forces

                if ( z == 0.0f )
                {
                    rigidbody.AddForce( groundInfo.Forward * (-alignedSpeed / maxForwardSpeed) * brakeAccel * Time.deltaTime );
                }
            }
        }

        void OnGUI ()
        {
            // (for debug only)

            GUILayout.Label( rigidbody.velocity.ToString() );
            GUILayout.Label( rigidbody.velocity.magnitude.ToString() );
        }
    }
}