﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    public interface IGroundInfo
    {
        bool IsGrounded { get; }
        RaycastHit Surface { get; }
        Vector3 Forward { get; }
        Vector3 Right { get; }
    }
}