﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AutoAim : MonoBehaviour {

	private Camera mainCamera;
	private GameObject[] enemies;
	private float smallestDistance;
	private Vector3 lockOnPosition;
	private Vector3 targetDirection;
	private Quaternion lookRotation;
	private Prototypes.Controllers.FirstPersonCameraController fpsControl;
	private GameObject theTarget;

	void Start () {
		mainCamera = Camera.main;
		fpsControl = GetComponent<Prototypes.Controllers.FirstPersonCameraController> ();
	}

	public void LockOnToTarget(){
		enemies = GameObject.FindGameObjectsWithTag ("enemy");
		smallestDistance = 10000.0f;

		foreach (GameObject e in enemies) {
			if(Vector3.Distance(mainCamera.transform.position, e.transform.position) < smallestDistance){
				smallestDistance = Vector3.Distance (mainCamera.transform.position, e.transform.position);
				lockOnPosition = e.transform.position;
				theTarget = e;
			}
		}

		theTarget.GetComponent ("Halo").GetType ().GetProperty ("enabled").SetValue (this, true, null);
		targetDirection = (lockOnPosition - transform.position).normalized;
		lookRotation = Quaternion.LookRotation (targetDirection);
		transform.rotation = lookRotation;
	}


	void Update () {
		
		if (Input.GetButtonDown ("Fire2")) {
			fpsControl.Freeze ();
			LockOnToTarget ();
			fpsControl.SetRotationXandY (transform.eulerAngles.y, transform.eulerAngles.x);

		}
	}
}
