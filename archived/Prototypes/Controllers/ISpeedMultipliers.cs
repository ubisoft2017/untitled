﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Prototypes.Controllers
{
    public interface ISpeedMultipliers
    {
        float ForwardSpeedMult { get; set; }
        float LateralSpeedMult { get; set; }
        float BackwardSpeedMult { get; set; }
    }
}