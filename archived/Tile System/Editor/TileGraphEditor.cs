﻿using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( TileGraph ), true )]
public class TileGraphEditor : Editor
{
	[SerializeField] private int x, y;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		serializedObject.Update();

		EditorGUILayout.PrefixLabel( "Insertion Coordinates" );

		EditorGUILayout.BeginHorizontal();

		x = EditorGUILayout.IntField( x );
		y = EditorGUILayout.IntField( y );

		EditorGUILayout.EndHorizontal();

		var graph = target as TileGraph;

		if ( GUILayout.Button( "Insert" ) && !graph.HasTileAt( new Int2D( x, y ) ) )
		{
			var coordinatesProperty = serializedObject.FindProperty( "serializedCoordinates" );
			var tilesProperty = serializedObject.FindProperty( "serializedTiles" );

			coordinatesProperty.InsertArrayElementAtIndex( coordinatesProperty.arraySize );
			tilesProperty.InsertArrayElementAtIndex( tilesProperty.arraySize );

			coordinatesProperty.GetArrayElementAtIndex( coordinatesProperty.arraySize - 1 ).FindPropertyRelative( "x" ).intValue = x;
			coordinatesProperty.GetArrayElementAtIndex( coordinatesProperty.arraySize - 1 ).FindPropertyRelative( "y" ).intValue = y;

			tilesProperty.GetArrayElementAtIndex( tilesProperty.arraySize - 1 ).FindPropertyRelative( "coordinates" ).FindPropertyRelative( "x" ).intValue = x;
			tilesProperty.GetArrayElementAtIndex( tilesProperty.arraySize - 1 ).FindPropertyRelative( "coordinates" ).FindPropertyRelative( "y" ).intValue = y;
		}

		EditorGUILayout.LabelField( "Tiles: " + graph.Count );

		serializedObject.ApplyModifiedProperties();
	}
}
