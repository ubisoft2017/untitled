﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Tile
{
	[SerializeField][HideInInspector] private Int2D coordinates;

	[SerializeField][HideInInspector] private BlockInstance block;

	public Int2D Coordinates
	{
		get
		{
			return coordinates;
		}
	}

	public Tile ( Int2D coordinates )
	{
		this.coordinates = coordinates;
	}

#if UNITY_EDITOR
	public void DrawGizmo ( float stride )
	{
		Gizmos.DrawWireCube( new Vector3( coordinates.x, 0.5f, coordinates.y ) * stride, Vector3.one * stride );
	}
#endif
}
