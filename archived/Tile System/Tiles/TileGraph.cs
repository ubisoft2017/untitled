﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TileGraph : MonoBehaviour, IEnumerable<Tile>, ISerializationCallbackReceiver
{
	[Tooltip( "Length / width of each tile." )]
	[SerializeField] private float stride = 1.0f;

	public float Stride
	{
		get
		{
			return stride;
		}
	}

	private Dictionary<Int2D,Tile> tiles;

	public int Count
	{
		get
		{
#if UNITY_EDITOR
			return tiles == null ? serializedTiles.Length : tiles.Count;
#else
			return tiles.count;
#endif
		}
	}

	public Tile this[int x, int y]
	{
		get
		{
			return this[new Int2D( x, y )];
		}
	}

	public Tile this[Int2D i]
	{
		get
		{
			Tile tile;

#if UNITY_EDITOR
			if ( tiles != null )
#endif
			{
				tiles.TryGetValue( i, out tile );

				return tile;
			}

			return null;
		}
	}

	private void Awake ()
	{
		if ( tiles == null )
		{
			tiles = new Dictionary<Int2D, Tile>();
		}
	}

	public Tile AddTileAt( Int2D coordinates )
	{
		// Check that the coordinates are free
		if ( tiles.ContainsKey( coordinates ) )
		{
			throw new InvalidOperationException( "A tile already exists at " + coordinates );
		}

		//TODO: pooling

		Tile tile = new Tile( coordinates );

		tiles.Add( coordinates, tile );

		return tile;
	}

	public bool HasTileAt ( Int2D coordinates )
	{
		return tiles != null && tiles.ContainsKey( coordinates );
	}

	IEnumerator<Tile> IEnumerable<Tile>.GetEnumerator ()
	{
		return tiles.Values.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator ()
	{
		return tiles.Values.GetEnumerator();
	}

#region Serialization
	[SerializeField][HideInInspector] private Int2D[] serializedCoordinates;
	[SerializeField][HideInInspector] private Tile[] serializedTiles;

	void ISerializationCallbackReceiver.OnBeforeSerialize ()
	{
		if ( tiles != null )
		{
			serializedCoordinates = tiles.Keys.ToArray();
			serializedTiles = tiles.Values.ToArray();
		}
	}

	void ISerializationCallbackReceiver.OnAfterDeserialize ()
	{
		if ( tiles == null )
		{
			tiles = new Dictionary<Int2D, Tile>();
		}

		if ( serializedCoordinates != null && serializedTiles != null )
		{
			for ( int i = 0; i < serializedCoordinates.Length; i++ )
			{
				tiles[serializedCoordinates[i]] = serializedTiles[i];
			}
		}
	}
#endregion

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		if ( tiles != null )
		{
			foreach ( var tile in tiles.Values )
			{
				tile.DrawGizmo( stride );
			}
		}
	}
#endif
}
