﻿using System;
using UnityEngine;

/// <summary>
/// A Block represents a single object that can be added or removed to a level. A block may span a single tile, or several.
/// </summary>
[Serializable]
public abstract class Block : ScriptableObject
{
	[SerializeField] private Int2D anchor;
	[SerializeField] private bool [,] footprint;

	public Int2D Lengths
	{
		get
		{
			return new Int2D( footprint.GetLength( 0 ), footprint.GetLength( 1 ) );
		}
	}

	/// <summary>
	/// Determine the data necessary to fully instantiated a Block.
	/// </summary>
	/// <param name="graph">The graph in which the Block will be instantiated.</param>
	/// <param name="coordinates">The coordinates at which the Block will be instantiated.</param>
	/// <param name="direction">The heading with which the Block will be instantiated.</param>
	/// <param name="data">Fully determined instantiation data.</param>
	/// <returns>True, if the coordinates support instantiation of the block.</returns>
	public abstract bool TryGetInstantiation ( TileGraph graph, Int2D coordinates, CardinalDirection direction, ref BlockInstanceData data );
}
