﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct BlockInstanceData
{
	[SerializeField] private GameObject prefab;

	[SerializeField] private Int2D anchor;

	[SerializeField] private CardinalDirection direction;

	[SerializeField] private bool[,] mask;

	public BlockInstanceData ( GameObject prefab, CardinalDirection direction )
	{
		this.prefab = prefab;
		this.direction = direction;

		throw new NotImplementedException();
	}
}
