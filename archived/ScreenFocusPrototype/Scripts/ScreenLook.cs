﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenLook : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Transform cameraTransform = Camera.main.transform;

        RaycastHit hit;

        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit))
        {
            if (hit.transform.tag == "screen")
            {
                hit.transform.GetComponent<ScreenTexture>().setFocus();
            }
        }

    }
}
