﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTexture : MonoBehaviour {

    public Material focusMaterial;
    public Material unfocusMaterial;

    bool focus;
    bool lastFocus;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //if the focus was changed in the last frame
        if(focus != lastFocus)
        {
            if (focus == true) //if we should focus
            {
                GetComponent<Renderer>().material = focusMaterial;
            }
            else //if focus is gone
            {
                GetComponent<Renderer>().material = unfocusMaterial;
            }
        }

        //sets up for next frame
        lastFocus = focus;

        //reset focus, setFocus() will set the right value if it needs to
        focus = false;
	}


    //called from ScreenLook
    public void setFocus()
    {
        focus = true;
    }
}
