﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recoil : MonoBehaviour {


    float fireRate;
    TrackingGun gun;
    [SerializeField]
    Transform arm;
    Quaternion startRotation;
    Vector3 startPosition;
    NoiseBehaviour noise;
    [SerializeField]
    public AnimationCurve curve;
    [SerializeField]
    float rangeY;
    float x, y;
    float counter = 0;
    bool hasFired = false;

    [SerializeField]
    Vector3 rotationMax;
    [SerializeField]
    Vector3 endPosition;
    void Awake() {
        gun = GetComponent<TrackingGun>();
        startRotation = arm.localRotation;
        startPosition = arm.localPosition;
        noise = arm.GetComponent<NoiseBehaviour>();
    }

    void Update()
    {
        if (hasFired && x < fireRate)
        {
            y = curve.Evaluate(x / fireRate) * rangeY;
            x += Time.deltaTime;
           
            arm.localRotation = Quaternion.Slerp(startRotation, Quaternion.Euler(rotationMax), y);
            arm.localPosition = Vector3.Lerp(startPosition,endPosition, y);


        }
        else {
            hasFired = false;
            x = 0;
        }
    }
    public void recoil() {
        hasFired = true;
        x = 0;
        noise.enabled = false;
        Invoke("readyNextFire",fireRate);
    }

    void readyNextFire() {
        noise.enabled = true;
        gun.reloaded();

    }

    public void setFireRate(float r) {
        fireRate = r;
    }


}
