﻿Shader "Unlit/WrapTexture"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		[Toggle] _shouldFlow("Text should move?", Int) = 1
		_mask("Text should move?", Int) = 0
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			int _mask = 0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};


			int _shouldFlow;

			sampler2D _MainTex;
			float4 _MainTex_ST;

			uniform float limits[6];
			int _disappear = 0;


			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float2 coord = i.uv;

				if (_shouldFlow && ((coord.y <= limits[0] && coord.y >= limits[1]) || (coord.y <= limits[2] && coord.y >= limits[3]) || (coord.y <= limits[4] && coord.y >= limits[5]))){
					float push = frac(_Time[1]);

					coord.x -= push;


					if (coord.x < 0 && !_disappear) {
						coord.x = 1 - abs(coord.x);
					}
				}

				// sample the texture
				fixed4 col = tex2D(_MainTex, coord);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
