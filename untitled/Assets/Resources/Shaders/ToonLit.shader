Shader "Toon/Lit" {
	Properties {
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}
		_Explode( "Explode", Float ) = 0.0
		_WaveExplode( "Wave Explode", Float ) = 0.0
		_Freq( "Frequency", Float ) = 0.0
		_T( "Time", Float ) = 0.0
		_Size( "Size", Vector ) = (1.0, 1.0, 0.0, 0.0)
		_Center ("Center", Vector ) = (0.0, 0.0, 0.0, 0.0)
		_Cutout( "Cutout", Float ) = 0.5
		_Shift ( "Shift", Float ) = 0.1
		_RimColor( "Rim Color", Color ) = (0.26,0.19,0.16,0.0)
		_RimPower( "Rim Power", Range( 0.5,8.0 ) ) = 3.0
		_RimThresh( "Rim Boost", Range( 0.0,0.999 ) ) = 0.5
		_RimClamp( "Rim Clamp", Range( 0.0,1.0 ) ) = 1.0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf ToonRamp vertex:vert

		sampler2D _Ramp;

		const float PI = 3.14159f;
		const float PI2 = 6.28318f;

		float _Explode;
		float _WaveExplode;
		float _Shift;
		float _RimThresh;

		float4 _Center = float4( 0, 0, 0, 0 );
		float4 _Size = float4( 1.0f, 1.0f, 0, 0 );
		float _Freq = 1.0f;
		float _T = 0.0f;
		float _Cutout = 0.5f;

		float4 _RimColor;
		float _RimPower;
		float _RimClamp;

		// custom lighting function that uses a texture ramp based
		// on angle between light direction and normal
		#pragma lighting ToonRamp exclude_path:prepass
		inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
		{
			#ifndef USING_DIRECTIONAL_LIGHT
			lightDir = normalize(lightDir);
			#endif

			half3 viewDir = UNITY_MATRIX_IT_MV[2].xyz;

			half d = clamp( dot (s.Normal, lightDir) * 0.5 + 0.5, 0.0, 1.0 );

			d = pow( d, _Shift );

			//d *= dot( s.Normal, viewDir );

			half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
	
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
			c.a = 0;
			return c;
		}

		sampler2D _MainTex;
		float4 _Color;

		// Vertex modifier function
		void vert( inout appdata_full v ) {

			float4 worldSpaceVertex = mul( unity_ObjectToWorld, v.vertex ); // object to world

			float vertDist = distance( worldSpaceVertex, _WorldSpaceCameraPos );

			float peakDist = _T * _Freq;

			float wave = sin( max( _Size.x - abs( vertDist - peakDist ), 0.0f ) / _Size.x );

			worldSpaceVertex.y += wave * _Size.y;

			//v.vertex = mul( unity_WorldToObject, worldSpaceVertex ); // world to object

			float expl = _Explode + wave * _WaveExplode;

			v.vertex.x += v.normal.x * expl;
			v.vertex.y += v.normal.y * expl;
			v.vertex.z += v.normal.z * expl;
		}

		struct Input {
			float2 uv_MainTex : TEXCOORD0;
			float3 viewDir;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			o.Albedo = c.rgb;
			o.Alpha = 1.0f;

			half rim = 1.0 - saturate( dot( normalize( IN.viewDir ), o.Normal ) );

			rim = pow( rim, _RimPower );

			o.Emission = _RimColor.rgb * lerp( rim, floor( rim + _RimThresh ), _RimClamp );
		}
		ENDCG

	} 

	Fallback "Diffuse"
}
