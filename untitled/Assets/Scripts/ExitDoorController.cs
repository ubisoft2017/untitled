﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoorController : InteractionController<ExitDoor>
{
	public Tweening.TweenEvent doneEvent;

	Tweening.TweenEvent fadeOut;

	protected override void ControllerAwake ()
	{
		base.ControllerAwake();
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		if ( canNewlyInteract )
		{
			Register( Result.GetAction(), () =>
			{
				var session = FindObjectOfType<SessionController>();

				var list = FindObjectOfType<LevelList>();

				if ( session.LoadedIndex < list.Levels.Count - 1 )
				{
					session.UnloadLevel( session.LoadedIndex + 1 );
				}
				else
				{
					///TODO: "you win!"

					AgentToHacker.NotifyDone();

					doneEvent.Invoke();
				}
			} );
		}
		else if ( canNoLongerInteract )
		{
			Clear();
		}
	}
}
