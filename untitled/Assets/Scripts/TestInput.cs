﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Controls;

namespace Tests
{
	public class TestInput : ControllerBase
	{
		public ButtonAction action;

		private int counter;

		protected override void ControllerAwake ()
		{
			Register( action, () => Debug.Log( "Callback! " + (++counter) ) );
		}
	}
}
