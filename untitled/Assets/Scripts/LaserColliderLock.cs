﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserColliderLock : MonoBehaviour {

	[SerializeField] Transform laserCollider;

	private Vector3 origin;

	// Use this for initialization
	void Start () {
		origin = new Vector3( 0, 0, 0 );
	}
	
	// Update is called once per frame
	void Update () {
		laserCollider.localPosition = origin;
	}
}
