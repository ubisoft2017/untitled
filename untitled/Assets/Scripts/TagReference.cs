﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TagReference
{
	[SerializeField]
	private string tag;

	public string Tag
	{
		get
		{
			return tag;
		}
	}

	public static implicit operator string ( TagReference tag )
	{
		return tag.tag;
	}
}
