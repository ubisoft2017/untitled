﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class DevAction
{
	public readonly string name;

	public readonly Action action;

	public readonly KeyCode[] shortcut;

	public bool HasShortcut
	{
		get
		{
			return shortcut.Length > 0;
		}
	}

	public DevAction ( string name, Action action, params KeyCode[] shortcut )
	{
		this.name = name;
		this.action = action;
		this.shortcut = shortcut;
	}
}

public class DevToggle
{
	public readonly string name;

	public readonly Func<bool> get;
	public readonly Action<bool> set;

	public readonly KeyCode[] shortcut;

	public bool HasShortcut
	{
		get
		{
			return shortcut.Length > 0;
		}
	}

	public DevToggle ( string name, Func<bool> get, Action<bool> set, params KeyCode[] shortcut )
	{
		this.name = name;

		this.get = get;
		this.set = set;

		this.shortcut = shortcut;
	}
}

public class DevSlider
{
	public readonly string name;

	public readonly Func<float> getf;
	public readonly Action<float> setf;

	public readonly Func<int> geti;
	public readonly Action<int> seti;

	public readonly float minf;
	public readonly float maxf;

	public readonly int mini;
	public readonly int maxi;

	public readonly bool isInteger;
	public readonly bool isFloat;

	public DevSlider ( string name, Func<float> get, Action<float> set, float min = float.MinValue, float max = float.MaxValue )
	{
		this.name = name;

		getf = get;
		setf = set;

		minf = min;
		maxf = max;

		isFloat = true;
	}

	public DevSlider ( string name, Func<int> get, Action<int> set, int min = int.MinValue, int max = int.MaxValue )
	{
		this.name = name;

		geti = get;
		seti = set;

		mini = min;
		maxi = max;

		isInteger = true;
	}
}