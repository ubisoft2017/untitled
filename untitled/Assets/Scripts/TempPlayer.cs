﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

public class TempPlayer : MonoBehaviour
{
	[EnumObject( typeof( PlayerType ) )]
	[SerializeField] private PlayerType type;

	[SerializeField] private new string name = "jim";

	private Player player;

	private void Awake ()
	{
		StartCoroutine( Coroutine() );
	} 

	private IEnumerator Coroutine ()
	{
		while ( !PlayerProfiles.Ready )
		{
			yield return null;
		}

		player = new Player();

		Debug.Log( player.TryJoinAs( new PlayerProfile( name ) ) );

		player.State = type.DefaultState;

		foreach ( var controller in GetComponentsInChildren<ControllerBase>() )
		{
			controller.Player = player;
		}
	}
}
