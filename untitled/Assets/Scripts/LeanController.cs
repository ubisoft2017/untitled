﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

public class LeanController : ControllerBase {

	[SerializeField]
	private ButtonAction rightBumper;
	[SerializeField]
	private ButtonAction leftBumper;
	[SerializeField]
	private float durationLeanIn;
	[SerializeField]
	private float durationLeanOut;
	[SerializeField]
	Transform theEye;
	[SerializeField]
	Transform eyeLean;
	[SerializeField]
	Transform leftLeanPos;
	[SerializeField]
	Transform rightLeanPos;
	[SerializeField]
	float leanTolerance;
	[SerializeField]
	CameraTransform eyeTransformSwitch;

	private float lerpTimeLeanTo;
	private float lerpTimeLeanFrom;
	private RaycastHit leftLeanHit;
	private RaycastHit rightLeanHit;
	private int agentMask = 1 << 14;
	private Vector3 localOrigin;
	private Quaternion localRot;
	private bool leaningLeft;
	private bool leaningRight;

	protected override void ControllerAwake ()
	{
		base.ControllerAwake();
		agentMask = ~agentMask;
		localOrigin = eyeLean.localPosition;
		localRot = eyeLean.localRotation;

		Register( leftBumper, () =>
		{
			if ( leftLeanHit.distance > leanTolerance && !leaningRight )
			{
				leaningLeft = true;
				eyeTransformSwitch.remote = eyeLean;
				input = true;
				lerpTimeLeanFrom = 0.0f;
				lerpTimeLeanTo += Time.deltaTime / durationLeanIn;

				if ( lerpTimeLeanTo < 1.0f )
				{
					eyeLean.localPosition = Vector3.Lerp( localOrigin, leftLeanPos.localPosition, Mathf.SmoothStep( 0, 1, lerpTimeLeanTo ) );
					eyeLean.localRotation = Quaternion.Lerp( localRot, leftLeanPos.localRotation, Mathf.SmoothStep( 0, 1, lerpTimeLeanTo ) );
				}
			}
		} );

		Register( rightBumper, () =>
		{
			if ( rightLeanHit.distance > leanTolerance && !leaningLeft )
			{
				leaningRight = true;
				eyeTransformSwitch.remote = eyeLean;
				input = true;
				lerpTimeLeanFrom = 0.0f;
				lerpTimeLeanTo += Time.deltaTime / durationLeanIn;

				if ( lerpTimeLeanTo < 1.0f )
				{
					eyeLean.localPosition = Vector3.Lerp( localOrigin, rightLeanPos.localPosition, Mathf.SmoothStep( 0, 1, lerpTimeLeanTo ) );
					eyeLean.localRotation = Quaternion.Lerp( localRot, rightLeanPos.localRotation, Mathf.SmoothStep( 0, 1, lerpTimeLeanTo ) );
				}
			}
		} );
	}

	private bool input;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();
		
		Physics.Raycast( theEye.position, (theEye.transform.right * -1), out leftLeanHit, Mathf.Infinity, agentMask );
		Physics.Raycast( theEye.position, (theEye.transform.right), out rightLeanHit, Mathf.Infinity, agentMask );

		if ( !input )
		{
			leaningLeft = false;
			leaningRight = false;

			lerpTimeLeanTo = 0.0f;
			lerpTimeLeanFrom += Time.deltaTime / durationLeanOut;
	
			if( lerpTimeLeanFrom  < 1.0f )
			{
				eyeLean.localPosition = Vector3.Lerp( eyeLean.localPosition, localOrigin, Mathf.SmoothStep( 0, 1, lerpTimeLeanFrom ) );
				eyeLean.localRotation = Quaternion.Lerp( eyeLean.localRotation, localRot, Mathf.SmoothStep( 0, 1, lerpTimeLeanFrom ) );
			}
			else
			{
				eyeTransformSwitch.remote = null;
			}
		}
	}

	private void LateUpdate ()
	{
		input = false;
	}
}
