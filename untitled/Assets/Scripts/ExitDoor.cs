﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

public class ExitDoor : MonoBehaviour, IInteractable {

	[SerializeField]
	private ButtonAction exitAction;
	private ButtonPrompt buttonPrompt;

	public bool canInteract ()
	{
		return true; //valauble checl
	}

	public ButtonAction GetAction ()
	{
		return exitAction;
	}

	public void HidePrompt ()
	{
		if ( buttonPrompt.Visible )
		{
			buttonPrompt.Hide();
		}
	}

	public bool promptVisible ()
	{
		return buttonPrompt.Visible;
	}

	public void ShowPrompt ( PlayerProfile player )
	{
		if ( !buttonPrompt.Visible )
		{
			buttonPrompt.Show( player, exitAction );
		}
	}

	// Use this for initialization
	void Awake () {
		buttonPrompt = GetComponentInChildren<ButtonPrompt>();
	}
}
