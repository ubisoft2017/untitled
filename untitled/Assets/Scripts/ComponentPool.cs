﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public enum PoolPolicy
{
	Strict,
	Increment,
	Double
}

public interface IPool<T>
{
	T Get ();
	void Release ( T resource );
	int Capacity { get; }
	int Headroom { get; }
	int Count { get; }
}

public class ComponentPool<T> : MonoBehaviour, IPool<T> where T : Component
{
	[SerializeField]
	private PoolPolicy policy = PoolPolicy.Strict;

	public PoolPolicy Policy
	{
		get
		{
			return policy;
		}

		set
		{
			policy = value;
		}
	}

	[SerializeField]
	private int capacity = 8;

	public int Capacity
	{
		get
		{
			return array.Length;
		}
	}

	public int Headroom
	{
		get
		{
			return Capacity - head + 1;
		}
	}

	public int Count
	{
		get
		{
			return head + 1;
		}
	}

	[SerializeField]
	[HideInInspector]
	private T[] array;

	[SerializeField]
	[HideInInspector]
	private int head = - 1;

	[SerializeField]
	[HideInInspector]
	private bool init;

	private void Initialize ()
	{
		if ( !init )
		{
			array = new T[capacity];

			Fill( 0, capacity );

			head = array.Length - 1;

			init = true;
		}
	}

	private void Awake ()
	{
		Initialize();
	} 

	protected virtual T Preprocess ( T resource ) { return resource; }
	protected virtual T Postprocess ( T resource ) { return resource; }

	private void Fill ( int offset, int amount )
	{
		for ( int i = offset; i < offset + amount; i++ )
		{
			array[i] = gameObject.AddComponent<T>();
		}
	}

	public T Get ()
	{
		Initialize();

		if ( head < 0 )
		{
			if ( !Expand() )
			{
				Debug.LogError( "The pool has no more resources, and was not able to expand.", this );

				return null;
			}
		}

		return Preprocess( array[head--] );
	}

	public void Release ( T resource )
	{
		if ( head < Capacity - 1 )
		{
			array[++head] = Postprocess( resource );
		}
		else
		{
			Debug.LogError( "The pool is already full.", this );
		}
	}

	private bool Expand ()
	{
		switch ( policy )
		{
		case PoolPolicy.Increment:

			array = new T[Capacity + 1];

			Fill( 0, 1 );

			head = 0;

			return true;

		case PoolPolicy.Double:

			array = new T[Capacity * 2];

			Fill( 0, Capacity / 2 );

			head = Capacity - 1;

			return true;
		}

		return false;
	}
}
