﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

public class PingController : ControllerBase
{

    [SerializeField]
    private ButtonAction pingAction;

    [SerializeField]
    private AudioClip pingClip;

    private bool firstPing = true;



    protected override void ControllerAwake()
    {
        base.ControllerAwake();

        SFX sfx = gameObject.AddComponent<SFX>();

        Register(pingAction, () =>
        {

            bool showPrompt = FindObjectOfType<SessionController>().LoadedIndex == 0 && firstPing;

            AgentToHacker.NotifyPing(transform.position, showPrompt);

            sfx.PlayClip(pingClip, 0.1f, true);

            firstPing = false;

        });
    }
}
