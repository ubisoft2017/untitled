﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if UNITY_EDITOR
interface IRespondsToInspectorAtRuntime
{
	void OnModifiedByInspector ();
}
#endif
