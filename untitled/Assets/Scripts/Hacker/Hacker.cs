﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controls;
using UnityEngine.UI;

public class Hacker : SFX
{
	[SerializeField] private PlayerState initialState;
	[SerializeField] private AudioClip introClip;
	[SerializeField] private Text scoreText;

	protected override void Awake ()
	{
		base.Awake();

		if ( FindObjectOfType<SessionController>().LocalSelection == PlayerSelection.Hacker )
		{
			SessionController.onScoreChange += ( sc ) => scoreText.text = ScoreFormatter.Format( sc.BufferedScore, false );

			FindObjectOfType<SessionController>().Bump();

			PlayClip( introClip, 0.7f, false );

			var player = FindObjectOfType<ProfileController>().Player;

			player.State = initialState;

			foreach ( var controller in GetComponentsInChildren<ControllerBase>() )
			{
				controller.Player = player;
			}
		}
	}
}
