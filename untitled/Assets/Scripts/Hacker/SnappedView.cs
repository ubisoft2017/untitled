﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappedView : MonoBehaviour
{
	[SerializeField] private CameraLookAt cameraLookAt;

	[SerializeField] private Transform initial;

	[SerializeField][Range( -1.0f, 1.0f )] private float tolerance = 0.0f;

	[SerializeField] private List<SnapEdge> edges = new List<SnapEdge>();

	[SerializeField][HideInInspector] private Transform current;

	private void Awake ()
	{
		current = initial;
	}

	/// <summary>
	/// Push the view along an edge.
	/// </summary>
	/// <param name="vector">A normalized 2D vector representing the push direction in camera space.</param>
	public void Look ( Vector2 vector )
	{
		// Find the neighbour whose delta from the current target is closest to the push vector

		Transform neighbour;
		Transform candidate = null;

		float max = float.NegativeInfinity;

		for ( int i = 0; i < edges.Count; i++ )
		{
			// Figure out which side of the edge the current target falls on, if it is part of the edge at all

			if ( edges[i].A == current )
			{
				neighbour = edges[i].B;
			}
			else if ( edges[i].B == current )
			{
				neighbour = edges[i].A;
			}
			else
			{
				continue;
			}

			// Get the camera space direction vector from the current target to the neighbour
			Vector2 direction = Camera.main.transform.TransformDirection( (neighbour.position - current.position) );

			direction = direction.normalized;

			// Take the dot product between the direction vector and the push vector, as a measure of their similarity
			float dot = Vector2.Dot( vector, direction );

			// If the dot product is within the tolerance, and is the highest thus far
			if ( dot >= tolerance && dot > max )
			{
				// Select the neighbour as the candidate
				candidate = neighbour;

				// Record the new highest dot product
				max = dot;
			}
		}

		// If we found a candidate neighbour to go to
		if ( candidate != null )
		{
			// Make the neighbour the current target
			current = candidate;
		}
	}

	private void Update ()
	{
		// Force CameraLookAt's settings to use 

		if ( cameraLookAt != null )
		{
			cameraLookAt.targetTransform = current;
			cameraLookAt.type = CameraLookAt.TargetType.Object;
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		if ( edges != null )
		{
			foreach ( var edge in edges )
			{
				if ( edge.A != null && edge.B != null )
				{
					Debug.DrawLine( edge.A.position, edge.B.position, Color.magenta );
				}
			}
		}

		if ( initial != null )
		{
			Debug.DrawLine( transform.position, initial.position, Color.yellow );
		}
	}
#endif
}
