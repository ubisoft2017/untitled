﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public partial class TrackingMonitor : MonoBehaviour
{
	[SerializeField] private GameObject pingPrefab;

	[Tooltip( "How long each ping should stay on the screen." )]
	[SerializeField] private float pingLifetime;

	[Tooltip( "How bright the ping is over the course of its lifetime." )]
	[SerializeField] private AnimationCurve pingIntensityCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 0.0f );

	[Tooltip( "Whether or not a ping should follow the tracked object over the course of its lifetime." )]
	[SerializeField] private bool pingFollow;

	private struct PingData
	{
		// The transform of the object being tracked by this ping.
		public readonly Transform transform;

		// The UI image representing this ping.
		public readonly Image image;

		// The time at which this ping was created.
		public readonly float timestamp;

		public PingData ( Transform transform, Image image )
		{
			this.transform = transform;
			this.image = image;

			this.timestamp = Time.time;
		}
	}

	private Queue<PingData> pings;

	/// <summary>
	/// Create a ping at a location.
	/// </summary>
	/// <param name="transform">Transform to ping on.</param>
	public void Ping ( Transform transform )
	{
		var instance = Instantiate( pingPrefab, transform );

		pings.Enqueue( new PingData( transform, instance.GetComponentInChildren<Image>() ) );

		UpdatePing( pings.Peek() );
	}

	private void Update ()
	{
		// Dequeue dead pings
		while ( pings.Count > 0 && pings.Peek().timestamp < Time.time - pingLifetime )
		{
			pings.Dequeue();
		}

		// Update live pings
		foreach ( var ping in pings )
		{
			UpdatePing( ping );
		}
	}

	private void UpdatePing ( PingData ping )
	{
		if ( pingFollow )
		{
			//TODO: move the prefab to reflect the tracked transform's current position
		}

		ping.image.color = new Color( 1.0f, 1.0f, 1.0f, pingIntensityCurve.Evaluate( (Time.time - ping.timestamp) / pingLifetime ) );
	}
}
