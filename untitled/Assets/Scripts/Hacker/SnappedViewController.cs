﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( SnappedView ) )]
public class SnappedViewController : ControllerBase
{
	[SerializeField] private JoystickAction lookAction;

	[SerializeField][Range(  0.0f, 1.0f )] private float threshold;

	[SerializeField][Range( -1.0f, 1.0f )] private float tolerance;

	[SerializeField][HideInInspector] private SnappedView view;

	[SerializeField][HideInInspector] private bool reset;

	[SerializeField][HideInInspector] private Vector2 last;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		view = GetComponent<SnappedView>();

		reset = true;
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		Vector2 vector = GamePad.Query( lookAction, Player.Profile, Player.State );

		float magnitude = vector.magnitude;

		if ( magnitude >= threshold )
		{
			vector /= magnitude;

			if ( reset || Vector2.Dot( last, vector ) <= tolerance )
			{
				view.Look( vector.normalized );

				reset = false;

				last = vector;
			}
		}
		else
		{
			reset = true;
		}
	}
}
