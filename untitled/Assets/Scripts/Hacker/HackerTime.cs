﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class HackerTime : MonoBehaviour
{
	[SerializeField]
	private TweenEvent rewindEvent;

	public void Rewind ()
	{
		rewindEvent.Invoke();
	}
}
