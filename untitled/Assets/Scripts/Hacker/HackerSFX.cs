﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackerSFX : SFX
{
	[SerializeField]
	private AudioClip[] keyboardClips = new AudioClip[1];

	[SerializeField]
	[Range( 0.0f, 2.0f )] private float delay = 0.5f;

	[SerializeField]
	private AnimationCurve delayCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );

	[SerializeField]
	[Range( 0.0f, 1.0f )] private float volume = 1.0f;

	private int index;

	public void Type ( int strokes )
	{
		if ( strokes > 0 )
		{
			StopAllCoroutines();

			StartCoroutine( Coroutine( strokes ) );
		}
	}

	private IEnumerator Coroutine ( int strokes )
	{
		do
		{
			index += Random.Range( 1, keyboardClips.Length - 1 );
			index %= keyboardClips.Length;

			PlayClip( keyboardClips[index], volume );

			yield return new WaitForSeconds( delayCurve.Evaluate( Random.value ) * delay );
		}
		while ( --strokes > 0 );
	}
}
