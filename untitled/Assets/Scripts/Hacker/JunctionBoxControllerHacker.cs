﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Controls;

public class JunctionBoxControllerHacker : ControllerBase
{
    bool isPaused = false;

    public JunctionBoxHacker junctionBox;

    [SerializeField]
    ButtonAction inputA, inputB, inputX, inputY;

    protected override void ControllerStart()
    {
        base.ControllerStart();

        if (junctionBox)
        {
            Register(inputA, () =>
            {
                junctionBox.InputButton('A');
            });

            Register(inputB, () =>
            {
                junctionBox.InputButton('B');
            });

            Register(inputX, () =>
            {
                junctionBox.InputButton('X');
            });

            Register(inputY, () =>
            {
                junctionBox.InputButton('Y');
            });
        }
    }
}
