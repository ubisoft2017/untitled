﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Controls;

public class HackerTimeController : ControllerBase
{
	[SerializeField]
	private ButtonAction rewindAction;

	[SerializeField]
	private HackerTime time;

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		time.Rewind();	
	}
}
