﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HackerClockText : MonoBehaviour
{
	public AgentToHacker facade;

	public Text text;

	private float second, minute, hour, day;
	private bool smoothSecondHand;

	private void Update ()
	{
		if ( facade != null )
		{
			second = facade.LocalTime % 60.0f;

			minute = facade.LocalTime / 60.0f;

			hour = minute / 60.0f;

			day = hour / 24.0f;

			minute %= 60;

			hour %= 12;

			if ( !smoothSecondHand )
			{
				second = Mathf.Floor( second );
			}

			text.text = string.Format( "{0:00}:{1:00}:{2:00}",
				Util.Math.mod(Mathf.FloorToInt( hour ), 12),
                Util.Math.mod(Mathf.FloorToInt( minute ), 60),
                Util.Math.mod(Mathf.FloorToInt( second ), 60) );
		}
		else
		{
			facade = FindObjectOfType<AgentToHacker>();
		}
	} 
}
