﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class HackerGuardView : MonoBehaviour
    {
        VisionProcessor vision;
        VisionSettings settings;
        Eye eye;
        MeshFilter meshFilter;
        private Mesh mesh;
        private Ray ray;
        private RaycastHit hit;
        public int layerMask = (1 << 8) | (1 << 9) | (1 << 11) | (1<<14)| (1 << 16);

        [SerializeField]
        [Range(3, 31)]
        private int resolution;
        private int previousResolution;
        private Quaternion[] rayRotations;
        private Vector3[] rayDirections;
        private Vector3[] verticies;
        private int[] triangles;


        Vector3 previousPosition;
        Quaternion previousRotation;
        bool doneCreating = false;

        void Start()
        {
            if (!doneCreating)
            {
                
                previousResolution = resolution;

                vision = GetComponentInParent<VisionProcessor>();
                settings = vision.getSettings();
                eye = GetComponentInParent<Eye>();
                meshFilter = GetComponent<MeshFilter>();
                GetComponent<MeshRenderer>().material = Resources.Load("Materials/Prototypes/mat_proto_guard_view_cone", typeof(Material)) as Material;
                mesh = new Mesh();
                mesh.MarkDynamic();
                meshFilter.mesh = mesh;
                
                if (resolution < 2)
                    resolution = 3;
                if (resolution % 2 == 0)
                    resolution++;
                rayDirections = new Vector3[resolution];
                verticies = new Vector3[resolution + 1];
                triangles = new int[resolution * 3 - 3];

                previousPosition = eye.EyeTransform.position;
                previousRotation = eye.EyeTransform.rotation;
                setMeshInitial();
                doneCreating = true;
            }
        }
        void Update()
        {

            if (previousRotation != eye.EyeTransform.rotation)
            {
                setRotations();
            }

            setMesh();
            transform.rotation = Quaternion.identity;

        }


        void setMeshInitial()
        {
            transform.localPosition = eye.EyeTransform.localPosition;
            setRotations();
            setMesh();
        }
        void setMesh()
        {
            Vector3 offset = - eye.EyeTransform.position;
            ray.origin = eye.EyeTransform.position;
            verticies[0] = eye.EyeTransform.position + offset;
            for (int i = 0; i < rayDirections.Length; i++)
            {
                ray.direction = rayDirections[i];

                if (Physics.Raycast(ray.origin, ray.direction, out hit, settings.Range, layerMask))
                {
                    verticies[i + 1] = hit.point + offset;
                }
                else
                    verticies[i + 1] = eye.EyeTransform.position + rayDirections[i] * settings.Range + offset;
            }

            Vector3[] entryVerticies = new Vector3[verticies.Length];



            int j = 0;
            for (int i = 0; i < triangles.Length - 2; i += 3)
            {
                triangles[i] = 0;
                triangles[i + 1] = j + 1;
                triangles[i + 2] = j + 2;
                j++;

            }

            mesh.vertices = verticies;
            mesh.triangles = triangles;

        }

        void setRotations()
        {
            float angleDelta = 2 * settings.FieldOfView / (rayDirections.Length);
            int halfRes = rayDirections.Length / 2;
            for (int i = -halfRes; i <= halfRes; i++)
            {
                rayDirections[i + halfRes] = Quaternion.Euler(0, angleDelta * i, 0) * eye.EyeTransform.forward;
            }
        }



    }
}