﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FloatExtensions
{
	public static Vector2 AsVector2 ( this float degrees )
	{
		float radians = Mathf.Deg2Rad * degrees;

		float x = Mathf.Cos( radians );
		float y = Mathf.Sin( radians );

		return new Vector2( x, y );
	}
}
	

public class HackerSideGuardMovement : MonoBehaviour {

	private static float time;

	void Start () {
		
	}
	
	public static Vector2 InterpolatePositionBasedOnPingAndVelocity (Vector2 newNetworkPosition, Vector2 currentLocalPosition, float currentNetworkRotation, Vector2 magnitude)
	{
		


		Vector2 newPosition = Vector2.MoveTowards (currentLocalPosition, newNetworkPosition, magnitude.magnitude * Time.deltaTime);


		return newPosition;
	}

	public static float InterpolateRotationBasedOnPingAndVelocity (float currentNetworkRotation, float newNetworkRotation, int currentPing, double currentTime, double lastNetworkedDataReceived )
	{
		float newRotation = 0;
		float pingInSeconds = (float)currentPing * 0.001f;
		float timeSinceLastUpdate = (float)(currentTime - lastNetworkedDataReceived);
		float totalTimePassed = pingInSeconds + timeSinceLastUpdate; // unsure if useful for lerp

		if (currentNetworkRotation != newNetworkRotation) {
			time += Time.deltaTime;
			newRotation = Mathf.Lerp (currentNetworkRotation, newNetworkRotation, time); //need better t value here
		} 
		else
		{
			time = 0.0f;
		}

		return newRotation;
	}
		

	public Vector2 InterpolatePositionBasedOnVelocity (Vector2 newNetworkPosition, Vector2 currentLocalPosition, Vector2 magnitude, double currentTime, double lastNetworkedDataReceived) //simpler interp way may not need but added just in case
	{
		float syncDelay = (float)(currentTime - lastNetworkedDataReceived);
		Vector2 syncNextPosition = newNetworkPosition + magnitude * syncDelay;
		Vector2 newPosition = Vector2.MoveTowards (currentLocalPosition, syncNextPosition, Time.deltaTime * magnitude.magnitude);

		return newPosition;

	}
		
}
