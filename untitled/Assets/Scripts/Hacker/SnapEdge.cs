﻿using System;
using UnityEngine;

[Serializable]
public class SnapEdge
{
	[SerializeField] private Transform a, b;

	public Transform A
	{
		get
		{
			return a;
		}
	}

	public Transform B
	{
		get
		{
			return b;
		}
	}
}
