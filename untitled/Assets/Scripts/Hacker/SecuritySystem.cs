﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controls;
using Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SecuritySystem : SFX
{
	[Header("Parameters")]

	[SerializeField]
	[Range( -10.0f, 10.0f )]
	private float rate;

	[SerializeField]
	private float maximum;

	[SerializeField]
	[Range( 0.0f, 1.0f )] private float warning;

	[Header("UI")]

	[SerializeField]
	private Image indicator;

	[SerializeField]
	private Graphic warn;

	[SerializeField]
	private Gradient gradient;

	[SerializeField]
	private TweenEvent fadeEvent;

	[SerializeField]
	private float fillRate, warnBlinkRate;

	[SerializeField]
	private PlayerState failState;

	[SerializeField]
	private Text failText, hintText;

	[SerializeField]
	private AudioClip revokeClip;

	private float awareness;

	public float Awareness
	{
		get
		{
			return awareness;
		}
	}

	private float buffer;

	private bool failed;

	private void Update ()
	{
		/// Empty buffer into primary awareness

		float b = Mathf.Max( buffer * Time.deltaTime, 0.0f );

		buffer -= b;
		awareness += b;

		/// Constant drain / fill
		awareness += rate * Time.deltaTime;

		awareness = Mathf.Clamp( awareness, 0, maximum );

		/// Show awareness level

		float t = awareness / maximum;

		indicator.fillAmount = t;
		indicator.color = gradient.Evaluate( t );

		warn.enabled = t >= warning && ( Time.time % warnBlinkRate) / warnBlinkRate > 0.5f;

		if ( Mathf.Round( awareness ) + float.Epsilon > maximum )
		{
			Fail( "the system detected you" );

			failText.enabled = Time.time % 1.0f > 0.75f;
		}
	}

	public void Fail ( string hint )
	{
		if ( !failed )
		{
			if ( fadeEvent != null )
			{
				fadeEvent.Invoke();
			}

			HackerToAgent.NotifyAlarm();

			PlayClip( revokeClip, 0.8f );

			FindObjectOfType<ProfileController>().Player.State = failState;

			failText.gameObject.SetActive( true );
			hintText.text = hint;

			Debug.Log( "GAME OVER!" );

			failed = true;

			GamePad.OnPress += Retry;
		}
	} 

	private void Retry ( DeviceButton b )
	{
		GamePad.OnPress -= Retry;

		if ( b == DeviceButton.X || b == DeviceButton.A )
		{
			var session = FindObjectOfType<SessionController>();

			session.UnloadLevel( session.LoadedIndex );
		}
	}

	public void Add ( float awareness )
	{
		buffer += awareness;
	}
}
