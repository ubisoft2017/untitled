﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instructions : MonoBehaviour
{

    private Text text;
    IEnumerator currentPrintInstruction;


    private string currentInstruction;

    private bool locked = false;

    private void Awake()
    {
        text = GetComponent<Text>();
        text.text = "";
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Reset()
    {
        StopAllCoroutines();
        text.text = "";
    }

    public void SetInstruction(string instruction, float waitTime = 0.06f, float lockTime = 0f)
    {
        if (!locked)
        {
            if (currentInstruction != instruction && !locked && instruction != "")
            {
                Reset();
                currentPrintInstruction = PrintInstruction(instruction, waitTime);
                StartCoroutine(currentPrintInstruction);
                currentInstruction = instruction;

                if (lockTime != 0f)
                {
                    locked = true;
                    Invoke("ResetLock", lockTime);
                }
            }
            else if (instruction == "")
            {
                Reset();
                text.text = "";
            }
        }
    }


    private IEnumerator PrintInstruction(string instruction, float waitTime)
    {
        int substringLength = 0;
        while (true)
        {
            if (substringLength > instruction.Length)
            {
                break;
            }
            else
            {
                text.text = "[" + instruction.Substring(0, substringLength) + "]";
                substringLength++;
                yield return new WaitForSeconds(waitTime);

            }
        }
    }

    private void ResetLock()
    {
        locked = false;
    }
}
