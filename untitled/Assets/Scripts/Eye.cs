﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour
{
	[SerializeField] private new Transform transform;

	public Transform EyeTransform
	{
		get
		{
			return transform;
		}
	}

	public Vector3 Position
	{
		get
		{
			return transform.position;
		}
	}

	public Vector3 Direction
	{
		get
		{
			return transform.forward;
		}
	}

	public Vector3 Right
	{
		get
		{
			return transform.right;
		}
	}

	public Vector3 Up
	{
		get
		{
			return transform.up;
		}
	}
}
