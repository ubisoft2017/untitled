﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ReparentBehaviour
{
	Awake,
	Start,
	Manual
}

public class Reparenter : MonoBehaviour
{
	[SerializeField]
	private TagReference tag;

	[SerializeField]
	private ReparentBehaviour behaviour = ReparentBehaviour.Awake;

	[SerializeField]
	private bool worldPositionStays;

	public void Reparent ()
	{
		transform.SetParent( GameObject.FindGameObjectWithTag( tag ).transform, worldPositionStays );
	}

	private void Awake ()
	{
		if ( behaviour == ReparentBehaviour.Awake )
		{
			Reparent();
		}
	}
	
	private void Start ()
	{
		if ( behaviour == ReparentBehaviour.Start )
		{
			Reparent();
		}
	}
}
