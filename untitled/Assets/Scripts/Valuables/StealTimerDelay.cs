﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealTimerDelay : MonoBehaviour {

    [SerializeField]
    float offset;

    public float Offset {
        get {
            return offset;
        }
    }
}
