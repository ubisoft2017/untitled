﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

/// <summary>
/// Models an instance of a valuable object in the world.
/// </summary>
public partial class Valuable : Node, IInteractable
{
    
	[EnumObject( typeof( ValuableType ), ValuableType.RESOURCE_PATH )]
	[SerializeField] private ValuableType type;

    [EnumObject(typeof(ButtonAction), ButtonAction.RESOURCE_PATH)]
    [SerializeField]
    ButtonAction stealAction;


    private float stealCounter = 0;
    private float stealTime;
    private bool hidden = false;

    public ValuableType Type
	{
		get
		{
			return type;
		}
	}

	[SerializeField] private Transform container;
    [SerializeField] private GameObject promptCanvasPrefab;

    private Collider trigger;
	private GameObject instance;

	private bool hasBeenStolen;

    private ButtonPrompt buttonPrompt;

    public bool HasBeenStolen
	{
		get
		{
			return hasBeenStolen;
		}
	}

	public override NodeType NodeType
	{
		get
		{
			return NodeType.Valuable;
		}
	}

	private void Start ()
	{
		// Initialize dependencies

		trigger = GetComponent<Collider>();
		instance = Instantiate( type.WorldPrefab, container, false ) as GameObject;

        GameObject canvas = Instantiate(promptCanvasPrefab, container, false);
        RectTransform rt = canvas.GetComponent<RectTransform>();

        rt.localPosition = Vector3.zero;
        rt.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        buttonPrompt = canvas.GetComponent<ButtonPrompt>();

        // Zero the 3D model to the container's local position / rotation

        instance.transform.localPosition = Vector3.zero;
		instance.transform.localRotation = Quaternion.identity;
        stealTime = GameObject.FindObjectOfType<StealTimerDelay>().Offset;

    }

    void Update()
    {
        if (!hidden&&hasBeenStolen )
        {
            stealCounter += Time.deltaTime;
            if (stealCounter > stealTime)
            {
                hidden = true;
                Phase.Push(new StealValuableCommand(this));
            }
        }
    }

    /// <summary>
    /// Steal this valuable.
    /// </summary>
    /// <param name="backpack">The backpack in which to place the stolen item.</param>
    /// <returns>True, if this valuable was not already stolen.</returns>
    public bool TrySteal ( Backpack backpack )
	{
		// Check that this valuable has not already been stolen
		if ( !hasBeenStolen )
		{
            // Mark the valuable as stolen
            hasBeenStolen = true;

            // Disable interaction
            trigger.enabled = false;

            // Add the valuable to the Backpack
            backpack.Add( type, 1 );

			FindObjectOfType<SessionController>().OnSteal( type );

			AgentToHacker.NotifySwipeValuable( this );

            return true;
		}

		return false;
	}



    public void ShowPrompt(PlayerProfile player)
    {
        if (!buttonPrompt.Visible)
        {
            buttonPrompt.Show(player, stealAction);
        }
    }

    public void HidePrompt()
    {
        if (buttonPrompt.Visible)
        {
            buttonPrompt.Hide();
        }
    }


    public bool promptVisible()
    {
        return buttonPrompt.Visible;
    }

    public bool canInteract()
    {
        return !HasBeenStolen;
    }

    public ButtonAction GetAction()
    {
       return stealAction;
    }


#if UNITY_EDITOR
    private void OnDrawGizmos ()
	{
		// Show a wireframe preview of the valuable that will be featured by this display

		if ( type != null && container != null )
		{
			if ( type.WorldPrefab != null )
			{
				var meshFilter = type.WorldPrefab.GetComponentInChildren<MeshFilter>();

				if ( meshFilter != null )
				{
					if ( meshFilter.sharedMesh != null )
					{
						Gizmos.DrawWireMesh( meshFilter.sharedMesh, container.position, container.rotation );
					}
				}
			}
		}
	}

    ButtonAction IInteractable.GetAction()
    {
        throw new NotImplementedException();
    }
#endif
}
