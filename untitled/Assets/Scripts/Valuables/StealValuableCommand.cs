﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public partial class Valuable
{
	private class StealValuableCommand : ICommand
	{
		public readonly Valuable valuable;

		public StealValuableCommand ( Valuable valuable )
		{
			this.valuable = valuable;
		}

		public void Execute ()
		{


			// Hide the 3D representation of the valuable in the world
			valuable.instance.SetActive( false );
		}

		public void Retract ()
		{
			// Mark the valuable as not stolen
			valuable.hasBeenStolen = false;

			// Enable interaction
			valuable.trigger.enabled = true;

			// Show the 3D representation of the valuable in the world
			valuable.instance.SetActive( true );

		}
	}
}
