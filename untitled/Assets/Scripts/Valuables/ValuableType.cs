﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "New ValuableType.asset", menuName = "Valuable Type" )]
public class ValuableType : ScriptableObject, IInitialize
{
	public const string RESOURCE_PATH = "Valuable Types";

	private static Dictionary<string,ValuableType> names;

	static ValuableType ()
	{
		names = new Dictionary<string, ValuableType>();
	}

	public static ValuableType Get ( string name )
	{
		return names[name];
	}

	[Header( "Info" )]

	[Tooltip( "The noun used when describing this valuable to the player." )]
	[SerializeField] private string label;

	public string Label
	{
		get
		{
			return label;
		}
	}

	[Tooltip( "Used to construct sentences where this valuable is the subject." )]
	[SerializeField] private Determiner determiner;

	public Determiner Determiner
	{
		get
		{
			return determiner;
		}
	}

	[Tooltip( "How much this valuable is worth." )]
	[SerializeField] private int value;

	public int Value
	{
		get
		{
			return value;
		}
	}

	[Header( "Dependencies" )]

	[Tooltip( "A prefab containing the 3D model for this valuable, without functionality." )]
	[SerializeField] private GameObject worldPrefab;

	public GameObject WorldPrefab
	{
		get
		{
			return worldPrefab;
		}
	}

	public void Initialize ()
	{
		names[name] = this;
	}
}
