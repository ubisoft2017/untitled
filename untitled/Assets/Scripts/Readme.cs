﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
public class Readme : MonoBehaviour
{
	public Color background = new Color( 1, 1, 1, 0.5f );

	public bool showOnStart;

	[HideInInspector] public bool show;

	[SerializeField] private string text;

	public string Text
	{
		get
		{
			return text;
		}
	}

	public void Awake ()
	{
		if ( showOnStart )
		{
			show = true;
		}
	} 
}
#endif
