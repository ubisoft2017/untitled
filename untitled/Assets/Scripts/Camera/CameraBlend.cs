﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBlend : BinaryCameraOperator
{
	[SerializeField] private AnimationCurve blendCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );

	[Range( 0.0f, 1.0f )] public float blend = 0.5f;

	public override CameraState Operate ( CameraState state )
	{
		if ( LHS == null && RHS == null )
		{
			return state;
		}
		else if ( LHS == null )
		{
			return RHS.Operate( state );
		}
		else if ( RHS == null )
		{
			return LHS.Operate( state );
		}

		return CameraState.Lerp( LHS.Operate( state ), RHS.Operate( state ), blendCurve.Evaluate( blend ) );
	}
}
