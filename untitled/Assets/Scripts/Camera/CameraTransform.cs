﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransform : UnaryCameraOperator
{
	[Tooltip( "Set if you want to use another object's transform." )]
	public Transform remote;

	public Modifier modifier = Modifier.Absolute;

	public override CameraState Operate ( CameraState state )
	{
		Transform transform;

		if ( remote != null )
		{
			transform = remote;
		}
		else
		{
			transform = this.transform;
		}

		Vector3 position = transform.position;
		Quaternion rotation = transform.rotation;

		switch ( modifier )
		{
		case Modifier.Absolute:

			state.position = position;
			state.rotation = rotation;

			break;

		case Modifier.Relative:

			state.position += state.rotation * position;
			state.rotation *= rotation;

			break;
		}

		return next == null ? state : next.Operate( state );
	}
}
