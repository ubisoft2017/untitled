﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : UnaryCameraOperator
{
	public enum CenterType { Object, Position };
	public enum HintType { Follow, Avoid, Direction };

	public CenterType type;
	public HintType hint;

	public float distance;

	public Vector3 axis = Vector3.up;

	[HideInInspector] Space space;

	[HideInInspector] public Transform centerTransform;
	[HideInInspector] public Vector3 centerPosition;

	[HideInInspector] public Transform hintTransform;
	[HideInInspector] public Vector3 hintDirection;

	public override CameraState Operate ( CameraState state )
	{
		Vector3 vector, right;
		Vector3 center = Vector3.zero;

		Vector3 axis = this.axis;
		Vector3 hint = hintDirection;

		switch ( type )
		{
		case CenterType.Object:

			if ( centerTransform == null )
			{
				return next == null ? state : next.Operate( state );
			}

			center = centerTransform.position;

			break;

		case CenterType.Position:

			center = centerPosition;

			break;
		}

		state.position = center;

		switch ( this.hint )
		{
		case HintType.Follow:

			if ( hintTransform == null )
			{
				return next == null ? state : next.Operate( state );
			}

			hint = hintTransform.position - center;

			break;

		case HintType.Avoid:

			if ( hintTransform == null )
			{
				return next == null ? state : next.Operate( state );
			}

			hint = center - hintTransform.position;

			break;

		case HintType.Direction:

			hint = hintDirection;

			break;
		}

		axis = axis.normalized;
		hint = hint.normalized;

		right  = Vector3.Cross( axis,  hint ).normalized;
		vector = Vector3.Cross( right, axis ).normalized;

		state.position += vector * distance;

		Debug.DrawRay( center, hint );

		Debug.DrawRay( center, right, Color.red );
		Debug.DrawRay( center, axis, Color.green );
		Debug.DrawRay( center, vector, Color.blue );

		return next == null ? state : next.Operate( state );
	}
}