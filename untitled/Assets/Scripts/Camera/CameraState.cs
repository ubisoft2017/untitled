﻿using System;
using UnityEngine;

public struct CameraState
{
	public static CameraState Lerp ( CameraState a, CameraState b, float t )
	{
		CameraState c = new CameraState();

		if ( t < 0.5f )
		{
			c.space = a.space;
			c.orthographic = a.orthographic;
		}
		else
		{
			c.space = b.space;
			c.orthographic = b.orthographic;
		}

		c.position = Vector3.Lerp( a.position, b.position, t );
		c.rotation = Quaternion.Lerp( a.rotation, b.rotation, t );

		c.fieldOfView = Mathf.Lerp( a.fieldOfView, b.fieldOfView, t );
		c.nearClipPlane = Mathf.Lerp( a.nearClipPlane, b.nearClipPlane, t );
		c.farClipPlane = Mathf.Lerp( a.farClipPlane, b.farClipPlane, t );
		c.orthographicSize = Mathf.Lerp( a.orthographicSize, b.orthographicSize, t );
		c.aspect = Mathf.Lerp( a.aspect, b.aspect, t );

		return c;
	}

	public Transform space;

	public Vector3 position;
	public Quaternion rotation;

	public float fieldOfView, nearClipPlane, farClipPlane, orthographicSize, aspect;

	public bool orthographic;

	public CameraState ( Camera camera )
	{
		space = camera.transform;

		position = camera.transform.position;
		rotation = camera.transform.rotation;

		fieldOfView = camera.fieldOfView;
		nearClipPlane = camera.nearClipPlane;
		farClipPlane = camera.farClipPlane;
		orthographicSize = camera.orthographicSize;
		aspect = camera.aspect;

		orthographic = camera.orthographic;
	}

	public CameraState ( CameraState state )
	{
		space = state.space;

		position = state.position;
		rotation = state.rotation;

		fieldOfView = state.fieldOfView;
		nearClipPlane = state.nearClipPlane;
		farClipPlane = state.farClipPlane;
		orthographicSize = state.orthographicSize;
		aspect = state.aspect;

		orthographic = state.orthographic;
	}

	public void ApplyTo ( Camera camera )
	{
		camera.transform.position = position;
		camera.transform.rotation = rotation;
		camera.fieldOfView = fieldOfView;
		camera.nearClipPlane = nearClipPlane;
		camera.farClipPlane = farClipPlane;
		camera.orthographicSize = orthographicSize;
		camera.aspect = aspect;
		camera.orthographic = orthographic;
	}
}