﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnchor : MonoBehaviour
{
	private static List<CameraAnchor> anchors;

	static CameraAnchor ()
	{
		anchors = new List<CameraAnchor>( 16 );
	}

	public static CameraAnchor GetMain ()
	{
		int max = int.MinValue;

		CameraAnchor main = null;

		for ( int i = 0; i < anchors.Count; i++ )
		{
			if ( anchors[i].head != null && anchors[i].priority >= max )
			{
				max = anchors[i].priority;

				main = anchors[i];
			}
		}

		return main;
	}

	[Tooltip( "Modifications during play mode will have no effect." )]
	[SerializeField] public int priority;

	[SerializeField] private CameraOperator head;

	public void ApplyTo ( Camera camera, params Camera[] slaves )
	{
		if ( head != null )
		{
			CameraState state = head.Operate( new CameraState( camera ) );

			state.ApplyTo( camera );

			for ( int i = 0; i < slaves.Length; i++ )
			{
				state.ApplyTo( slaves[i] );
			}

			camera.ResetAspect();
		}
	}

	private void OnEnable ()
	{
		anchors.Add( this );
	}

	private void OnDisable ()
	{
		anchors.Remove (this);
	}
}
