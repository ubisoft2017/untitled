﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class CopyCamera : MonoBehaviour
{
	[SerializeField] private Camera source;

	[SerializeField][HideInInspector] private new Camera camera;

	void Start ()
	{
		camera = GetComponent<Camera>();
	}
	
	void Update ()
	{
		camera.fieldOfView = source.fieldOfView;
		camera.orthographic = source.orthographic;
		camera.orthographicSize = source.orthographicSize;
	}
}
