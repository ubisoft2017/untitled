﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrthographicSize : UnaryCameraOperator
{
	[SerializeField] private Modifier modifier = Modifier.Absolute;

	public float value;

	public override CameraState Operate ( CameraState state )
	{
		switch ( modifier )
		{
		case Modifier.Absolute:
			state.orthographicSize = value;
			break;

		case Modifier.Relative:
			state.orthographicSize += value;
			break;
		}

		return next == null ? state : next.Operate( state );
	}
}
