﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraProjection : UnaryCameraOperator
{
	[Tooltip( "Set whether or not the camera uses perspective or orthographic projection." )]
	public bool orthographic;

	public override CameraState Operate ( CameraState state )
	{
		state.orthographic = orthographic;

		return next == null ? state : next.Operate( state );
	}
}
