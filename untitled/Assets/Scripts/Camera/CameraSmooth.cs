﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SmoothingType
{
	Previous,
	Next
}

public class CameraSmooth : UnaryCameraOperator
{
	[SerializeField] SmoothingType type;

	[Range( 0.0f, 1.0f )] public float smoothing = 0.7f;

    private bool initialized = false;

	[SerializeField][HideInInspector] private CameraState average;

	private CameraState state;

	public override CameraState Operate ( CameraState state )
	{
		if ( !initialized )
		{
			switch ( type )
			{
			case SmoothingType.Previous:

				initialized = true;

				return average = this.state = state;

			case SmoothingType.Next:

				if ( next != null )
				{
					initialized = true;

					average = next.Operate( state );
				}

				break;
			default:
				break;
			}
		}

		//TODO: make smoothing independent of framerate

		switch ( type )
		{
		case SmoothingType.Previous:

			this.state = state;

			return next == null ? average : next.Operate( average );

		case SmoothingType.Next:

			return next == null ? state : average;

		}

		throw new InvalidOperationException();
	}

	private void FixedUpdate ()
	{
		if ( initialized )
		{
			switch ( type )
			{
			case SmoothingType.Previous:

				average = CameraState.Lerp( state, average, Mathf.Sqrt( smoothing ) );

				break;
			case SmoothingType.Next:

				if ( next != null )
				{
					average = CameraState.Lerp( next.Operate( average ), average, Mathf.Sqrt( smoothing ) );
				}

				break;
			}
		}
	}
}
