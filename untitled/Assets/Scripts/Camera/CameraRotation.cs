﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation: UnaryCameraOperator
{
	[SerializeField] private Modifier modifier = Modifier.Absolute;

	public Vector3 value;

	public override CameraState Operate ( CameraState state )
	{
		switch ( modifier )
		{
		case Modifier.Absolute:
			state.rotation = Quaternion.Euler( value );
			break;

		case Modifier.Relative:
			state.rotation = Quaternion.Euler( value ) * state.rotation;
			break;
		}

		return next == null ? state : next.Operate( state );
	}
}