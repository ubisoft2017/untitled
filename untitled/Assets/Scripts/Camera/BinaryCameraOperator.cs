﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BinaryCameraOperator : CameraOperator
{
	[Tooltip( "The left hand side modifier in the split." )]
	[SerializeField] public CameraOperator LHS;

	[Tooltip( "The right hand side modifier in the split." )]
	[SerializeField] public CameraOperator RHS;
}
