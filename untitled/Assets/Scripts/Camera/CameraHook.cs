﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hooks a Camera onto the highest-priority CameraAnchor.
/// </summary>
[RequireComponent( typeof( Camera ) )]
public class CameraHook : MonoBehaviour
{
	[SerializeField]
	[HideInInspector]
	private new Camera camera;

	[SerializeField]
	[Tooltip( "If checked, the CameraAnchor with the highest priority will be used automatically." )]
	private bool auto = true;

	[SerializeField]
	[Tooltip( "Used when auto is unchecked." )]
	private CameraAnchor anchor;

	[SerializeField]
	[Tooltip( "Slave cameras copy all of their parameters from this Camera." )]
	private Camera[] slaves;

	private void Start ()
	{
		camera = GetComponent<Camera>();
	}

	private void Update ()//OnPreRender ()
	{
		CameraAnchor anchor = this.anchor;

		if ( auto )
		{
			// Get the anchor with the highest priority
			anchor = CameraAnchor.GetMain();
		}

		// Check that an anchor was found
		if ( anchor != null )
		{
			// Apply anchor to camera
			anchor.ApplyTo( camera, slaves );
		}
	}
}
