﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : UnaryCameraOperator
{
	[SerializeField] private Modifier modifier = Modifier.Absolute;
	[SerializeField] private Space space = Space.World;

	public Vector3 position;

	public override CameraState Operate ( CameraState state )
	{
		Vector3 position = this.position;

		if ( space == Space.Self )
		{
			position = state.space.TransformVector( position );
		}

		switch ( modifier )
		{
		case Modifier.Absolute:
			state.position = position;
			break;

		case Modifier.Relative:
			state.position += position;
			break;
		}

		return next == null ? state : next.Operate( state );
	}
}
