﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class CullingMaskSetter : MonoBehaviour
{
    [SerializeField]
    private LayerMask[] cullingMasks;

	//public LayerMask CullingMask
	//{
	//	get
	//	{
	//		return cullingMask;
	//	}
	//}

	[SerializeField]
	private TagReference cameraTag;

	public TagReference CameraTag
	{
		get
		{
			return cameraTag;
		}
	}

	private void Awake ()
	{
        Camera cam = GameObject.FindGameObjectWithTag(cameraTag.Tag).GetComponent<Camera>();
        cam.cullingMask = 0;

        //add each culling mask
        foreach (LayerMask mask in cullingMasks)
        {
            cam.cullingMask |= mask;
        }
    }

}
