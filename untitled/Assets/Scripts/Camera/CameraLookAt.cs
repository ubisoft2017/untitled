﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : UnaryCameraOperator
{
	public enum TargetType { Object, Position, Direction };

	public TargetType type;

	public bool useHint = true;

	public Vector3 hint = Vector3.up;

	[HideInInspector] public Transform targetTransform;

	[HideInInspector] public Vector3 targetPosition;

	[HideInInspector] public Vector3 direction;

	public override CameraState Operate ( CameraState state )
	{
		switch ( type )
		{
		case TargetType.Object:

			if ( targetTransform != null )
			{
				state.rotation = Quaternion.LookRotation( targetTransform.position - state.position, useHint ? state.rotation * Vector3.up : hint );
			}

			break;

		case TargetType.Position:

			state.rotation = Quaternion.LookRotation( targetPosition - state.position, useHint ? state.rotation * Vector3.up : hint );

			break;

		case TargetType.Direction:

			state.rotation = Quaternion.LookRotation( direction, useHint ? state.rotation * Vector3.up : hint );

			break;
		}

		return next == null ? state : next.Operate( state );
	}
}