﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class DamageVisuals : MonoBehaviour {

    float vignetteIntensity = 0;
    [SerializeField] private float damageSpeed = 1.5f;
    [SerializeField] private float gravity = 1f;


    [SerializeField] private VignetteModel.Settings settings;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(vignetteIntensity > 0)
        {
            vignetteIntensity -= Time.deltaTime*gravity;
        }

        vignetteIntensity = Mathf.Max(vignetteIntensity, 0);


        GetComponent<PostProcessingBehaviour>().profile.vignette.settings = new VignetteModel.Settings
        {
            mode = settings.mode,
            color = settings.color,
            center = settings.center,
            intensity = vignetteIntensity,
            smoothness = settings.smoothness,
            roundness = settings.roundness,
            mask = settings.mask,
            opacity = settings.opacity
        };

    }

    public void DealDamage()
    {
        vignetteIntensity += Time.deltaTime*damageSpeed;
    }
}
