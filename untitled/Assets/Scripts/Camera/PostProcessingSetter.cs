﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessingSetter : MonoBehaviour
{
    [SerializeField]
    private PostProcessingProfile profile;

	//public LayerMask CullingMask
	//{
	//	get
	//	{
	//		return cullingMask;
	//	}
	//}

	[SerializeField]
	private TagReference cameraTag;

	public TagReference CameraTag
	{
		get
		{
			return cameraTag;
		}
	}

	private void Awake ()
	{
		var behaviour = GameObject.FindGameObjectWithTag( cameraTag.Tag ).GetComponent<PostProcessingBehaviour>();

		behaviour.profile = profile;
	}
}
