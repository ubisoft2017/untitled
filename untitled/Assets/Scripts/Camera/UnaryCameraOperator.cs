﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnaryCameraOperator : CameraOperator
{
	[Tooltip( "The left hand side modifier in the split." )]
	[SerializeField] protected CameraOperator next;
}
