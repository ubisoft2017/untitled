﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFieldOfView : UnaryCameraOperator
{
	[SerializeField] private Modifier modifier = Modifier.Absolute;

	public float value;

	public override CameraState Operate ( CameraState state )
	{
		switch ( modifier )
		{
		case Modifier.Absolute:
			state.fieldOfView = value;
			break;

		case Modifier.Relative:
			state.fieldOfView += value;
			break;
		}

		return next == null ? state : next.Operate( state );
	}
}
