﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraOperator : MonoBehaviour
{
	public abstract CameraState Operate ( CameraState state );
}
