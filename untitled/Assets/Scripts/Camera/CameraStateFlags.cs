﻿using System;

[Flags]
public enum CameraStateFlags : byte
{
	None = 0,
	Position = 1,
	Rotation = 2,
	FieldOfView = 4,
	Projection = 8,
	Aspect = 16,
	OrthographicSize = 32,
	NearClipPlane = 64,
	FarClipPlane = 128
}

public static class CameraStateFlagsExtensions
{
	public static CameraStateFlags Intersection ( this CameraStateFlags lhs, CameraStateFlags rhs )
	{
		return lhs & rhs;
	}

	public static CameraStateFlags Union ( this CameraStateFlags lhs, CameraStateFlags rhs )
	{
		return lhs | rhs;
	}
}
