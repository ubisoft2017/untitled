﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using UnityEngine;
using UnityEngine.SceneManagement;

using Tweening;

public class LevelLoader : MonoBehaviour
{
	public event Action<Level> LevelLoaded, LevelUnloaded;

	[SerializeField]
	private LevelList levelList;

	[SerializeField]
	private TweenEvent busyEvent, doneEvent;

	private Level loadedLevel;

	public Level LoadedLevel
	{
		get
		{
			return loadedLevel;
		}
	}

	private Scene loadedScene;

	public Scene LoadedScene
	{
		get
		{
			return loadedScene;
		}
	}

	private bool isBusy;

	public bool IsBusy
	{
		get
		{
			return isBusy;
		}
	}

	private bool isLoaded;

	public bool IsLoaded
	{
		get
		{
			return isLoaded;
		}
	}

	public bool Load ( Level level )
	{
		if ( !isBusy && !isLoaded )
		{
			loadedLevel = level;

			SceneManager.sceneLoaded += HandleSceneLoaded;

			PhotonNetwork.LoadLevelAdditive( level.SceneReference.GetSceneName() );

			isBusy = true;

			busyEvent.Invoke();

			return true;
		}

		return false;
	}

	private void HandleSceneLoaded ( Scene scene, LoadSceneMode mode )
	{
		SceneManager.sceneLoaded -= HandleSceneLoaded;

		loadedScene = scene;

		isLoaded = true;

		isBusy = false;

		if ( LevelLoaded != null )
		{
			LevelLoaded.Invoke( loadedLevel );
		}

		doneEvent.Invoke();
	}

	public bool Unload ()
	{
		if ( !isBusy && isLoaded )
		{
			SceneManager.sceneUnloaded += HandleSceneUnloaded;

			isBusy = true;

			busyEvent.Invoke();

			return true;
		}

		return false;
	}

	private void HandleSceneUnloaded ( Scene scene )
	{
		SceneManager.sceneUnloaded -= HandleSceneUnloaded;

		loadedLevel = null;

		loadedScene = default( Scene );

		isLoaded = false;

		isBusy = false;

		if ( LevelUnloaded != null )
		{
			LevelUnloaded.Invoke( loadedLevel );
		}

		doneEvent.Invoke();
	}
}
