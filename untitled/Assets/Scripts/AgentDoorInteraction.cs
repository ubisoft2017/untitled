﻿using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

[RequireComponent(typeof(Door))]
public class AgentDoorInteraction : MonoBehaviour, IInteractable {

	Door attachedDoor;
	// Use this for initialization
	void Start () {
		attachedDoor = GetComponent<Door>();
	}
	

	public void Interact ()
	{
		if(attachedDoor.State == 0.0f && attachedDoor.Locked == false )
		{
			attachedDoor.Open();
		}
		else if(attachedDoor.State > 0.0f && attachedDoor.Locked == false )
		{
			attachedDoor.Close();
		}
	}

    public void ShowPrompt(PlayerProfile player)
    {
        throw new NotImplementedException();
    }

    public void HidePrompt()
    {
        throw new NotImplementedException();
    }

    public bool canInteract()
    {
        throw new NotImplementedException();
    }

    public bool promptVisible()
    {
        throw new NotImplementedException();
    }

    public ButtonAction GetAction()
    {
        throw new NotImplementedException();
    }
}
