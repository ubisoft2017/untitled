﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Controls;

public class TutorialLevel2 : MonoBehaviour {

	[SerializeField] private Transform clockIndicatorPosition;
	[SerializeField] private GameObject theAgent;
	[SerializeField] private Transform laserIndicatorPosition;
	[SerializeField] private GameObject clockText;
	[SerializeField] private GameObject laserText;
	

	private RaycastHit clockHit;
	private RaycastHit laserHit;
	private RaycastHit lightHit;
	private bool clockUsed;
	void Start ()
	{
		clockText.SetActive( false );
		laserText.SetActive( false );
        theAgent = GameObject.FindGameObjectWithTag("Agent");

    }

	public void DisableLaserText ()
	{
		laserText.SetActive( false );
	}

    public void RemoveClockText ()
	{
		clockUsed = true;
		clockText.SetActive( false );
		
	}

	// Update is called once per frame
	void Update ()
	{
		

		if ( Physics.Raycast( theAgent.transform.position, clockIndicatorPosition.position - theAgent.transform.position, out clockHit ) )
		{

			if ( clockHit.distance < 4.5f && clockHit.collider.tag == "Tutorial" && !clockUsed )
			{
				clockText.SetActive( true );
			}
			else
			{
				clockText.SetActive( false );
			}
		}


		if ( Physics.Raycast( theAgent.transform.position, laserIndicatorPosition.position - theAgent.transform.position, out laserHit ) )
		{ 

			if ( laserHit.distance < 4.5f && laserHit.collider.tag == "Tutorial2" )
			{
				laserText.SetActive( true );
			}
			else
			{
				laserText.SetActive( false );
			}
		}
	}
}
