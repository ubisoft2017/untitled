﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tweening;
using UnityEngine;

public class DestroyListener : MonoBehaviour
{
	public TweenEvent destroyEvent;

	private void Awake ()
	{
		destroyEvent.OnInvoke += Destroy;
	}

	private void Destroy ( TweenEvent e )
	{
		destroyEvent.OnInvoke -= Destroy;

		if ( this != null && gameObject != null )
		{
			Destroy( gameObject );
		}
	}
}
