﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Phase : MonoBehaviour
{
	private class TimestampedCommand : ICommand
	{
		private ICommand command;
		public readonly float timestamp;

		public TimestampedCommand ( ICommand command, float timestamp )
		{
			this.command = command;
			this.timestamp = timestamp;
		}

		public void Execute ()
		{
			command.Execute();
		}

		public void Retract ()
		{
			command.Retract();
		}
	}
}
