﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phases : MonoBehaviour
{
	private static Phases instance;

	private void Awake ()
	{
		if ( instance != null )
		{
			Destroy( this );

			Debug.LogError( "There should not be more than 1 instance of a Phases behaviour per level." );

			return;
		}

		instance = this;
	}
}
