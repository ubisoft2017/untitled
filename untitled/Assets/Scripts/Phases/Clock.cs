﻿using Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class Clock : Node, IInteractable
{
    [SerializeField] private Phase phase;
    [SerializeField] private Camera myCamera;
	[SerializeField] private bool isTutorial;
	[SerializeField] private GameObject clockFace;
	[SerializeField] private GameObject brokenClockFace;
	[SerializeField] private SFX sfx;
	[SerializeField] private AudioClip visitClip;

	[SerializeField]
	private TweenEvent visitEvent;

    [EnumObject(typeof(ButtonAction), ButtonAction.RESOURCE_PATH)]
    [SerializeField]
    ButtonAction visitAction;

	private bool isBroken;
    private ButtonPrompt buttonPrompt;
	private TutorialLevel2 tutorialController;

    public Phase Phase
    {
        get
        {
            return phase;
        }
    }

    public override NodeType NodeType
    {
        get
        {
            return NodeType.Clock;
        }
    }

    [Header("Setup")]

    [SerializeField]
    private LineRenderer hourHand;
    [SerializeField] private LineRenderer minuteHand;
    [SerializeField] private LineRenderer secondHand;

    [Header("Appearance")]

    [SerializeField]
    private float hourHandLength;
    [SerializeField] private float minuteHandLength;
    [SerializeField] private float secondHandLength;

    [SerializeField] private float offset;

    [SerializeField] private bool smoothSecondHand;

    /// <summary>
    /// Visit this clock's Phase.
    /// </summary>
    /// <returns>True, if a Phase is linked and has not already been visited.</returns>
    public bool TryVisit()
    {
        if (phase != null)
        {
            if (phase.TryVisit())
            {
				isBroken = true;

				visitEvent.Invoke();

				if ( isTutorial )
				{
					tutorialController.RemoveClockText();
				}

				sfx.PlayClip( visitClip, 1.0f, false );

				// Tell the hacker's client that a new phase has just started
				AgentToHacker.NotifyUseClock(this);

                HidePrompt();
				clockFace.SetActive( false );
				brokenClockFace.SetActive( true );

                return true;
            }
        }
        else
        {
            Debug.LogWarning("This Clock does not reference a Phase. Please connect one.", this);
        }

        return false;
    }

    protected override void Awake()
    {
		base.Awake();

        if (phase == null)
        {
            Debug.LogWarning("This Clock does not reference a Phase. Please connect one.", this);
        }

		if ( isTutorial )
		{
			tutorialController = FindObjectOfType<TutorialLevel2>();
		}

		myCamera = GameObject.FindGameObjectWithTag( "MainCamera" ).GetComponent<Camera>();
        secondHand.SetPosition(0, Vector3.forward * offset);
        minuteHand.SetPosition(0, Vector3.forward * offset);
        hourHand.SetPosition(0, Vector3.forward * offset);

        buttonPrompt = GetComponentInChildren<ButtonPrompt>();
    }

    [SerializeField] [HideInInspector] private float second, minute, hour, day;

    private void Update()
    {
        // Update the decomposition of the linked Phase's local time
        PollTime();

		// Update the physical hands on the clock's face
		if ( !isBroken )
		{
			ShowTime();
		}
    }

    public void ShowTime()
    {
        float hourTheta = Mathf.Deg2Rad * (hour / 12.0f) * 360.0f;
        float minuteTheta = Mathf.Deg2Rad * (minute / 60.0f) * 360.0f;
        float secondTheta = Mathf.Deg2Rad * (second / 60.0f) * 360.0f;

        hourHand.SetPosition(1, Vector3.forward * offset + hourHandLength * (Vector3.up * Mathf.Cos(hourTheta) + Vector3.left * Mathf.Sin(hourTheta)));
        minuteHand.SetPosition(1, Vector3.forward * offset + minuteHandLength * (Vector3.up * Mathf.Cos(minuteTheta) + Vector3.left * Mathf.Sin(minuteTheta)));
        secondHand.SetPosition(1, Vector3.forward * offset + secondHandLength * (Vector3.up * Mathf.Cos(secondTheta) + Vector3.left * Mathf.Sin(secondTheta)));
    }

    public void PollTime()
    {
        if (phase != null)
        {
            // Decompose the Phase's local time into seconds, minutes, hours, and days

            second = phase.LocalTime % 60.0f;

            minute = phase.LocalTime / 60.0f;

            hour = minute / 60.0f;

            day = hour / 24.0f;

            minute %= 60;

            hour %= 12;

            if (!smoothSecondHand)
            {
                second = Mathf.Floor(second);
            }
        }
    }

    public void ShowPrompt(PlayerProfile player)
    {
        if (!buttonPrompt.Visible)
        {
            buttonPrompt.Show(player, visitAction);
        }
    }

    public void HidePrompt()
    {
        if (buttonPrompt.Visible)
        {
            buttonPrompt.Hide();
        }
    }


    public bool promptVisible()
    {
        return buttonPrompt.Visible;
    }

    public bool canInteract()
    {
        return phase != null && !phase.HasBeenVisited;
    }

    public ButtonAction GetAction()
    {
        return visitAction;
    }
}
