﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPhaseInOut
{
	void Localize ();
	void Delocalize ();
}
