﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Phase : MonoBehaviour
{
	[Serializable]
	private class PhaseStack : UndoRedoStack<TimestampedCommand>
	{
		public PhaseStack ( int allocation = - 1 ) : base( allocation ) { }
	}

	private const int allocation = 1024;

	private static PhaseStack stack;

	[RuntimeInitializeOnLoadMethod]
	private static void Initialize ()
	{
		stack = new PhaseStack( 1024 );
	}

	public static void Push ( ICommand command )
	{
		stack.Push( new TimestampedCommand( command, active.localTime ) );
	}
}
