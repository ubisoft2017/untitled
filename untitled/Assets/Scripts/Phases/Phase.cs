﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Phase : MonoBehaviour
{
	#region Chronological Constants
	public const int SECONDS_PER_MINUTE = 60;

	public const int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60;

	public const int SECONDS_PER_DAY = SECONDS_PER_HOUR * 24;
	#endregion

	public const float PHASE_SHIFT_DURATION = 2.0f;

	private static Phase active;

	public static Phase Active
	{
		get
		{
			return active;
		}
	}

	[SerializeField]
	private string label = "untitled";

	public string Label
	{
		get
		{
			return label;
		}
	}

	[SerializeField] private bool isStartPhase;

	[SerializeField][HideInInspector] private bool hasBeenVisited;

	public bool HasBeenVisited
	{
		get
		{
			return hasBeenVisited;
		}
	}

	[Tooltip( "How many hours ahead or behind this clock is." )]
	[SerializeField][Range( -24, 24 )]  private int hourOffset;

#if UNITY_EDITOR
	/// <summary>
	/// How many hours (from 0) this Phase is offset by.
	/// Do not use this at runtime.
	/// </summary>
	public int HourOffset
	{
		get
		{
			return hourOffset;
		}
	}
#endif

	// Since we are playing with "time zones", it doesn't make sense to expose any of the other offsets to the user
	[SerializeField][HideInInspector] private int dayOffset;
	[SerializeField][HideInInspector] private int minuteOffset;
	[SerializeField][HideInInspector] private int secondOffset;

	// Track the in-and-out interval for this Phase
	[SerializeField][HideInInspector] private float visitStartTime;
	[SerializeField][HideInInspector] private float visitEndTime;

	[SerializeField][HideInInspector] private float localTime;

	/// <summary>
	/// The local time in this Phase. Always up-to-date.
	/// </summary>
	public float LocalTime
	{
		get
		{
			return localTime;
		}
	}

	/// <summary>
	/// Visit this Phase.
	/// </summary>
	/// <returns>True, if this Phase has not yet been visited.</returns>
	public bool TryVisit ()
	{
		if ( hasBeenVisited || active == this )
		{
			return false;
		}

		var old = active;

		//if phase hasnt been visited, then we know we are switching phases, at this point we can make a call to a networked object which can make an RPC call to tell the other client to update his phase too

		// Check if there was already a Phase being visited (always true, except at start-up)
		if ( active != null )
		{
			active.isActive = false;

			// Remember what time (local to the active Phase) the active Phase stopped being visited
			active.visitEndTime = active.localTime;

			// Turn off all interactive and sensory behaviours belonging to the active Phase
			active.DelocalizeChildren();
		}

		this.isActive = true;

		active = this;

		// Remember what time (local to the newly active Phase) the newly active Phase started being visited
		active.visitStartTime = active.localTime;

		// Mark the newly active Phase as visited
		active.hasBeenVisited = true;

		// Turn on all interactive and sensory behaviours belonging to the newly active Phase
		active.LocalizeChildren();

		StartCoroutine( ShiftSun( active, old ) );

		return true;
	}

	private IEnumerator ShiftSun ( Phase newPhase, Phase oldPhase = null, float duration = PHASE_SHIFT_DURATION )
	{
		var sun = GameObject.FindGameObjectWithTag( "Sun" );

		if ( sun != null )
		{
			if ( oldPhase != null )
			{
				float localStartTime = newPhase.localTime;
				float localEndTime = localStartTime + duration;

				float a = oldPhase.localTime;
				float b = localEndTime;

				while ( newPhase.localTime < localEndTime )
				{
					float t = (newPhase.localTime - localStartTime) / duration;

					float time = Mathf.Lerp( a, b, t );

					sun.transform.localRotation = Quaternion.Euler( SunPitch( time ), 0.0f, 0.0f );

					yield return null;
				}
			}

			while ( active == newPhase )
			{
				sun.transform.localRotation = Quaternion.Euler( SunPitch( newPhase.localTime ), 0.0f, 0.0f );


				yield return null;
			}
		}

		yield break;
	}

	public static float SunPitch ( float time )
	{
		return (time / SECONDS_PER_DAY) * 360.0f + 90.0f;
	}

	private bool isActive;

	public bool IsActive
	{
		get
		{
			return isActive;
		}
	}

#if UNITY_EDITOR
	[SerializeField]
	[HideInInspector]
	private bool isActiveInEditor;

	public bool IsActiveInEditor
	{
		get
		{
			return isActiveInEditor;
		}
	}
#endif

	public void Show ()
	{
#if UNITY_EDITOR
		isActiveInEditor = true;

		if ( Application.isPlaying )
		{
#endif
			//Turn on all interactive and sensory behaviours belonging to this Phase
			LocalizeChildren();

#if UNITY_EDITOR
		}
		else
		{
			//If this is being called from the Editor, and the game is not running, simply turn on every top-level GameObject in this Phase
			ActivateChildren();
		}
#endif
	}

	public void Hide ()
	{
#if UNITY_EDITOR
		isActiveInEditor = false;

		if ( Application.isPlaying )
		{
#endif
			//Turn off all interactive and sensory behaviours belonging to this Phase
			DelocalizeChildren();

#if UNITY_EDITOR
		}
		else
		{
			//If this is being called from the Editor, and the game is not running, simply turn off every top-level GameObject in this Phase
			DeactivateChildren();
		}
#endif
	}

#pragma warning disable CS0162 // Unreachable code detected
	private void LocalizeChildren ()
	{
		/// Phases no longer affect anything except the sun
		return;

		foreach ( var member in GetComponentsInChildren<PhaseMember>() )
		{
			member.Localize();
		}
	}

	private void DelocalizeChildren ()
	{
		/// Phases no longer affect anything except the sun
		return;

		foreach ( var member in GetComponentsInChildren<PhaseMember>() )
		{
			member.Delocalize();
		}
	}
#pragma warning restore CS0162 // Unreachable code detected

	private void ActivateChildren ()
	{
		for ( int i = 0; i <transform.childCount; i++ )
		{
			transform.GetChild( i ).gameObject.SetActive( true );
		}
	}

	public void DeactivateChildren ()
	{
		for ( int i = 0; i < transform.childCount; i++ )
		{
			transform.GetChild( i ).gameObject.SetActive( false );
		}
	}

	private void Awake ()
	{
		ActivateChildren();

		foreach ( var node in GetComponentsInChildren<Node>() )
		{
			node.Owner = this;
		}
	}

	private void Start ()
	{
		if ( isStartPhase )
		{
			TryVisit();
		}
		else
		{
			DelocalizeChildren();
		}
	} 

	private void Update ()
	{
		// Update the local time for this Phase
		CalculateLocalTime( Time.time );

		// Maintain stack consistency with the local time in the active Phase
		if ( active == this )
		{
			while ( stack.UndoCount > 0 && stack.PeakUndo().timestamp > localTime )
			{
				stack.Undo();
			}

			while ( stack.RedoCount > 0 && stack.PeakRedo().timestamp < localTime )
			{
				stack.Redo();
			}
		}
	}

	public void CalculateLocalTime ( float time )
	{
		localTime = time +
			dayOffset * SECONDS_PER_DAY +
			hourOffset * SECONDS_PER_HOUR +
			minuteOffset * SECONDS_PER_MINUTE +
			secondOffset;
	}
}
