﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UndoRedoStack<T> where T : ICommand
{
	[SerializeField][HideInInspector] private List<T> undo;
	[SerializeField][HideInInspector] private List<T> redo;

	public int UndoCount
	{
		get
		{
			return undo.Count;
		}
	}

	public int RedoCount
	{
		get
		{
			return redo.Count;
		}
	}

	public T PeakUndo ()
	{
		return undo[undo.Count - 1];
	}

	public T PeakRedo ()
	{
		return redo[redo.Count - 1];
	}

	public UndoRedoStack ( int allocation = -1 )
	{
		if ( allocation < 0 )
		{
			undo = new List<T>();
			redo = new List<T>();
		}
		else
		{
			undo = new List<T>( allocation );
			redo = new List<T>( allocation );
		}
	}

	public void Push ( T command, bool execute = true )
	{
		if ( execute )
		{
			// Execute the command, applying any changes it implements
			command.Execute();
		}

		// Push the command onto the undo stack, so that it can be undone
		undo.Add( command );

		// A canonical command pattern implementation would clear the redo stack at this point,
		// however, in order to preserve actions taken in the future, it is left intact.
	}

	public void Undo ()
	{
		// Get the index of the top-most command on the undo stack
		int index = undo.Count - 1;

		// Query the top-most command on the undo stack
		var command = undo[index];

		// Pop the command off the undo stack, since it has been consumed
		undo.RemoveAt( index );

		// Retract the command, reverting any changes it implements
		command.Retract();

		// Add the command to the redo stack, so that it can be redone
		redo.Add( command );
	}

	public void Redo ()
	{
		// Get the index of the top-most command on the redo stack
		int index = redo.Count - 1;

		// Query the top-most command on the redo stack
		var command = redo[index];

		// Pop the command off the redo stack, since it has been consumed
		redo.RemoveAt( index );

		// Execute the command, applying any changes it implements
		command.Execute();

		// Add the command to the undo stack, so that it can be undone
		undo.Add( command );
	}
}
