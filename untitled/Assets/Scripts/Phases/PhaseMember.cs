﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Utility behaviour used by the Phase system. Localizing and delocalizing a specific object or hierarchy of objects.
/// </summary>
public class PhaseMember : MonoBehaviour
{
	[SerializeField] private int remoteLayer;

	private int localLayer;

	private List<IPhaseInOut> interfaces;
	private List<Collider> colliders;
	private List<Renderer> renderers;

    private bool inPhase;

	[Tooltip( "Whether or not to manage child behaviours in the hierarchy. Not fully robust." )]
	[SerializeField] private bool includeChildren;

	private void Awake ()
	{
		// Remember which layer this member is supposed to be on when localized
		localLayer = gameObject.layer;

		// Initialize dependencies
		interfaces = new List<IPhaseInOut>( 32 );
		colliders = new List<Collider>( 32 );
		renderers = new List<Renderer>( 32 );

		GatherDependencies( gameObject, includeChildren );
	}

	public void GatherDependencies ( GameObject gameObject, bool includeChildren = true )
	{
		interfaces.AddRange( includeChildren ? GetComponentsInChildren<IPhaseInOut>() : GetComponents<IPhaseInOut>() );
		colliders.AddRange( includeChildren ? GetComponentsInChildren<Collider>() : GetComponents<Collider>() );
		renderers.AddRange( includeChildren ? GetComponentsInChildren<Renderer>() : GetComponents<Renderer>() );
	}

	public void Localize ()
	{
        gameObject.layer = localLayer;

		for ( int i = 0; i < interfaces.Count; i++ )
		{
			interfaces[i].Localize();
		}

		for ( int i = 0; i < colliders.Count; i++ )
		{
			var character = colliders[i] as CharacterController;

			if ( character != null )
			{
				character.detectCollisions = true;
			}
			else
			{
				colliders[i].enabled = true;
			}
		}

		for ( int i = 0; i < renderers.Count; i++ )
		{
			renderers[i].enabled = true;
		}
        inPhase = true;

    }

    public void Delocalize ()
	{
		gameObject.layer = remoteLayer;

		for ( int i = 0; i < interfaces.Count; i++ )
		{
			interfaces[i].Delocalize();
		}

		for ( int i = 0; i < colliders.Count; i++ )
		{
			var character = colliders[i] as CharacterController;

			if ( character != null )
			{
				character.detectCollisions = false;
			}
			else
			{
				colliders[i].enabled = false;
			}
		}

		for ( int i = 0; i < renderers.Count; i++ )
		{
			renderers[i].enabled = false;
		}
        inPhase = false;
    }

    public bool isInPhase() {
        return inPhase;
    }
}
