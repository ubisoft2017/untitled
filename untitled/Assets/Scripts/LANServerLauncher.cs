﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;

public class LANServerLauncher : NetworkDiscovery
{

    NetworkDiscovery broadcastRecieveMessage;
    private string lanIp;

    PhotonServerLauncher punServerLauncher;

    void Start()
    {
        Initialize();

        punServerLauncher = GetComponent<PhotonServerLauncher>();
    }
    // Use this for initialization
    public Tweening.TweenEvent RunLan()
    {
        punServerLauncher.StartServer();

        return punServerLauncher.serverReadyEvent;
    }

    public void StartLanClient()
    {
        Debug.Log("StartLanClient");
        lanIp = null;
        StartAsClient();
    }

    public string GetIPOfHost()
    {
        return lanIp;
    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        if (data.Contains("Juncture"))
        {
            if (fromAddress.LastIndexOf(":") >= 0)
            {
                lanIp = fromAddress.Substring(fromAddress.LastIndexOf(":") + 1);
                print("Received broadcast from " + lanIp);

            }
            else
            {
                lanIp = fromAddress;
            }
        }
    }
}
