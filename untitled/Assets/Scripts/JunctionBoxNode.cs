﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum JunctionBoxStream
{
	Black,
	White,
	Cyan,
	Magenta,
	Yellow
}

public class JunctionBoxNode : Node
{
	public override NodeType NodeType
	{
		get
		{
			return NodeType.JunctionBox;
		}
	}

	[SerializeField]
	private JunctionBoxAgent box;

	public JunctionBoxAgent Box
	{
		get
		{
			return box;
		}
	}
}
