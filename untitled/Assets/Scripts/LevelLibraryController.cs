﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLibraryController : MonoBehaviour
{
	[SerializeField]
	private LevelList levelList;

	[SerializeField]
	private RectTransform leftContainer, rightContainer, centerContainer;

	[SerializeField]
	private GameObject levelCardPrefab;

	[SerializeField]
	private TweenEvent showLevelsEvent, hideLevelsEvent;

	[SerializeField]
	private TweenEventList onSelect;

	private List<LevelCard> cards;

	private EventSystem es;

	private GameObject history;

	private void Awake ()
	{
		es = FindObjectOfType<EventSystem>();

		showLevelsEvent.OnInvoke += ( e ) =>
		{
			history = es.currentSelectedGameObject;

			es.SetSelectedGameObject( cards[levelList.Index].button.gameObject );
		};

		hideLevelsEvent.OnInvoke += ( e ) =>
		{
			es.SetSelectedGameObject( history );
		};
	}

	private void Start ()
	{
		cards = new List<LevelCard>( PlayerProfiles.AvailableProfiles.Count );

		/// Instantiate card UI prefabs for each profile
		for ( int i = 0; i < levelList.Levels.Count; i++ )
		{
			CreateCardFor( levelList[i], i );
		}

		/// Setup explicit navigation
		SetupNavigation();
		
		/// Setup layout
		//Layout( levelList.Index );
	}

	private LevelCard CreateCardFor ( Level level, int index )
	{
		var instance = Instantiate( levelCardPrefab, leftContainer, false );

		var card = instance.GetComponent<LevelCard>();

		card.label.text = level.Title;

		card.thumbnail.sprite = level.Thumbnail;

		card.tutorial.SetActive( level.IsTutorial );

		cards.Add( card );

		/// Hook up events
		card.button.onClick.AddListener( () =>
		{
			Debug.Log( "click " + level.Title );

			var session = FindObjectOfType<SessionController>();

			session.UnloadLevel( index );
		} );

		SetupSelectHandler( card, instance, index );

		return card;
	}

	private void SetupSelectHandler( LevelCard card, GameObject instance, int index )
	{
		var entry = new EventTrigger.Entry();

		var callback = new EventTrigger.TriggerEvent();

		callback.AddListener( ( e ) =>
		{
			levelList.Index = index;

			Layout( index );
		} );

		entry.eventID = EventTriggerType.Select;

		entry.callback = callback;

		card.eventTrigger.triggers.Add( entry );
	}

	private void Layout ( int index )
	{
		for ( int i = 0; i < cards.Count; i++ )
		{
			var child = cards[i].transform;

			if ( i < index )
			{
				child.parent = leftContainer;

				goto Finish;
			}
			
			if ( i > index )
			{
				cards[i].transform.parent = rightContainer;

				goto Finish;
			}

			child.parent = centerContainer;

			Finish:

			child.localScale = Vector3.one;
		}
	}

	private void SetupNavigation ()
	{
		for ( int i = 0; i < cards.Count; i++ )
		{
			var nav = cards[i].button.navigation;

			nav.mode = Navigation.Mode.Explicit;

			if ( i > 0 )
			{
				nav.selectOnLeft = cards[i - 1].button;
			}

			if ( i < cards.Count - 1 )
			{
				nav.selectOnRight = cards[i + 1].button;
			}

			cards[i].button.navigation = nav;
		}
	}
}
