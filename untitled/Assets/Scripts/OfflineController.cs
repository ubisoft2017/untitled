﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OfflineController : MonoBehaviour
{
	PlayerSelection selection = PlayerSelection.None;

	[SerializeField]
	private SceneReference hackerScene, offlineScene;

	[EnumObject( typeof( PlayerType ) )]
	[SerializeField]
	private PlayerType agent, hacker;

	[EnumObject( typeof( TweenEvent ) )]
	[SerializeField]
	private TweenEvent connectOfflineEvent, connectOnlineEvent, connectLocalEvent, selectAgentEvent, selectHackerEvent, loadGameEvent;

	[SerializeField]
	private TweenEventList onBusy, onConnect;

	private ProfileController profileController;

	private LevelList levelList;

	private bool isGameLoadedOrLoading;

	private bool isActive;

	public void Reset ()
	{
		isActive = false;
		scenesLoaded = 0;
	}

	private void Awake ()
	{
		profileController = FindObjectOfType<ProfileController>();
		levelList = FindObjectOfType<LevelList>();

		connectOfflineEvent.OnInvoke += ( e ) =>
		{
			isActive = true;
		};

		connectOnlineEvent.OnInvoke += ( e ) => Reset();
		connectLocalEvent.OnInvoke += ( e ) => Reset();

		loadGameEvent.OnInvoke += ( e ) =>
		{
			LoadGame();
		};
	}

	private int scenesLoaded = 0;

	private Scene loadedHackerScene, loadedAgentScene;

	private string postHackerSceneName;
	private string postAgentSceneName;

	private UnityEngine.Events.UnityAction<Scene,LoadSceneMode> postHackerCallback;
	private UnityEngine.Events.UnityAction<Scene,LoadSceneMode> postAgentCallback;

	private void HandleHackerLoaded ( Scene scene, LoadSceneMode mode )
	{
		scenesLoaded++;

		SceneManager.sceneLoaded -= HandleHackerLoaded;

		loadedHackerScene = scene;

		if ( selection == PlayerSelection.Hacker )
		{
			//if ( !SceneManager.SetActiveScene( scene ) )
			//{
			//	Debug.LogError( "SetActiveScene failed." );
			//}
		}

		SceneManager.sceneLoaded += postHackerCallback;

		PhotonNetwork.LoadLevelAdditive( postHackerSceneName );
	}

	private void HandleAgentLoaded ( Scene scene, LoadSceneMode mode )
	{
		scenesLoaded++;

		SceneManager.sceneLoaded -= HandleAgentLoaded;

		loadedAgentScene = scene;

		if ( selection == PlayerSelection.Agent )
		{
			//if ( !SceneManager.SetActiveScene( scene ) )
			//{
			//	Debug.LogError( "SetActiveScene failed." );
			//}
		}

		SceneManager.sceneLoaded += postAgentCallback;

		PhotonNetwork.LoadLevelAdditive( postAgentSceneName );
	}

	private void HandleOfflineLoaded ( Scene scene, LoadSceneMode mode )
	{
		SceneManager.sceneLoaded -= HandleOfflineLoaded;

		if ( selection == PlayerSelection.Hacker )
		{
			profileController.Player.State = hacker.DefaultState;

			foreach ( var controller in FindObjectsOfType<ControllerBase>() )
			{
				if ( controller.gameObject.scene == loadedHackerScene )
				{
					controller.Player = profileController.Player;
				}
			}

			GameObject.FindGameObjectWithTag( "Hacker" ).GetComponentInChildren<CameraAnchor>().priority = 10;
		}
		else if ( selection == PlayerSelection.Agent )
		{
			profileController.Player.State = agent.DefaultState;

			foreach ( var controller in FindObjectsOfType<ControllerBase>() )
			{
				if ( controller.gameObject.scene == loadedAgentScene )
				{
					controller.Player = profileController.Player;
				}
			}

			GameObject.FindGameObjectWithTag( "Agent" ).GetComponentInChildren<CameraAnchor>().priority = 10;
		}

		if ( onConnect != null )
		{
			onConnect.InvokeAll();
		}
	}


	private void Update ()
	{
		if ( isActive )
		{
			if ( GamePad.Query( DeviceButton.Left ).WasJustPressed || GamePad.Query( DeviceJoystick.Left ).Position.x < -0.1f || GamePad.Query( DeviceJoystick.Right ).Position.x < -0.1f )
			{
				if ( selection != PlayerSelection.Agent )
				{
					selection = PlayerSelection.Agent;

					selectAgentEvent.Invoke();

					Debug.Log( "offline is agent" );
				}
			}

			if ( GamePad.Query( DeviceButton.Right ).WasJustPressed || GamePad.Query( DeviceJoystick.Left ).Position.x > 0.1f || GamePad.Query( DeviceJoystick.Right ).Position.x > 0.1f )
			{
				if ( selection != PlayerSelection.Hacker )
				{
					selection = PlayerSelection.Hacker;

					selectHackerEvent.Invoke();

					Debug.Log( "offline is hacker" );
				}
			}
		}
	}

	private void LoadGame ()
	{
		if ( isActive && selection != PlayerSelection.None )
		{
			isGameLoadedOrLoading = true;

			isActive = false;

			if ( onBusy != null )
			{
				onBusy.InvokeAll();
			}

			string agentSceneName = levelList[levelList.Index].SceneReference.GetSceneName();
			string hackerSceneName = hackerScene.GetSceneName();
			string offlineSceneName = offlineScene.GetSceneName();

			if ( selection == PlayerSelection.Agent )
			{
				SceneManager.sceneLoaded += HandleHackerLoaded;

				postHackerSceneName = agentSceneName;
				postAgentSceneName = offlineSceneName;

				postHackerCallback = HandleAgentLoaded;
				postAgentCallback = HandleOfflineLoaded;

				PhotonNetwork.LoadLevelAdditive( hackerSceneName );
			}
			else if ( selection == PlayerSelection.Hacker )
			{
				SceneManager.sceneLoaded += HandleAgentLoaded;

				postAgentSceneName = hackerSceneName;
				postHackerSceneName = offlineSceneName;

				postAgentCallback = HandleHackerLoaded;
				postHackerCallback = HandleOfflineLoaded;

				PhotonNetwork.LoadLevelAdditive( agentSceneName );
			}
		}
	}
}
