﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;
using UI;

public class Dependencies : MonoBehaviour
{
	[Tooltip( "Paths to all start-up dependencies." )]
	[SerializeField] private string[] paths;

	// Make sure that all of our dependencies get loaded into memory
	private void OnEnable ()
	{
		for ( int i = 0; i < paths.Length; i++ )
		{
			Debug.Log( "load all @Resources/" + paths[i] );

			foreach ( var resource in Resources.LoadAll( paths[i] ) )
			{
				if ( resource is IInitialize )
				{
					(resource as IInitialize).Initialize();
				}
			}
		}
	}
}
