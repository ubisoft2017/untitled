﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tweening;

public class fovInterp : Interpolator<float> {

	[SerializeField] private Camera myCamera;

	// Use this for initialization
	void Start () {
		
	}
		
	public override void Interpolate (float t)
	{
		myCamera.fieldOfView = Mathf.Lerp (a, b, t);
	}
}
