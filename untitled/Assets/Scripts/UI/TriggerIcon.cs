﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Controls;

namespace UI
{
	[CreateAssetMenu( fileName = "New TriggerIcon.asset", menuName = "Icon/Trigger Icon" )]
	public class TriggerIcon : ScriptableObject
	{
		private static Sprite[] sprites;

		static TriggerIcon ()
		{
			sprites = new Sprite[Enum.GetValues( typeof( DeviceTrigger ) ).Length];
		}

		public static Sprite GetTriggerIcon ( DeviceTrigger trigger )
		{
			return sprites[(int) trigger];
		}

		[SerializeField] private DeviceTrigger trigger;
		[SerializeField] private Sprite sprite;

		private void OnEnable ()
		{
			sprites[(int) trigger] = sprite;
		}
	}
}

