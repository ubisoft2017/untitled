﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Controls;

namespace UI
{
	[CreateAssetMenu( fileName = "New ButtonIcon.asset", menuName = "Icon/Button Icon" )]
	public class ButtonIcon : ScriptableObject
	{
		private static Sprite[] sprites;

		static ButtonIcon ()
		{
			sprites = new Sprite[Enum.GetValues( typeof( DeviceButton ) ).Length];
		}

		public static Sprite GetButtonIcon ( DeviceButton button )
		{
			return sprites[(int) button];
		}

		[SerializeField] private DeviceButton button;
		[SerializeField] private Sprite sprite;

		private void OnEnable ()
		{
			sprites[(int) button] = sprite;
		}
	}
}

