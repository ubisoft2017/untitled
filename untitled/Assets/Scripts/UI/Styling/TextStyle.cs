﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu( fileName = "new TextStyle.asset", menuName = "Styles/Text Style", order = 60 )]
public class TextStyle : ScriptableObject
{
	[SerializeField]
	private int size;

	public int Size
	{
		get
		{
			return size;
		}
	}

	[SerializeField]
	private Font font;

	public Font Font
	{
		get
		{
			return font;
		}
	}

	[SerializeField]
	private FontStyle style;

	public FontStyle FontStyle
	{
		get
		{
			return style;
		}
	}

	[SerializeField]
	private float lineSpacing;

	public float LineSpacing
	{
		get
		{
			return lineSpacing;
		}
	}

	[SerializeField]
	private float letterSpacing;

	public float LetteSpacing
	{
		get
		{
			return letterSpacing;
		}
	}

	[SerializeField]
	private bool richText;

	public bool RichText
	{
		get
		{
			return richText;
		}
	}

	[SerializeField]
	private TextAlignment alignment;

	public TextAlignment Alignment
	{
		get
		{
			return alignment;
		}
	}


	[SerializeField]
	private HorizontalWrapMode horizontalOverflow;

	public HorizontalWrapMode HorizontalOverflow
	{
		get
		{
			return horizontalOverflow;
		}
	}


	[SerializeField]
	private VerticalWrapMode verticalOverflow;

	public VerticalWrapMode VerticalOverflow
	{
		get
		{
			return verticalOverflow;
		}
	}

	[SerializeField]
	private bool bestFit;

	public bool BestFit
	{
		get
		{
			return bestFit;
		}
	}

	[SerializeField]
	private Vector2 shadow;

	public Vector2 Shadow
	{
		get
		{
			return shadow;
		}
	}

	[SerializeField]
	private TextStyle fallback;

	public TextStyle Fallback
	{
		get
		{
			return fallback;
		}
	}
}