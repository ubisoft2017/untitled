﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Style : ScriptableObject, IStyle
{
	public void ApplyTo ( object target )
	{
		if ( target is Graphic )
		{
			ApplyTo( target as Graphic );

			if ( target is Image )
			{
				ApplyTo( target as Image );
			}
			else if ( target is Text )
			{
				ApplyTo( target as Text );
			}
			else if ( target is Button )
			{
				ApplyTo( target as Button );
			}
			else if ( target is Slider )
			{
				ApplyTo( target as Slider );
			}
			else if ( target is Scrollbar )
			{
				ApplyTo( target as Scrollbar );
			}
			else if ( target is Toggle )
			{
				ApplyTo( target as Toggle );
			}
		}
	}

	protected virtual void ApplyTo ( Image image )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Slider slider )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Scrollbar scrollbar )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Toggle toggle )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Button button )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Text text )
	{
		throw new NotImplementedException();
	}

	protected virtual void ApplyTo ( Graphic graphic )
	{
		throw new NotImplementedException();
	}
}