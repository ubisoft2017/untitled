﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingMenu : MonoBehaviour
{
	[SerializeField]
	private SlidingMenu prev;

	[SerializeField]
	private SlidingMenu next;
}
