﻿using Controls;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPrompt : MonoBehaviour
{

    private Tweener[] tweeners;

    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent showPromptTweenEvent;


    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent hidePromptTweenEvent;


    public Sprite this[DeviceButton a]
    {
        get
        {
            return Resources.Load<Sprite>("Sprites/Devices/XboxOne/SmallButtons/" + a.ToString() + "_Button_Small");
        }
    }

    public bool Visible { get{ return GetComponentInChildren<CanvasGroup>().alpha > 0f; } }


    // Use this for initialization
    void Start()
    {
        tweeners = GetComponentsInChildren<Tweener>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Show(PlayerProfile player, PlayerAction action)
    {

        if (action is ButtonAction)
        {
            DeviceButton button = player[action as ButtonAction].mapping;
            GetComponentInChildren<Image>().sprite = this[button];
        }

        GetComponentInChildren<Text>().text = action.Label;

        foreach (Tweener tweener in tweeners)
        {
            tweener.InvokeLocal(showPromptTweenEvent);
        }
    }

    public void Hide()
    {
        foreach (Tweener tweener in tweeners)
        {
            tweener.InvokeLocal(hidePromptTweenEvent);
        }
    }
}
