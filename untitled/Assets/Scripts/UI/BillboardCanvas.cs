﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BillboardCanvas : MonoBehaviour {

    private RectTransform rt;

    Quaternion initialRot;

	[SerializeField]
	private bool reflect;

	// Use this for initialization
	void Start () {
        rt = GetComponent<RectTransform>();
        initialRot = rt.rotation;
        
    }
	
	// Update is called once per frame
	void Update () {
		if ( !reflect )
		{
			rt.rotation = Quaternion.LookRotation( Camera.main.transform.forward * -1 );
		}
		else
		{
			rt.rotation = Quaternion.LookRotation( Camera.main.transform.forward);
		}
    }
}
