﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class PromptUI : MonoBehaviour
	{
		[SerializeField] private Transform container;

		[SerializeField] private Text text;
		[SerializeField] private Image image;

		[SerializeField] private bool startHidden = true;

		private void Awake ()
		{
			// Move container into Canvas hierarchy so that text / image will be rendered
			container.SetParent( GameObject.FindGameObjectWithTag( "World Space Canvas" ).transform, false );

			if ( startHidden )
			{
				Hide();
			}
		} 

		public void Hide ()
		{
			text.enabled = false;
			image.enabled = false;
		}

		public void Face ( Eye eye )
		{
			Vector3 delta = eye.Position - transform.position;

			float distance = delta.magnitude;

			delta = delta.normalized;

			container.GetComponent<RectTransform>().position = transform.position + delta * 0.5f;

			container.rotation = Quaternion.LookRotation( -delta, eye.Up );
		}

		public void Move ( Vector3 point, Vector3 normal )
		{
			container.GetComponent<RectTransform>().position = point;

			container.rotation = Quaternion.LookRotation( -normal, Vector3.up );
		}

		public void ShowText ( string text )
		{
			this.text.text = text;
			this.text.rectTransform.SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, this.text.preferredWidth );
			this.text.enabled = true;
		}

		public void ShowIcon ( Controls.DeviceButton button )
		{
			image.enabled = true;

			image.sprite = ButtonIcon.GetButtonIcon( button );
		}

		public void ShowIcon ( Controls.DeviceTrigger trigger )
		{
			image.enabled = true;

			image.sprite = TriggerIcon.GetTriggerIcon( trigger );
		}

		public void ShowIcon ( Controls.DeviceJoystick joystick )
		{
			image.enabled = true;

			image.sprite = JoystickIcon.GetJoystickIcon( joystick );
		}
	}
}

