﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class PromptAnchor : MonoBehaviour
	{
		[Tooltip( "Must be a prefab with a PromptUI component in its hierarchy." )]
		[SerializeField] private GameObject prefab;

		private void Awake ()
		{
			Instantiate( prefab, transform, false ).GetComponent<PromptUI>();
			Debug.LogError( "prompt object: " + name );
		}
	}
}
