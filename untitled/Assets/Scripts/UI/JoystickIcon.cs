﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Controls;

namespace UI
{
	[CreateAssetMenu( fileName = "New JoystickIcon.asset", menuName = "Icon/Joystick Icon" )]
	public class JoystickIcon : ScriptableObject
	{
		private static Sprite[] sprites;

		static JoystickIcon ()
		{
			sprites = new Sprite[Enum.GetValues( typeof( DeviceJoystick ) ).Length];
		}

		public static Sprite GetJoystickIcon ( DeviceJoystick joystick )
		{
			return sprites[(int) joystick];
		}

		[SerializeField] private DeviceJoystick joystick;
		[SerializeField] private Sprite sprite;

		private void OnEnable ()
		{
			sprites[(int) joystick] = sprite;
		}
	}
}

