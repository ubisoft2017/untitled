﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent( typeof( Image ) )]
public class Spinner : MonoBehaviour
{
	private Image image;
	private float zero = 0.0f;

	[SerializeField]
	private float period = 1.0f;

	[SerializeField]
	private TweenEventList triggers;

	private void Awake ()
	{
		image = GetComponent<Image>();

		foreach ( var trigger in triggers )
		{
			trigger.OnInvoke += ( e ) => Zero();
		}
	}
	
	private void Update ()
	{
		image.fillAmount = ((Time.time - zero) / period) % 1.0f;
	}

	public void Zero ()
	{
		zero = Time.time;
	} 
}
