﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controls;
using UnityEngine;
using UnityEngine.UI;

public class JunctionBoxHacker : JunctionBox
{
    public Instructions instructions;

    private bool codeInputted = false;

    public NodeUI ui;

    int nmTry = 0;

    [SerializeField] private MoveText[] texts;
    [SerializeField] private Renderer screen;
    [SerializeField] private Text PassFailText;
    [SerializeField] private Color passTextColor;
    [SerializeField] private Color failTextColor;
    [SerializeField] private TweenEventList onPass;
    [SerializeField] private NodeView view;
    [SerializeField] [EnumObject] private PlayerState junctionBoxState, nodeState;
    [SerializeField] private float failCost;
	[SerializeField]
	private AudioClip grantedClip, clickClip, failClip;
	[SerializeField]
	private SFX sfx;

	private string inputString = "";
    private float[] boxesYLimits;
    private float lastFrac;

    protected override void Awake()
    {
        DevTools.Register(string.Format("Enter Code ({0})", GetType().Name), EnterCorrectCode);

        base.Awake();
    }

    // Use this for initialization
    void Start()
    {

        float[] limits = new float[texts.Length * 2];

        for (int y = 0; y < texts.Length; y++)
        {
            RectTransform t = texts[y].gameObject.GetComponent<RectTransform>();

            float yMin = Mathf.Abs(t.anchoredPosition.y);
            float yMax = Mathf.Abs(t.anchoredPosition.y) + t.sizeDelta.y;

            limits[y * 2] = 1f - (yMin / 720f);
            limits[y * 2 + 1] = 1f - (yMax / 720f);
        }

        screen.material.SetFloatArray("limits", limits);

        instructions.SetInstruction("WAIT FOR AGENT UPLINK");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            Codes[] ccodes = { Codes.ABXY, Codes.AXYB, Codes.BAYX };
            Codes rCode = Codes.ABXY;

            Initialize(ccodes, rCode);
        }

        //Display only the right code
        float frac = Time.time - Mathf.Floor(Time.time);
        bool wrapped = frac < lastFrac;

        if (codeInputted && wrapped && state != State.Wire)
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].enabled = false;
                screen.material.SetInt("_shouldFlow", 0);
            }

            state = State.Wire;
            instructions.SetInstruction("AGENT MUST SELECT " + wireColors.colorNames[rightColor].ToUpper() + " WIRE");
        }

        lastFrac = frac;
    }

    public void Initialize(Codes[] codes, Codes rightCode)
    {
        FindObjectOfType<ProfileController>().Player.State = junctionBoxState;

        inputString = "";
        lastFrac = 0f;

        chosenCodes = codes;

        this.rightCode = rightCode;

        float delay = nmTry > 0 ? 3f : 0f;

        Invoke("StartStreams", delay);

    }


    private void StartStreams()
    {

        screen.material.SetInt("_disappear", 0);

        int i = 0;

        foreach (MoveText text in texts)
        {
            text.SetText("");
            text.gameObject.SetActive(true);
            text.enabled = true;

            string str = RandomString(26);

            int randomIndex = Random.Range(0, str.Length - 1);

            Codes randomCode = chosenCodes[i];

            str = str.Substring(0, randomIndex) + randomCode + str.Substring(randomIndex);

            //str = str.Replace("A", "<color=green>A</color>");
            //str = str.Replace("B", "<color=red>B</color>");
            //str = str.Replace("X", "<color=cyan>X</color>");
            //str = str.Replace("Y", "<color=yellow>Y</color>");

            // text.text = str;
            text.SetText(str);
            i++;
        }

        //screen.material.SetFloat("_speed", 0.6f);
        state = State.Streams;

        instructions.SetInstruction("AGENT WILL TELL CODE");
    }

    private string RandomString(int Size)
    {
        string input = "abcdefghijklmnopqrstuvwxyz0123456789";

        StringBuilder builder = new StringBuilder();

        char ch;

        for (int i = 0; i < Size; i++)
        {
            ch = input[Random.Range(0, input.Length)];
            builder.Append(ch);
        }

        return builder.ToString();
    }

    public void InputButton(char button)
    {
        //make sure it  is a button char
        if (!("ABXY").Contains(button))
        {
            return;
        }

        if (state == State.Streams && inputString.Length < 4)
        {
			sfx.PlayClip( clickClip, 1.0f, true );

            foreach (MoveText text in texts)
            {
                text.Highlight(button);
            }

            inputString += button;

            if (inputString.Length >= 4)
            {
                if (rightCode.ToString() == inputString)
                {
                    EnterCorrectCode();
                }
                else
                {
                    HackerToAgent.NotifyFailJunctionBox(ui);
                    Fail();
                }
            }
        }
    }

    private void EnterCorrectCode()
    {
        int rightCodeIndex = 0;

        //find the right code index
        for (int i = 0; i < chosenCodes.Length; i++)
        {
            if (chosenCodes[i] == rightCode)
            {
                rightCodeIndex = i;
            }
        }

        //mask = rightCodeIndex + 1;

        //screen.material.SetInt( "_mask", mask );
        codeInputted = true;
        screen.material.SetInt("_disappear", 1);

        rightColor = Random.Range(0, 4);

        HackerToAgent.NotifyFoundStreamJunctionBox(rightColor, ui);
    }

    public override void Pass()
    {
        foreach (MoveText text in texts)
        {
            text.gameObject.SetActive(false);
        }

        screen.material.SetFloat("_speed", 0f);

        instructions.Reset();

		sfx.PlayClip( grantedClip, 1.0f, true );

        PassFailText.text = "[ACCESS GRANTED]";
        PassFailText.color = passTextColor;
        PassFailText.enabled = true;

        Invoke("OnPass", 1.0f);
    }

    public override void Fail()
    {
		sfx.PlayClip( failClip, 1.0f, true );

        nmTry++;
        state = State.Retry;

        codeInputted = false;

        foreach (MoveText text in texts)
        {
            text.gameObject.SetActive(false);
        }

        screen.material.SetInt("_shouldFlow", 0);

        instructions.Reset();

        PassFailText.text = "[ACCESS DENIED]";
        PassFailText.color = failTextColor;
        PassFailText.enabled = true;

        StartCoroutine(TellRetry(nmTry + 1));

        FindObjectOfType<SecuritySystem>().Add(failCost);
    }

    private IEnumerator TellRetry(int i)
    {
        yield return new WaitForSeconds(1f);
        PassFailText.enabled = false;
        instructions.SetInstruction(Util.Text.Ordinal(i).ToUpper() + " TRY");
    }

    private void OnPass()
    {
        if (onPass != null)
        {
            onPass.InvokeAll();
        }

        Invoke("OnPass2", 0.5f);
    }

    private void OnPass2()
    {
        FindObjectOfType<ProfileController>().Player.State = nodeState;

        view.Select(ui, true);
        view.Selection.Discover();
        view.Selection.Hack();
    }


    private static string colorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2") + color.a.ToString("X2");
        return "#" + hex;
    }
}
