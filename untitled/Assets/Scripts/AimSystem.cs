﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.ImageEffects;
using Controls;
using Tweening;
using UnityEngine.PostProcessing;

public class AimSystem : ControllerBase {


	[SerializeField] private TweenEvent lockIn;
	[SerializeField] private TweenEvent normalAim;
	[SerializeField] private PostProcessingProfile activeProfile;
	[SerializeField] private float lerpMultiplier;
	[SerializeField] private TriggerAction leftTrigger;

	private DepthOfFieldModel.Settings depthOfFieldSettings;
	private Camera myCamera;
	private float distanceToTarget;
	private bool buttonDown;
	private bool canLerp;
	private float lerpTime;

	// Use this for initialization
	protected override void ControllerStart ()
	{
		base.ControllerStart();
		activeProfile = Camera.main.GetComponent<PostProcessingBehaviour>().profile;
		myCamera = Camera.main;
		depthOfFieldSettings = activeProfile.depthOfField.settings;
		depthOfFieldSettings.aperture = 2;
		
	}


	private void FindTargetForFocus()
	{
		RaycastHit hit;
		if (Physics.Raycast(myCamera.transform.position, myCamera.transform.forward, out hit, Mathf.Infinity ) )
		{
			depthOfFieldSettings = activeProfile.depthOfField.settings;
			distanceToTarget = hit.distance;
		}
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		activeProfile.depthOfField.settings = depthOfFieldSettings;

		FindTargetForFocus();

		if(GamePad.Query( leftTrigger, Player.Profile, Player.State ) > .1f && !buttonDown )
		{
			buttonDown = true;
			lockIn.Invoke();
			activeProfile.depthOfField.enabled = true;
		}

		else if ( GamePad.Query( leftTrigger, Player.Profile, Player.State ) < .1f && buttonDown )
		{
			buttonDown = false;
			normalAim.Invoke();
			activeProfile.depthOfField.enabled = false;
		}
	}

	void FixedUpdate ()
	{
		depthOfFieldSettings.focusDistance = Mathf.Lerp( depthOfFieldSettings.focusDistance, distanceToTarget, Time.deltaTime * lerpMultiplier );
	}
	
}
