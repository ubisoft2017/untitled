﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverSystem : SFX
{
    [SerializeField]
    private float blendSpeed;

	[SerializeField]
	private Tweening.TweenEvent gameOverEvent, restartEvent;

    [SerializeField]
    private Text hintText;

	[SerializeField]
	private GameObject gameOverUI;

	[SerializeField]
	private AudioClip laserClip;

    private bool guardGameOver, hackerGameOver;
	private string lossTip = "";

	private static int laserCount = 0;
	private static int guardLightCount = 0;
	private static int guardDetectionCount = 0;
	private bool gameOver;

	void Start()
    {
		restartEvent.OnInvoke += ( e ) =>
		{
			if ( gameOver )
			{
				gameOver = false;
				
				Time.timeScale = 1.0f;

				var session = FindObjectOfType<SessionController>();

				session.UnloadLevel( session.LoadedIndex );
			}
		};
    }

    public void DetectedByGuardLight ()
    {
		Debug.Log( "DETECTED BY LIGHT!!" );
		GetAGuardLightTip();
		guardGameOver = true;
		StartCoroutine( FireGameOver(1.5f) );
	}

	public void DetectedByGuard ( )
	{
		Debug.Log( "DETECTED BY PROXY!!" );
		GetAGuardDetectionTip();
		guardGameOver = true;
		StartCoroutine( FireGameOver(0.0f) );
	}

	public void HackerDetected ()
	{
		Debug.Log( "HACKER DETECTED!!" );
		hackerGameOver = true;
		lossTip = "The hacker was detected";
		StartCoroutine( FireGameOver(0.0f) );
	}

	private void GetALaserTip ()
	{
		switch ( laserCount )
		{
			case 0:
				lossTip = "Hacker can manipulate the lasers";
				laserCount++;
				break;

			case 1:
				lossTip = "Don't let the lasers touch you";
				laserCount++;
			break;

			case 2:
				lossTip = "Communicate with the hacker to avoid lasers";
				laserCount = 0;
			break;
		}
	}

	private void GetAGuardDetectionTip ()
	{
		switch ( guardDetectionCount )
		{
			case 0:
				lossTip = "Guards will notice you if you get to close";
				guardDetectionCount++;
				break;

			case 1:
				lossTip = "The Hacker can disrupt a guard to let you by";
				guardDetectionCount++;
				break;

			case 2:
				lossTip = "Sneaking by the guards is not your best option";
				guardDetectionCount = 0;
				break;
		}
	}

	private void GetAGuardLightTip ()
	{
		switch ( guardLightCount )
		{
			case 0:
				lossTip = "The guards will always spot you in light";
				guardLightCount++;
				break;

			case 1:
				lossTip = "Shift the time of day to change where there is light";
				guardLightCount++;
				break;

			case 2:
				lossTip = "Use clocks to shift the time of day";
				guardLightCount = 0;
				break;
		}
	}

	public void DetectedByLaser()
    {
		PlayClip( laserClip );

		GetALaserTip();
		StartCoroutine( FireGameOver(.5f) );
    }

	IEnumerator FireGameOver (float delay)
	{
		yield return new WaitForSeconds( delay );
		GameOver();
	}

	private void GameOver ()
	{
		Debug.Log( "GAME OVER OUTER" );

		if ( !gameOver )
		{
			Debug.Log( "GAME OVER INNER" );
			gameOver = true;
			gameOverUI.SetActive( true );
			hintText.text = lossTip;
			gameOverEvent.Invoke();

			//Time.timeScale = 0.0f;
			AgentToHacker.NotifyAlarm();
		}
    }
}

