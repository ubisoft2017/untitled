﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelDesign
{
	[Serializable]
	public class Join
	{
		[EnumObject( typeof( JoinType ), JoinType.RESOURCE_PATH )]
		[SerializeField] private JoinType type;

		public JoinType Type
		{
			get
			{
				return type;
			}
		}

		[SerializeField] private Transform join;

		public bool IsValid
		{
			get
			{
				return join != null && type != null;
			}
		}

		public Vector3 Position
		{
			get
			{
				return join.position;
			}
		}

		public Vector3 Forward
		{
			get
			{
				return join.forward;
			}
		}

		public Vector3 Right
		{
			get
			{
				return join.right;
			}
		}

		public Vector3 Up
		{
			get
			{
				return join.up;
			}
		}
	}
}
