﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelDesign
{
[CreateAssetMenu( fileName = "New JoinType.asset", menuName = "Join Type" )]
	public class JoinType : ScriptableObject
	{
		public const string RESOURCE_PATH = "Join Types";

		[SerializeField] private Color color = Color.white;

		public Color Color
		{
			get
			{
				return color;
			}
		}
	}
}
