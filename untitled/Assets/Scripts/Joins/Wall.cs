﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using UnityEngine;

[Serializable]
public class Wall
{
	[SerializeField]
	[Range( 1, 4 )]
	private int height = 1;

	public int Height
	{
		get
		{
			return height;
		}
	}

	[SerializeField]
	private List<Vector2> corners = new List<Vector2>();

	public ReadOnlyCollection<Vector2> Corners
	{
		get
		{
			return corners.AsReadOnly();
		}
	}
}