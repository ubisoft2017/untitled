﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using LevelDesign;

public struct WallInfo
{
	public readonly Vector2 position;
	public readonly float rotation, height;

	public readonly Vector2[] corners;

	public WallInfo ( Block block, int index )
	{
		position = new Vector2( block.transform.position.x, block.transform.position.z );
		rotation = block.transform.rotation.eulerAngles.y;

		corners = block.Walls[index].Corners.ToArray();

		height = block.Walls[index].Height;
	}

	public byte[] ToBytes ()
	{
		var bytes = new byte[16 + corners.Length * 8];

		BitConverter.GetBytes( position.x ).CopyTo( bytes, 0 );
		BitConverter.GetBytes( position.y ).CopyTo( bytes, 4 );

		BitConverter.GetBytes( rotation ).CopyTo( bytes, 8 );
		BitConverter.GetBytes( height ).CopyTo( bytes, 12 );

		for ( int i = 0, j = 16; i < corners.Length; i++, j += 8 )
		{
			BitConverter.GetBytes( corners[i].x ).CopyTo( bytes, j );
			BitConverter.GetBytes( corners[i].y ).CopyTo( bytes, j + 4 );
		}

		return bytes;
	}

	public WallInfo ( byte[] bytes )
	{
		float x = BitConverter.ToSingle( bytes, 0 );
		float y = BitConverter.ToSingle( bytes, 4 );

		position = new Vector2( x, y );

		rotation = BitConverter.ToSingle( bytes, 8 );
		height = BitConverter.ToSingle( bytes, 12 );

		int n = (bytes.Length - 16) / 8;

		corners = new Vector2[n];

		for ( int i = 0, j = 16; i < n; i++, j += 8 )
		{
			x = BitConverter.ToSingle( bytes, j );
			y = BitConverter.ToSingle( bytes, j + 4 );

			corners[i] = new Vector2( x, y );
		}
	}
}
