﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace LevelDesign
{
	public class Block : MonoBehaviour
	{
		public const string RESOURCE_PATH = "Prefabs/Blocks";

		[SerializeField] private List<Join> joins = new List<Join>();

		public ReadOnlyCollection<Join> Joins
		{
			get
			{
				return joins.AsReadOnly();
			}
		}

		[SerializeField] private List<Wall> walls;

		public ReadOnlyCollection<Wall> Walls
		{
			get
			{
				return walls.AsReadOnly();
			}
		}

		[SerializeField]
		private GameObject linePrefab;

		private void Awake ()
		{
			AgentToHacker.Enqueue( this );

			if ( linePrefab != null )
			{
				foreach ( var wall in walls )
				{
					var gameObject = Instantiate( linePrefab, transform, false );

					gameObject.transform.localPosition = Vector3.zero;
					gameObject.transform.localRotation = Quaternion.identity;

					var renderer = gameObject.GetComponent<LineRenderer>();

					renderer.numPositions = wall.Corners.Count;

					for ( int i = 0; i < wall.Corners.Count; i++ )
					{
						renderer.SetPosition( i, new Vector3( wall.Corners[i].x, 0.0f, wall.Corners[i].y ) );
					}
				}
			}
		}

#if UNITY_EDITOR
		private void OnDrawGizmos ()
		{
			if ( joins != null )
			{
				foreach ( var join in joins )
				{
					if ( join.IsValid )
					{
						Gizmos.color = join.Type.Color;

						Vector3 tip = join.Position - join.Forward * 0.5f + Vector3.up * 0.02f;

						Gizmos.DrawLine( join.Position, tip );
						Gizmos.DrawLine( join.Position - join.Right* 0.5f, tip );
						Gizmos.DrawLine( join.Position + join.Right * 0.5f, tip );
					}
				}

				Gizmos.color = Color.white;
			}

			if ( walls != null )
			{
				Gizmos.matrix = transform.localToWorldMatrix;
				Gizmos.color = Color.blue;

				foreach ( var wall in walls )
				{
					for ( int i = 1; i < wall.Corners.Count; i++ )
					{
						Vector3 a = wall.Corners[i - 1];
						Vector3 b = wall.Corners[i];

						a = new Vector3( a.x, 0.0f, a.y );
						b = new Vector3( b.x, 0.0f, b.y );

						for ( int j = 0; j <= wall.Height; j++ )
						{
							Gizmos.DrawLine( a + Vector3.up * j, b + Vector3.up * j );
						}
					}
				}

				Gizmos.matrix = Matrix4x4.identity;
				Gizmos.color = Color.white;
			}
		}
#endif
	}
}
