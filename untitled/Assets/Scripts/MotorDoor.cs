﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tweening;

public class MotorDoor : Door {

	[SerializeField] private Tweener tweener;
	[SerializeField] private TweenEvent openEvent, closeEvent;

	public override float State
	{
		get
		{
			return tweener.Position;
		}
	}

	protected override void OnClose ()
	{
		tweener.InvokeLocal(closeEvent);
	}

	protected override void OnOpen ()
	{
		tweener.InvokeLocal( openEvent );
	}

	protected override void OnLock ()
	{
		throw new NotImplementedException();
	}

	protected override void OnUnlock ()
	{
		throw new NotImplementedException();
	}
}
