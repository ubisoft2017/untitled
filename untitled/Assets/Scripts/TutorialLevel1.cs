﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Controls;
using Tweening;

public class TutorialLevel1 : MonoBehaviour {

	[SerializeField] private Transform leanIndicatorPosition;
	[SerializeField] private GameObject theAgentEye;
	[SerializeField] private Transform shootIndicatorPosition;
	[SerializeField] private Transform stealIndicatorPosition;
	[SerializeField] private GameObject leanText;
	[SerializeField] private GameObject aimText;
	[SerializeField] private GameObject shootText;
	[SerializeField] private GameObject stealText;
	[SerializeField] private GameObject lightSightText;
	[SerializeField] private GameObject jbText;
	[SerializeField] private Transform aimShootIndicatorPosition;
	[SerializeField] private Transform lightSightIndicatorPosition;
	[SerializeField] private GameObject pingText;
	[SerializeField] private Tweener leanTween, aimTween, shootTween, lightTween, jbTween, pingTween, stealTween;
	[SerializeField] private TweenEvent textIn, textOut;

	
	private RaycastHit leanHit;
	private RaycastHit shootHit;
	private RaycastHit stealHit;
	private RaycastHit lightHit;
	private bool leftTriggerConsumed;


	void Start ()
	{
		theAgentEye = GameObject.FindGameObjectWithTag("LeanEye");
	
    }

	// Update is called once per frame
	void Update ()
	{
		if ( Physics.Raycast( theAgentEye.transform.position, leanIndicatorPosition.position - theAgentEye.transform.position, out leanHit ) )
		{
			if(leanHit.distance < 4.5f && leanHit.collider.tag == "Tutorial")
			{
				leanText.SetActive( true );
			}
			else
			{
				leanText.SetActive( false );
			}
		}
		
		if(Vector3.Distance(pingText.transform.position, theAgentEye.transform.position) < 5)
		{
			pingText.SetActive( true );
		}
		else
		{
			pingText.SetActive( false );
		}

		if ( Physics.Raycast( theAgentEye.transform.position, shootIndicatorPosition.position - theAgentEye.transform.position, out shootHit ) && !leftTriggerConsumed )
		{


			if ( shootHit.distance < 5 && shootHit.collider.tag == "Tutorial2" )
			{
				aimText.SetActive( true );
			}
			else
			{
				aimText.SetActive( false );
			}
		}

		if ( GamePad.Query( DeviceTrigger.Left ).Position > .1f && shootHit.distance < 5 && !leftTriggerConsumed )
		{
			leftTriggerConsumed = true;
			aimText.SetActive( false );
			shootText.SetActive( true );
		}


		if ( Physics.Raycast( theAgentEye.transform.position, stealIndicatorPosition.position - theAgentEye.transform.position, out stealHit ))
		{

			if ( stealHit.collider.tag == "Tutorial3" && stealHit.distance < 5)
			{
				stealText.SetActive( true );
			}
			else
			{
				stealText.SetActive( false );
			}
		}

		if ( Physics.Raycast( theAgentEye.transform.position, lightSightIndicatorPosition.position - theAgentEye.transform.position, out lightHit ) )
		{
			if ( lightHit.collider.tag == "Tutorial4" && lightHit.distance < 7 )
			{
				lightSightText.SetActive( true );
			}
			else
			{
				lightSightText.SetActive( false );
			}
		}

		if(Vector3.Distance(theAgentEye.transform.position, jbText.transform.position) > 8 )
		{
			jbText.SetActive( false );
		}
		else
		{
			jbText.SetActive( true );
		}

		if ( GamePad.Query( DeviceTrigger.Right ).Position > .1f && leftTriggerConsumed )
		{
			shootText.SetActive( false );
		}
	}
}

