﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class ChromaticTweener : MonoBehaviour
{

	private VignetteAndChromaticAberration chromaticSettings;
	private float startIntensityValue;
	private float startAberationValue;

	private float maxIntensityValue = 1.5f;
	private float maxAberationValue = 500;

	private float intensityTime = 0.0f;
	private float aberationTime = 0.0f;
	private bool reverseEffect;

	public bool startEffect = false;
	public bool reset = false;

    [SerializeField]
    private Tweening.TweenEvent levelDeform; 

	// Use this for initialization
	void Start ()
	{
		chromaticSettings = GetComponent<VignetteAndChromaticAberration>();
		startIntensityValue = chromaticSettings.intensity;
		startAberationValue = chromaticSettings.chromaticAberration;
		
	}

	void Update ()
	{

		if ( reset )
		{
			reset = false;
			aberationTime = 0.0f;
			startEffect = false;

		}

		if ( startEffect )
		{
			chromaticSettings.intensity = 2.0f;
			if ( !reverseEffect )
			{
				
				if ( aberationTime < 1.0f )
				{
					aberationTime += Time.deltaTime *1.5f;
					chromaticSettings.chromaticAberration = Mathf.Lerp( startAberationValue, maxAberationValue, aberationTime );
				}

				if ( aberationTime >= 1.0f )
				{
					aberationTime = 0.0f;
					reverseEffect = true;
				}
			}
			else if ( reverseEffect )
			{
				
				if ( aberationTime < 1.0f )
				{
					aberationTime += Time.deltaTime * 1.5f;
					chromaticSettings.chromaticAberration = Mathf.Lerp( maxAberationValue, startAberationValue, aberationTime );
				}else {
					reverseEffect = false;
					chromaticSettings.intensity = 0.0f;
					reset = true;
                    levelDeform.Invoke();
                }
			}
		}
	}
}