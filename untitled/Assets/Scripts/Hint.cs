﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "New Hint.asset", menuName = "Hint" )]
public class Hint : ScriptableObject
{
	[Tooltip( "Hint text shown on loading screens, etc." )]
	[SerializeField] private string text;

	public string Text
	{
		get
		{
			return text;
		}
	}
}
