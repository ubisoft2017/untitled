﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRenderOnTop : MonoBehaviour {

    List<Renderer> renderers;
    Camera cam;
    bool isActivated = false;

    [SerializeField]
    LayerMask newMask;
    [SerializeField]
    private CameraClearFlags newFlags;
    [SerializeField]
    private LayerMask oldMask;
    [SerializeField]
    private CameraClearFlags oldFlags;

    void Awake()
    {
        renderers = new List<Renderer>(0);
        cam = GetComponent<Camera>();
    }
   public void Activate()
    {
        if (!isActivated)
        {
            isActivated = true;
        }
    }

    void OnPreRender() {
        cam.cullingMask = oldMask;
        cam.clearFlags = oldFlags;
    }
    void OnPostRender() {
        cam.cullingMask = newMask;
        cam.clearFlags = newFlags;
        cam.Render();




    }
    public void reset() {
        isActivated = false;
    }

    public void addRenderer(Renderer r) {
        renderers.Add(r);
    }



}
