﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using AI;
using Tweening;

public class Guard : Node, IPhaseInOut
{
	public override NodeType NodeType
	{
		get
		{
			return NodeType.Guard;
		}
	}

	[SerializeField]
	private VisionProcessor vision;

    [SerializeField]
    private LayerMask layerMask;

	[SerializeField]
	private Transform lookAtTarget;

	private GameOverSystem gameOverSystem;

	[SerializeField]
	private float visionScoreThreshold;

	[SerializeField]
	private float awarenessRateVision;

	[SerializeField]
	private PhaseMember phaseMemberA, phaseMemberB;

	[SerializeField]
	private ParticleSystem outParticles, inParticles;

	[SerializeField]
	private float inDelay;

	[SerializeField]
	private Animator animator;

    [SerializeField]
    private Animator tagAnimator;

    [SerializeField]
	private new SkinnedMeshRenderer renderer, hat;

	[SerializeField]
	private ShaderUniformInterpolator interpolator;

	[SerializeField]
	private Tweener tweener, detectionTweener;

	[SerializeField]
	private TweenEvent inEvent, detectionIn, detectionOut;

	[SerializeField]
	private AnimationCurve detectionCurve;

	[SerializeField]
	private Image detectionUI;

	[SerializeField]
	private CanvasGroup detectionUICanvas;

	[SerializeField]
	private SFX sfx;

	[SerializeField]
	private AudioClip detectClip, tagClip, startClip, stopClip;

	private float guardAwareness;
	private GameObject theAgent;
	private LookController agentController;
	private Material material;
	private bool tweenFired;
	private bool gameOver;

    public Material Material
    {
        get
        {
            return material;
        }
    }

	public void Tag ()
	{
		sfx.PlayClip( tagClip, 1.0f, true );
	}

	private float sum;

	private bool disrupted;

	public void StartDisrupting ()
	{
		if ( !disrupted )
		{
			sfx.PlayClip( startClip, 1.0f, true );

			phaseMemberA.Delocalize();
			phaseMemberB.Delocalize();

			animator.enabled = false;

			outParticles.Stop();
			outParticles.Play();

			disrupted = true;

            TrackingChip[] chips = GetComponentsInChildren<TrackingChip>();

            foreach (TrackingChip t in chips) {
                t.disable();
            }
		}
	}

	public void StopDisrupting ()
	{
		if ( disrupted )
		{
			sfx.PlayClip( stopClip, 1.0f, true );

			inParticles.Stop();
			inParticles.Play();

			phaseMemberB.Localize();

			tweener.InvokeLocal( inEvent );

			StartCoroutine( StopDisruptingCoroutine() );

			disrupted = false;
		}
	}

	private IEnumerator StopDisruptingCoroutine ()
	{
		yield return new WaitForSeconds( inDelay );

		phaseMemberA.Localize();

		yield break;
	}

	protected override void Awake ()
	{
		base.Awake();

		theAgent = FindObjectOfType<Agent>().gameObject;
		agentController = theAgent.GetComponent<LookController>();
		vision.OnProcessTarget += ( t, s ) => sum += s;

		gameOverSystem = FindObjectOfType<GameOverSystem>();

		material = new Material( renderer.sharedMaterial );

		renderer.sharedMaterial = material;
		hat.sharedMaterial = material;

		interpolator.mat = material;

	}

	private void Update ()
	{
        float rate = 0.0f;

        //RaycastHit hit;

		//if ( Physics.Raycast( transform.position, (theAgent.transform.position - transform.position).normalized, out hit, layerMask ) )
		//{
			//if ( hit.transform.root.gameObject == theAgent && !disrupted )
			if ( !disrupted )
			{
				rate = detectionCurve.Evaluate( Vector3.Distance( transform.position, theAgent.transform.position ) );
			}
		//}
		
		if ( rate > 0.0f)
		{
			guardAwareness += (rate * Time.deltaTime);
		}
		else
		{
			guardAwareness -= Time.deltaTime;
			
		}

		guardAwareness = Mathf.Clamp01( guardAwareness );

		detectionUI.fillAmount = guardAwareness;

		if ( guardAwareness > 0.0f && !disrupted)
		{
			detectionUICanvas.alpha = 1;
		}
		else
		{
			detectionUICanvas.alpha = 0;
		}

		if ( guardAwareness >= 0.999f && !gameOver && !disrupted )
		{
			gameOverSystem.DetectedByGuard();
			DetectAgent();
		}
	}

	private void LateUpdate ()
	{
		if ( sum > visionScoreThreshold && !gameOver && !disrupted )
		{
			gameOverSystem.DetectedByGuardLight();
			DetectAgent();
		}

		sum = 0.0f;

		if ( gameOver )
		{
			transform.LookAt( theAgent.transform.position );
		}
	}

	private void DetectAgent ()
	{
		sfx.PlayClip( detectClip );

		gameOver = true;

		agentController.LookAtGuardWhoSpottedYou( transform );

		detectionUICanvas.alpha = 1;
		detectionUI.fillAmount = 1;
		FindObjectOfType<Agent>().GetCaught();
	}

	public void Localize ()
	{
		animator.enabled = true;
    }

	public void Delocalize ()
	{
		animator.enabled = false;
	}

}
