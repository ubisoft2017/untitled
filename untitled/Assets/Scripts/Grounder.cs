﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounder : MonoBehaviour {

	[SerializeField] Transform characterTransform;
	[SerializeField] CharacterController characterController;

	private float yComponent;
	private Vector3 forcedPosition;

	void Start () {
		yComponent = characterTransform.position.y + characterController.skinWidth;
	}
	
	// Update is called once per frame
	void Update () {
		forcedPosition = new Vector3(characterTransform.position.x, yComponent, characterTransform.position.z);
		characterTransform.position = forcedPosition;
	}
}
