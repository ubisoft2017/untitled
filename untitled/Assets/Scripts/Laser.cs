﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;


[RequireComponent( typeof( Raycaster ) )]
public class Laser : MonoBehaviour
{
	[SerializeField] private LineRenderer lineRenderer;
	[SerializeField] private Transform laserColliderPosition;
	[SerializeField] private CapsuleCollider laserCollider;
	[SerializeField] private GameObject dotPrefab;

	[SerializeField][HideInInspector] private Light rayDotLight;
	[SerializeField][HideInInspector] private Light tipDotLight;

	[SerializeField] private float tipDotOffset;
	[SerializeField] private float rayDotOffset;
	[SerializeField] private float infinity;

	[SerializeField] private float minWidth;
	[SerializeField] private float maxWidth;

	[SerializeField] private bool on;

	private Raycaster raycaster;


	public void SetLaserStatus(bool onOrOff)
	{
		on = onOrOff;
	}

	public bool GetLaserStatus()
	{
		return on;
	}

	private void Awake ()
	{
		rayDotLight = Instantiate( dotPrefab, transform ).GetComponent<Light>();
		tipDotLight = Instantiate( dotPrefab, transform ).GetComponent<Light>();

		tipDotLight.transform.localPosition = Vector3.forward * tipDotOffset;

		raycaster = GetComponent<Raycaster>();
		laserCollider.radius = 0.025f;
		laserCollider.direction = 2;

	}

	private void Start ()
	{
		
		raycaster.caster = transform;
	}

	private void Update ()
	{
		
		if ( on )
		{
			tipDotLight.enabled = true;

			lineRenderer.enabled = true;

			raycaster.Raycast();

			if ( raycaster.Hit )
			{
				laserCollider.height = raycaster.Nearest.distance;
				laserColliderPosition.position = (transform.position + raycaster.Nearest.point )/2;
				laserColliderPosition.rotation = Quaternion.LookRotation( transform.position - raycaster.Nearest.point );

				lineRenderer.SetPositions( new Vector3[] { transform.position, raycaster.Nearest.point } );

				rayDotLight.transform.position = raycaster.Nearest.point + -transform.forward * rayDotOffset;

				rayDotLight.enabled = true;

				var target = raycaster.Nearest.collider.GetComponent<ILaserTarget>();

				if ( target != null )
				{
					target.OnHit();
				}
			}
			else
			{
				lineRenderer.SetPositions( new Vector3[] { transform.position, raycaster.Nearest.point } );

				rayDotLight.enabled = false;
			}
		}
		else
		{
			raycaster.Clear();

			tipDotLight.enabled = false;
			rayDotLight.enabled = false;

			lineRenderer.enabled = false;
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		if ( !Application.isPlaying )
		{
			raycaster = GetComponent<Raycaster>();

			raycaster.caster = transform;

			raycaster.Raycast();

			Gizmos.color = Color.red;

			if ( raycaster.Hit )
			{
				Gizmos.DrawLine( transform.position, raycaster.Nearest.point );
			}
			else
			{
				Gizmos.DrawRay( transform.position, transform.forward * infinity );
			}

			Gizmos.color = Color.white;

			raycaster.Clear();

			raycaster.caster = null;

			raycaster = null;
		}
	}
		
#endif
}
