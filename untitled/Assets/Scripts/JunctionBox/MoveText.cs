﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveText : MonoBehaviour
{

    [SerializeField] private Text text1;
    [SerializeField] private Text text2;

    [SerializeField] private RectTransform rt1;
    [SerializeField] private RectTransform rt2;

    public int speed = 700;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 local = rt1.anchoredPosition;

        local.x += Time.deltaTime * speed;

        if (local.x > 1280f)
        {
            local.x = -1280f;
        }

        rt1.anchoredPosition = local;

        Vector2 local2 = rt2.anchoredPosition;
        local2.x += Time.deltaTime * speed;

        if (local2.x > 1280f)
        {
            local2.x = -1280f;
        }

        rt2.anchoredPosition = local2;
    }

    public void SetText(string text)
    {
        text1.text = text;
        text2.text = text;
    }


    public void Highlight(char button)
    {
        switch (button)
        {
            case 'A':
                text1.text = text1.text.Replace("A", "<color=green>A</color>");
                break;
            case 'B':
                text1.text = text1.text.Replace("B", "<color=red>B</color>");
                break;
            case 'X':
                text1.text = text1.text.Replace("X", "<color=cyan>X</color>");
                break;
            case 'Y':
                text1.text = text1.text.Replace("Y", "<color=yellow>Y</color>");
                break;
        }

        text2.text = text1.text;
    }
}
