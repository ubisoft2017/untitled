﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWireColorSet.asset", menuName = "JunctionBox/WireColors", order = 100)]
public class JunctionBoxWireColors : ScriptableObject {
    public Color[] colors;
    public string[] colorNames;

    void OnEnable()
    {
        if(colors.Length != 4)
        {
            colors = new Color[4];
        }

        if (colorNames.Length != 4)
        {
            colorNames = new string[4];
        }
    }
}
