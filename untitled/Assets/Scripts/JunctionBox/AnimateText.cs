﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateText : MonoBehaviour {

    Text text;

    public float textAlpha = 1.0f;

    // Use this for initialization
    void Awake() {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    StartCoroutine(TryAgain());
        //}
        Color c = text.color;
        c.a  = textAlpha;
        text.color = c;

    }

    IEnumerator Loading() {

        int currentState = 0;
        char[] loadingBars = { '|', '/', '-', '\\' };


        while (true)
        {
            currentState = (currentState + 1) % loadingBars.Length;

            char c1 = loadingBars[currentState];
            char c2 = loadingBars[(currentState + 0) % loadingBars.Length];
            char c3 = loadingBars[(currentState + 0) % loadingBars.Length];
            char c4 = loadingBars[(currentState + 0) % loadingBars.Length];


            text.text = string.Format("{0}{1}{2}{3}", c1, c2, c3, c4);

            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator Retry(string text, float delay, float finalDelay)
    {
        for(int i = 0; i+4 <= text.Length; i++)
        {
            this.text.text = text.Substring(i, 4);
            yield return new WaitForSeconds(delay);
        }

        yield return new WaitForSeconds(finalDelay);


        StartCoroutine(Loading());

    }

    public void StartLoading()
    {
        StopAllCoroutines();
        StartCoroutine(Loading());
    }

    public float StartRetry(int i)
    {
        float delay = 0.2f;
        float finalDelay = 0.6f;

        string text = "  " + Util.Text.Ordinal(i).ToUpper() + "  TRY";



        StopAllCoroutines();
        StartCoroutine(Retry(text, delay, finalDelay));

        return (text.Length - 4) * delay + finalDelay;
    }


    public void DisplayText(string text)
    {
        StopAllCoroutines();
        this.text.text = text;
    }
}
