﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickSwitch : MonoBehaviour {


    private bool open;

    Vector3 originalPos;
    Quaternion originalRot;


    public Vector3 pos;
    public Vector3 rot;

    [SerializeField]
    private GameObject prompt;


    // Use this for initialization
    void Start () {
        originalPos = transform.localPosition;
        originalRot = transform.localRotation;
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void Flick()
    {
        open = !open;

        if (open)
        {
            transform.localPosition = pos;
            transform.localRotation = Quaternion.Euler(rot.x, rot.x, rot.z);
        }
        else
        {
            transform.localPosition = originalPos;
            transform.localRotation = originalRot;
        }
    }

    public void Select()
    {
        prompt.SetActive(true);
    }

    public void Deselect()
    {
        prompt.SetActive(false);
    }

    public void Close()
    {
        open = false;

        if (open)
        {
            transform.localPosition = pos;
            transform.localRotation = Quaternion.Euler(rot.x, rot.x, rot.z);
        }
        else
        {
            transform.localPosition = originalPos;
            transform.localRotation = originalRot;
        }
    }
}
