﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class JunctionBox : MonoBehaviour
{

    public enum Codes
    {
        ABXY,
        ABYX,
        AXBY,
        AXYB,
        AYBX,
        AYXB,
        BAXY,
        BAYX,
        BXAY,
        BXYA,
        BYAX,
        BYXA,
        XABY,
        XAYB,
        XBAY,
        XBYA,
        XYAB,
        XYBA,
        YABX,
        YAXB,
        YBAX,
        YBXA,
        YXAB,
        YXBA
    }

    public enum State
    {
        WaitForUplink,
        Streams,
        Retry,
        Wire
    }

    protected State state = State.WaitForUplink;

    protected Codes[] chosenCodes;

    protected Codes rightCode;

    public JunctionBoxWireColors wireColors;

    protected int rightColor;

    public abstract void Pass();
    public abstract void Fail();

	protected virtual void Awake ()
	{
		DevTools.Register( string.Format( "Pass ({0})", GetType().Name ), Pass );
        DevTools.Register(string.Format("Fail ({0})", GetType().Name), Fail);

    }
}
