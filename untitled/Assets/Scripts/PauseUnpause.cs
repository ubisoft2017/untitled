﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;
using Tweening;

public class PauseUnpause : ControllerBase
{
    [SerializeField]
    private PlayerState pauseState;

    [SerializeField]
    [EnumObject(typeof(Tweening.TweenEvent))]
    private Tweening.TweenEvent pauseEvent, unpauseEvent;

	[SerializeField]
	private ButtonAction pauseAction;

    private bool isPaused;

    private ConnectionController connectionController;

    private PlayerState state;

    protected override void ControllerAwake()
    {
        base.ControllerAwake();

        isPaused = false;

        pauseEvent.OnInvoke += Pause;
        unpauseEvent.OnInvoke += Unpause;

        connectionController = FindObjectOfType<ConnectionController>();

		Register( pauseAction, () =>
		{
			if ( isPaused )
			{
				unpauseEvent.Invoke();
			}
			else
			{
				pauseEvent.Invoke();
			}
		} );
    }

    private void Pause(TweenEvent e)
    {
		if ( Player != null )
		{
			if ( !isPaused )
			{
				state = Player.State;

				Player.State = pauseState;

				Debug.Log( "pause", this );

				Time.timeScale = 0.0f;

				//connectionController.PauseGame();
				isPaused = true;
			}
		}
    }

    private void Unpause(TweenEvent e)
	{
		if ( Player != null )
		{
			if ( isPaused )
			{
				Player.State = state;

				Debug.Log( "unpause", this );

				Time.timeScale = 1.0f;

				//connectionController.UnpauseGame();

				isPaused = false;
			}
		}
    }
}
