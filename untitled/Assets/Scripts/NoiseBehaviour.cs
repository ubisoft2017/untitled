﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using LibNoise.Generator;

public class NoiseBehaviour : MonoBehaviour
{
	[Range( 0.0f, 1.0f )] public float smoothing = 0.7f;

	[Range( 0.0f, 120.0f )] public float interpolation = 30.0f;

	private Vector3 rand;

	private Perlin x, y, z;

	[SerializeField]
	private float frequency, amplitude, lacunarity, persistence;

	[SerializeField]
	private int octaves;

	[SerializeField]
	private LibNoise.QualityMode quality;

	private int Seed
	{
		get
		{
			return UnityEngine.Random.Range( int.MinValue, int.MaxValue );
		}
	}

	private void Awake ()
	{
		x = new Perlin( frequency, lacunarity, persistence, octaves, Seed, quality );
		y = new Perlin( frequency, lacunarity, persistence, octaves, Seed, quality );
		z = new Perlin( frequency, lacunarity, persistence, octaves, Seed, quality );
	}

	private void FixedUpdate ()
	{
		rand = Vector3.Lerp(
			new Vector3(
				(float) x.GetValue( Time.time, Time.time, Time.time ),
				(float) y.GetValue( Time.time, Time.time, Time.time ),
				(float) z.GetValue( Time.time, Time.time, Time.time ) ),
			rand,
			Mathf.Sqrt( smoothing ) );
	}

	private void Update ()
	{
		transform.localPosition = Vector3.Lerp(
			Vector3.zero + rand * amplitude,
			transform.localPosition,
			interpolation * Time.deltaTime );
	}
}
