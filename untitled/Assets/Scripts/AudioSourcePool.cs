﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class AudioSourcePool : ComponentPool<AudioSource>
{
	protected override AudioSource Preprocess ( AudioSource resource )
	{
		return base.Preprocess( resource );
	}

	protected override AudioSource Postprocess ( AudioSource resource )
	{
		resource.Stop();

		resource.clip = null;

		resource.volume = 1.0f;

		resource.bypassEffects = false;
		resource.bypassListenerEffects = false;
		resource.bypassReverbZones = false;

		resource.panStereo = 0.0f;

		return base.Postprocess( resource );
	}
}
