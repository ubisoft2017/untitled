﻿using UnityEngine;

public enum EnumFlagsStyle
{
	Popup,
	Toggle,
	Mask
}

/// <summary>
/// source: http://wiki.unity3d.com/index.php/EnumFlagPropertyDrawer
/// </summary>
public class EnumFlagsAttribute : PropertyAttribute
{
	public string name;

	// Defaults to Toggle since all integer types (byte, short, int, long) are supported
	public EnumFlagsStyle style;

	public EnumFlagsAttribute ( EnumFlagsStyle style = EnumFlagsStyle.Toggle )
	{
		this.style = style;
	}

	public EnumFlagsAttribute ( string name, EnumFlagsStyle style = EnumFlagsStyle.Toggle ) : this( style )
	{
		this.name = name;
	}
}