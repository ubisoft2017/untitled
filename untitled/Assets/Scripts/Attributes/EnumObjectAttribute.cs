﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AttributeUsage( AttributeTargets.Field )]
public class EnumObjectAttribute : PropertyAttribute
{
	public readonly Type type;
	public readonly string path;

	/// <summary>
	/// Display this field as a drop-down list of available asset references.
	/// </summary>
	/// <param name="type">The type of asset used to populate the dropdown for this field.</param>
	/// <param name="path">The Resources subdirectory to searching for assets.</param>
	public EnumObjectAttribute ( Type type = null, string path = "" )
	{
		this.type = type;
		this.path = path;
	}
}
