﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	[CreateAssetMenu( fileName = "New ButtonAction.asset", menuName = "Input/Button Action", order = 100 )]
	public class ButtonAction : PlayerAction
	{
		private static int counter;

		public static int Count
		{
			get
			{
				return counter;
			}
		}

		private static ButtonAction[] buffer;

		public static ButtonAction GetAction ( int number )
		{
			return buffer[number];
		}

		static ButtonAction ()
		{
			buffer = new ButtonAction[BUFFER_SIZE];
		}

		private int number;

		public int Number
		{
			get
			{
				return number;
			}
		}

		public override void Initialize ()
		{
			// Establish a total ordering of actions
			// This will allow us to have O(1) action mapping lookups in player profiles

			buffer[counter] = this;

			number = counter++;
		}

		[Header( "Button" )]

		[SerializeField] private DeviceButton defaultMapping = DeviceButton.A;

		public DeviceButton DefaultMapping
		{
			get
			{
				return defaultMapping;
			}
		}

		[Tooltip( "Whether this actions happens when a button is pressed, released, or held for some amount of time." )]
		[SerializeField] private ButtonEvent buttonEvent = ButtonEvent.Press;

		public ButtonEvent ButtonEvent
		{
			get
			{
				return buttonEvent;
			}
		}

		[Tooltip( "How long the button must be held before this action happens." )]
		[SerializeField][HideInInspector] private float holdTime = 0.0f;

		public float HoldTime
		{
			get
			{
				return holdTime;
			}
		}

		[Tooltip( "Whether or not this action happens continuously as long as the button is held." )]
		[SerializeField][HideInInspector] private bool repeat = false;

		public bool Repeat
		{
			get
			{
				return repeat;
			}
		}

#if UNITY_EDITOR
		public override string HintPreview
		{
			get
			{
				return "You can " + buttonEvent.ToString().ToLower() + " " + defaultMapping + " to " + Description.ToLower();
			}
		}
#endif
	}
}