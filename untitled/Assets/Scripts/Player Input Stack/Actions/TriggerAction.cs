﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	[CreateAssetMenu( fileName = "New TriggerAction.asset", menuName = "Input/Trigger Action", order = 100 )]
	public class TriggerAction : PlayerAction
	{
		private static int counter;

		public static int Count
		{
			get
			{
				return counter;
			}
		}

		private static TriggerAction[] buffer;

		public static TriggerAction GetAction ( int number )
		{
			return buffer[number];
		}

		static TriggerAction ()
		{
			buffer = new TriggerAction[128];
		}

		private int number;

		public int Number
		{
			get
			{
				return number;
			}
		}

		public override void Initialize ()
		{
			// Establish a total ordering of actions
			// This will allow us to have O(1) action mapping lookups in player profiles

			buffer[counter] = this;

			number = counter++;
		}

		[Header( "Trigger" )]

		[SerializeField] private DeviceTrigger defaultMapping = DeviceTrigger.Left;

		public DeviceTrigger DefaultMapping
		{
			get
			{
				return defaultMapping;
			}
		}

		[SerializeField] private DeviceAxisProperty property;

		public DeviceAxisProperty Property
		{
			get
			{
				return property;
			}
		}

#if UNITY_EDITOR
		public override string HintPreview
		{
			get
			{
				return "You can use " + defaultMapping + " Trigger to " + Description.ToLower();
			}
		}
#endif
	}
}
