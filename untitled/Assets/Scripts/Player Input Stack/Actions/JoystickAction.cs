﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	[CreateAssetMenu( fileName = "New JoystickAction.asset", menuName = "Input/Joystick Action", order = 100 )]
	public class JoystickAction : PlayerAction
	{
		private static int counter;

		public static int Count
		{
			get
			{
				return counter;
			}
		}

		private static JoystickAction[] buffer;

		public static JoystickAction GetAction ( int number )
		{
			return buffer[number];
		}

		static JoystickAction ()
		{
			buffer = new JoystickAction[128];
		}

		private int number;

		public int Number
		{
			get
			{
				return number;
			}
		}

		public override void Initialize ()
		{
			// Establish a total ordering of actions
			// This will allow us to have O(1) action mapping lookups in player profiles

			buffer[counter] = this;

			number = counter++;
		}

		[Header( "Joystick" )]

		[SerializeField] private DeviceJoystick defaultMapping = DeviceJoystick.Left;

		public DeviceJoystick DefaultMapping
		{
			get
			{
				return defaultMapping;
			}
		}

		[SerializeField][Range( 1, 5 )] private int defaultSensitivity = 3;

		public int DefaultSensitivity
		{
			get
			{
				return defaultSensitivity;
			}
		}

		[SerializeField] private JoystickInversion defaultInversion = JoystickInversion.None;

		public JoystickInversion DefaultInversion
		{
			get
			{
				return defaultInversion;
			}
		}

		[SerializeField] private DeviceAxisProperty property;

		public DeviceAxisProperty Property
		{
			get
			{
				return property;
			}
		}

#if UNITY_EDITOR
		public override string HintPreview
		{
			get
			{
				return "You can use the " + defaultMapping + " Joystick to " + Description.ToLower();
			}
		}
#endif
	}
}
