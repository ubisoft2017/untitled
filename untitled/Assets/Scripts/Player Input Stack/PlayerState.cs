﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Controls
{
    [CreateAssetMenu(fileName = "New PlayerState.asset", menuName = "Input/Player State", order = 110)]
    public class PlayerState : ScriptableObject, IInitialize
    {
        public const string RESOURCE_PATH = "Player States";

        private bool[] buttonActionMask;
        private bool[] joystickActionMask;
        private bool[] triggerActionMask;

        [SerializeField]
        private List<PlayerAction> actions = new List<PlayerAction>();

        public ReadOnlyCollection<PlayerAction> Actions
        {
            get
            {
                return actions.AsReadOnly();
            }
        }

        [SerializeField]
        [Tooltip("If an input event is not consumed by a PlayerAction local to this state, it will try and hand it off to each of its includes, until a consumer is found.")]
        private List<PlayerState> includes = new List<PlayerState>();

        public ReadOnlyCollection<PlayerState> Includes
        {
            get
            {
                return includes.AsReadOnly();
            }
        }

        private bool init;

        public void Initialize()
        {
			buttonActionMask = new bool[PlayerAction.BUFFER_SIZE];
            triggerActionMask = new bool[PlayerAction.BUFFER_SIZE];
            joystickActionMask = new bool[PlayerAction.BUFFER_SIZE];

            for (int i = 0; i < actions.Count; i++)
            {
                if (actions[i] is ButtonAction)
                {
                    buttonActionMask[(actions[i] as ButtonAction).Number] = true;
                    continue;
                }
                if (actions[i] is TriggerAction)
                {
                    triggerActionMask[(actions[i] as TriggerAction).Number] = true;
                    continue;
                }
                if (actions[i] is JoystickAction)
                {
                    joystickActionMask[(actions[i] as JoystickAction).Number] = true;
                    continue;
                }
            }

            for (int i = 0; i < includes.Count; i++)
            {
                // Make sure the dependency has been initialized
                includes[i].Initialize();

                for (int j = 0; j < ButtonAction.Count; j++)
                {
                    buttonActionMask[j] |= includes[i].buttonActionMask[j];
                }

                for (int j = 0; j < TriggerAction.Count; j++)
                {
                    triggerActionMask[j] |= includes[i].triggerActionMask[j];
                }

                for (int j = 0; j < JoystickAction.Count; j++)
                {
                    joystickActionMask[j] |= includes[i].joystickActionMask[j];
                }
            }

            init = true;
        }

        public bool Implements(ButtonAction action)
        {
            return buttonActionMask[action.Number];
        }

        public bool Implements(TriggerAction action)
        {
            return triggerActionMask[action.Number];
        }

        public bool Implements(JoystickAction action)
        {
            return joystickActionMask[action.Number];
        }
    }
}