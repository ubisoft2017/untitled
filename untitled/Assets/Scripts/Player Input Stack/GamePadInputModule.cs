﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controls;
using UnityEngine;
using UnityEngine.EventSystems;

public class GamePadInputModule : BaseInputModule
{
	private static GamePadInputModule instance;

	public static void Bind ()
	{
		instance.DoBind();
	}

	[SerializeField]
	private float repeatHoldTime = 0.5f, moveDeadZone = 0.1f, repeatPeriod = 0.1f;

	[SerializeField]
	private List<DeviceButton> submitButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.A, DeviceButton.X } );

	[SerializeField]
	private List<DeviceButton> cancelButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.B } );

	[SerializeField]
	private List<DeviceButton> leftButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.Left } );

	[SerializeField]
	private List<DeviceButton> rightButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.Right } );

	[SerializeField]
	private List<DeviceButton> upButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.Up } );

	[SerializeField]
	private List<DeviceButton> downButtons = new List<DeviceButton>( new DeviceButton[] { DeviceButton.Down } );

	[SerializeField]
	private List<DeviceJoystick> moveJoysticks = new List<DeviceJoystick>( new DeviceJoystick[] { DeviceJoystick.Left, DeviceJoystick.Right } );

	private float[] holdTimes;
	private float lastRepeat;

	public override bool IsActive ()
	{
		return isActiveAndEnabled;
	}

	public override bool IsModuleSupported ()
	{
		return true;
	}

	public override bool ShouldActivateModule ()
	{
		return true;
	}

	public override void ActivateModule ()
	{
		base.ActivateModule();

		eventSystem.SetSelectedGameObject( eventSystem.firstSelectedGameObject );
		Debug.Log( GetType().Name, this );
	}

	protected override void Awake ()
	{
		base.Awake();

		instance = this;

		DoBind();
	}

	private void DoBind ()
	{
		foreach ( var button in submitButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button && eventSystem.currentSelectedGameObject != null )
				{
					var data = GetBaseEventData();

					ExecuteEvents.Execute( eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler );
				}
			};
		}

		foreach ( var button in cancelButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button && eventSystem.currentSelectedGameObject != null )
				{
					var data = GetBaseEventData();

					ExecuteEvents.Execute( eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler );
				}
			};
		}

		foreach ( var button in upButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button )
				{
					MoveButton( button, MoveDirection.Up, Vector2.up );
				}
			};
		}

		foreach ( var button in downButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button )
				{
					MoveButton( button, MoveDirection.Down, Vector2.down );
				}
			};
		}

		foreach ( var button in leftButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button )
				{
					MoveButton( button, MoveDirection.Left, Vector2.left );
				}
			};
		}

		foreach ( var button in rightButtons )
		{
			GamePad.OnPress += ( b ) =>
			{
				if ( b == button )
				{
					MoveButton( button, MoveDirection.Right, Vector2.right );
				}
			};
		}
	}

	public override void Process ()
	{
		if ( holdTimes == null || holdTimes.Length != moveJoysticks.Count )
		{
			holdTimes = new float[moveJoysticks.Count];
		}

		for ( int i = 0; i < moveJoysticks.Count; i++ )
		{
			float holdTime = holdTimes[i];

			MoveJoystick( moveJoysticks[i], ref holdTime );

			holdTimes[i] = holdTime;
		}
	}

	private bool MoveButton ( DeviceButton button, MoveDirection direction, Vector2 vector )
	{
		var state = GamePad.Query( button );

		if ( state.WasJustPressed || state.HoldTime >= repeatHoldTime )
		{
			var data = GetAxisEventData( vector.x, vector.y, moveDeadZone );

			data.moveDir = direction;
			data.selectedObject = eventSystem.currentSelectedGameObject;

			ExecuteEvents.Execute( eventSystem.currentSelectedGameObject, data, ExecuteEvents.moveHandler );

			return true;
		}

		return false;
	}

	private bool MoveJoystick ( DeviceJoystick joystick, ref float holdTime )
	{
		var state = GamePad.Query( joystick );

		if ( state.Magnitude > moveDeadZone )
		{
			if ( holdTime == 0.0f || (holdTime >= repeatHoldTime && (Time.time - lastRepeat >= repeatPeriod)) )
			{
				lastRepeat = Time.time;

				var data = GetAxisEventData( state.Position.x, state.Position.y, moveDeadZone );

				data.selectedObject = eventSystem.currentSelectedGameObject;

				ExecuteEvents.Execute( eventSystem.currentSelectedGameObject, data, ExecuteEvents.moveHandler );
			}

			holdTime += Time.unscaledDeltaTime;
		}
		else
		{
			holdTime = 0.0f;
		}

		return false;
	}
}
