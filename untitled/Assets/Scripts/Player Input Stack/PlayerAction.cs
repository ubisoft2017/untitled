﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	public abstract class PlayerAction : ScriptableObject, IInitialize
	{
		public const string RESOURCE_PATH = "Player Actions";

		public const int BUFFER_SIZE = 128;

		[Header( "Info" )]

		[Tooltip( "Short label used in options menu, etc." )]
		[SerializeField] private string label;

		public string Label
		{
			get
			{
				return label;
			}
		}

		[Tooltip( "Description of this action. May be appear on help screens, options menu, etc." )]
		[SerializeField] private string description;

		public string Description
		{
			get
			{
				return description;
			}
		}

		[Tooltip( "Whether or not this action can appear as a hint (on a loading screen, for example)." )]
		[SerializeField] private bool showAsHint;

		public bool ShowAsHint
		{
			get
			{
				return showAsHint;
			}
		}

		[Tooltip( "Text shown on in-game prompts." )]
		[SerializeField] protected string prompt;

		public string Prompt
		{
			get
			{
				return prompt;
			}
		}

		public abstract void Initialize ();

#if UNITY_EDITOR
		public abstract string HintPreview { get; }
#endif
	}
}