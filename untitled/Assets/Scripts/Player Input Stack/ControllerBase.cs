﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	/// <summary>
	/// Base class for all behaviours responsible for executing player actions.
	/// </summary>
	public abstract class ControllerBase : MonoBehaviour
	{
		private List<Action<DeviceButton>> handlers;
		private List<ButtonEvent> events;

		private Player player;

		public Player Player
		{
			get
			{
				return player;
			}

			set
			{
				player = value;
			}
		}

		private void Awake ()
		{
			handlers = new List<Action<DeviceButton>>();
			events = new List<ButtonEvent>();

			ControllerAwake();
		}

		protected virtual void ControllerAwake () { }

		private void Start ()
		{
			ControllerStart();
		}

		protected virtual void ControllerStart () { }

		private void Update ()
		{
			if ( player != null && player.State != null )
			{
				ControllerUpdate();
			}
		}

		protected virtual void ControllerUpdate () { }

		private void FixedUpdate ()
		{
			if ( player != null && player.State != null )
			{
				ControllerFixedUpdate();
			}
		}

		protected virtual void ControllerFixedUpdate () { }

		protected void Register ( ButtonAction action, Action callback )
		{
			Action<DeviceButton> handler = ( b ) =>
			{
				if ( player != null && player.Profile != null && player.State != null )
				{
					if ( GamePad.Query( action, player.Profile, player.State ))
					{
						callback();
					}
				}
			};

			handlers.Add( handler );
			events.Add( action.ButtonEvent );

			switch ( action.ButtonEvent )
			{
			case ButtonEvent.Press:
				GamePad.OnPress += handler;
				break;
			case ButtonEvent.Release:
				GamePad.OnRelease += handler;
				break;
			case ButtonEvent.Hold:
				GamePad.OnHold += handler;
				break;
			}
		}

		protected void Clear ()
		{
			for ( int i = 0; i < handlers.Count; i++ )
			{
				switch ( events[i] )
				{
					case ButtonEvent.Press:
						GamePad.OnPress -= handlers[i];
						break;
					case ButtonEvent.Release:
						GamePad.OnRelease -= handlers[i];
						break;
					case ButtonEvent.Hold:
						GamePad.OnHold -= handlers[i];
						break;
				}
			}
		}

		private void OnDestroy ()
		{
			Clear();
		} 
	}
}
