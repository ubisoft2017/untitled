﻿using System;

namespace Controls
{
	public enum DeviceJoystick : byte
	{
		Left,
		Right
	}
}