﻿using System;

namespace Controls
{
	public enum DeviceAxisProperty : byte
	{
		Position,
		Velocity
	}
}
