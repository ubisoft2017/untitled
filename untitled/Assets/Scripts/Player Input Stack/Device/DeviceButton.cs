﻿using System;

namespace Controls
{
	public enum DeviceButton : short
	{
		A,
		B,
		X,
		Y,
		LB,
		RB,
		Back,
		Start,
		LeftJoystickClick,
		RightJoystickClick,
		Left,
		Right,
		Up,
		Down
	}
}