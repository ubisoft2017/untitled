﻿using System;
using UnityEngine;

namespace Controls
{
	[Serializable]
	public struct DeviceButtonState
	{
		[SerializeField] private float holdTime, previousHoldTime;

		public float HoldTime
		{
			get
			{
				return holdTime;
			}
		}

		public bool HasBeenHeldFor( float seconds, bool repeat = true )
		{
			return holdTime > seconds && (repeat || previousHoldTime <= seconds);
		}

		[SerializeField] private bool isDown, wasJustPressed, wasJustReleased;

		public bool IsDown
		{
			get
			{
				return isDown;
			}
		}

		public bool IsUp
		{
			get
			{
				return !isDown;
			}
		}

		public bool WasJustPressed
		{
			get
			{
				return wasJustPressed;
			}
		}

		public bool WasJustReleased
		{
			get
			{
				return wasJustReleased;
			}
		}

		public DeviceButtonState ( bool isDown, DeviceButtonState previous )
		{
			this.isDown = isDown;

			holdTime = isDown && previous.isDown ? previous.holdTime + Time.unscaledDeltaTime : 0.0f;

			previousHoldTime = previous.holdTime;

			wasJustPressed = isDown && !previous.isDown;
			wasJustReleased = previous.isDown && !isDown;
		}


        public DeviceButtonState(bool isDown, bool previous)
        {
            this.isDown = isDown;

            holdTime = 0.0f;

            previousHoldTime = 0.0f;

            wasJustPressed = isDown && !previous;
            wasJustReleased = isDown && !isDown;
        }
    }
}