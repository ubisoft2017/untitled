﻿using System;
using UnityEngine;

namespace Controls
{
	[Serializable]
	public struct DeviceTriggerState
	{
		[SerializeField] private float position, velocity;

		public float Position
		{
			get
			{
				return position;
			}
		}

		public float Velocity
		{
			get
			{
				return velocity;
			}
		}

		public DeviceTriggerState ( float position, DeviceTriggerState previous )
		{
			this.position = position;

			velocity = (position - previous.position) / Time.unscaledDeltaTime;
		}
	}
}