﻿using System;
using UnityEngine;

namespace Controls
{
	[Serializable]
	public struct DeviceJoystickState
	{
		[SerializeField] private Vector2 position, velocity;
		[SerializeField] private float magnitude;
        [SerializeField] private DeviceButtonState up, down, left, right;

        public DeviceButtonState Up
        {
            get
            {
                return up;
            }
        }

        public DeviceButtonState Down
        {
            get
            {
                return down;
            }
        }

        public DeviceButtonState Left
        {
            get
            {
                return left;
            }
        }

        public DeviceButtonState Right
        {
            get
            {
                return right;
            }
        }


        public Vector2 Position
		{
			get
			{
				return position;
			}
		}

		public Vector2 Velocity
		{
			get
			{
				return velocity;
			}
		}

		public float Magnitude
		{
			get
			{
				return magnitude;
			}
		}

		public DeviceJoystickState ( Vector2 position, DeviceJoystickState previous, bool up, bool down, bool left, bool right)
		{
			this.position = position;

         
            this.up = new DeviceButtonState(up, previous.up.IsDown);
            this.down = new DeviceButtonState(down, previous.down.IsDown);
            this.left = new DeviceButtonState(left, previous.left.IsDown);
            this.right = new DeviceButtonState(right, previous.right.IsDown);

            velocity = (position - previous.position) / Time.unscaledDeltaTime;

			magnitude = position.magnitude;
		}
	}
}