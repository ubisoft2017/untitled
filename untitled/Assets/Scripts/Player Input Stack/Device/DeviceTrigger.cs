﻿using System;

namespace Controls
{
	public enum DeviceTrigger : byte
	{
		Left,
		Right
	}
}
