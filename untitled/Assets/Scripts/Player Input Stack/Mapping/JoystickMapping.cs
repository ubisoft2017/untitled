﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	public class JoystickMapping : Mapping
	{
		[SerializeField] private JoystickAction action;

		public JoystickAction Action
		{
			get
			{
				return action;
			}
		}

		[SerializeField] public DeviceJoystick mapping;

		[SerializeField] public int sensitivity;

		[SerializeField] public JoystickInversion inversion;

		public JoystickMapping ( JoystickAction action )
		{
			this.action = action;

			RestoreDefaults();
		}

		public override void RestoreDefaults ()
		{
			mapping = action.DefaultMapping;
			sensitivity = action.DefaultSensitivity;
			inversion = action.DefaultInversion;
		}

		public void CopyFrom ( JoystickMapping other )
		{
			mapping = other.mapping;
			sensitivity = other.sensitivity;
			inversion = other.inversion;
		}
	}
}
