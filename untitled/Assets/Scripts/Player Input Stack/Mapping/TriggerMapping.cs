﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	public class TriggerMapping : Mapping
	{
		[SerializeField] private TriggerAction action;

		public TriggerAction Action
		{
			get
			{
				return action;
			}
		}

		[SerializeField] public DeviceTrigger mapping;

		public TriggerMapping ( TriggerAction action )
		{
			this.action = action;

			RestoreDefaults();
		}

		public override void RestoreDefaults ()
		{
			mapping = action.DefaultMapping;
		}

		public void CopyFrom ( TriggerMapping other )
		{
			this.mapping = other.mapping;
		}
	}
}
