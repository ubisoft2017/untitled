﻿using System;

namespace Controls
{
	public enum ButtonEvent : byte
	{
		Press,
		Release,
		Hold
	}
}