﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	[Serializable]
	public abstract class Mapping
	{
		public abstract void RestoreDefaults ();
	}
}
