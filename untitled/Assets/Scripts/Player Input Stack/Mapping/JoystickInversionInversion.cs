﻿using System;

namespace Controls
{
	public enum JoystickInversion : byte
	{
		None = 0,
		X = 1,
		Y = 2
	}
}
