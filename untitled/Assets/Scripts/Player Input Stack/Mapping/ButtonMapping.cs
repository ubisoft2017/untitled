﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controls
{
	[Serializable]
	public class ButtonMapping : Mapping
	{
		[SerializeField] private ButtonAction action;

		public ButtonAction Action
		{
			get
			{
				return action;
			}
		}

		[SerializeField] public DeviceButton mapping = DeviceButton.A;

		public ButtonMapping ( ButtonAction action )
		{
			this.action = action;

			RestoreDefaults();
		}

		public override void RestoreDefaults ()
		{
			mapping = action.DefaultMapping;
		}

		public void CopyFrom ( ButtonMapping other )
		{
			this.mapping = other.mapping;
		}
	}
}
