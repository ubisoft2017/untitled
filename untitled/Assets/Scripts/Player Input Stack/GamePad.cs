﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using InControl;

namespace Controls
{
	/// <summary>
	/// Abstraction of the connected game pad.
	/// Provides convenience methods for querying the raw inputs as well as PlayerActions.
	/// 
	/// TODO: Cache button / trigger / joystick strings to reduce overhead
	/// </summary>
	public class GamePad : MonoBehaviour
	{
		public static GamePad instance { get; private set; }
        private InputDevice inputDevice;

		public static event Action<DeviceButton> OnPress;
		public static event Action<DeviceButton> OnHold;
		public static event Action<DeviceButton> OnRelease;

		public static void Clear ()
		{
			OnPress = delegate{ };
			OnHold = delegate{ };
			OnRelease = delegate { };
		}

		[SerializeField][HideInInspector] private DeviceButtonState[] buttons;

		/// <summary>
		/// Get the state of a button.
		/// </summary>
		/// <param name="button">Button.</param>
		/// <returns>Button's state.</returns>
		public static DeviceButtonState Query ( DeviceButton button )
		{
			return instance.buttons[(int) button];
		}
		
		[SerializeField][HideInInspector] private DeviceTriggerState[] triggers;

		/// <summary>
		/// Get the state of a trigger.
		/// </summary>
		/// <param name="trigger">Trigger.</param>
		/// <returns>Trigger's state.</returns>
		public static DeviceTriggerState Query ( DeviceTrigger trigger )
		{
			return instance.triggers[(int) trigger];
		}

		[SerializeField][HideInInspector] private DeviceJoystickState[] joysticks;

		/// <summary>
		/// Get the state of a joystick.
		/// </summary>
		/// <param name="joystick">Joystick.</param>
		/// <returns>Joystick's state.</returns>
		public static DeviceJoystickState Query ( DeviceJoystick joystick )
		{
			return instance.joysticks[(int) joystick];
		}

		/// <summary>
		/// Check the game pad's current state against a ButtonAction.
		/// </summary>
		/// <param name="action">The action determines whether to check for a press, release, or hold.</param>
		/// <param name="profile">The PlayerProfile determines which button is mapped to the action.</param>
		/// <returns>True, if the game pad's state expresses the action.</returns>
		public static bool Query ( ButtonAction action, PlayerProfile profile )
		{
			switch ( action.ButtonEvent )
			{
			case ButtonEvent.Press:
				return instance.buttons[(int) profile[action].mapping].WasJustPressed;

			case ButtonEvent.Release:
				return instance.buttons[(int) profile[action].mapping].WasJustReleased;

			case ButtonEvent.Hold:
				return instance.buttons[(int) profile[action].mapping].HasBeenHeldFor( action.HoldTime, action.Repeat );
			}

			return false;
		}

		/// <summary>
		/// Check the game pad's current state against a ButtonAction.
		/// </summary>
		/// <param name="action">The action determines whether to check for a press, release, or hold.</param>
		/// <param name="profile">The PlayerProfile determines which button is mapped to the action.</param>
		/// <param name="state">The current state of the player.</param>
		/// <returns>True, if the game pad's state expresses the action.</returns>
		public static bool Query ( ButtonAction action, PlayerProfile profile, PlayerState state )
		{
			if ( state.Implements( action ) )
			{
				return Query( action, profile );
			}

			return false;
		}

		/// <summary>
		/// Check the state of a trigger mapped to a TriggerAction.
		/// </summary>
		/// <param name="action">The action determines whether to read the trigger's position or velocity.</param>
		/// <param name="profile">The PlayerProfile determines which trigger to read.</param>
		/// <returns>The appropriate trigger property.</returns>
		public static float Query ( TriggerAction action, PlayerProfile profile )
		{
			switch ( action.Property )
			{
			case DeviceAxisProperty.Position:
				return instance.triggers[(int) profile[action].mapping].Position;

			case DeviceAxisProperty.Velocity:
				return instance.triggers[(int) profile[action].mapping].Velocity;
			}

			return 0.0f;
		}

		/// <summary>
		/// Check the game pad's current state against a TriggerAction.
		/// </summary>
		/// <param name="action">The action determines whether to read the trigger's position or velocity.</param>
		/// <param name="profile">The PlayerProfile determines which button is mapped to the action.</param>
		/// <param name="state">The current state of the player.</param>
		/// <returns>True, if the game pad's state expresses the action.</returns>
		public static float Query ( TriggerAction action, PlayerProfile profile, PlayerState state )
		{
			if ( state.Implements( action ) )
			{
				return Query( action, profile );
			}

			return 0.0f;
		}

		/// <summary>
		/// Check the state of a joystick mapped to a JoystickAction. Handles enforcement of sensitivity and axis inversions.
		/// </summary>
		/// <param name="action">The action determines whether to read the joystick's position or velocity.</param>
		/// <param name="profile">The PlayerProfile determines which joystick to read.</param>
		/// <returns>The appropriate joystick property.</returns>
		public static Vector2 Query ( JoystickAction action, PlayerProfile profile )
		{
			Vector2 property;

			switch ( action.Property )
			{
			case DeviceAxisProperty.Position:
				property = instance.joysticks[(int) profile[action].mapping].Position;
				break;

			case DeviceAxisProperty.Velocity:
				property = instance.joysticks[(int) profile[action].mapping].Velocity;
				break;

			default:
				return Vector2.zero;
			}

			property = new Vector2
			(
				property.x * ((profile[action].inversion & JoystickInversion.X) == JoystickInversion.None ? 1.0f : -1.0f ),
				property.y * ((profile[action].inversion & JoystickInversion.Y) == JoystickInversion.None ? 1.0f : -1.0f)
			);

			return property * (1.0f + Mathf.Log10( profile[action].sensitivity ) );
		}

		/// <summary>
		/// Check the game pad's current state against a JoystickAction.
		/// </summary>
		/// <param name="action">The action determines whether to read the trigger's position or velocity.</param>
		/// <param name="profile">The PlayerProfile determines which button is mapped to the action.</param>
		/// <param name="state">The current state of the player.</param>
		/// <returns>True, if the game pad's state expresses the action.</returns>
		public static Vector2 Query ( JoystickAction action, PlayerProfile profile, PlayerState state )
		{
			if ( state.Implements( action ) )
			{
				return Query( action, profile );
			}

			return Vector2.zero;
		}

		private void Awake ()
		{
			if ( instance != null )
			{
				Destroy( this );

				return;
			}

			DontDestroyOnLoad( this );
		} 

		private void OnEnable ()
		{
			buttons = new DeviceButtonState[Enum.GetValues( typeof( DeviceButton ) ).Length];
			triggers = new DeviceTriggerState[Enum.GetValues( typeof( DeviceTrigger ) ).Length];
			joysticks = new DeviceJoystickState[Enum.GetValues( typeof( DeviceJoystick ) ).Length];

            inputDevice = InputManager.ActiveDevice;

            instance = this;
		}

		//Everything here and down (for switching to InControl / ReWired)

		private static bool pressed, horizontal;
		private static int i;
		private static string str;
		private static float triggerPositon, dPadX, dPadY, dPadXAbs, dPadYAbs;
		private static Vector2 joystickPosition;

		private void Update ()
		{
			ReadStates();
        }

		private void OnGUI ()
		{
			ReadStates();
		}


		public void ReadStates ()
		{
			for ( i = 0; i < buttons.Length; i++ )
			{
				ReadButtonState( (DeviceButton) i );
			}

			for ( i = 0; i < triggers.Length; i++ )
			{
				ReadTriggerState( (DeviceTrigger) i );
			}

			for ( i = 0; i < joysticks.Length; i++ )
			{
				ReadJoystickState( (DeviceJoystick) i );
			}
		}

		private void ReadButtonState ( DeviceButton button )
		{
			i = (int) button;

            bool buttonValue = false;

            InputDevice inputDevice = InputManager.ActiveDevice;

            switch (button)
            {
            case DeviceButton.A:
                buttonValue = inputDevice.Action1.IsPressed;
                break;
            case DeviceButton.B:
                buttonValue = inputDevice.Action2.IsPressed;
                break;
            case DeviceButton.Y:
                buttonValue = inputDevice.Action4.IsPressed;
                break;
            case DeviceButton.X:
                buttonValue = inputDevice.Action3.IsPressed;
                break;
            case DeviceButton.LB:
                buttonValue = inputDevice.LeftBumper.IsPressed;
                break;
            case DeviceButton.RB:
                buttonValue = inputDevice.RightBumper.IsPressed;
                break;
            case DeviceButton.Back:
                buttonValue = inputDevice.GetControl(InputControlType.Back).IsPressed;
                break;
            case DeviceButton.Start:
                buttonValue = inputDevice.GetControl(InputControlType.Start).IsPressed;
                break;
            case DeviceButton.LeftJoystickClick:
                buttonValue = inputDevice.LeftStickButton.IsPressed;
                break;
            case DeviceButton.RightJoystickClick:
                buttonValue = inputDevice.RightStickButton.IsPressed;
                break;
            case DeviceButton.Left:
                buttonValue = inputDevice.DPadLeft.IsPressed;
                break;
            case DeviceButton.Right:
                buttonValue = inputDevice.DPadRight.IsPressed;
                break;
            case DeviceButton.Up:
                buttonValue = inputDevice.DPadUp.IsPressed;
                break;
            case DeviceButton.Down:
                buttonValue = inputDevice.DPadDown.IsPressed;
                break;
			default:
				return;
            }

            buttons[i] = new DeviceButtonState(buttonValue, buttons[i] );

			if ( buttons[i].WasJustPressed && OnPress != null )
			{
				OnPress( button );
			}

			if ( buttons[i].HoldTime > 0.0f && OnHold != null )
			{
				OnHold( button );
			}

			if ( buttons[i].WasJustReleased && OnRelease != null )
			{
				OnRelease( button );
			}
		}

		private void ReadTriggerState ( DeviceTrigger trigger )
		{
			i = (int) trigger;
    
            InputDevice inputDevice = InputManager.ActiveDevice;

            float triggerValue = 0f;

            switch (trigger)
            {
                case DeviceTrigger.Left:
                    triggerValue = inputDevice.LeftTrigger.Value;
                    break;
                case DeviceTrigger.Right:
                    triggerValue = inputDevice.RightTrigger.Value;
                    break;
            }

            triggers[i] = new DeviceTriggerState(triggerValue, triggers[i] );
		}

		private void ReadJoystickState ( DeviceJoystick joystick )
		{
			i = (int) joystick;

            InputDevice inputDevice = InputManager.ActiveDevice;

            TwoAxisInputControl inControlJoystick = joystick == DeviceJoystick.Left ? inputDevice.LeftStick : inputDevice.RightStick;

            joysticks[i] = new DeviceJoystickState(inControlJoystick.Vector, joysticks[i], inControlJoystick.Up.IsPressed, inControlJoystick.Down.IsPressed, inControlJoystick.Left.IsPressed, inControlJoystick.Right.IsPressed);
		}
	}
}
