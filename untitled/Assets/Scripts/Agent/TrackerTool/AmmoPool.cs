﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPool : MonoBehaviour {

    public int size;
    public TrackingChip chip;
	public GameObject parent;
    TrackingChip[] pool;
    int index = 0;

	private void Awake ()
	{
		parent = GameObject.FindWithTag( "Interior Parent" );
	}

	 void Start () {

        if (size > 0)
        {
            pool = new TrackingChip[size];

            for (int i = 0; i < pool.Length; i++)
            {
				pool[i] = (TrackingChip) Instantiate( chip, transform.position - Vector3.down * 100, Quaternion.identity, parent.transform );
                pool[i].GetComponent<Rigidbody>().useGravity = false;
                pool[i].setSFX(GetComponent<SFX>());
            }
        }
	}

    public TrackingChip getNext() {
        if (index >= size)
            index = 0;
        if (size == -1)
            return (TrackingChip)Instantiate(chip, transform.position, Quaternion.identity);
        else {
            TrackingChip temp = pool[index++];
			temp.transform.parent = Phase.Active.transform;

			temp.gameObject.SetActive(true);
            temp.reset();
            return temp;
        }
            

    }
}
