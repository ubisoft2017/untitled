﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardTaggedEffect : MonoBehaviour {
    
    Guard guard;
    public Color endColor;
    public float boostEnd, ClampEnd;
    private Color startColor;
    private Color rimColor;
    private float rimBoost, rimClamp;
    [SerializeField]
    private AnimationCurve colorChangeSpeed;
    private bool doneAnimation = false;
    private bool wasHit = false;
    public float scaleX, scaleY;
    private float xPosition = 0;
    void Start() {

        guard = GetComponent<Guard>();
        startColor = guard.Material.GetColor("_Color");
   //     rimColor = guard.Material.GetColor("_RimColor");
   //     rimBoost = guard.Material.GetFloat("_RimThresh");
    //    rimClamp = guard.Material.GetFloat("_RimClamp");


    }
    void Update() {
        if (wasHit && !doneAnimation) {
            animate();
        }
    }
    public void hit() {
        wasHit = true;
        doneAnimation = false;
        xPosition = 0;
        
    }

    private void animate()
    {
        if (xPosition < scaleX)
        {
            float yValue = colorChangeSpeed.Evaluate(xPosition / scaleX) * scaleY;
            float difference = scaleY - yValue;
            Color c = (startColor * (difference) + endColor * (yValue)) / scaleY;
            Color c2 = (rimColor * (difference) + endColor * (yValue)) / scaleY;
            float boost = (rimBoost * (difference) + boostEnd * (yValue)) / scaleY;
            float clamp = (rimClamp * (difference) + rimClamp * (yValue)) / scaleY;

            guard.Material.SetColor("_Color", c);
       //     guard.Material.SetColor("_RimColor", c2);
       //     guard.Material.SetFloat("_RimThresh", boost);
      //      guard.Material.SetFloat("_RimClamp", clamp);



            xPosition += Time.deltaTime;
        }
        else
        {
            doneAnimation = true;
            guard.Material.SetColor("_Color", startColor);
          //  guard.Material.SetColor("_RimColor", rimColor);
         //   guard.Material.SetFloat("_RimThresh", rimBoost);
        //    guard.Material.SetFloat("_RimClamp", rimClamp);

        }

    }
}
