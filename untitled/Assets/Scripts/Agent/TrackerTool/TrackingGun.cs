﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tweening;

public class TrackingGun : MonoBehaviour
{
	[SerializeField]
	private Animator animator;
    
	[SerializeField]
	private string aimBool = "aim";

	[SerializeField]
	private Transform firePosition;

	[SerializeField]
	[Tooltip("The tracker that sticks onto guards")]
	private TrackingChip chip;

	[SerializeField]
	[Tooltip("Velocity of tracking chip once fired")]
	private float fireVelocity;

	[SerializeField]
	[Tooltip("Rate at which we can fire")]
	private float fireRate;

	[SerializeField]
	[Tooltip("Ammo count, -1 for unlimited")]
	private int ammo;
    private Recoil recoil;

	[EnumObject(typeof(TweenEvent), "Tweening/Events")]
	[SerializeField]
	private TweenEvent crossIn;

	[EnumObject(typeof(TweenEvent), "Tweening/Events")]
	[SerializeField]
	private TweenEvent crossOut;

	public Transform eye;
	private bool aiming;
	private bool canFire;
	private bool hasFired;
    public AmmoPool ammoPool;
    [SerializeField]
    public LayerMask layerMask;
    // public int layerMask = (1 << 9);// (1 << 8) | (1 << 9) | (1 << 11)| (1 << 16);
	Ray ray;
	RaycastHit hit;
    bool loaded = true;
    [SerializeField]
    SFX sfx;
    [SerializeField]
    private AudioClip fireClip;
    [SerializeField]
    private float volume = 0.8f;
    void Awake() {
        recoil = GetComponent<Recoil>();
        recoil.setFireRate(fireRate);
    }
	void Start ()
	{

	}

	void Update ()
	{
	}

	public void shootTracker ()
	{
		if ( loaded && !hasFired && canFire && (ammo > 0 || ammo == -1) )
		{
			ray.origin = eye.position;
			ray.direction = eye.forward;

			Vector3 direction;

			if ( Physics.Raycast( ray.origin,ray.direction, out hit, Mathf.Infinity, layerMask))
			{
				direction = (hit.point - firePosition.position).normalized;
                Debug.Log("Shooting at: " +hit.transform.tag+" Layer: " + hit.transform.gameObject.layer);
                

			}
			else
			{
				direction = eye.forward;
			}

			hasFired = true;
            loaded = false;
            if (sfx != null)
            {
                sfx.PlayClip(fireClip, volume, true);
            }
            recoil.recoil();
            TrackingChip tempChip = ammoPool.getNext();
            tempChip.transform.position = firePosition.transform.position;
            tempChip.transform.rotation = Quaternion.identity;
            //TrackingChip tempChip = (TrackingChip) Instantiate( chip, firePosition.position, Quaternion.identity );
            //tempChip.GetComponent<Rigidbody>().velocity = direction * fireVelocity;
            tempChip.fire(direction * fireVelocity);
            if(ammo>0)
                ammo--;
		}
	}

	

	public void isAiming ()
	{
#if UNITY_EDITOR
        Debug.DrawRay(eye.position, eye.forward*25, Color.cyan);
#endif
        if ( !aiming )
		{
			crossIn.Invoke();
			aiming = true;

			animator.SetBool( aimBool, true );

			// can fire after animation is done
			canFire = true;
		}
	}

	public void isNotAiming ()
	{
		if ( aiming )
		{
			crossOut.Invoke();

			canFire = false;
			aiming = false;

			animator.SetBool( aimBool, false );
		}
	}
    public void resetGun() {
        hasFired = false;
    }
    public void reloaded() {
        loaded = true;
    }
    
}
