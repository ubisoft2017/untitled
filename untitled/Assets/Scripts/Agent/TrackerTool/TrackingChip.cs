﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrackingChip : MonoBehaviour {

    [Tooltip("How quickly tail fades after fade initiated")]
    public float trailFade;
    [Tooltip("At what velocity will initiade tail fade")]
    public float fadeVelocity;
    [Tooltip("How bouncy it is  (0-1)")]
    [Range(0, 1)]
    public float bounceFactor;

    [SerializeField]
    SFX sfx;

    [SerializeField]
    private AudioClip wallHit;

    private bool stuck;
    private bool hasBeenFired;
    public float trailTimeStart;

    public float setInactiveVelThreshold;
    public float vanishTime;
    float vanishCounter = 0;
    private Vector3 previousVel = Vector3.zero;

    //refrerences so we dont use getComponent
    private Rigidbody rb;
    private TrailRenderer trail;
    private Collider chipCollider;
    private bool startVanish;
    float fireVelMagnitute;
    void Start() {
        rb = GetComponent<Rigidbody>();
        trail = GetComponentInChildren<TrailRenderer>();
        chipCollider = GetComponent<Collider>();
        trailTimeStart = trail.time;
        reset();
        gameObject.SetActive(false);

    }
    void Update() {

        if (trail.enabled )
        {
            if (stuck || rb.velocity.magnitude < fadeVelocity)
            {
                trail.time -= trailFade;
                if (trail.time <= 0)
                    trail.enabled = false;
            }
        }
        if (!stuck && !startVanish) {
            if (rb.velocity.magnitude < setInactiveVelThreshold)
                startVanish = true;
                vanishCounter = 0;
        }
        if (startVanish) {
            vanishCounter += Time.deltaTime;
            if (vanishCounter >= vanishTime)
                disable(); 

        }

#if UNITY_EDITOR
        if (rb!=null)
        Debug.DrawRay(transform.position, rb.velocity, Color.blue);
#endif

    }

    void FixedUpdate()
    {
        if(rb!=null)
        previousVel = rb.velocity;
    }
    public void reset() {
        hasBeenFired = true;
        stuck = false;
        trail.time = trailTimeStart;
        trail.Clear();
        trail.enabled = true;
		GetComponent<PhaseMember>().Localize();
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>() as Rigidbody;
        }
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.interpolation = RigidbodyInterpolation.Interpolate;
        chipCollider.enabled = true;
        rb.useGravity = false;
        rb.isKinematic = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        startVanish = false;
      
    }

    void OnCollisionEnter(Collision col) {
 
        if (!stuck && hasBeenFired)
        {
            if (col.transform.tag == "GuardCollider")
            {
                transform.SetParent(col.transform.parent.transform);
                transform.position = col.contacts[0].point;
                transform.rotation = Quaternion.LookRotation(col.contacts[0].normal);
                Destroy(rb);
                chipCollider.enabled = false;
                stuck = true;
                // trail.enabled = false;
                trail.time /= 2;
                Guard guard = col.transform.root.GetComponent<Guard>();
                AgentToHacker.NotifyTagGuard(guard);
                guard.GetComponent<GuardTaggedEffect>().hit();
            }
            else {
                rb.useGravity = true;
                Vector3 norm = col.contacts[0].normal;
                rb.velocity = Vector3.Reflect(previousVel*bounceFactor,norm);
                float mag = rb.velocity.magnitude;
                if (sfx!=null && mag > setInactiveVelThreshold) {
                    sfx.PlayClip(wallHit, (mag / (fireVelMagnitute * 3)), false);
                 }
                
#if UNITY_EDITOR

                Debug.DrawRay(col.contacts[0].point, norm);
#endif
            }


        }
    }

    public void fire(Vector3 vel) {
        rb.velocity = vel;
        fireVelMagnitute = vel.magnitude;
    }
    public void setSFX(SFX x) {
        sfx = x;
    }
    public void disable() {

        gameObject.SetActive(false);
    }



}
