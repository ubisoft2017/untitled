﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controls;

public class TrackingGunController : ControllerBase
{
    [EnumObject(typeof(TriggerAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private TriggerAction aimAction;

    [EnumObject(typeof(TriggerAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private TriggerAction fireAction;
    public float triggerThreshold;


    [SerializeField]
    private TrackingGun trackingGun;

    protected override void ControllerStart()
    {
        base.ControllerStart();
        if (aimAction == null || fireAction == null)
        {
            Debug.LogError("TrackingGunController is missing one or more actions.");
            Debug.Break();
        }
    }
    protected override void ControllerUpdate()
    {
        base.ControllerUpdate();

		if (GamePad.Query(aimAction, Player.Profile, Player.State) > triggerThreshold)
		{
			trackingGun.isAiming();
		}
		else
		{
			trackingGun.isNotAiming();
		}

		if (GamePad.Query(fireAction, Player.Profile, Player.State) > triggerThreshold)
		{
			trackingGun.shootTracker();
		}
		else
		{
			trackingGun.resetGun();
		}
    }
}
