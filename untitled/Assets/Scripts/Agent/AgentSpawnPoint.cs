﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to indicate the initial position and orientation of the Agent upon entering a level.
/// </summary>
public class AgentSpawnPoint : MonoBehaviour
{
    public Transform player;
    void Awake() {

        Debug.Log("Player CREATED");
        Transform p = Instantiate(player, transform.position,transform.rotation);
        p.transform.parent = transform;
        p.transform.parent = null;
        Destroy(this.gameObject);
    }
#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		Gizmos.DrawLine( transform.position + transform.forward, transform.position + transform.right );
		Gizmos.DrawLine( transform.position + transform.forward, transform.position - transform.right );
		Gizmos.matrix = Matrix4x4.TRS( transform.position, Quaternion.LookRotation( Vector3.up, -transform.forward ), Vector3.one );
		Gizmos.DrawFrustum( Vector3.zero, 20.0f, 2.0f, 0.0f, 1.0f );
		Gizmos.matrix = Matrix4x4.identity;

	}
#endif
}
