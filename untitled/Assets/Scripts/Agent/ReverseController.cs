﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( LookController ) )]
public class ReverseController : ControllerBase
{
	[EnumObject( typeof( ButtonAction ), PlayerAction.RESOURCE_PATH )]
	[SerializeField] private ButtonAction reverseToRightAction;

	[EnumObject( typeof( ButtonAction ), PlayerAction.RESOURCE_PATH )]
	[SerializeField] private ButtonAction reverseToLeftAction;

	[Tooltip( "degrees / second" )]
	[SerializeField][Range( 0.0f, 1080.0f )] private float speed = 180.0f;

	[SerializeField][HideInInspector] private LookController look;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		look = GetComponent<LookController>();

		if ( reverseToRightAction == null || reverseToLeftAction == null )
		{
			Debug.LogError( "ReverseController is missing one or more actions." );

			Debug.Break();
		}
	}

	private int queue;

	private float debt = 0.0f;

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		float abs = Mathf.Abs( debt );
		float sign = Mathf.Sign( debt );

		if ( GamePad.Query( reverseToRightAction, Player.Profile, Player.State ) )
		{
			debt -= 180.0f;
		}

		if ( GamePad.Query( reverseToLeftAction, Player.Profile, Player.State ) )
		{
			debt += 180.0f;
		}

		if ( abs > 0.0f )
		{
			float budget = speed * Time.deltaTime;

			float actual = sign * Mathf.Min( budget, abs );

            look.AddToYaw(actual);

			debt -= actual;
		}
	}
}
