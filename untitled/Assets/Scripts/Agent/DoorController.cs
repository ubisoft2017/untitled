﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( InteractionRaycaster ) )]
public class DoorController : InteractionController<AgentDoorInteraction>
{
	[EnumObject( typeof( ButtonAction ), PlayerAction.RESOURCE_PATH )]
	[SerializeField] private ButtonAction doorAction;

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		if ( HasResult )
		{
			if ( GamePad.Query( doorAction, Player.Profile, Player.State ) )
			{
				Result.Interact();
			}
		}
	}
}
