﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( InteractionRaycaster ), typeof( Backpack ) )]
public class ValuableController : InteractionController<Valuable>
{
	[SerializeField][HideInInspector] private Backpack backpack;

	[SerializeField] private Animator animator;
	[SerializeField] private string trigger = "swipe";
	[SerializeField] private SFX sfx;
	[SerializeField] private AudioClip swipeClip;

	protected override void ControllerAwake ()
	{
		base.ControllerAwake();
	}

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		backpack = GetComponent<Backpack>();
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		if ( HasResult )
		{
			if ( GamePad.Query( Result.GetAction(), Player.Profile, Player.State ) )
			{
				if ( Result.TrySteal( backpack ) )
				{
					animator.SetTrigger( trigger );

					sfx.PlayClip( swipeClip, 1.0f, true );

					Debug.Log( "You swiped " + Result.Type.Determiner.ToString().ToLower() + " " + Result.Type.Label + "!" );
				}
			}
		}
	}
}
