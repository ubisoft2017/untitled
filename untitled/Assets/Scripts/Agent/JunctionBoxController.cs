﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent(typeof(InteractionRaycaster))]
public class JunctionBoxController : InteractionController<JunctionBoxAgent>
{

    private JunctionBoxAgent currentBox;

    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction interactAction;


    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction upAction;

    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction downAction;

    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction leftAction;

    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction rightAction;


    [EnumObject(typeof(PlayerState), PlayerState.RESOURCE_PATH)]
    [SerializeField]
    private PlayerState junctionState;

    [SerializeField]
    private Animator agentAnimator;

    [SerializeField]
    private string placeTrigger;

    private PlayerState initialState;

    private CameraTransform initialCameraTransform;

    private bool moveCamera = false;
    private float lerpValue = 0f;

    [Range(0.0f, 5.0f)]
    public float blendSpeed = 1f;

    [Range(0.0f, 5.0f)]
    public float secondBlendSpeed = 1f;

    [SerializeField]
    private CameraBlend blend;

    protected override void ControllerUpdate()
    {
        base.ControllerUpdate();

        bool jcBoxCond = currentBox == null ? true : currentBox.opened;


        if (moveCamera && jcBoxCond)
        {
            lerpValue += Time.deltaTime * blendSpeed;

            if (lerpValue > 1f)
            {
                lerpValue = 1f;
                moveCamera = false;
                if (currentBox)
                {
                    if (blend.LHS == initialCameraTransform)
                    {
                        Invoke("PlaceUplink", 0.6f);
                        agentAnimator.SetTrigger(placeTrigger);

                    }
                    else
                    {
                        blend.LHS = initialCameraTransform;
                    }
                }
            }

            if (lerpValue < 0f)
            {
                lerpValue = 0f;
                moveCamera = false;
            }

            blend.blend = lerpValue;
        }

        if (HasResult)
        {
            if (GamePad.Query(Result.GetAction(), Player.Profile, Player.State) && !Result.isActive())
            {
                CameraTransform to = Result.Activate();

                currentBox = Result;

                initialCameraTransform = blend.LHS as CameraTransform;

                blend.RHS = to;

                moveCamera = true;
                blendSpeed = Mathf.Abs(blendSpeed);

                initialState = Player.State;
                Player.State = junctionState;


            }
        }


        if (currentBox && currentBox.isActive())
        {
            if (GamePad.Query(currentBox.GetAction(), Player.Profile, Player.State))
            {
                if (currentBox.Interact())
                {
                    Reset();
                }
            }

            //print("HHA " + GamePad.Query(DeviceJoystick.Left).Velocity);

            DeviceJoystickState left = GamePad.Query(DeviceJoystick.Left);

            if (Player.State == junctionState)
            {

                if (GamePad.Query(upAction, Player.Profile, Player.State) || (left.Position.y > 0f && left.Velocity.y > 20f))
                {
                    currentBox.Select(new Vector2(0, 1));
                }
                else if (GamePad.Query(downAction, Player.Profile, Player.State) || (left.Position.y < 0f && left.Velocity.y < -20f))
                {
                    currentBox.Select(new Vector2(0, -1));
                }
                else if (GamePad.Query(leftAction, Player.Profile, Player.State) || left.Left.WasJustPressed)
                {
                    currentBox.Select(new Vector2(-1, 0));
                }
                if (GamePad.Query(rightAction, Player.Profile, Player.State) || left.Right.WasJustPressed)
                {
                    currentBox.Select(new Vector2(1, 0));
                }
            }
        }
    }

    private void Reset()
    {
        moveCamera = true;

        blendSpeed = -Mathf.Abs(blendSpeed);

        currentBox = null;

        Player.State = initialState;
    }

    private void PlaceUplink()
    {
        if (currentBox)
        {
            blend.blend = 0f;

            blend.LHS = blend.RHS;

            CameraTransform to = currentBox.LoadUplink();

            blend.RHS = to;

            lerpValue = 0f;
            blendSpeed = secondBlendSpeed;


            moveCamera = true;
        }
    }
}
