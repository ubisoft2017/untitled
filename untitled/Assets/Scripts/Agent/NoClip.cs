﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoClip : MonoBehaviour {

    bool isNoclip;

    void Awake() {
            DevTools.Register("Noclip", () => isNoclip, Set, KeyCode.N);
    }

    void Set( bool noclip )
    {
        isNoclip = noclip;

        if ( isNoclip )
        {
            gameObject.layer = LayerMask.NameToLayer("NoClip");
        }
        else
        {
            gameObject.layer = LayerMask.NameToLayer("Navigation");
       }
    }
}
