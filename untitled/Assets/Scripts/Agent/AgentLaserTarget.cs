﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentLaserTarget : MonoBehaviour, ILaserTarget
{
	[SerializeField] private GameObject overlayPrefab;

	[SerializeField][HideInInspector] private Image overlay;

	[SerializeField] private float speed;
	[SerializeField] private float gravity;

	private float alpha;
	private bool wait;
	private GameOverSystem gameOverSystem;

	private void Start ()
	{
		var canvas = GameObject.FindWithTag( "Overlay Canvas" );
		gameOverSystem = GameObject.FindWithTag( "Game Over System" ).GetComponent<GameOverSystem>();
		if ( canvas != null )
		{
			overlay = Instantiate( overlayPrefab, canvas.transform, false ).GetComponent<Image>();
		}
	}

	public void OnHit ()
	{
		//alpha = Mathf.Clamp01( alpha + speed * Time.deltaTime );
		//wait = true;
		//awarenessSystem.DetectedByLaser();

	}

	private void Update ()
	{
		if ( !wait )
		{
			alpha = Mathf.Clamp01( alpha - gravity * Time.deltaTime );
		}

		wait = false;

		if ( overlay != null )
		{
			overlay.color = new Color( 1, 1, 1, alpha );
		}
	}

	private void ShowFX ()
	{
		alpha = Mathf.Clamp01( alpha + speed * Time.deltaTime );
		wait = true;
		gameOverSystem.DetectedByLaser();
	}

	void OnTriggerStay(Collider coll )
	{
		ShowFX();
	}

	void OnTriggerEnter(Collider coll )
	{
		ShowFX();
	}
}
