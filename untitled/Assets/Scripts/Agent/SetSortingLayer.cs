﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSortingLayer : MonoBehaviour {

    [SerializeField]
    string sortLayer;

    [SerializeField]
    int sortOrder;

    [SerializeField]
    Renderer render;
    void Start () {
        Debug.Log("Sorting order changed from " + render.sortingOrder + " to " + sortOrder);
        render.sortingLayerName = sortLayer;
        render.sortingOrder = sortOrder;
	}
	
}
