﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

/// <summary>
/// Models a collection of item type and quantity pairs.
/// </summary>
public partial class Backpack : MonoBehaviour
{
	private List<IContent> items;

	private void Awake ()
	{
		items = new List<IContent>();
	}

	/// <summary>
	/// Add contents to this Backpack.
	/// </summary>
	/// <param name="type">Item type.</param>
	/// <param name="count">Item quantity.</param>
	public void Add ( ValuableType type, int count )
	{
		if ( count < 1 )
		{
			throw new InvalidOperationException( "Contents cannoted be added with count less than 1." );
		}

		for ( int i = 0; i < items.Count; i++ )
		{
			if ( items[i].Type == type )
			{
				items[i] = new Content( type, items[i].Count + count );

				return;
			}
		}

		items.Add( new Content( type, count ) );
	}

	/// <summary>
	/// A read-only collection of the contents (item types and quantities) of this Backpack.
	/// </summary>
	public ReadOnlyCollection<IContent> Contents
	{
		get
		{
			return items.AsReadOnly();
		}
	}

	/// <summary>
	/// Clear all of the items from the Backpack.
	/// </summary>
	public void Empty ()
	{
		items.Clear();
	}
}
