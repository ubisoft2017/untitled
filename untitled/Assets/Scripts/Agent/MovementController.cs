﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
#endif

using Controls;

[RequireComponent( typeof( CharacterController ), typeof( Eye ) )]
public class MovementController : ControllerBase
{
	[EnumObject( typeof( JoystickAction ), PlayerAction.RESOURCE_PATH )]
	[SerializeField] private JoystickAction moveAction;

	[Tooltip( "meters / second" )]
	[SerializeField] private float speed = 1.0f;

	[SerializeField][HideInInspector] private CharacterController character;
	[SerializeField][HideInInspector] private Eye eye;

#if UNITY_EDITOR // Allow keyboard input when running in the IDE
	[SerializeField][HideInInspector] private bool keys;
#endif

	protected override void ControllerAwake ()
	{
		base.ControllerAwake();

#if UNITY_EDITOR // Allow keyboard input when running in the IDE
		DevTools.Register( "Keyboard Input", () => keys, ( k ) => keys = k, KeyCode.K );
#endif
	}

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		// Initialize dependencies

		character = GetComponent<CharacterController>();
		eye = GetComponent<Eye>();
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		// Compute the movement vector for this frame based on the input device state
		Vector2 movement = GamePad.Query( moveAction, Player.Profile, Player.State );

#if UNITY_EDITOR // Allow keyboard input when running in the IDE
		if ( keys )
		{
			movement += Vector2.right * Input.GetAxis( "Horizontal" );
			movement += Vector2.up * Input.GetAxis( "Vertical" );
		}
#endif
		// Apply speed coefficient
		movement *= speed;

		// Apply frame-rate correction factor
		movement *= Time.deltaTime;

		// Compute the world-space heading
		Quaternion direction = Quaternion.LookRotation( Vector3.Cross( eye.Right, Vector3.up ), Vector3.up );

		// Apply the movement vector, oriented along the world-space heading
		character.Move( direction * new Vector3( movement.x, 0.0f, movement.y ) );
	}
}
