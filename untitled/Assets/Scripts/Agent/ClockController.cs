﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( InteractionRaycaster ) )]
public class ClockController : InteractionController<Clock>
{
	[EnumObject( typeof( ButtonAction ), PlayerAction.RESOURCE_PATH )]
	
	[SerializeField] private Animator animator;
	[SerializeField] private string trigger = "touch";

	protected override void ControllerAwake ()
	{
		base.ControllerAwake();
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		if ( canNewlyInteract )
		{
			Register( Result.GetAction(), () =>
			{
				if ( Result.TryVisit() )
				{
					animator.SetTrigger( trigger );

					Debug.Log( "You have just visited a new phase!" );
				}
			} );
		}
		else if ( canNoLongerInteract )
		{
			Clear();
		}
	}
}
