﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Controls;

public class WatchController : ControllerBase
{
	[SerializeField]
	private Animator animator;

	[EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction lookAtWatchAction;

	[SerializeField]
	private string lookAtWatchBool = "watch";

    protected override void ControllerStart()
    {
        base.ControllerStart();

        if (lookAtWatchAction == null)
        {
            Debug.LogError( "WatchController is missing one or more actions." );
            Debug.Break();
        }
    }
    protected override void ControllerUpdate()
    {
        base.ControllerUpdate();

		animator.SetBool( lookAtWatchBool, GamePad.Query( lookAtWatchAction, Player.Profile, Player.State ) );
    }
}
