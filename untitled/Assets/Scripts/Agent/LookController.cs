﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Controls;
using Tweening;

/// <summary>
/// based off: http://wiki.unity3d.com/index.php/SmoothMouseLook
/// </summary>
[RequireComponent(typeof(Eye))]
public class LookController : ControllerBase, IInterpolator
{
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }

    [EnumObject(typeof(JoystickAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private JoystickAction lookAction;

    [SerializeField]
    [HideInInspector]
    private Eye eye;

    [SerializeField]
    private float sensitivityXLook = 90F;
    [SerializeField]
    private float sensitivityYLook = 90F;

	[SerializeField]
    private float sensitivityXAim = 90F;
    [SerializeField]
    private float sensitivityYAim = 90F;

	private float sensitivityX = 90F;
	private float sensitivityY = 90F;

	[SerializeField]
    private float minimumY = -60F;
    [SerializeField]
    private float maximumY = 60F;

	[SerializeField]
	private TriggerAction aimAction;

	[SerializeField]
	private PlayerState noControls;

	[SerializeField]
	private float rotateSpeed;

	float yaw = 0f;
    float pitch = 0f;

    private Vector3 lookDir = Vector3.forward;
    private Vector3 up = Vector3.up;
    private Vector3 right;
	private float time =0.0f;
	private float dcx = 1.0f, dcy = 1.0f;

	private Quaternion lookAtGuard;
	private Quaternion startRotation;
	private bool rotateTowardsGuard;
	

#if UNITY_EDITOR // Allow mouse input when running in the IDE
	[SerializeField]
    [HideInInspector]
    private bool mouse;
#endif

    protected override void ControllerAwake()
    {
		base.ControllerAwake();

#if UNITY_EDITOR// Allow mouse input when running in the IDE
		DevTools.Register("Mouse Input", () => mouse, (m) => mouse = m, KeyCode.M);
        DevTools.Register("Look Sensitivity Adjustment (X)", () => dcx, (x) => dcx = x, 0.5f, 2.0f );
        DevTools.Register("Look Sensitivity Adjustment (Y)", () => dcy, (y) => dcy = y, 0.5f, 2.0f );
#endif
	}

    protected override void ControllerStart()
    {
		base.ControllerStart();

		eye = GetComponent<Eye>();

	}


    protected override void ControllerUpdate()
    {
        base.ControllerUpdate();

		Vector2 lookVector = GamePad.Query (lookAction, Player.Profile, Player.State);
			
		yaw += lookVector.x * Time.deltaTime * sensitivityX;
		pitch += lookVector.y * Time.deltaTime * sensitivityY;

		pitch = ClampAngle (pitch, minimumY, maximumY);

		lookDir.x = Mathf.Cos (pitch * Mathf.Deg2Rad) * Mathf.Sin (yaw * Mathf.Deg2Rad);
		lookDir.y = Mathf.Sin (pitch * Mathf.Deg2Rad);
		lookDir.z = Mathf.Cos (pitch * Mathf.Deg2Rad) * Mathf.Cos (yaw * Mathf.Deg2Rad);

		right = Vector3.Cross (lookDir, Vector3.up); // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.

		up = Vector3.Cross (right, lookDir);

		eye.EyeTransform.localRotation = Quaternion.LookRotation (lookDir, up);

		if(rotateTowardsGuard && time < 1.0f )
		{
			time += Time.deltaTime * rotateSpeed;
			InterpolateRotation( time );
		}
    }
		
	IEnumerator DelayControlLock ()
	{
		yield return new WaitForSeconds( .5f );
		Player.State = noControls;
	}

	public void LookAtGuardWhoSpottedYou(Transform guard )
	{
		time = 0.0f;
		startRotation = transform.rotation;
		lookAtGuard = Quaternion.LookRotation( guard.position - transform.position );
		rotateTowardsGuard = true;
		StartCoroutine( DelayControlLock() );
	}

    public void AddToYaw(float val)
    {
        yaw += val;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;

        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }

	public void Interpolate ( float t )
	{
		sensitivityX = Mathf.Lerp( sensitivityXLook, sensitivityXAim, t );
		sensitivityY = Mathf.Lerp( sensitivityYLook, sensitivityYAim, t );
	}

	public void InterpolateRotation (float t )
	{
		transform.rotation = Quaternion.Lerp( startRotation, lookAtGuard, t );
	}
}
