﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public partial class Backpack : MonoBehaviour
{
	/// <summary>
	/// Simple interface pairing a reference to an item flyweight with an integer quantity.
	/// </summary>
	public interface IContent
	{
		/// <summary>
		/// What kind of item this content represents.
		/// </summary>
		ValuableType Type { get; }

		/// <summary>
		/// The quantity of this specific item.
		/// </summary>
		int Count { get; }
	}

	// Internal interface implementation
	private struct Content : IContent
	{
		public readonly ValuableType type;

		public int count;

		public ValuableType Type
		{
			get
			{
				return type;
			}
		}

		public int Count
		{
			get
			{
				return count;
			}
		}

		public Content( ValuableType type, int count )
		{
			this.type = type;
			this.count = count;
		}
	}
}
