﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Agent : MonoBehaviour
{
	[SerializeField] private PlayerState initialState;
	[SerializeField] private Animator animator;
	[SerializeField] private string caughtTrigger = "caught";
	[SerializeField] private TweenEvent startEvent;
	[SerializeField] private Text scoreText;

	public void GetCaught ()
	{
		animator.SetTrigger( caughtTrigger );
	}

	private void Awake ()
	{
		startEvent.Invoke();

		if ( FindObjectOfType<SessionController>().LocalSelection == PlayerSelection.Agent )
		{
			SessionController.onScoreChange += ( sc ) => scoreText.text = ScoreFormatter.Format( sc.BufferedScore, false );

			FindObjectOfType<SessionController>().Bump();

			var player = FindObjectOfType<ProfileController>().Player;

			player.State = initialState;

			foreach ( var controller in GetComponentsInChildren<ControllerBase>() )
			{
				controller.Player = player;
			}
		}
	}
}
