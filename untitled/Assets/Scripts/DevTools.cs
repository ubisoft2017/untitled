﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class DevTools
{
	private static List<DevAction> actions;
	private static List<DevToggle> toggles;
	private static List<DevSlider> sliders;

	private static List<KeyCode> keys;

	static DevTools ()
	{
		actions = new List<DevAction>( 64 );
		toggles = new List<DevToggle>( 64 );
		sliders = new List<DevSlider>( 64 );

		keys = new List<KeyCode>( 32 );
	}

	[RuntimeInitializeOnLoadMethod]
	private static void Initialize ()
	{
		new GameObject( "DevTools" ).AddComponent<DevToolsBehaviour>().gameObject.hideFlags = HideFlags.HideAndDontSave;
	}

	public static void Register ( string name, Action action, params KeyCode[] shortcut )
	{
		actions.Add( new DevAction( name, action, shortcut ) );
	}

	public static void Register ( string name, Func<bool> get, Action<bool> set, params KeyCode[] shortcut )
	{
		toggles.Add( new DevToggle( name, get, set, shortcut ) );
	}

	public static void Register ( string name, Func<float> get, Action<float> set, float min = float.MinValue, float max = float.MaxValue )
	{
		sliders.Add( new DevSlider( name, get, set, min, max ) );
	}
	public static void Register ( string name, Func<int> get, Action<int> set, int min = int.MinValue, int max = int.MaxValue )
	{
		sliders.Add( new DevSlider( name, get, set, min, max ) );
	}

	private static void PollKeys ()
	{
		foreach ( var key in Enum.GetValues( typeof( KeyCode ) ) as KeyCode[] )
		{
			if ( Input.GetKeyDown( key ) || (Input.GetKey( key ) && !keys.Contains( key )) )
			{
				keys.Add( key );

				TryMatchActions();
			}

			if ( Input.GetKeyUp( key ) || (keys.Contains( key ) && !Input.GetKey( key )) )
			{
				keys.Remove( key );
			}
		}
	}

	private static void TryMatchActions ()
	{
		foreach ( var action in actions )
		{
			TryMatchAction( action );
		}

		foreach ( var toggle in toggles )
		{
			TryMatchToggle( toggle );
		}
	}

	private static void TryMatchAction ( DevAction action )
	{
		if ( action.shortcut.Length == keys.Count )
		{
			for ( int i = 0; i < action.shortcut.Length; i++ )
			{
				if ( action.shortcut[i] != keys[i] )
				{
					return;
				}
			}

			action.action();

			Debug.Log( action.name );
		}
	}

	private static void TryMatchToggle ( DevToggle toggle )
	{
		if ( toggle.shortcut.Length == keys.Count )
		{
			for ( int i = 0; i < toggle.shortcut.Length; i++ )
			{
				if ( toggle.shortcut[i] != keys[i] )
				{
					return;
				}
			}

			toggle.set( !toggle.get() );

			Debug.Log( "Toggle " + toggle.name );
		}
	}

	private class DevToolsBehaviour : MonoBehaviour
	{
		[SerializeField][HideInInspector] private bool show;

		[SerializeField][HideInInspector] private Vector2 scroll;

		private void OnGUI ()
		{
			PollKeys();

			if ( Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.F1 )
			{
				show = !show;
			}

			try
			{
				if ( show )
				{
					scroll = GUILayout.BeginScrollView( scroll );

					foreach ( var action in actions )
					{
						GUILayout.BeginHorizontal();

						if ( GUILayout.Button( action.name ) )
						{
							action.action();
						}

						if ( action.shortcut.Length > 0 )
						{
							string shortcut = "";

							for ( int i = 0; i < action.shortcut.Length; i++ )
							{
								shortcut += action.shortcut[i] + " + ";
							}

							GUILayout.Label( shortcut.TrimEnd( ' ', '+' ) );
						}

						GUILayout.EndHorizontal();
					}

					foreach ( var toggle in toggles )
					{
						GUILayout.BeginHorizontal();

						toggle.set( GUILayout.Toggle( toggle.get(), toggle.name ) );

						if ( toggle.shortcut.Length > 0 )
						{
							string shortcut = "";

							for ( int i = 0; i < toggle.shortcut.Length; i++ )
							{
								shortcut += toggle.shortcut[i] + " + ";
							}

							GUILayout.Label( shortcut.TrimEnd( ' ', '+' ) );
						}

						GUILayout.EndHorizontal();
					}

					foreach ( var slider in sliders )
					{
						GUILayout.BeginHorizontal();

						GUILayout.Label( slider.name );

						if ( slider.isFloat )
						{
							slider.setf( GUILayout.HorizontalSlider( slider.getf(), slider.minf, slider.maxf ) );
						}
						else
						{
							slider.seti( (int) GUILayout.HorizontalSlider( slider.geti(), slider.mini, slider.maxi ) );
						}

						GUILayout.EndHorizontal();
					}

					GUILayout.EndScrollView();
				}
			}
			catch ( ArgumentException ) { }
		}
	}
}
