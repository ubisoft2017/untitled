﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class LevelList : MonoBehaviour
{
	[SerializeField]
	private List<Level> levels = new List<Level>();

	public ReadOnlyCollection<Level> Levels
	{
		get
		{
			return levels.AsReadOnly();
		}
	}

	public Level this[int index]
	{
		get
		{
			return levels[index];
		}
	}

	[SerializeField]
	private int index = -1;

	public int Index
	{
		get
		{
			return index;
		}

		set
		{
			index = Mathf.Clamp( value, 0, levels.Count - 1 );
		}
	}

	[SerializeField]
	private int defaultIndex = 0;

	private void OnEnable ()
	{
		if ( index == -1 )
		{
			index = 0;
		}
	}

	private void OnValidate ()
	{
		defaultIndex = Mathf.Clamp( defaultIndex, 0, levels.Count - 1 );
	}
}
