﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tweening;

public class MainMenuMusic : MonoBehaviour {

	[SerializeField]
	private AudioSource mainMenuMusic;
	private float time;
	private bool lerpVolumeIn;
	private bool lerpVolumeOut;
	private float fadeInVolume = .1f;
	private float currentVol;

	// Use this for initialization
	void Awake () {
		mainMenuMusic.volume = fadeInVolume;
	}
	
	private void fadeIn ()
	{
		lerpVolumeIn = true;
		
	}

	public void fadeOutAndStop ()
	{
		lerpVolumeOut = true;
		currentVol = mainMenuMusic.volume;
	}

	// Update is called once per frame
	void Update () {
		if ( lerpVolumeIn && time < 1.0f )
		{
			time += Time.deltaTime;
			mainMenuMusic.volume = Mathf.Lerp( fadeInVolume, 1, time );
		}

		if ( lerpVolumeOut && time < 1.0f )
		{
			time += Time.deltaTime / 2;
			
			mainMenuMusic.volume = Mathf.Lerp( currentVol, 0, time );
		}

		if (time > 1.0f && lerpVolumeIn)
		{
			lerpVolumeIn = false;
			time = 0.0f;
		}

		if ( time > 1.0f && lerpVolumeOut )
		{
			lerpVolumeOut = false;
			mainMenuMusic.Stop();
			time = 0.0f;
		}

	}
}
