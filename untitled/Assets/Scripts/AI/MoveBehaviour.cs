﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	[RequireComponent( typeof( MoveDecisionMaker ), typeof( CharacterController ) )]
	public class MoveBehaviour : MonoBehaviour
	{
		[SerializeField][HideInInspector] private MoveDecisionMaker decisionMaker;
		[SerializeField][HideInInspector] private CharacterController character;

		[SerializeField] private float speed;

		private void Start ()
		{
			decisionMaker = GetComponent<MoveDecisionMaker>();
			character = GetComponent<CharacterController>();
		}

		private void Update ()
		{
			if ( decisionMaker.HasDecision )
			{
				// Get the vector from this AI's position to it's goal
				Vector3 delta = decisionMaker.Decision - transform.position;

				// Compute the total distance
				float distance = delta.magnitude;

				// Create a normalized direction vector towards the goal
				Vector3 direction = delta / distance;

				// Calculate the maximum displacement for this frame
				float budget = speed * Time.deltaTime;

				Vector3 move = direction * Mathf.Min( budget, distance );

				// Move towards the goal my the lesser of the maximum allowable / total distance
				character.Move( new Vector3( move.x, 0.0f, move.z ) );
			}
		}

		private void OnDrawGizmos ()
		{
			if ( decisionMaker != null && decisionMaker.HasDecision )
			{
				Gizmos.DrawLine( decisionMaker.Decision, transform.position );
			}
		}
	}
}