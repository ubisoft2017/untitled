﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using AI;
using System;

[RequireComponent( typeof( MoveDecisionMaker ) )]
public abstract class MoveProvider : MonoBehaviour, IMoveCandidateProvider
{
	// Used by decision maker
	public event Action<IDecisionCandidateProvider<Vector3>> OnChangeScoreOrValidity;

	[Header( "Decision Making" )]

	[Tooltip( "Score used by decision maker to sort this candidate's decision." )]
	[SerializeField] protected int score;

	// Used by decision maker
	public virtual int Score
	{
		get
		{
			return score;
		}
	}

	// Used by decision maker
	public abstract Vector3 Candidate { get; }
	public abstract bool IsValid { get; }
}
