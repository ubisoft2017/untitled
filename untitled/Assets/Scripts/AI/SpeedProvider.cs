﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using AI;
using System;

[RequireComponent( typeof( SpeedDecisionMaker ) )]
public abstract class SpeedProvider : MonoBehaviour, ISpeedCandidateProvider
{
	// Used by decision maker
	public event Action<IDecisionCandidateProvider<float>> OnChangeScoreOrValidity;

	[Header( "Decision Making" )]

	[Tooltip( "Score used by decision maker to sort this candidate's decision." )]
	[SerializeField] protected int score;

	// Used by decision maker
	public virtual int Score
	{
		get
		{
			return score;
		}
	}

	// Used by decision maker
	public abstract float Candidate { get; }
	public abstract bool IsValid { get; }
}
