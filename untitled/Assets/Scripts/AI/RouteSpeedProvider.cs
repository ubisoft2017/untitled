﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( AI.RouteFollower ), typeof( AI.AccelerationBehaviour ) )]
public class RouteSpeedProvider : SpeedProvider {

	[SerializeField][HideInInspector] private AI.RouteFollower follower;
	[SerializeField][HideInInspector] private AI.AccelerationBehaviour behaviour;

	[SerializeField] private AnimationCurve distanceSpeedCurve = AnimationCurve.EaseInOut( 0.0f, 0.0f, 1.0f, 1.0f );
	[SerializeField] private float breakDistance;

	public override bool IsValid
	{
		get
		{
			return follower.Route != null;
		}
	}

	public override float Candidate
    {
        get
        {
			Vector3 target = follower.Route[follower.Next].Position;

			if ( target == behaviour.Body.position )
			{
				return 0.0f;
			}

			Vector3 delta = target - behaviour.Body.position;

			float distance = delta.magnitude;

			Vector3 direction = delta / distance;

			float dot = Mathf.Clamp01( Vector3.Dot( direction, behaviour.Body.forward ) );

			float speed = distanceSpeedCurve.Evaluate( distance / breakDistance ) * behaviour.MaxForwardSpeed;// behaviour.Speed;

			return speed * dot;
        }
    }

	// Update is called once per frame
	void Awake ()
	{
		follower = GetComponent<AI.RouteFollower>();
		behaviour = GetComponent<AI.AccelerationBehaviour>();
	}
}
