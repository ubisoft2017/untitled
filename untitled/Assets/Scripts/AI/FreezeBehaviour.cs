﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Behaviour responsible for telling an AI to stop in their tracks when they become alert.
	/// </summary>
	[RequireComponent( typeof( AlertStatus ) )]
	public class FreezeBehaviour : MonoBehaviour, IMoveCandidateProvider
	{
		public event Action<IDecisionCandidateProvider<Vector3>> OnChangeScoreOrValidity;

		[Tooltip( "Score when all is quiet." )]
		[SerializeField] private int quietScore = 0;

		[Tooltip( "Score when alerted." )]
		[SerializeField] private int alertScore = 1000;

		public Vector3 Candidate
		{
			get
			{
				return new Vector3( transform.position.x, 0.0f, transform.position.z );
			}
		}

		// Current score
		[SerializeField][HideInInspector] private int score;

		public int Score
		{
			get
			{
				return score;
			}
		}

		private AlertStatus alertStatus;

		public bool IsValid
		{
			get
			{
				return alertStatus.IsAlerted;
			}
		}

		private void Start ()
		{
			alertStatus = GetComponent<AlertStatus>();

			HandleAlertStatusChange( alertStatus );

			alertStatus.OnChangeAlertStatus += HandleAlertStatusChange;
		}

		private void HandleAlertStatusChange ( AlertStatus alertStatus )
		{
			OnChangeScoreOrValidity( this );
		}

		private void OnDestroy ()
		{
			GetComponent<AlertStatus>().OnChangeAlertStatus -= HandleAlertStatusChange;
		}
	}
}