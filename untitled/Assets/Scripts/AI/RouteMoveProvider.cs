﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( AI.RouteFollower ) )]
public class RouteMoveProvider : MoveProvider {

    private AI.RouteFollower follower;

	public override bool IsValid
	{
		get
		{
			return follower.Route != null;
		}
	}

	public override Vector3 Candidate
    {
        get
        {
            return follower.Route[follower.Next].Position;
        }
    }

    // Update is called once per frame
    void Awake () {
        follower = GetComponent<AI.RouteFollower>();
	}
}
