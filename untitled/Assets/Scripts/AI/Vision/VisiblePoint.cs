﻿using System;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Used by VisionTarget. Used to discretize a continuous form into individual visible points.
	/// </summary>
	[Serializable]
	public class VisiblePoint
	{
		[Tooltip( "The transform around which this point is localized." )]
		public Transform transform;

		[Tooltip( "How much this point will contribute to the overall vision score for the target, if seen." )]
		public float score = 1;

		[SerializeField][HideInInspector] private Vector3 previous;
		[SerializeField][HideInInspector] private float speed;

		[HideInInspector] public bool inShadow;

		public float Speed
		{
			get
			{
				return speed;
			}
		}

		public Vector3 Position
		{
			get
			{
				return transform.position;
			}
		}

		public void Initialize ()
		{
			previous = transform.position;
		}

		public void Update ()
		{
			speed = (transform.position - previous).magnitude * Time.deltaTime;

			previous = transform.position;
		}
	}
}
