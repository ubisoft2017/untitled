﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Represents a continuous visible form.
	/// </summary>
	public class VisionTarget : Raycaster
	{
		[Tooltip( "Discrete points used to approximate this target's continuous form." )]
		[SerializeField] private List<VisiblePoint> points = new List<VisiblePoint>();

		private Transform sun;

		public ReadOnlyCollection<VisiblePoint> Points
		{
			get
			{
				return points.AsReadOnly();
			}
		}

		private void Start ()
		{
			var sun = GameObject.FindWithTag( "Sun" );

			if ( sun != null )
			{
				this.sun = sun.transform;
			}

			for ( int i = 0; i < points.Count; i++ )
			{
				points[i].Initialize();
			}
		} 

		void OnEnable ()
		{
			// Register this target with the vision system
			VisionSystem.Enable( this );
		}

		void OnDisable ()
		{
			// Unregister this target with the vision system
			VisionSystem.Disable( this );
		}

		private void Update ()
		{
			for ( int i = 0; i < points.Count; i++ )
			{
				if ( sun != null )
				{
					points[i].transform.rotation = Quaternion.LookRotation( -sun.forward );
				}

				caster = points[i].transform;

				points[i].inShadow = Raycast();

				points[i].Update();
			}
		}

#if UNITY_EDITOR
		private void OnDrawGizmosSelected ()
		{
			if ( points != null )
			{
				foreach ( var point in points )
				{
					if ( point.transform != null )
					{
						if ( !Application.isPlaying )
						{
							var sun = GameObject.FindWithTag( "Sun" );

							if ( sun != null )
							{
								point.transform.rotation = Quaternion.LookRotation( -sun.transform.forward );

								caster = point.transform;

								point.inShadow = Raycast();
							}
						}

						Gizmos.color = point.inShadow ? Color.black : Color.white;

						Gizmos.DrawSphere( point.transform.position, 0.02f );
					}
				}

				Gizmos.color = Color.white;
			}
		}
#endif
	}
}