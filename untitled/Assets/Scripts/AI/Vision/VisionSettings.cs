﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Flyweight for settings used to tweak VisionProcessor behaviour.
	/// </summary>
	[CreateAssetMenu( fileName = "New VisionSettings.asset", menuName = "Vision Settings" )]
	public class VisionSettings : ScriptableObject
	{
		public const string RESOURCE_PATH = "Vision Settings";

		[Tooltip( "Layers raycasted against." )]
		[SerializeField] private LayerMask occlusionLayerMask;

		public LayerMask OcclusionLayerMask
		{
			get
			{
				return occlusionLayerMask;
			}
		}

		[Header( "Parameters" )]

		[Tooltip( "The maximum distance at which a visible point can be perceived." )]
		[SerializeField] private float range;

		public float Range
		{
			get
			{
				return range;
			}
		}

		[Tooltip( "How well a visible point is perceived as it moves farther away." )]
		[SerializeField] private AnimationCurve distanceResponseCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 0.0f );

		public AnimationCurve DistanceResponseCurve
		{
			get
			{
				return distanceResponseCurve;
			}
		}

		[Tooltip( "The angle at which a visible point can be perceived. (0 = straight ahead, 180 = any direction)" )]
		[SerializeField][Range( 0.0f, 180.0f )] private float fieldOfView;

		public float FieldOfView
		{
			get
			{
				return fieldOfView;
			}
		}

		[Tooltip( "How well a visible point is perceived as it moves away from the center of vision." )]
		[SerializeField] private AnimationCurve peripheralResponseCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 1.0f );

		public AnimationCurve PeripheralResponseCurve
		{
			get
			{
				return peripheralResponseCurve;
			}
		}

		[Tooltip( "Multiplier applied to visibility score when a target is immobile." )]
		[SerializeField][Range( 0.0f, 1.0f )] private float targetImmobilityPenalty = 0.75f;

		public float TargetImmobilityPenalty
		{
			get
			{
				return targetImmobilityPenalty;
			}
		}

		[Tooltip( "Multiplier applied to visibility score when a target is in shadow." )]
		[SerializeField][Range( 0.0f, 1.0f )] private float targetInShadowPenalty = 0.0f;

		public float TargetInShadowPenalty
		{
			get
			{
				return targetInShadowPenalty;
			}
		}

		[Header( "Debugging" )]

		// Visualize the raycasts from each VisionProcessor toward each VisionTarget's VisionPoints.
		public bool drawRayHits;
		public bool drawRayMisses;

		// Draw vision gizmos for all VisionProcessor instances, not only selected ones.
		public bool drawGizmos;
	}
}