﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tweening;

public class VisionUI : MonoBehaviour
{
    private bool visible, lastVisible;

    AI.VisionTarget visionTarget;

    [SerializeField] TweenEvent visibleEvent, hiddenEvent;

    // Use this for initialization
    void Start()
    {
        visionTarget = GetComponent<AI.VisionTarget>();
        GameObject.FindGameObjectWithTag("VisionUI").SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        visible = false;
        for (int i = 0; i < visionTarget.Points.Count; i++)
        {
            if(!visionTarget.Points[i].inShadow)
            {
                visible = true;
                break;
            }
        }

        if(visible != lastVisible)
        {
            if (visible)
            {
                visibleEvent.Invoke();
            }
            else
            {
                hiddenEvent.Invoke();
            }
        }

        lastVisible = visible;

    }
}
