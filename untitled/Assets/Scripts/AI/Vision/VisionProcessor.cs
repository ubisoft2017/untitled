﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Behaviour used to simulate the vision process of an AI.
	/// </summary>
	[RequireComponent( typeof( Eye ) )]
	public class VisionProcessor : MonoBehaviour, IPhaseInOut
	{
		/// <summary>
		/// Subscribe to this event to track seen objects.
		/// </summary>
		public event Action<VisionTarget,float> OnProcessTarget;

		[EnumObject( typeof( VisionSettings ), VisionSettings.RESOURCE_PATH )]
		[Tooltip( "The vision settings used by this vision behaviour." )]
		[SerializeField]
		private VisionSettings settings;
		
		// We keep a local list of targets, in case we want to filter vision targets on a per-AI basis
		[SerializeField][HideInInspector] private List<VisionTarget> targets;

		private void HandleEnable ( VisionTarget target )
		{
			targets.Add( target );
		}

		private void HandleDisable ( VisionTarget target )
		{
			targets.Remove( target );
		}

		private void Awake ()
		{
			targets = new List<VisionTarget>( 64 );
		}

		[SerializeField][HideInInspector] private Eye eye;

		private void Start ()
		{
			eye = GetComponent<Eye>();

			if ( settings == null )
			{
				Debug.LogWarning( "The VisionSettings for this VisionProcessor have not been set." );
			}

			// Fetch the list of available targets
			targets.AddRange( VisionSystem.Targets );

			// Subscribe to target enable / disable events so that we can add / remove them from our local list
			VisionSystem.OnEnableTarget += HandleEnable;
			VisionSystem.OnDisableTarget += HandleDisable;
		}

		// Avoid unnecessary allocations in tight loops by using shared temporary variables

		private static int i, j, k, n;

		private static Ray ray;

		private static RaycastHit[] results;
		private static RaycastHit   nearest;

		private static float distance, minimum, theta, score;

		static VisionProcessor ()
		{
			// Initialize RaycastHit buffer, required to use Physics.RaycastNonAlloc
			results = new RaycastHit[256];
		}

		void Update ()
		{
			if ( eye != null && settings != null )
			{
				ray.origin = eye.Position;

				for ( i = 0; i < targets.Count; i++ )
				{
					// Reset score
					score = 0.0f;

					// Attempt to "see" each of the target's visible points
					for ( j = 0; j < targets[i].Points.Count; j++ )
					{
						// Calculate the distance from the eye to the visible point
						distance = Vector3.Distance( targets[i].Points[j].Position, eye.Position );

						// Check if the target point is within visible range
						if ( distance <= settings.Range )
						{
							// Calculate the direction vector from the eye to the visible point
							ray.direction = (targets[i].Points[j].Position - eye.Position).normalized;

							// Calculate the angle in degrees between the direction vector and the eye's local forward axis
							theta = Mathf.Rad2Deg * Mathf.Acos( Vector3.Dot( ray.direction, eye.Direction ) );

							// Check if the target point is within the field of view
							if ( theta <= settings.FieldOfView )
							{
								// Raycast to find all points of contact along the line-of-sight
								n = Physics.RaycastNonAlloc( ray, results, settings.Range, settings.OcclusionLayerMask );

								minimum = float.PositiveInfinity;

								// Find the nearest point of contact along the line-of-sight

								if ( n > 0 )
								{
									nearest = results[0];

									for ( k = 0; k < n; k++ )
									{
										if ( results[k].distance < minimum )
										{
											minimum = results[k].distance;
											nearest = results[k];
										}
									}
								}

								// Check if line-of-sight from the behaviour's eye to the target point is clear
								if ( distance < minimum )
								{
									// Score is summed for all visible points
									score += targets[i].Points[j].score
										* settings.DistanceResponseCurve.Evaluate( distance / settings.Range )
										* settings.PeripheralResponseCurve.Evaluate( theta / settings.FieldOfView )
										* (targets[i].Points[j].inShadow ? settings.TargetInShadowPenalty : 1.0f);

									// Debug stuff for visualizing raycasts
#if UNITY_EDITOR
									if ( settings.drawRayHits )
									{
										Debug.DrawLine( eye.Position, targets[i].Points[j].Position, Color.green );
									}
#endif
								}
#if UNITY_EDITOR
								else if ( settings.drawRayMisses )
								{
									Debug.DrawLine( eye.Position, nearest.point, Color.red );
								}
#endif
							}
						}
					}

					// If anyone is listening for vision processing events
					if ( OnProcessTarget != null )
					{
						// Give them the new score for the vision target
						OnProcessTarget( targets[i], score);
					}
				}
			}
		}

		private void OnDestroy ()
		{
			// Unsuubscribe from target enable / disable events
			VisionSystem.OnEnableTarget -= HandleEnable;
			VisionSystem.OnDisableTarget -= HandleDisable;
		}

		void IPhaseInOut.Localize ()
		{
			enabled = true;
		}

		void IPhaseInOut.Delocalize ()
		{
			enabled = false;
		}

        public VisionSettings getSettings() {
            return settings;
        }
#if UNITY_EDITOR
		private void OnDrawGizmos ()
		{
			if ( settings != null && settings.drawGizmos )
			{
				DrawGizmo();
			}
		}

		private void OnDrawGizmosSelected ()
		{
			if ( settings != null )
			{
				DrawGizmo();
			}
		}

		private void DrawGizmo ()
		{
			if ( eye != null )
			{
				Gizmos.DrawWireSphere( eye.Position, settings.Range );

				Vector3 fwd = eye.transform.forward * Mathf.Cos( Mathf.Deg2Rad * settings.FieldOfView );
				Vector3 right = eye.transform.right * Mathf.Sin( Mathf.Deg2Rad * settings.FieldOfView );

				Gizmos.DrawLine( eye.Position, eye.Position + (fwd + right) * settings.Range );
				Gizmos.DrawLine( eye.Position, eye.Position + (fwd - right) * settings.Range );
			}
		}
#endif
	}
}