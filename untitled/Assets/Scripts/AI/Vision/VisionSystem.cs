﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Singleton (oh noes!) which tracks available VisionTarget instances. Used primarily by VisionProcessor.
	/// </summary>
	public static class VisionSystem
	{
		public static event Action<VisionTarget> OnEnableTarget;

		public static event Action<VisionTarget> OnDisableTarget;

		private static List<VisionTarget> targets;

		static VisionSystem ()
		{
			targets = new List<VisionTarget>( 64 );
		}

		public static ReadOnlyCollection<VisionTarget> Targets
		{
			get
			{
				return targets.AsReadOnly();
			}
		}

		/// <summary>
		/// Called by VisionTarget in OnEnable.
		/// </summary>
		/// <param name="target">Calling instance.</param>
		public static void Enable ( VisionTarget target )
		{
			targets.Add( target );

			if ( OnEnableTarget != null )
			{
				OnEnableTarget( target );
			}
		}

		/// <summary>
		/// Called by VisionTarget in OnDisable.
		/// </summary>
		/// <param name="target">Calling instance.</param>
		public static void Disable ( VisionTarget target )
		{
			targets.Remove( target );

			if ( OnDisableTarget != null )
			{
				OnDisableTarget( target );
			}
		}
	}
}
