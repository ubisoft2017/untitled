﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Provides a candidate Vector3 representing a position in space that the AI should move to.
	/// </summary>
	public interface IMoveCandidateProvider : IDecisionCandidateProvider<Vector3> { }
}
