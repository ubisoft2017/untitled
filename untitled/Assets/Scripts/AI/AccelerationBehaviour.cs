﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	[RequireComponent( typeof( SpeedDecisionMaker ), typeof( CharacterController ) )]
	public class AccelerationBehaviour : MonoBehaviour
	{
		[SerializeField][HideInInspector] private SpeedDecisionMaker decisionMaker;
		[SerializeField][HideInInspector] private CharacterController character;

		[SerializeField] private Animator animator;
		[SerializeField] private string speedParameter;

		[SerializeField] private Transform body;

		public Transform Body
		{
			get
			{
				return body;
			}
		}

		[SerializeField] private float maxForwardSpeed;

		public float MaxForwardSpeed
		{
			get
			{
				return maxForwardSpeed;
			}
		}

		[SerializeField] private float maxReverseSpeed;

		public float MaxReverseSpeed
		{
			get
			{
				return maxReverseSpeed;
			}
		}

		[SerializeField] private float accelForce;
		[SerializeField] private float breakForce;
		[SerializeField] private float frictionForce;

		[SerializeField][HideInInspector]  private float speed;

		public float Speed
		{
			get
			{
				return speed;
			}
		}

		private void Start ()
		{
			decisionMaker = GetComponent<SpeedDecisionMaker>();
			character = GetComponent<CharacterController>();
		}

		private void Update ()
		{
			if ( decisionMaker.HasDecision )
			{
				speed = character.velocity.magnitude;

				// Get the vector from this AI's position to it's goal
				float delta = decisionMaker.Decision - speed;

				// Calculate the maximum displacement for this frame
				float budget = (delta > 0.0f ? accelForce : breakForce) * Time.deltaTime;

				// Accelerate towards the goal speed, respecting this frame's budget
				speed = Mathf.Clamp( Mathf.Lerp( speed, decisionMaker.Decision, budget / delta ), 0.0f, speed > 0.0f ? maxForwardSpeed : MaxReverseSpeed );

				character.Move( body.forward * speed * Time.deltaTime );
			}

			animator.SetFloat( speedParameter, speed );
		}

#if UNITY_EDITOR
		private void OnDrawGizmos ()
		{
			if ( decisionMaker != null && decisionMaker.HasDecision )
			{
				Gizmos.DrawRay( transform.position + Vector3.up * 0.1f, transform.forward * decisionMaker.Decision );
			}
		}
#endif
	}
}