﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Generic interface for all decision candidate providers.
	/// </summary>
	/// <typeparam name="T">Decision type.</typeparam>
	public interface IDecisionCandidateProvider<T>
	{
		event Action<IDecisionCandidateProvider<T>> OnChangeScoreOrValidity;

		// Candidate score (how likely it is that this candidate is the right one)
		int Score { get; }

		// Decision candidate
		T Candidate { get; }

		bool IsValid { get; }
	}
}
