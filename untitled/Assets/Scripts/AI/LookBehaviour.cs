﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	[RequireComponent( typeof( LookDecisionMaker ), typeof( Eye ) )]
	public class LookBehaviour : MonoBehaviour
	{
		[SerializeField][HideInInspector] private LookDecisionMaker decisionMaker;
		[SerializeField][HideInInspector] private Eye eye;
		[SerializeField] private Transform body, head;

        [Tooltip( "degrees / second" )]
        [SerializeField][Range( 0.0f, 1080.0f )] private float bodySpeed, headSpeed, eyeSpeed;

		private void Start ()
		{
			decisionMaker = GetComponent<LookDecisionMaker>();
			eye = GetComponent<Eye>();
		}

        private Quaternion DoLook ( float speed, Vector3 currentDirection, Vector3 lookDirection )
        {			
            // Compute the total angular displacement
            float angle = Vector3.Angle( lookDirection, currentDirection );
			
            // Calculate the maximum angular displacement for this frame
            float budget = speed * Time.deltaTime;

			// Rotate toward the goal rotation, making sure to respect the limit for this frame
			lookDirection = Vector3.Lerp( currentDirection, lookDirection, budget / angle );

			// Create a rotation to look towards the goal
			return Quaternion.LookRotation( lookDirection, Vector3.up );
		}

		private void Update ()
		{
			if ( decisionMaker.HasDecision )
			{
				Vector3 lookDirection = (decisionMaker.Decision - eye.Position).normalized;
				Vector3 bodyLookDirection = new Vector3( lookDirection.x, 0, lookDirection.z );

				body.rotation = DoLook( bodySpeed, body.forward, bodyLookDirection );
				head.rotation = DoLook( headSpeed, head.forward, lookDirection );
				eye.EyeTransform.rotation = DoLook( eyeSpeed, eye.EyeTransform.forward, lookDirection );
			}
		}
	}
}