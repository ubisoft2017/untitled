﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Provides a candidate Vector3 for a point in space that the AI should look at.
	/// </summary>
	public interface ILookCandidateProvider : IDecisionCandidateProvider<Vector3> { }
}
