﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Provides a candidate float representing a speed the AI should use to move.
	/// </summary>
	public interface ISpeedCandidateProvider : IDecisionCandidateProvider<float> { }
}
