﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PatrolRouteType
{
	PingPong,
	Wrap
}

public class PatrolRoute : Node
{
	public Color color = Color.white;

	[SerializeField]
	private PatrolRouteType type;

	public PatrolRouteType Type
	{
		get
		{
			return type;
		}
	}

	public PatrolPoint this[int i]
	{
		get
		{
			return transform.GetChild( i ).GetComponent<PatrolPoint>();
		}
	}

	public int Length
	{
		get
		{
			return transform.childCount;
		}
	}

	public override NodeType NodeType
	{
		get
		{
			return NodeType.Route;
		}
	}

	public void Next ( ref int next, ref Traversal traversal )
	{
		switch ( traversal )
		{
		case Traversal.Forward:

			next++;

			// Check if we have just reached the last patrol point
			if ( next == Length )
			{
				switch ( type )
				{
				case PatrolRouteType.PingPong:
					next -= 2;

					traversal = traversal.Flip();

					break;

				case PatrolRouteType.Wrap:

					next = 0;

					break;

				default:
					break;
				}
			}

			break;
		case Traversal.Reverse:

			next--;

			// Check if we have just reached the first patrol point
			if ( next == -1 )
			{
				switch ( type )
				{
				case PatrolRouteType.PingPong:

					next += 2;

					traversal = traversal.Flip();

					break;

				case PatrolRouteType.Wrap:

					next = Length - 1;

					break;

				default:
					break;
				}
			}

			break;
		default:
			break;
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		Gizmos.color = color;

		for ( int i = 1; i < transform.childCount; i++ )
		{
			Gizmos.DrawLine( transform.GetChild( i - 1 ).position, transform.GetChild( i ).position );
		}

		if ( type == PatrolRouteType.Wrap )
		{
			Gizmos.DrawLine( transform.GetChild( transform.childCount - 1 ).position, transform.GetChild( 0 ).position );
		}

		Gizmos.color = Color.white;
	}
#endif
}
