﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPoint : MonoBehaviour
{
	public Vector3 Position
	{
		get
		{
			return transform.position;
		}
	}
}
