﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	public class RouteFollower : MonoBehaviour
	{
		[Header( "Setup" )]
		[SerializeField]
		private PatrolRoute route;

        public PatrolRoute Route
        {
            get
            {
                return route;
            }
        }

		[SerializeField]
		private Traversal traversal;

		[SerializeField]
		private int first;

		[Header( "Parameters" )]
		[SerializeField]
		private float tolerance = 0.01f;

		// Index of the next patrol point
		[SerializeField]
		[HideInInspector]
		private int next;

		public int Next
        {
            get
            {
                return next;
            }
        }

        public void SetPatrolRoute (PatrolRoute route)
		{
			this.route = route;
		}

		private void Start ()
		{
			if ( route == null )
			{
				Debug.LogWarning( "This patrol does not have a route specified!", this );

				return;
			}

			next = Mathf.Clamp( first, 0, route.Length );
		}

		private void Update ()
		{
			if ( route != null )
			{
				// Check if we have reached the next patrol point
				if ( Vector2.Distance( new Vector2(transform.position.x, transform.position.z), new Vector2(route[next].Position.x, route[next].Position.z )) <= tolerance )
				{
					route.Next( ref next, ref traversal );
				}
			}
		}
	}
}