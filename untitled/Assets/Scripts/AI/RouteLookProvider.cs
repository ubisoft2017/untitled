﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof( AI.RouteFollower ) )]
public class RouteLookProvider : LookProvider {

    private AI.RouteFollower follower;

    [SerializeField]
    private float height;

    public override Vector3 Candidate
    {
        get
        {
            return follower.Route[follower.Next].Position + Vector3.up * height;
        }
    }

	public override bool IsValid
	{
		get
		{
			return follower.Route != null;
		}
	}

	// Update is called once per frame
	void Awake ()
	{
        follower = GetComponent<AI.RouteFollower>();
	}
}
