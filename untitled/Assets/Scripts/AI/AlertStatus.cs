﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

namespace AI
{
	/// <summary>
	/// Behaviour for tracking an AI's alert status.
	/// </summary>
	public abstract class AlertStatus : MonoBehaviour
	{
		public event Action<AlertStatus> OnChangeAlertStatus;

		[SerializeField][HideInInspector] private bool isAlerted;

		public bool IsAlerted
		{
			get
			{
				return isAlerted;
			}

			set
			{
				if ( value != isAlerted )
				{
					value = isAlerted;

					if ( OnChangeAlertStatus != null )
					{
						OnChangeAlertStatus( this );
					}
				}
			}
		}
	}
}