﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
	/// <summary>
	/// Abstract decision making class. Selects the highest scoring decision from a pool of candidates.
	/// </summary>
	/// <typeparam name="T">Decision type.</typeparam>
	public abstract class DecisionMaker<T,P> : MonoBehaviour where P : IDecisionCandidateProvider<T>
	{
		[Tooltip( "Whether or not to look for providers in child game objects." )]
		[SerializeField] private bool includeChildren;

		// Associated candidate providers
		protected List<P> providers;

		// Active (highest-scoring) candidate provider
		private P provider;

		public P Provider
		{
			get
			{
				return provider;
			}
		}

		public bool HasDecision
		{
			get
			{
				return provider != null;
			}
		}

		public T Decision
		{
			get
			{
				return provider.Candidate;
			}
		}

		void Start ()
		{
			// Get all of the providers used by this decision maker
			providers = new List<P>( includeChildren ? GetComponentsInChildren<P>() : GetComponents<P>() );

			if ( providers.Count == 0 )
			{
				Debug.LogWarning( "This decision maker does not have any associated decision providers.", this );

				return;
			}

			// Listen for score changes
			for ( i = 0; i < providers.Count; i++ )
			{
				providers[i].OnChangeScoreOrValidity += HandleScoreOrValidityChange;
			}

			// Check all providers to find the one with highest score
			CheckAll();
		}

		private void HandleScoreOrValidityChange ( IDecisionCandidateProvider<T> provider )
		{
			if ( provider.Equals( this.provider ) )
			{
				CheckAll();
			}
			else
			{
				if ( provider.Score > this.provider.Score )
				{
					this.provider = (P) provider;
				}
			}
		}

		private static int high, i;

		private void CheckAll ()
		{
			// Reset
			provider = default( P );

			// Look for the provider with the highest score

			high = int.MinValue;

			for ( i = 0; i < providers.Count; i++ )
			{
				// If the i'th provider has the highest score thus far
				if ( providers[i].IsValid && providers[i].Score >= high )
				{
					// Update the high score
					high = providers[i].Score;

					// Set the active provider = to the i'th provider
					provider = providers[i];
				}
			}
		}
	}
}