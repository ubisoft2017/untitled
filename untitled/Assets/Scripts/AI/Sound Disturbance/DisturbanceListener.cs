﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhaseMember))]
public class DisturbanceListener : MonoBehaviour {


    public void alert(SoundDisturbance d) {
        if (GetComponent<PhaseMember>().isInPhase())
        {
            Debug.Log("I HEARD SOMETHING: "+gameObject.tag ); 
        }
    }
}
