﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDisturbance : MonoBehaviour  {
    Ray ray;
    RaycastHit hit;

    [SerializeField]
    [Tooltip("The number of nodes surrounding object that will cast a ray to check if a listener can hear (0 or >2")]
    int numOfSoundNodes;
 
    [SerializeField]
    [Tooltip("This is how far the sound will be able to travel before not being detectable")]
    float range;

    [Tooltip("How important is this sound to the listener")]
    public float alertScore;

    Vector3[] soundNodeDirections; // so we dont have to compute every time, we store the directions here at start and use them
    Vector3[] soundNodePositions;

   // int layerMask;
    bool ready = false;
    Vector3 lastPosition;

    void OnDrawGizmosSelected()
    {
        if (ready)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, range);

            Gizmos.color = new Color(0.3f, 1.0f, 0, 0.75F);

            for (int i = 0; i < soundNodePositions.Length; i++)
            {
                Gizmos.DrawLine(transform.position, soundNodePositions[i]);
            }

            List<DisturbanceListener> listeners = getListenersInRadius();
            Gizmos.color = new Color(0.8f, 1.0f, 0, 1);
            foreach (DisturbanceListener L in listeners)
            {
                Gizmos.DrawLine(transform.position, L.transform.position);

            }
        }
        else
            setUp();
    }
    void Awake() {
        if (!ready)
            setUp();
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            makeNoise();
        }

        if (transform.position != lastPosition) {
            setNodePositions();
            lastPosition = transform.position;
        }
    }
    public virtual void setUp() {
      //  layerMask = (1 << 13); // this needs to be fixed
    //    layerMask = ~layerMask;
        soundNodeDirections = new Vector3[numOfSoundNodes];
        soundNodePositions = new Vector3[numOfSoundNodes];
        float degreeDelta = 2 * Mathf.PI / numOfSoundNodes;
        for (int i = 0; i < numOfSoundNodes; i++)
        {
            soundNodeDirections[i] = new Vector3(Mathf.Cos(degreeDelta * i), 0, Mathf.Sin(degreeDelta * i));

        }
        setNodePositions();
        lastPosition = transform.position;


        ready = true;
    }
    public virtual void makeNoise() {
        Debug.Log("Noise Made");
        List<DisturbanceListener> listeners = getListenersInRadius();
        Debug.Log("There are " + listeners.Count + " Listeners");
        foreach (DisturbanceListener l in listeners)
        {
            if(!sendSoundFromPosition(l,transform.position))
                sendSoundFromNodes(l);
        }
    }
    private void setNodePositions() {
        ray.origin = transform.position;

        for (int i = 0; i < numOfSoundNodes; i++)
        {
            ray.direction = soundNodeDirections[i];
            if (Physics.Raycast(ray, out hit, range))
            {
                soundNodePositions[i] = hit.point;
            }
            else
                soundNodePositions[i] = transform.position + soundNodeDirections[i] * range;
        }
    }
    private List<DisturbanceListener> getListenersInRadius() {
        List<DisturbanceListener> listeners = new List<DisturbanceListener>();
        Collider[] surroundingObjects = Physics.OverlapSphere(transform.position, range);
        foreach(Collider c in surroundingObjects)
        {
            DisturbanceListener l = c.GetComponent<DisturbanceListener>();
            if (l != null && l.GetComponent<PhaseMember>().isInPhase()&&!listeners.Contains(l))
            {
                listeners.Add(l);
            }
        }
        return listeners;
    }
    private bool sendSoundFromPosition(DisturbanceListener listener, Vector3 origin)
    {
        ray.direction = listener.transform.position - origin;
        ray.origin = origin;
        if (Physics.Raycast(ray, out hit, range))
        {
            DisturbanceListener L = hit.collider.GetComponent<DisturbanceListener>();
            if (L != null && L == listener)
            {
                alertListener(L);
                return true;
            }
        }
        return false;
    }
    private void sendSoundFromNodes(DisturbanceListener listener)
    {

        for (int i = 0; i < soundNodePositions.Length; i++)
        {
            if (sendSoundFromPosition(listener, soundNodePositions[i]))
            {
                Debug.DrawLine(listener.transform.position, soundNodePositions[i],Color.green);
                return;
            }
        }
    }
    private void alertListener(DisturbanceListener L) {
        L.alert(this);
    }
}
