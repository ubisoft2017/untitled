﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;
using UnityEngine.PostProcessing;



public class PostProcessProfileTween : Interpolator<PostProcessingProfile>
{
	[SerializeField]
	private PostProcessingProfile activeProfile;

	private AntialiasingModel.Settings antiAlisaingTempSettings;
	private AmbientOcclusionModel.Settings ambientOcclusionSettings;
	private ScreenSpaceReflectionModel.Settings screenSpaceSettings;
	private DepthOfFieldModel.Settings depthOfFieldSettings;
	private MotionBlurModel.Settings motionBlurSettings;
	private EyeAdaptationModel.Settings eyeAdpationSettings;
	private BloomModel.Settings bloomSettings;
	private ColorGradingModel.Settings colorSettings;
	private UserLutModel.Settings lutSettings;
	private ChromaticAberrationModel.Settings chromaticSettings;
	private GrainModel.Settings grainSettings;
	private VignetteModel.Settings vingetteSettings;

	public void setCurrentProfile ( PostProcessingProfile currentProfile )
	{
		a = currentProfile;
	}

	public void setDesiredProfile ( PostProcessingProfile desiredProfile )
	{
		b = desiredProfile;
	}

	public void LerpProfileSettingsFromTo ( PostProcessingProfile current, PostProcessingProfile desired, float time)
	{
		if ( current.ambientOcclusion.enabled == true && desired.ambientOcclusion.enabled == true ) 
		{
			ambientOcclusionSettings.intensity = Mathf.Lerp( current.ambientOcclusion.settings.intensity, desired.ambientOcclusion.settings.intensity, time );
			ambientOcclusionSettings.radius = Mathf.Lerp( current.ambientOcclusion.settings.radius, desired.ambientOcclusion.settings.radius, time );
			ambientOcclusionSettings.highPrecision = desired.ambientOcclusion.settings.highPrecision;
			ambientOcclusionSettings.downsampling = desired.ambientOcclusion.settings.downsampling;
			ambientOcclusionSettings.forceForwardCompatibility = desired.ambientOcclusion.settings.forceForwardCompatibility;
			ambientOcclusionSettings.ambientOnly = desired.ambientOcclusion.settings.ambientOnly;

			activeProfile.ambientOcclusion.settings = ambientOcclusionSettings;
		}

		if(current.depthOfField.enabled == true && desired.depthOfField.enabled == true )
		{
			depthOfFieldSettings.focusDistance = Mathf.Lerp( current.depthOfField.settings.focusDistance, desired.depthOfField.settings.focusDistance, time );
			depthOfFieldSettings.aperture = Mathf.Lerp( current.depthOfField.settings.aperture, desired.depthOfField.settings.aperture, time );
			depthOfFieldSettings.useCameraFov = desired.depthOfField.settings.useCameraFov;
			depthOfFieldSettings.kernelSize = desired.depthOfField.settings.kernelSize;

			activeProfile.depthOfField.settings = depthOfFieldSettings;
		}

		if(current.bloom.enabled == true && desired.bloom.enabled == true )
		{
			bloomSettings.bloom.intensity = Mathf.Lerp( current.bloom.settings.bloom.intensity, desired.bloom.settings.bloom.intensity, time );
			bloomSettings.bloom.threshold = Mathf.Lerp( current.bloom.settings.bloom.threshold, desired.bloom.settings.bloom.threshold, time );
			bloomSettings.bloom.softKnee = Mathf.Lerp( current.bloom.settings.bloom.softKnee, desired.bloom.settings.bloom.softKnee, time );
			bloomSettings.bloom.radius = Mathf.Lerp( current.bloom.settings.bloom.radius, desired.bloom.settings.bloom.radius, time );
			bloomSettings.bloom.antiFlicker = desired.bloom.settings.bloom.antiFlicker;
			activeProfile.bloom.settings = bloomSettings;
		}

		if(current.chromaticAberration.enabled == true && desired.chromaticAberration.enabled == true )
		{
			chromaticSettings.intensity = Mathf.Lerp( current.chromaticAberration.settings.intensity, desired.chromaticAberration.settings.intensity, time );
			activeProfile.chromaticAberration.settings = chromaticSettings;
		}

		if(current.grain.enabled == true && desired.grain.enabled == true )
		{
			grainSettings.intensity = Mathf.Lerp( current.grain.settings.intensity, desired.grain.settings.intensity, time );
			grainSettings.luminanceContribution = Mathf.Lerp( current.grain.settings.luminanceContribution, desired.grain.settings.luminanceContribution, time );
			grainSettings.size = Mathf.Lerp( current.grain.settings.size, desired.grain.settings.size, time );
			grainSettings.colored = desired.grain.settings.colored;

			activeProfile.grain.settings = grainSettings;
		}

		if(current.vignette.enabled == true && desired.vignette.enabled == true )
		{
			vingetteSettings.mode = desired.vignette.settings.mode;
			vingetteSettings.color = desired.vignette.settings.color;
			vingetteSettings.center = desired.vignette.settings.center;
			vingetteSettings.intensity = Mathf.Lerp( current.vignette.settings.intensity, desired.vignette.settings.intensity, time );
			vingetteSettings.smoothness = Mathf.Lerp( current.vignette.settings.smoothness, desired.vignette.settings.smoothness, time );
			vingetteSettings.roundness = Mathf.Lerp( current.vignette.settings.roundness, desired.vignette.settings.roundness, time );

			activeProfile.vignette.settings = vingetteSettings;
		}
	}

	public override void Interpolate ( float t )
	{
		LerpProfileSettingsFromTo( a, b, t );
	}
}
