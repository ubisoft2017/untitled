﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollow : PathInterface
{
	void Update ()
	{
		transform.position = PositionAt( Time.time );
	}
}
