﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( PlayerAction ), true )]
public class PlayerActionEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var action = target as PlayerAction;

		if ( action is ButtonAction && (action as ButtonAction).ButtonEvent == ButtonEvent.Hold )
		{
			serializedObject.Update();

			EditorGUILayout.PropertyField( serializedObject.FindProperty( "holdTime" ) );
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "repeat" ) );

			serializedObject.ApplyModifiedProperties();
		}

		EditorGUILayout.Space();

		if ( action.ShowAsHint )
		{
			EditorGUILayout.PrefixLabel( "Hint Preview: " );

			var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).GetStyle( "label" );

			var original = style.wordWrap;

			style.wordWrap = true;

			EditorGUILayout.SelectableLabel( action.HintPreview, style );

			style.wordWrap = original;
		}
	}
}
