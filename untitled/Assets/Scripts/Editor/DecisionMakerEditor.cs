﻿using UnityEngine;
using UnityEditor;

using AI;

public class DecisionMakerEditor<T,P> : Editor where P : IDecisionCandidateProvider<T>
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var decisionMaker = target as DecisionMaker<T, P>;

		if ( decisionMaker.HasDecision )
		{
			EditorGUILayout.LabelField( decisionMaker.Provider.GetType() + ": " + decisionMaker.Decision );
		}
		else
		{
			EditorGUILayout.LabelField( Application.isPlaying ? "(none)" : "" );
		}
	}
}

[CustomEditor( typeof( MoveDecisionMaker ), true )]
public class MoveDecisionMakerEditor : DecisionMakerEditor<Vector3, IMoveCandidateProvider> { }

[CustomEditor( typeof( LookDecisionMaker ), true )]
public class LookDecisionMakerEditor : DecisionMakerEditor<Vector3, ILookCandidateProvider> { }

[CustomEditor( typeof( SpeedDecisionMaker ), true )]
public class SpeedDecisionMakerEditor : DecisionMakerEditor<float, ISpeedCandidateProvider> { }
