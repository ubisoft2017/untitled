﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Tweening;
using UnityEditorInternal;

[Serializable]
[CustomEditor( typeof( LevelList ) )]
public class LevelListEditor : Editor
{
	[SerializeField]
	protected SerializedProperty levels, defaultIndex, index;

	[SerializeField]
	private ReorderableList levelList;

	private void OnEnable ()
	{
		levels = serializedObject.FindProperty( "levels" );
		index = serializedObject.FindProperty( "index" );
		defaultIndex = serializedObject.FindProperty( "defaultIndex" );

		levelList = new ReorderableList( serializedObject, levels );

		levelList.draggable = true;
		levelList.drawHeaderCallback = ( rect ) => EditorGUI.LabelField( rect, "Levels" );
		levelList.drawElementCallback = LevelElementCallback;
		levelList.elementHeight = EditorGUIUtility.singleLineHeight * 1.0f;
	}

	public override void OnInspectorGUI ()
	{
		if ( levels == null || levelList == null )
		{
			OnEnable();
		}

		serializedObject.Update();

		EditorGUILayout.Space();

		levelList.DoLayoutList();

		EditorGUILayout.Space();

		EditorGUILayout.PropertyField( index );
		//EditorGUILayout.PropertyField( defaultIndex );

		EditorGUILayout.Space();

		serializedObject.ApplyModifiedProperties();
	}

	private void LevelElementCallback ( Rect position, int index, bool isActive, bool isFocused )
	{
		float height = EditorStyles.objectField.CalcHeight( new GUIContent( "" ), 100.0f );

		position = new Rect( position.x, position.y, position.width, height );

		var level = levels.GetArrayElementAtIndex( index );

		EditorGUI.PropertyField( position, level, GUIContent.none );
	}
}
