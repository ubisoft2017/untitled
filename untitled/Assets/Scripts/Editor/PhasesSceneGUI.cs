﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public static class PhasesSceneGUI
{
	[InitializeOnLoadMethod]
	private static void Init ()
	{
		SceneView.onSceneGUIDelegate += OnSceneGUI;

		EditorApplication.update += Update;
	}

	private static void Update ()
	{
		if ( !Application.isPlaying )
		{
			foreach ( var phase in UnityEngine.Object.FindObjectsOfType<Phase>() )
			{
				phase.CalculateLocalTime( 0.0f );
			}
		}
	}

	private static void MenuItemEditPhase ( object argument )
	{
		Selection.activeGameObject = (argument as Phase).gameObject;
	}

	private static void MenuItemRemovePhase ( object argument )
	{
		var phase = argument as Phase;

		if ( EditorUtility.DisplayDialog(
			"Confirm Phase Removal",
			"Please confirm that you want to remove " + phase.name + ". All phase-specific objects will be removed in the process.",
			"Ok",
			"Cancel" ) )
		{
			Undo.DestroyObjectImmediate( phase.gameObject );
		}
	}

	private static void OnSceneGUI ( SceneView view )
	{
		Phases phases = UnityEngine.Object.FindObjectOfType<Phases>();

		if ( phases != null )
		{
			Handles.BeginGUI();

			DrawAddButton( view, phases );

			int count = CountPhases( phases );

			for ( int i = 0; i < phases.transform.childCount; i++ )
			{
				var phase = phases.transform.GetChild( i ).GetComponent<Phase>();

				if ( phase != null )
				{
					count++;
				}
			}

			if ( count > 0 )
			{
				DrawPhaseButtons( view, phases );
			}

			Handles.EndGUI();
		}
	}

	private static int CountPhases ( Phases phases )
	{
		int n = 0;

		for ( int i = 0; i < phases.transform.childCount; i++ )
		{
			var phase = phases.transform.GetChild( i ).GetComponent<Phase>();

			if ( phase != null )
			{
				n++;
			}
		}

		return n;
	}

	private static void DrawAddButton ( SceneView view, Phases phases )
	{
		float height = EditorGUIUtility.singleLineHeight;
		float width = 30.0f;

		float y = view.camera.pixelRect.height - height - 28.0f;
		float x = view.camera.pixelRect.width - width - 5.0f;

		//var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "OL Plus" );

		//if ( GUI.Button( new Rect( x, y, size, size ), GUIContent.none, style ) )
		if ( GUI.Button( new Rect( x, y, width, height ), new GUIContent( "new" ), EditorStyles.miniButton ) )
		{
			new GameObject( "Phase" ).AddComponent<Phase>().transform.SetParent( phases.transform );
		}
	}

	private static void DrawPhaseButtons ( SceneView view, Phases phases )
	{
		float width = Mathf.Ceil( view.camera.pixelRect.width / phases.transform.childCount );

		for ( int i = 0; i < phases.transform.childCount; i++ )
		{
			var phase = phases.transform.GetChild( i ).GetComponent<Phase>();

			if ( phase != null )
			{
				float height = EditorGUIUtility.singleLineHeight * 1.5f;

				if ( phase.IsActiveInEditor )
				{
					phase.CalculateLocalTime( 0.0f );

					var sun = GameObject.FindGameObjectWithTag( "Sun" );

					if ( sun != null )
					{
						sun.transform.localRotation = Quaternion.Euler( Phase.SunPitch( phase.LocalTime ), 0.0f, 0.0f );
					}

					GUI.backgroundColor = new Color( 0.8f, 0.8f, 0.8f );
				}
				else
				{
					GUI.backgroundColor = new Color( 0.5f, 0.5f, 0.5f );
					GUI.contentColor = new Color( 1, 1, 1, 0.5f );
					height -= 2.0f;
				}

				float y = view.camera.pixelRect.height - height;

				var rect = new Rect( width * i, y, width, height );

				if ( rect.Contains( Event.current.mousePosition ) && Event.current.type == EventType.MouseDown && Event.current.button == 1 )
				{
					GenericMenu menu = new GenericMenu();

					menu.AddItem( new GUIContent( "Edit" ), false, MenuItemEditPhase, phase );

					menu.AddSeparator( "" );

					if ( Application.isPlaying )
					{
						menu.AddDisabledItem( new GUIContent( "Remove" ) );
					}
					else
					{
						menu.AddItem( new GUIContent( "Remove" ), false, MenuItemRemovePhase, phase );
					}

					menu.ShowAsContext();

					Event.current.Use();
				}

				string terse = string.IsNullOrEmpty( phase.Label ) ? "" : (phase.Label.Length > 20 ? phase.Label.Substring( 0, 17 ) + "..." : phase.Label) + " ";

				string label = terse + string.Format( "({0})", (phase.HourOffset > 0 ? "+" : "") + phase.HourOffset );

				if ( GUI.Button( rect, new GUIContent( label ), EditorStyles.miniButtonMid ) )
				{
					for ( int j = 0; j < phases.transform.childCount; j++ )
					{
						if ( j != i )
						{
							var hide = phases.transform.GetChild( j ).GetComponent<Phase>();

							if ( hide != null )
							{
								hide.Hide();
							}
						}
					}

					Selection.activeGameObject = phase.gameObject;

					phase.Show();
				}

				GUI.contentColor = Color.white;
				GUI.backgroundColor = Color.white;
			}
		}
	}
}
