﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using Controls;
using System.Collections.Generic;

[CustomEditor( typeof( PlayerState ), true )]
public class PlayerStateEditor : Editor
{
	[SerializeField]
	private SerializedProperty actions, includes;

	[SerializeField]
	private ReorderableList actionList, includeList;

	private void OnEnable ()
	{
		actions = serializedObject.FindProperty( "actions" );
		includes = serializedObject.FindProperty( "includes" );

		actionList = new ReorderableList( serializedObject, actions );
		includeList = new ReorderableList( serializedObject, includes );

		actionList.drawHeaderCallback = ( p ) => EditorGUI.LabelField( p, actions.displayName );
		includeList.drawHeaderCallback = ( p ) => EditorGUI.LabelField( p, includes.displayName );

		actionList.drawElementCallback = ( p, i, a, f ) => DrawElementCallback( p, i, a, f, actions );
		includeList.drawElementCallback = ( p, i, a, f ) => DrawElementCallback( p, i, a, f, includes );

		actionList.onAddCallback = ( l ) => AddCallback( l, Resources.LoadAll<PlayerAction>( PlayerAction.RESOURCE_PATH ) );
		includeList.onAddCallback = ( l ) => AddCallback( l, Resources.LoadAll<PlayerState>( PlayerState.RESOURCE_PATH ) );

		actionList.draggable = false;
	}

	private void DrawElementCallback ( Rect position, int index, bool isActive, bool isFocused, SerializedProperty property )
	{
		var element = property.GetArrayElementAtIndex( index );

		if ( element.objectReferenceValue != null )
		{
			var content = new GUIContent( element.objectReferenceValue.name );

			EditorGUI.LabelField( position, content );
		}
		else
		{
			var content = new GUIContent( "(missing)", "Please remove this list item to prevent runtime errors." );

			EditorGUI.LabelField( position, content );
		}
	}

	private void AddItem ( object arg )
	{
		serializedObject.Update();

		var args = arg as object[];

		var list = args[0] as ReorderableList;
		var item = args[1] as UnityEngine.Object;

		int index = list.serializedProperty.arraySize;

		list.serializedProperty.InsertArrayElementAtIndex( index );
		list.serializedProperty.GetArrayElementAtIndex( index ).objectReferenceValue = item;

		serializedObject.ApplyModifiedProperties();
	}

	private void AddCallback<T> ( ReorderableList list, IEnumerable<T> source ) where T : UnityEngine.Object
	{
		GenericMenu menu = new GenericMenu();

		foreach ( var element in source )
		{
			menu.AddItem( new GUIContent( element.name ), false, AddItem, new object[] { list, element } );
		}

		menu.ShowAsContext();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		actionList.DoLayoutList();
		includeList.DoLayoutList();

		serializedObject.ApplyModifiedProperties();
	}
}
