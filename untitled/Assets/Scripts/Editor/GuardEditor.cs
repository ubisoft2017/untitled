﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( Guard ) )]
public class GuardEditor : NodeEditor
{
	[SerializeField] private bool place;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		if ( Application.isPlaying )
		{
			var guard = target as Guard;

			if ( GUILayout.RepeatButton( "Start Disrupt" ) )
			{
				guard.StartDisrupting();
			}

			if ( GUILayout.RepeatButton( "Stop Disrupt" ) )
			{
				guard.StopDisrupting();
			}
		}
		else
		{
			GUILayout.Label( "" );
		}

		DrawNodeInspector();
	}
}
