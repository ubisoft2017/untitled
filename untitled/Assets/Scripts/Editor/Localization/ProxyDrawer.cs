﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

using Localization;

[CustomPropertyDrawer( typeof( Proxy ) )]
public class ProxyDrawer : PropertyDrawer
{
	private static float height;

	static ProxyDrawer ()
	{
		height = EditorGUIUtility.singleLineHeight;
	}

	public override float GetPropertyHeight ( SerializedProperty property, GUIContent label )
	{
		if ( property.FindPropertyRelative( "adapter" ).objectReferenceValue == null )
		{
			return height * 1.0f;
		}
		else
		{
			//var headerProperty = property.FindPropertyRelative( "header" );

			//var adapterProperty = property.FindPropertyRelative( "adapter" );

			//var adapter = adapterProperty.objectReferenceValue as IAdapter;

			//EditorStyles.wordWrappedLabel.CalcHeight( inspec )
			//EditorGUILayout.LabelField( "Preview: ", adapter.Query( headerProperty.objectReferenceValue as RowHeader ), EditorStyles.wordWrappedLabel );

			return height * 2.0f;
		}
	}

	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		var adapterRect = new Rect( position.x, position.y, position.width, height );

		var adapterProperty = property.FindPropertyRelative( "adapter" );

		EditorGUI.PropertyField( adapterRect, adapterProperty );

		if ( adapterProperty.objectReferenceValue != null )
		{
			var headerRect = new Rect( position.x, position.y + height, position.width, height );

			var headerProperty = property.FindPropertyRelative( "header" );

			var adapter = adapterProperty.objectReferenceValue as IAdapter;

			if ( adapter.Table.Rows.Count > 0 )
			{
				var displayedOptions = new GUIContent[adapter.Table.Rows.Count];

				int selectedIndex = 0;

				for ( int i = 0; i < adapter.Table.Rows.Count; i++ )
				{
					displayedOptions[i] = new GUIContent( adapter.Table.Rows[i].Title );

					if ( headerProperty.objectReferenceValue == adapter.Table.Rows[i] )
					{
						selectedIndex = i;
					}
				}

				GUIContent culture = new GUIContent( "Row" );

				headerProperty.objectReferenceValue = adapter.Table.Rows[EditorGUI.Popup( headerRect, culture, selectedIndex, displayedOptions )];

				//EditorGUILayout.LabelField( "Preview: ", adapter.Query( headerProperty.objectReferenceValue as RowHeader ), EditorStyles.wordWrappedLabel );
			}
			else
			{
				EditorGUI.LabelField( position, label, new GUIContent( "(no options found)" ) );
			}
		}
	}
}