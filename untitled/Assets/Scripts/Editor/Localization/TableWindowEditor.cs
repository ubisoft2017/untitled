﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Localization;

[Serializable]
public class TableWindowEditor : TableEditorBase
{
	[SerializeField]
	private Vector2 scroll;

	[SerializeField]
	private List<DataWindow> editors;

	GenericMenu menu;

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		var table = (target as Table);

		var backgroundStyle = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "CurveEditorBackground" );
		
		scroll = GUILayout.BeginScrollView( scroll, true, true );
		{
			GUILayout.BeginVertical( backgroundStyle, GUILayout.ExpandHeight( false ) );
			{
				var headerStyle = new GUIStyle( EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "toolbarbutton" ) );
				var cellStyle = new GUIStyle( EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreferencesSectionBox" ) );

				cellStyle.fixedHeight = headerStyle.fixedHeight;
				cellStyle.padding = new RectOffset( 2, 2, 2, 2 );

				Color cellTextLocal = new Color( 0.9f, 0.9f, 0.9f );
				Color cellTextRemote = new Color( 0.9f, 0.9f, 0.9f, 0.5f );

				Color cellBackLocal = new Color( 0.7f, 0.7f, 0.7f );
				Color cellBackRemote = new Color( 0.8f, 0.8f, 1.0f );
				
				cellStyle.normal.textColor = Color.white;
				cellStyle.active.textColor = Color.white;
				cellStyle.focused.textColor = Color.white;
				cellStyle.hover.textColor = Color.white;
				cellStyle.onNormal.textColor = Color.white;
				cellStyle.onActive.textColor = Color.white;
				cellStyle.onFocused.textColor = Color.white;
				cellStyle.onHover.textColor = Color.white;

				float width = 200.0f;

				ColumnHeader[] remotes = new ColumnHeader[columns.arraySize];

				GUILayout.BeginHorizontal();
				{
					if ( GUILayout.Button( "", headerStyle, GUILayout.Width( width ) ) )
					{
						Selection.activeObject = table;
					}

					for ( int j = 0; j < columns.arraySize; j++ )
					{
						var obj = serializedObject;

						var reference = columns.GetArrayElementAtIndex( j ).objectReferenceValue;

						var header = columns.GetArrayElementAtIndex( j ).objectReferenceValue as ColumnHeader;

						var content = new GUIContent( header.DisplayName, header.Remote == null ? "" : "(remote: " + header.Remote.DisplayName + ")" );

						var rect = GUILayoutUtility.GetRect( content, headerStyle, GUILayout.Width( width ) );

						remotes[j] = header.Remote;

						if ( rect.Contains( Event.current.mousePosition ) && Event.current.type == EventType.MouseDown && Event.current.button == 1 )
						{
							GenericMenu menu = new GenericMenu();

							menu.AddItem( new GUIContent( "Remove" ), false, MenuItemRemoveColumn, header );

							menu.ShowAsContext();

							Event.current.Use();
						}

						if ( GUI.Button( rect, content, headerStyle ) )
						{
							Selection.activeObject = header;
						}
					}
				}
				GUILayout.EndHorizontal();

				headerStyle.alignment = TextAnchor.MiddleLeft;

				for ( int i = 0; i < rows.arraySize; i++ )
				{
					GUILayout.BeginHorizontal();
					{
						var reference = rows.GetArrayElementAtIndex( i ).objectReferenceValue;

						var header = rows.GetArrayElementAtIndex( i ).objectReferenceValue as RowHeader;

						var content = new GUIContent( header.Title );

						var rect = GUILayoutUtility.GetRect( content, headerStyle, GUILayout.Width( width ) );

						if ( rect.Contains( Event.current.mousePosition ) && Event.current.type == EventType.MouseDown && Event.current.button == 1 )
						{
							GenericMenu menu = new GenericMenu();

							menu.AddItem( new GUIContent( "Remove" ), false, MenuItemRemoveRow, header );

							menu.ShowAsContext();

							Event.current.Use();
						}

						if ( GUI.Button( rect, content, headerStyle ) )
						{
							Selection.activeObject = header;
						}

						for ( int j = 0; j < columns.arraySize; j++ )
						{
							var cell = cells.GetArrayElementAtIndex( Table.FlatIndex( i, j, rows.arraySize, columns.arraySize ) );

							var flags = (Data.MetaFlags) (byte) cell.FindPropertyRelative( "flags" ).intValue;

							bool remote = (flags & Data.MetaFlags.Remote) != Data.MetaFlags.None;

							bool valid = remotes[j] != null;

							GUI.contentColor = remote && valid ? cellTextRemote : cellTextLocal;
							GUI.backgroundColor = remote && valid ? cellBackRemote : cellBackLocal;

							var text = table.Query( i, j );

							content = new GUIContent( text, text );

							rect = GUILayoutUtility.GetRect( content, cellStyle, GUILayout.Width( width ) );

							if ( rect.Contains( Event.current.mousePosition ) && Event.current.type == EventType.MouseDown && Event.current.button == 1 )
							{
								GenericMenu menu = new GenericMenu();

								if ( !valid )
								{
									menu.AddDisabledItem( new GUIContent( "Remote" ) );
								}
								else
								{
									menu.AddItem( new GUIContent( "Remote" ), remote, MenuItemRemote, cell );
								}

								menu.AddItem( new GUIContent( "Local" ), !remote || !valid, MenuItemLocal, cell );

								menu.ShowAsContext();

								Event.current.Use();
							}

							if ( GUI.Button( rect, content, cellStyle ) )
							{
								if ( editors == null )
								{
									editors = new List<DataWindow>();
								}

								editors.Add( DataWindow.EditData
									(
									() =>
									{
										serializedObject.Update();
										return cell.FindPropertyRelative( "localString" ).stringValue;
									},
									( s ) =>
									{
										serializedObject.Update();
										cell.FindPropertyRelative( "localString" ).stringValue = s;
										serializedObject.ApplyModifiedProperties();
									},
									( e ) =>
									{
										editors.Remove( e );

										if ( e.Save )
										{
											SetRemote( cell, false );
										}
									},
									header,
									columns.GetArrayElementAtIndex( j ).objectReferenceValue as ColumnHeader
									) );
							}
						}

						GUI.contentColor = Color.white;
						GUI.backgroundColor = Color.white;
					}
					GUILayout.EndHorizontal();
				}

				GUILayout.FlexibleSpace();
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndScrollView();

		if ( Event.current.type == EventType.MouseDown && Event.current.button == 0 )
		{
			Selection.activeObject = table;
		}

		serializedObject.ApplyModifiedProperties();
	}

	private void MenuItemRemoveRow ( object argument )
	{
		var header = argument as RowHeader;

		if ( EditorUtility.DisplayDialog
			(
			"Localization Table",
			"This will permanently remove the row '" + header.Title + "' and all of its cells from the table. Continue?",
			"Yes", "No"
			) )
		{
			RemoveRow( header );
		}
	}

	private void MenuItemRemoveColumn ( object argument )
	{
		var header = argument as ColumnHeader;

		if ( EditorUtility.DisplayDialog
			(
			"Localization Table",
			"This will permanently remove the column '" + header.DisplayName + "' and all of its cells from the table. Continue?",
			"Yes", "No"
			) )
		{
			RemoveColumn( header );
		}
	}

	private void MenuItemRemote ( object argument )
	{
		SetRemote( argument as SerializedProperty, true );
	}

	private void MenuItemLocal ( object argument )
	{
		SetRemote( argument as SerializedProperty, false );
	}

	private void OnDestroy ()
	{
		if ( editors != null )
		{
			int s = 0;

			while ( editors.Count > 0 )
			{
				editors[0].Close();

				if ( s++ > 100 )
				{
					Debug.LogError( "Broken!" );
				}
			}
		}
	}
}
