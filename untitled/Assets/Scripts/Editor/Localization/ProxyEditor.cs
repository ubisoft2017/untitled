﻿//using System;
//using System.Globalization;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using UnityEngine;
//using UnityEditor;

//using Localization;

//[Serializable]
//[CustomEditor( typeof( ProxyObject ) )]
//public class ProxyEditor : Editor
//{
//	[SerializeField]
//	protected SerializedProperty adapter;

//	[SerializeField]
//	protected SerializedProperty header;

//	private void OnEnable ()
//	{
//		adapter = serializedObject.FindProperty( "adapter" );

//		header = serializedObject.FindProperty( "header" );
//	}

//	public override void OnInspectorGUI ()
//	{
//		serializedObject.Update();

//		EditorGUILayout.PropertyField( this.adapter );

//		var adapter = this.adapter.objectReferenceValue as AdapterObject;

//		if ( adapter != null )
//		{
//			var table = adapter.Table;

//			if ( table != null )
//			{
//				if ( table.Rows.Count > 0 )
//				{
//					var displayedOptions = new GUIContent[adapter.Table.Rows.Count];

//					int selectedIndex = 0;

//					for ( int i = 0; i < table.Rows.Count; i++ )
//					{
//						displayedOptions[i] = new GUIContent( table.Rows[i].Title );

//						if ( header.objectReferenceValue == table.Rows[i] )
//						{
//							selectedIndex = i;
//						}
//					}

//					GUIContent culture = new GUIContent( "Row" );

//					header.objectReferenceValue = table.Rows[EditorGUILayout.Popup( culture, selectedIndex, displayedOptions )];

//					EditorGUILayout.LabelField( "Preview: ", adapter.Query( header.objectReferenceValue as RowHeader ), EditorStyles.wordWrappedLabel );
//				}
//				else
//				{
//					EditorGUILayout.LabelField( new GUIContent( "(no options found)" ) );
//				}
//			}
//		}

//		serializedObject.ApplyModifiedProperties();
//	}
//}
