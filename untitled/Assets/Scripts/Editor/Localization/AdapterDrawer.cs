﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

using Localization;

[CustomPropertyDrawer( typeof( Adapter ) )]
public class AdapterDrawer : PropertyDrawer
{
	private static float height;

	static AdapterDrawer ()
	{
		height = EditorGUIUtility.singleLineHeight;
	}

	public override float GetPropertyHeight ( SerializedProperty property, GUIContent label )
	{
		if ( property.FindPropertyRelative( "table" ).objectReferenceValue == null )
		{
			return height * 1.0f;
		}
		else
		{
			return height * 2.0f;
		}
	}

	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		var tableRect = new Rect( position.x, position.y, position.width, height );

		var tableProperty = property.FindPropertyRelative( "table" );

		EditorGUI.PropertyField( tableRect, tableProperty );

		if ( tableProperty.objectReferenceValue != null )
		{
			var headerRect = new Rect( position.x, position.y + height, position.width, height );

			var headerProperty = property.FindPropertyRelative( "header" );

			var table = tableProperty.objectReferenceValue as Table;

			if ( table.Columns.Count > 0 )
			{
				var displayedOptions = new GUIContent[table.Columns.Count];

				int selectedIndex = 0;

				for ( int i = 0; i < table.Columns.Count; i++ )
				{
					displayedOptions[i] = new GUIContent( table.Columns[i].DisplayName );

					if ( headerProperty.objectReferenceValue == table.Columns[i] )
					{
						selectedIndex = i;
					}
				}

				GUIContent culture = new GUIContent( "Culture" );

				headerProperty.objectReferenceValue = table.Columns[EditorGUI.Popup( headerRect, culture, selectedIndex, displayedOptions )];
			}
			else
			{
				EditorGUI.LabelField( position, label, new GUIContent( "(no options found)" ) );
			}
		}
	}
}