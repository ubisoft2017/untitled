﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Localization;

[Serializable]
[CustomEditor( typeof( Table ) )]
public class TableInspectorEditor : TableEditorBase
{
	protected override void OnHeaderGUI ()
	{
		EditorGUILayout.BeginVertical( EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "LockedHeaderBackground" ) );

		EditorGUILayout.Separator();

		var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreLabel" );

		if ( GUILayout.Button( new GUIContent( target.name ), style ) )
		{
			EditorGUIUtility.PingObject( target );
		}

		EditorGUILayout.EndVertical();

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		EditorGUILayout.PropertyField( title, GUIContent.none );
		
		EditorGUILayout.Separator();

		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.LabelField( "Rows: " + rows.arraySize, EditorStyles.miniLabel );

			if ( GUILayout.Button( "Add", EditorStyles.miniButtonLeft ) )
			{
				ButtonAddRow();
			}

			if ( GUILayout.Button( "Clear", EditorStyles.miniButtonRight ) )
			{
				ButtonClearRows();
			}
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		{
			EditorGUILayout.LabelField( "Columns: " + columns.arraySize, EditorStyles.miniLabel );

			if ( GUILayout.Button( "Add", EditorStyles.miniButtonLeft ) )
			{
				ButtonAddColumn();
			}

			if ( GUILayout.Button( "Clear", EditorStyles.miniButtonRight ) )
			{
				ButtonClearColumns();
			}
		}
		EditorGUILayout.EndHorizontal();

		//EditorGUILayout.BeginHorizontal();
		//{
		//	EditorGUILayout.LabelField( "Cells: " + cells.arraySize, EditorStyles.miniLabel );

		//	if ( GUILayout.Button( "Clear", EditorStyles.miniButton ) )
		//	{
		//		cells.ClearArray();
		//	}
		//}
		//EditorGUILayout.EndHorizontal();

		//EditorGUILayout.BeginHorizontal();
		//{
		//	EditorGUILayout.LabelField( "Assets: " + AssetDatabase.LoadAllAssetsAtPath( AssetDatabase.GetAssetPath( target ) ).Length, EditorStyles.miniLabel );

		//	if ( GUILayout.Button( "Clear", EditorStyles.miniButton ) )
		//	{
		//		foreach ( var asset in AssetDatabase.LoadAllAssetsAtPath( AssetDatabase.GetAssetPath( target ) ) )
		//		{
		//			if ( asset != target )
		//			{
		//				DestroyImmediate( asset, true );

		//				AssetDatabase.Refresh();
		//			}
		//		}
		//	}
		//}
		//EditorGUILayout.EndHorizontal();

		//if ( GUILayout.Button( "Trim" ) )
		//{
		//	Trim();
		//}

		EditorGUILayout.Separator();

		serializedObject.ApplyModifiedProperties();
	}

	private void ButtonAddRow ()
	{
		AddRow();
	}

	private void ButtonAddColumn ()
	{
		GenericMenu menu = new GenericMenu();

		foreach ( var culture in cultures )
		{
			menu.AddItem( new GUIContent( culture.DisplayName ), false, ContextMenuAddCulumn, culture );
		}

		menu.ShowAsContext();
	}

	private void ContextMenuAddCulumn ( object argument )
	{
		AddColumn( argument as CultureInfo );
	}

	private void ButtonClearRows ()
	{
		if ( EditorUtility.DisplayDialog
			(
			"Localization Table",
			"This will permanently remove all of the rows and their cells from this table. Continue?",
			"Yes", "No"
			) )
		{
			ClearRows();
		}
	}

	private void ButtonClearColumns ()
	{
		if ( EditorUtility.DisplayDialog
			(
			"Localization Table",
			"This will permanently remove all of the columns and their cells from this table. Continue?",
			"Yes", "No"
			) )
		{
			ClearColumns();
		}
	}
}
