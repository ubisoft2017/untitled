﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Localization;

[Serializable]
[CustomEditor( typeof( RowHeader ) )]
public class RowEditor : Editor
{
	[SerializeField]
	protected SerializedProperty title;

	[SerializeField]
	protected SerializedProperty table;

	private void OnEnable ()
	{
		title = serializedObject.FindProperty( "title" );

		table = serializedObject.FindProperty( "table" );
	}

	protected override void OnHeaderGUI ()
	{
		EditorGUILayout.BeginVertical( EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "LockedHeaderBackground" ) );

		EditorGUILayout.Separator();

		var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreLabel" );

		if ( GUILayout.Button( new GUIContent( table.objectReferenceValue.name ), style ) )
		{
			EditorGUIUtility.PingObject( table.objectReferenceValue );
		}

		EditorGUILayout.EndVertical();

		EditorGUILayout.Separator();

		style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "ButtonMid" );

		if ( GUILayout.Button( "Back", style ) )
		{
			Selection.activeObject = table.objectReferenceValue;
		}

		EditorGUILayout.Separator();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		EditorGUILayout.PropertyField( title, GUIContent.none );

		EditorGUILayout.Separator();

		serializedObject.ApplyModifiedProperties();
	}
}
