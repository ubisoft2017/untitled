﻿using System;

using UnityEngine;
using UnityEditor;

using Localization;

[Serializable]
public class TableWindow : EditorWindow
{
	[MenuItem( "Window/Localization" )]
	private static void MenuItemLocalization ()
	{
		GetWindow<TableWindow>();
	}

	[SerializeField] private TableWindowEditor editor;
	[SerializeField] private Table table;

	private void OnInspectorUpdate ()
	{
		Repaint();
	}

	private void OnEnable ()
	{
		titleContent = new GUIContent( "Localization", "Edit localized string tables." );
	}

	private void OnGUI ()
	{
		try
		{
			if ( Selection.activeObject is Table )
			{
				if ( table == null )
				{
					table = Selection.activeObject as Table;
				}
				else if ( Selection.activeObject != table )
				{
					table = null;
				}
			}

			if ( table == null )
			{
				editor = null;
			}
			else if ( editor == null )
			{
				editor = Editor.CreateEditor( table, typeof( TableWindowEditor ) ) as TableWindowEditor;
			}

			Rect rect = new Rect( 0.0f, 0.0f, position.width, position.height );

			string prompt = "";

			if ( rect.Contains( Event.current.mousePosition ) )
			{
				if ( DragAndDrop.objectReferences.Length == 1 )
				{
					var table = DragAndDrop.objectReferences[0] as Table;

					if ( table != null )
					{
						if ( Event.current.type == EventType.DragPerform )
						{
							editor = null;

							this.table = table;

							DragAndDrop.AcceptDrag();
						}

						DragAndDrop.visualMode = DragAndDropVisualMode.Link;
					}
					else
					{
						prompt = "Data type not supported.";

						DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
					}
				}
				else if ( DragAndDrop.objectReferences.Length > 0 )
				{
					prompt = "Multi-editing not supported.";

					DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
				}
				else
				{
					prompt = "Drop a Localization Table here to start editing.";
				}
			}
			else
			{
				prompt = "Drop a Localization Table here to start editing.";
			}

			if ( editor == null )
			{
				var promptStyle = EditorStyles.centeredGreyMiniLabel;

				GUI.Label( rect, new GUIContent( prompt ), promptStyle );
			}
			else
			{
				var tabStyle = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "dragtabdropwindow" );

				GUI.Box( rect, new GUIContent( "Table" ), tabStyle );

				float pad = 17.0f;

				rect = new Rect( rect.x, rect.y + pad, rect.width, rect.height - pad );

				GUILayout.BeginArea( rect );

				editor.OnInspectorGUI();

				GUILayout.EndArea();
			}
		}
		catch ( ArgumentException ) { }
	}
}
