﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Localization;
using UnityEditorInternal;

[Serializable]
[CustomEditor( typeof( ColumnHeader ) )]
public class ColumnEditor : Editor
{
	[SerializeField]
	protected SerializedProperty remote;

	[SerializeField]
	protected SerializedProperty table;

	[SerializeField]
	protected SerializedProperty fonts;

	[SerializeField]
	protected ReorderableList list;

	private void OnEnable ()
	{
		remote = serializedObject.FindProperty( "remote" );

		table = serializedObject.FindProperty( "table" );

		fonts = serializedObject.FindProperty( "fonts" );

		list = new ReorderableList( serializedObject, fonts );

		list.drawHeaderCallback = None;
		list.drawHeaderCallback = None;
		list.drawElementBackgroundCallback = DrawElementBackgroundCallback;
		list.drawElementCallback = DrawElementCallback;

		list.showDefaultBackground = false;
		list.displayAdd = false;
		list.displayRemove = false;

		list.headerHeight = 0.0f;
		list.footerHeight = 0.0f;
	}

	private void None ( Rect position )
	{
		//EditorGUI.LabelField( position, new GUIContent( "Font Compatibility" ) );
	}

	private void DrawElementBackgroundCallback ( Rect position, int index, bool isActive, bool isFocused )
	{
		if ( isActive || isFocused )
		{
			GUI.backgroundColor = Color.grey;

			GUI.Box( position, GUIContent.none, EditorStyles.helpBox );

			GUI.backgroundColor = Color.white;
		}
	}

	private void DrawElementCallback ( Rect position, int index, bool isActive, bool isFocused )
	{
		var rect = new Rect( position.x, position.y + 2.0f, position.width, EditorGUIUtility.singleLineHeight );

		EditorGUI.PropertyField( rect, fonts.GetArrayElementAtIndex( index ), GUIContent.none, false );
	}

	protected override void OnHeaderGUI ()
	{
		EditorGUILayout.BeginVertical( EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "LockedHeaderBackground" ) );

		EditorGUILayout.Separator();

		var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreLabel" );

		if ( GUILayout.Button( new GUIContent( table.objectReferenceValue.name ), style ) )
		{
			EditorGUIUtility.PingObject( table.objectReferenceValue );
		}

		EditorGUILayout.EndVertical();

		EditorGUILayout.Separator();

		style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "ButtonMid" );

		if ( GUILayout.Button( "Back", style ) )
		{
			Selection.activeObject = table.objectReferenceValue;
		}

		EditorGUILayout.Separator();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		EditorGUILayout.LabelField( serializedObject.FindProperty( "displayName" ).stringValue, EditorStyles.boldLabel );

		var remote = this.remote.objectReferenceValue as ColumnHeader;
		var table = this.table.objectReferenceValue as Table;

		var options = new List<GUIContent>( table.Columns.Count );
		var headers = new List<ColumnHeader>( table.Columns.Count );

		options.Add( new GUIContent( "None" ) );
		headers.Add( null );

		int index = 0;

		for ( int i = 0; i < table.Columns.Count; i++ )
		{
			if ( table.Columns[i] == target )
			{
				continue;
			}

			options.Add( new GUIContent( table.Columns[i].DisplayName ) );
			headers.Add( table.Columns[i] );

			if ( table.Columns[i] == remote )
			{
				index = options.Count - 1;
			}
		}

		index = EditorGUILayout.Popup( new GUIContent( "Remote", "Use this to override entries from another column." ), index, options.ToArray() );

		this.remote.objectReferenceValue = headers[index];

		EditorGUILayout.Separator();

		EditorGUILayout.LabelField( new GUIContent( "Compatible Fonts", "Collection of fonts whose character sets support the selected language." ), EditorStyles.centeredGreyMiniLabel );

		list.DoLayoutList();

		EditorGUILayout.BeginHorizontal();

		if ( GUILayout.Button( "Add", EditorStyles.miniButtonLeft ) )
		{
			fonts.InsertArrayElementAtIndex( fonts.arraySize );
		}

		if ( GUILayout.Button( "Remove", EditorStyles.miniButtonRight ) )
		{
			if ( list.index > -1 )
			{
				int size = fonts.arraySize;

				fonts.DeleteArrayElementAtIndex( list.index );

				if ( fonts.arraySize == size )
				{
					fonts.DeleteArrayElementAtIndex( list.index );
				}
			}
		}

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Separator();

		serializedObject.ApplyModifiedProperties();
	}
}