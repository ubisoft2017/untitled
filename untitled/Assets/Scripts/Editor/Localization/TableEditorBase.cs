﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Localization;

[Serializable]
[InitializeOnLoad]
public class TableEditorBase : Editor
{
	protected static CultureInfo[] cultures;

	static TableEditorBase ()
	{
		cultures = CultureInfo.GetCultures( CultureTypes.AllCultures );
	}

	[SerializeField]
	protected SerializedProperty title;

	[SerializeField]
	protected SerializedProperty cells;

	[SerializeField]
	protected SerializedProperty rows;

	[SerializeField]
	protected SerializedProperty columns;

	private void OnEnable ()
	{
		if ( target == null )
		{
			return;
		}

		title = serializedObject.FindProperty( "title" );

		cells = serializedObject.FindProperty( "cells" );

		rows = serializedObject.FindProperty( "rows" );
		columns = serializedObject.FindProperty( "columns" );
	}

	protected void Trim ()
	{
		for ( int i = 0; i < rows.arraySize; i++ )
		{
			if ( rows.GetArrayElementAtIndex( i ).objectReferenceValue == null )
			{
				int size = rows.arraySize;

				while ( rows.arraySize == size )
				{
					rows.DeleteArrayElementAtIndex( i );
				}

				i--;
			}
		}

		for ( int i = 0; i < columns.arraySize; i++ )
		{
			if ( columns.GetArrayElementAtIndex( i ).objectReferenceValue == null )
			{
				int size = rows.arraySize;

				while ( columns.arraySize == size )
				{
					columns.DeleteArrayElementAtIndex( i );
				}

				i--;
			}
		}

		int mult = rows.arraySize * columns.arraySize;

		while ( cells.arraySize > mult )
		{
			cells.DeleteArrayElementAtIndex( mult );
		}
	}

	protected void SetRemote ( SerializedProperty cell, bool remote )
	{
		serializedObject.Update();

		var flags = cell.FindPropertyRelative( "flags" );

		if ( remote )
		{
			flags.intValue = flags.intValue | (int) Data.MetaFlags.Remote;
		}
		else
		{
			flags.intValue = flags.intValue & (~(int) Data.MetaFlags.Remote);
		}

		serializedObject.ApplyModifiedProperties();
	}

	protected void AddRow ()
	{
		serializedObject.Update();

		for ( int i = 0; i < columns.arraySize; i++ )
		{
			int index = cells.arraySize;

			cells.InsertArrayElementAtIndex( index );

			cells.GetArrayElementAtIndex( index ).FindPropertyRelative( "localString" ).stringValue = "";

			if ( (columns.GetArrayElementAtIndex( i ).objectReferenceValue as ColumnHeader).Remote == null )
			{
				cells.GetArrayElementAtIndex( index ).FindPropertyRelative( "flags" ).intValue = 0;
			}
			else
			{
				cells.GetArrayElementAtIndex( index ).FindPropertyRelative( "flags" ).intValue = (int) Data.MetaFlags.Remote;
			}
		}

		var row = RowHeader.CreateInstance( target as Table, rows.arraySize, "Row " + rows.arraySize );

		row.hideFlags = HideFlags.HideInHierarchy;

		rows.InsertArrayElementAtIndex( rows.arraySize );

		AssetDatabase.AddObjectToAsset( row, target );

		rows.GetArrayElementAtIndex( rows.arraySize - 1 ).objectReferenceValue = row;

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		AssetDatabase.Refresh();

		AssetDatabase.SaveAssets();
	}

	protected void AddColumn ( CultureInfo culture )
	{
		serializedObject.Update();

		for ( int i = 0; i < rows.arraySize; i++ )
		{
			int index = columns.arraySize * (i + 1) + i;

			cells.InsertArrayElementAtIndex( index );

			cells.GetArrayElementAtIndex( index ).FindPropertyRelative( "localString" ).stringValue = "";
			cells.GetArrayElementAtIndex( index ).FindPropertyRelative( "flags" ).intValue = 0;
		}

		var column = ColumnHeader.CreateInstance( target as Table, columns.arraySize, culture );

		column.hideFlags = HideFlags.HideInHierarchy;

		columns.InsertArrayElementAtIndex( columns.arraySize );

		AssetDatabase.AddObjectToAsset( column, target );

		columns.GetArrayElementAtIndex( columns.arraySize - 1 ).objectReferenceValue = column;

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		AssetDatabase.Refresh();

		AssetDatabase.SaveAssets();
	}

	protected void ClearRows ()
	{
		serializedObject.Update();

		for ( int i = 0; i < rows.arraySize; i++ )
		{
			DestroyImmediate( rows.GetArrayElementAtIndex( i ).objectReferenceValue, true );
		}

		rows.ClearArray();
		cells.ClearArray();

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		AssetDatabase.Refresh();
	}

	protected void ClearColumns ()
	{
		serializedObject.Update();

		for ( int i = 0; i < columns.arraySize; i++ )
		{
			DestroyImmediate( columns.GetArrayElementAtIndex( i ).objectReferenceValue, true );
		}

		columns.ClearArray();
		cells.ClearArray();

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		AssetDatabase.Refresh();
	}

	protected void RemoveRow ( RowHeader header )
	{
		serializedObject.Update();

		int index = columns.arraySize * header.Index;

		for ( int i = 0; i < columns.arraySize; i++ )
		{
			cells.DeleteArrayElementAtIndex( index );
		}

		rows.GetArrayElementAtIndex( header.Index ).objectReferenceValue = null;

		rows.DeleteArrayElementAtIndex( header.Index );

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		for ( int i = header.Index; i < rows.arraySize; i++ )
		{
			(rows.GetArrayElementAtIndex( i ).objectReferenceValue as Header).RefreshIndex();
		}

		DestroyImmediate( header, true );

		AssetDatabase.Refresh();
	}

	protected void RemoveColumn ( ColumnHeader header )
	{
		serializedObject.Update();

		int index = header.Index;

		for ( int i = 0; i < rows.arraySize; i++ )
		{
			cells.DeleteArrayElementAtIndex( index );

			index += columns.arraySize - 1;
		}

		columns.GetArrayElementAtIndex( header.Index ).objectReferenceValue = null;

		columns.DeleteArrayElementAtIndex( header.Index );

		serializedObject.ApplyModifiedPropertiesWithoutUndo();

		for ( int i = header.Index; i < columns.arraySize; i++ )
		{
			(columns.GetArrayElementAtIndex( i ).objectReferenceValue as Header).RefreshIndex();
		}

		DestroyImmediate( header, true );

		AssetDatabase.Refresh();
	}
}
