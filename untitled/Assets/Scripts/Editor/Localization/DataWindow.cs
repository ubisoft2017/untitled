﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEditor;
using System;

using Localization;

[Serializable]
public class DataWindow : EditorWindow
{
	public static DataWindow EditData ( Func<string> getter, Action<string> setter, Action<DataWindow> callback, RowHeader row, ColumnHeader column )
	{
		var editor = CreateInstance<DataWindow>();

		editor.row = row;
		editor.column = column;

		editor.callback = callback;

		editor.getter = getter;
		editor.setter = setter;

		editor.titleContent = new GUIContent( "Text Editor" );

		Undo.IncrementCurrentGroup();

		Undo.SetCurrentGroupName( "Edit " + row.Title + " (" + column.DisplayName + ")" );

		editor.group = Undo.GetCurrentGroup();

		editor.Show();

		return editor;
	}

	[SerializeField]
	private int group;

	private RowHeader row;
	private ColumnHeader column;

	private Action<DataWindow> callback;
	private Func<string> getter;
	private Action<string> setter;

	[SerializeField]
	private bool dirty;

	public bool Dirty
	{
		get
		{
			return dirty;
		}
	}

	[SerializeField]
	private bool save;

	public bool Save
	{
		get
		{
			return save;
		}
	}

	private void OnEnable ()
	{
		EditorApplication.update += Update;
	}

	private void OnLostFocus ()
	{
		Focus();
	}

	private void Update ()
	{
		if ( this == null )
		{
			EditorApplication.update -= Update;

			return;
		}

		if ( EditorApplication.isCompiling )
		{
			try
			{
				Close();
			}
			catch ( Exception ) { }
		}
		else
		{
			Repaint();
		}
	}

	private void OnGUI ()
	{
		if ( setter == null || getter == null )
		{
			Close();

			return;
		}

		var style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "PreLabel" );

		EditorGUILayout.LabelField( row.Title, style );

		EditorGUILayout.LabelField( " (" + column.DisplayName + ")", EditorStyles.centeredGreyMiniLabel );

		EditorGUILayout.Separator();

		string content = getter();

		content = EditorGUILayout.TextArea( content, GUILayout.ExpandHeight( true ) );

		if ( content != getter() )
		{
			dirty = true;
			setter( content );
		}

		style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "ButtonMid" );

		if ( GUILayout.Button( "Save", style ) )
		{
			save = true;

			Close();
		}
	}

	private void OnDisable ()
	{
		if ( !dirty || (!save && !EditorUtility.DisplayDialog
			(
			"Localized Data",
			"You have unsaved changes that will be lost if you close. Save now?",
			"Save", "Discard"
			)) )
		{
			Undo.RevertAllDownToGroup( group );

			dirty = false;
		}
		else
		{
			Undo.CollapseUndoOperations( group );

			save = true;
		}
	}

	private void OnDestroy ()
	{
		EditorApplication.update -= Update;

		if ( callback != null )
		{
			callback( this );
		}
	}
}
