﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[CustomPropertyDrawer( typeof( EnumObjectAttribute ) )]
public class EnumObjectDrawer : PropertyDrawer
{
	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		EditorGUI.PropertyField( position, property );

		return;

		EnumObjectAttribute enumObjectAttribute = attribute as EnumObjectAttribute;

		var type = enumObjectAttribute.type;

		if ( type == null )
		{
			type = fieldInfo.FieldType;
		}

		var objects = Resources.LoadAll( enumObjectAttribute.path, type );

		if ( objects.Length > 0 )
		{
			var displayedOptions = new GUIContent[objects.Length];

			int selectedIndex = 0;

			for ( int i = 0; i < objects.Length; i++ )
			{
				displayedOptions[i] = new GUIContent( objects[i].name );

				if ( property.objectReferenceValue == objects[i] )
				{
					selectedIndex = i;
				}
			}

			property.objectReferenceValue = objects[EditorGUI.Popup( position, label, selectedIndex, displayedOptions )];
		}
		else
		{
			EditorGUI.LabelField( position, label, new GUIContent( "(no options found)" ) );
		}

		property.serializedObject.ApplyModifiedProperties();
	}
}