﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
public static class ReadmeGUI
{
	[InitializeOnLoadMethod]
	private static void Init ()
	{
		SceneView.onSceneGUIDelegate += Draw;
	}

	private static void Draw ( SceneView sceneView )
	{
		Handles.BeginGUI();

		GUIStyle expandedStyle = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Inspector ).FindStyle( "WindowBackground" );
		GUIStyle collapseStyle = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Inspector ).FindStyle( "Button" );

		expandedStyle.wordWrap = true;

		foreach ( var readme in Object.FindObjectsOfType<Readme>() )
		{
			if ( readme.show )
			{
				GUIContent content = new GUIContent( readme.Text );

				float width = sceneView.position.width - 10.0f;
				float height = expandedStyle.CalcHeight( content, width );

				float x = 5.0f;
				float y = sceneView.position.height - height - 22.0f;

				Rect rect = new Rect( x, y, width, height );

				GUI.backgroundColor = readme.background;

				readme.show = !GUI.Button( rect, content, expandedStyle );

				GUI.backgroundColor = Color.white;
			}
			else
			{
				GUIContent content = new GUIContent( "Show" );

				float width = 100.0f;
				float height = collapseStyle.CalcHeight( content, width );

				float x = (sceneView.position.width - 100.0f) * 0.5f;
				float y = sceneView.position.height - height - 22.0f;

				Rect rect = new Rect( x, y, width, height );

				GUI.backgroundColor = readme.background;

				readme.show = GUI.Button( rect, content, collapseStyle );

				GUI.backgroundColor = Color.white;
			}
		}

		expandedStyle.wordWrap = false;

		Handles.EndGUI();
	}
}
#endif
