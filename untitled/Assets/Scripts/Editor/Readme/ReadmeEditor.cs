﻿using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( Readme ), true )]
public class ReadmeEditor : Editor
{
	SerializedProperty text;
	SerializedProperty background;

	private void OnEnable ()
	{
		(target as Behaviour).GetComponent<Transform>().hideFlags = HideFlags.HideInInspector;

		text = serializedObject.FindProperty( "text" );
		background = serializedObject.FindProperty( "background" );
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		EditorStyles.textField.wordWrap = true;

		EditorGUILayout.LabelField( "Text" );

		text.stringValue = EditorGUILayout.TextArea( text.stringValue, GUILayout.ExpandHeight( true ) );

		EditorGUILayout.Space();

		EditorGUILayout.PropertyField( background );

		EditorGUILayout.Space();

		serializedObject.ApplyModifiedProperties();
	}
}
