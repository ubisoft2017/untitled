﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( GamePad ), true )]
public class GamePadEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		if ( Application.isPlaying )
		{
			var buttons = serializedObject.FindProperty( "buttons" );
			var triggers = serializedObject.FindProperty( "triggers" );
			var joysticks = serializedObject.FindProperty( "joysticks" );

			for ( int i = 0; i < buttons.arraySize; i++ )
			{
				EditorGUILayout.LabelField( ((DeviceButton) i).ToString() + ": " + buttons.GetArrayElementAtIndex( i ).FindPropertyRelative( "isDown" ).boolValue );
				//EditorGUILayout.LabelField( ((DeviceButton) i).ToString() + ": " + buttons.GetArrayElementAtIndex( i ).FindPropertyRelative( "holdTime" ).floatValue );
			}

			EditorGUILayout.Space();

			for ( int i = 0; i < triggers.arraySize; i++ )
			{
				EditorGUILayout.LabelField( ((DeviceTrigger) i).ToString() + " Trigger: " + triggers.GetArrayElementAtIndex( i ).FindPropertyRelative( "position" ).floatValue );
			}

			EditorGUILayout.Space();

			for ( int i = 0; i < joysticks.arraySize; i++ )
			{
				EditorGUILayout.LabelField( ((DeviceJoystick) i).ToString() + " Joystick: " + joysticks.GetArrayElementAtIndex( i ).FindPropertyRelative( "position" ).vector2Value );
			}
		}
		else
		{
			EditorGUILayout.LabelField( "Enter play mode to visualize GamePad state." );
		}
	}
}
