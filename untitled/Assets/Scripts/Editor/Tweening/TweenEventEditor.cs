﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Tweening;
using UnityEditorInternal;

[Serializable]
[CustomEditor( typeof( TweenEvent ) )]
public class TweenEventEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		EditorGUILayout.Space();

		if ( Application.isPlaying )
		{
			if ( GUILayout.Button( new GUIContent( "Invoke" ) ) )
			{
				(target as TweenEvent).Invoke();
			}
		}
		else
		{
			EditorGUILayout.LabelField( "" );
		}

		EditorGUILayout.Space();
	}
}
