﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Tweening;
using UnityEditorInternal;

[Serializable]
[CustomEditor( typeof( Tweener ) )]
public class TweenerEditor : Editor
{
	[SerializeField]
	protected SerializedProperty runInBackground;

	[SerializeField]
	protected SerializedProperty stopSiblings;

	[SerializeField]
	protected SerializedProperty startPosition;

	[SerializeField]
	protected SerializedProperty timeType;

	[SerializeField]
	protected new SerializedProperty targets;

	[SerializeField]
	protected SerializedProperty listeners;

	[SerializeField]
	private ReorderableList targetList;

	[SerializeField]
	private ReorderableList listenerList;

	private void OnEnable ()
	{
		runInBackground = serializedObject.FindProperty( "runInBackground" );
		stopSiblings = serializedObject.FindProperty( "stopSiblings" );
		startPosition = serializedObject.FindProperty( "startPosition" );
		timeType = serializedObject.FindProperty( "timeType" );
		targets = serializedObject.FindProperty( "targets" );
		listeners = serializedObject.FindProperty( "listeners" );

		targetList = new ReorderableList( serializedObject, targets );

		targetList.draggable = false;
		targetList.drawHeaderCallback = TargetHeaderCallback;
		targetList.drawElementCallback = TargetElementCallback;
		targetList.elementHeight = EditorGUIUtility.singleLineHeight * 1.5f;

		listenerList = new ReorderableList( serializedObject, listeners );

		listenerList.draggable = false;
		listenerList.drawHeaderCallback = ListenerHeaderCallback;
		listenerList.drawElementCallback = ListenerElementCallback;
		listenerList.elementHeight = EditorGUIUtility.singleLineHeight * 3.0f;
		listenerList.onAddCallback = ( list ) =>
		{
			int index = listeners.arraySize;
			listeners.InsertArrayElementAtIndex( index );
			listeners.GetArrayElementAtIndex( index ).FindPropertyRelative( "multiplier" ).floatValue = 1.0f;
		};
	}

	public override void OnInspectorGUI ()
	{
		if ( listenerList == null || targetList == null )
		{
			OnEnable();
		}

		serializedObject.Update();

		EditorGUILayout.Space();

		listenerList.DoLayoutList();

		EditorGUILayout.Space();

		targetList.DoLayoutList();

		EditorGUILayout.Space();

		EditorGUILayout.PropertyField( timeType, new GUIContent( "Time" ) );

		EditorGUILayout.PropertyField( runInBackground );

		EditorGUILayout.PropertyField( stopSiblings );

		EditorGUILayout.PropertyField( startPosition );

		EditorGUILayout.Space();

		serializedObject.ApplyModifiedProperties();
	}

	private void TargetHeaderCallback ( Rect position )
	{
		EditorGUI.LabelField( position, new GUIContent( "Targets" ) );
	}

	private void ListenerHeaderCallback ( Rect position )
	{
		EditorGUI.LabelField( position, new GUIContent( "Listeners" ) );
	}

	private float ElementHeightCallback ( int index )
	{
		float height = EditorGUIUtility.singleLineHeight;

		return height;
	}
	
	private void TargetElementCallback ( Rect position, int index, bool isActive, bool isFocused )
	{
		float height = EditorStyles.objectField.CalcHeight( new GUIContent( "" ), 100.0f );

		position = new Rect( position.x + 10.0f, position.y + height * 0.25f, position.width - 10.0f, height );

		var property = targets.GetArrayElementAtIndex( index );

		property.objectReferenceValue = EditorGUI.ObjectField( position, property.objectReferenceValue, typeof( IInterpolator ), true );
	}

	private void ListenerElementCallback ( Rect position, int index, bool isActive, bool isFocused )
	{
		float height = EditorStyles.objectField.CalcHeight( new GUIContent( "" ), 100.0f );

		float width = position.width - 10.0f;

		float half = width - 82.0f;

		float left = position.x + 10.0f;

		float x = left;

		float y = position.y + height * 0.5f;

		position = new Rect( x, y, half, height );

		var trigger = listeners.GetArrayElementAtIndex( index ).FindPropertyRelative( "trigger" );

		EditorGUI.PropertyField( position, trigger, GUIContent.none );

		position = new Rect( width - height - 25.0f, y + 1, 34.0f, height - 1);

		var content = new GUIContent( "", "Duration multiplier." );

		EditorGUI.PropertyField( position, listeners.GetArrayElementAtIndex( index ).FindPropertyRelative( "multiplier" ), GUIContent.none );

		if ( Application.isPlaying && trigger.objectReferenceValue != null )
		{
			position = new Rect( position.x + position.width, y, position.width, height );

			if ( GUI.Button( position, new GUIContent( "Test" ), EditorStyles.miniButton ) )
			{
				(target as Tweener).InvokeLocal( trigger.objectReferenceValue as TweenEvent );
			}
		}

		position = new Rect( width - height - 25.0f, y = y + height, 69.0f, height );

		EditorGUI.PropertyField( position, listeners.GetArrayElementAtIndex( index ).FindPropertyRelative( "modifiers" ), GUIContent.none );

		position = new Rect( x, y, half, height );

		EditorGUI.PropertyField( position, listeners.GetArrayElementAtIndex( index ).FindPropertyRelative( "parameters" ), GUIContent.none );
	}
}
