﻿using UnityEngine;
using UnityEditor;

using Controls;

[InitializeOnLoad]
public class ClockSceneGUI : Editor
{
	static ClockSceneGUI ()
	{
		SceneView.onSceneGUIDelegate += DrawPhaseLabels;
	}

	private static void DrawPhaseLabels ( SceneView view )
	{
		Handles.BeginGUI();

		// Find all of the clocks in the scene
		foreach ( var clock in FindObjectsOfType<Clock>() )
		{
			// Show the correct hand configuration on the clock's face based on the local time delta in the clock's Phase

			clock.PollTime();
			clock.ShowTime();

			var transform = clock.transform;

			// Check if the user is looking toward the clock
			if ( Vector3.Dot( view.camera.transform.forward, (transform.position - view.camera.transform.position).normalized ) > 0.0f )
			{
				// Draw a label, centered under the clock, indicating the local time delta in the clock's Phase

				var style = EditorStyles.label;

				GUIContent content = new GUIContent( clock.Phase == null ? "(missing)" : (clock.Phase.HourOffset > 0 ? "+" : "") + clock.Phase.HourOffset );

				float min, max;

				style.CalcMinMaxWidth( content, out min, out max );

				float height = style.CalcHeight( content, min );

				Vector2 point = HandleUtility.WorldToGUIPoint( transform.position + Vector3.down * 0.5f );

				GUI.Label( new Rect( point.x - min * 0.5f, point.y - height * 0.5f, min, height ), content, style );
			}
		}

		Handles.EndGUI();
	}
}
