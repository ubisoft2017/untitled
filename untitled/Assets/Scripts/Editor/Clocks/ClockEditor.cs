﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( Clock ) )]
public class ClockEditor : NodeEditor
{
	[SerializeField] private bool place;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		// Show a live interpretation of the local delta for the Phase linked to this clock

		(target as Clock).PollTime();

		//EditorGUILayout.LabelField( "Day: " + serializedObject.FindProperty( "day" ).floatValue );
		//EditorGUILayout.LabelField( "Hour: " + serializedObject.FindProperty( "hour" ).floatValue );
		//EditorGUILayout.LabelField( "Minute: " + serializedObject.FindProperty( "minute" ).floatValue );
		//EditorGUILayout.LabelField( "Second: " + serializedObject.FindProperty( "second" ).floatValue );

		DrawNodeInspector();
	}

	private void OnSceneGUI ()
	{
		// If the user is performaing an interactive placement
		if ( place )
		{
			// Check if the left mouse button was pressed
			if ( Event.current.type == EventType.MouseDown && Event.current.button == 0 )
			{
				RaycastHit hit;

				// Perform a raycast against the scene through the user's mouse position
				if ( Physics.Raycast( HandleUtility.GUIPointToWorldRay( Event.current.mousePosition ), out hit ) )
				{
					var transform = (target as Clock).transform;

					// Position the clock at the point where the ray hit, projected onto the XZ plane at the standard clock height
					transform.position = new Vector3( hit.point.x, 1.8f, hit.point.z );
					
					// Align the clock to the surface normal where the ray hit
					transform.rotation = Quaternion.LookRotation( hit.normal );
				}
			}
		}
	}
}
