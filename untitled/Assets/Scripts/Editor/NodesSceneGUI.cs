﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public static class NodesSceneGUI
{
	private static bool active = false;

	public static bool Active
	{
		get
		{
			return active;
		}
	}

	[InitializeOnLoadMethod]
	private static void Init ()
	{
		SceneView.onSceneGUIDelegate += OnSceneGUI;

		EditorApplication.update += Update;

		if ( EditorPrefs.HasKey( "nodeSceneGUIactive" ) )
		{
			active = EditorPrefs.GetBool( "nodeSceneGUIactive" );
		}
	}

	//[MenuItem( "LD Tools/Visual Node Editor" )]
	public static void Activate ()
	{
		active = true;

		EditorPrefs.SetBool( "nodeSceneGUIactive", true );

		var transform = SceneView.lastActiveSceneView.camera.transform;

		EditorPrefs.SetFloat( "nodeSceneGUIcampx", transform.position.x );
		EditorPrefs.SetFloat( "nodeSceneGUIcampy", transform.position.y );
		EditorPrefs.SetFloat( "nodeSceneGUIcampz", transform.position.z );

		EditorPrefs.SetFloat( "nodeSceneGUIcamqx", transform.rotation.x );
		EditorPrefs.SetFloat( "nodeSceneGUIcamqy", transform.rotation.y );
		EditorPrefs.SetFloat( "nodeSceneGUIcamqz", transform.rotation.z );
		EditorPrefs.SetFloat( "nodeSceneGUIcamqw", transform.rotation.w );

		SceneView.RepaintAll();
	}

	public static void Deactivate ()
	{
		active = false;

		SceneView.lastActiveSceneView.orthographic = false;
		SceneView.lastActiveSceneView.isRotationLocked = false;

		ToolsSupport.Hidden = false;

		EditorPrefs.SetBool( "nodeSceneGUIactive", false );

		SceneView.lastActiveSceneView.LookAtDirect(
			new Vector3(
				EditorPrefs.GetFloat( "nodeSceneGUIcampx" ),
				EditorPrefs.GetFloat( "nodeSceneGUIcampy" ),
				EditorPrefs.GetFloat( "nodeSceneGUIcampz" ) ),
			new Quaternion(
				EditorPrefs.GetFloat( "nodeSceneGUIcamqx" ),
				EditorPrefs.GetFloat( "nodeSceneGUIcamqy" ),
				EditorPrefs.GetFloat( "nodeSceneGUIcamqz" ),
				EditorPrefs.GetFloat( "nodeSceneGUIcamqw" ) ) );

		SceneView.RepaintAll();
	}

	private static void Update ()
	{
		if ( !Application.isPlaying )
		{
			foreach ( var phase in UnityEngine.Object.FindObjectsOfType<Node>() )
			{
				//phase.CalculateLocalTime( 0.0f );
			}
		}
	}

	private static Node source;
	private static Node destination;

	private static void OnSceneGUI ( SceneView view )
	{
		if ( active )
		{
			ToolsSupport.Hidden = true;

			GUIStyle style;
			Rect rect;
			GUIContent content;

			view.LookAtDirect(
				new Vector3( view.camera.transform.position.x, 10.0f, view.camera.transform.position.z ),
				Quaternion.LookRotation( Vector3.down, Vector3.forward ) );

			view.orthographic = true;
			view.isRotationLocked = true;

			var nodes = UnityEngine.Object.FindObjectsOfType<Node>();

			Handles.color = Color.blue;

			foreach ( var connection in NodeGraph.Current.Connections )
			{
				Handles.DrawAAPolyLine( 0.2f, new Vector3[] { connection.A.transform.position, connection.B.transform.position } );
			}

			Handles.color = Color.white;

			if ( source != null )
			{
				Handles.DrawAAPolyLine( new Vector3[] { source.transform.position, HandleUtility.GUIPointToWorldRay( Event.current.mousePosition ).origin } );
			}

			Handles.BeginGUI();

			foreach ( var node in nodes )
			{
				if ( Selection.gameObjects.Contains( node.gameObject ) )
				{
					GUI.backgroundColor = Color.white;
					style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "flow node 0" );
				}
				else
				{
					GUI.backgroundColor = new Color( 0.6f, 0.6f, 0.6f );
					style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "flow node 0" );
				}

				float diameter = EditorGUIUtility.singleLineHeight * 2.0f;
				float radius = diameter * 0.5f;

				Vector2 center = HandleUtility.WorldToGUIPoint( node.transform.position );

				rect = new Rect( center.x - radius, center.y - radius, diameter, diameter );

				if ( rect.Contains( Event.current.mousePosition ) )
				{
					if ( Event.current.type == EventType.MouseDrag )
					{
						if ( source == null )
						{
							source = node;
						}

						Event.current.Use();
					}

					destination = node;
				}

				if ( Event.current.type == EventType.MouseUp || Event.current.type == EventType.DragExited )
				{
					if ( source != null & destination != null )
					{
						if ( source != destination )
						{
							NodeGraph.Current.Add( source, destination );

							Event.current.Use();
						}
					}

					source = null;
					destination = null;
				}

				if ( GUI.Button( rect, GUIContent.none, style ) )
				{
					Selection.activeGameObject = node.gameObject;
				}
			}

			GUI.backgroundColor = Color.white;

			style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "LargeButton" );
			content = new GUIContent( "Close Visual Editor" );

			var size = style.CalcSize( content );

			float y = view.camera.pixelRect.height - size.y - 32.0f;
			float x = view.camera.pixelRect.width * 0.5f - size.x * 0.5f;

			rect = new Rect( x, y, size.x, size.y );

			if ( GUI.Button( rect, content ) )
			{
				Deactivate();
			}

			Handles.EndGUI();
		}
	}
}
