﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using Boo.Lang;
using System.Collections;
using System.Collections.Generic;

[CustomEditor( typeof( Block ), true )]
public partial class BlockEditor : Editor
{
	// source: http://answers.unity3d.com/questions/656869/foldunfold-gameobject-from-code.html
	#region GameObject expand/collapse helper
	public static void SetExpandedRecursive ( GameObject go, bool expand )
	{
		var type = typeof( EditorWindow ).Assembly.GetType( "UnityEditor.SceneHierarchyWindow" );
		var methodInfo = type.GetMethod( "SetExpandedRecursive" );

		EditorApplication.ExecuteMenuItem( "Window/Hierarchy" );
		var window = EditorWindow.focusedWindow;

		methodInfo.Invoke( window, new object[] { go.GetInstanceID(), expand } );
	}
	#endregion

	// Serialized List<Join> from Block.joins
	SerializedProperty joins;

	// Serialized List<Wall> from Block.walls
	SerializedProperty walls;

	// Serialized runner line prefab
	SerializedProperty linePrefab;

	// The index of the Join handle the user is currently interacting with, -1 = none
	[SerializeField] private int joinIndex = -1;

	// Quick-access to the serialized JoinType of the Join at joinIndex
	[SerializeField] private JoinType joinType;

	// Quick-access to the serialized Transform of the Join at joinIndex
	[SerializeField] private Transform joinTransform;

	private void OnEnable ()
	{
		joins = serializedObject.FindProperty( "joins" );
		walls = serializedObject.FindProperty( "walls" );
		linePrefab = serializedObject.FindProperty( "linePrefab" );

		InitializeListGUI();

		if ( quickBlock == (target as Block).gameObject )
		{
			QuickBlockInstancePass( quickBlock );
		}
	}

	private void OnDisable ()
	{
		ToolsSupport.Hidden = false;
	}

	private bool TargetHasDuplicateJoins ()
	{
		for ( int i = 0; i < joins.arraySize; i++ )
		{
			for ( int j = 0; j < joins.arraySize; j++ )
			{
				if ( i == j )
				{
					continue;
				}

				if ( joins.GetArrayElementAtIndex( i ).FindPropertyRelative( "join" ).objectReferenceValue == joins.GetArrayElementAtIndex( j ).FindPropertyRelative( "join" ).objectReferenceValue )
				{
					return true;
				}
			}
		}

		return false;
	}

	private void ShowChildren ()
	{
		var blockTransform = (target as Behaviour).transform;

		SetExpandedRecursive( blockTransform.gameObject, true );

		for ( int i = 0; i < blockTransform.childCount; i++ )
		{
			var child = blockTransform.GetChild( i );

			if ( child != null )
			{
				(child as Transform).gameObject.hideFlags = HideFlags.None;
			}
		}

		EditorApplication.RepaintHierarchyWindow();
	}

	private void HideChildren ()
	{
		var blockTransform = (target as Behaviour).transform;

		SetExpandedRecursive( blockTransform.gameObject, false );

		for ( int i = 0; i < blockTransform.childCount; i++ )
		{
			var child = blockTransform.GetChild( i );

			if ( child != null )
			{
				(child as Transform).gameObject.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;
			}
		}

		EditorApplication.RepaintHierarchyWindow();
	}

	private bool IsStillMeshPrefab ()
	{
		var gameObject = (target as Block).gameObject;

		return PrefabUtility.GetPrefabType( gameObject ) == PrefabType.ModelPrefabInstance || PrefabUtility.GetPrefabType( gameObject ) == PrefabType.DisconnectedModelPrefabInstance;
	}

	private void CreateBlockResource ()
	{
		var gameObject = (target as Block).gameObject;

		PrefabUtility.CreatePrefab( "Assets/Resources/" + Block.RESOURCE_PATH + "/" + gameObject.name + ".prefab", gameObject, ReplacePrefabOptions.ConnectToPrefab );
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}
}
