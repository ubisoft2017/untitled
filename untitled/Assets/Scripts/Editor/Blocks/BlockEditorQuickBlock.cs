﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using Boo.Lang;
using System.Collections;
using System.Collections.Generic;

public partial class BlockEditor : Editor
{
	private static GameObject quickBlock;

	[MenuItem( "GameObject/Block/Move Mesh To New Child", false, 0 )]
	private static void MenuItemMoveMesh ()
	{
		MoveMeshToNewChild( Selection.activeGameObject );
	}

	public static void MoveMeshToNewChild ( GameObject gameObject )
	{
		if ( gameObject == null )
		{
			return;
		}

		if ( AssetDatabase.Contains( gameObject ) )
		{
			Debug.LogWarning( "Please move the prefab into the scene hierarchy and try again." );

			return;
		}

		if ( Application.isPlaying )
		{
			Debug.LogWarning( "Please exit play mode and try again." );

			return;
		}

		var meshFilter = gameObject.GetComponent<MeshFilter>();

		if ( meshFilter == null )
		{
			return;
		}

		var meshRenderer = gameObject.GetComponent<MeshRenderer>();

		if ( meshRenderer == null )
		{
			return;
		}

		var child = new GameObject( "mesh" );

		child.AddComponent<MeshFilter>().sharedMesh = meshFilter.sharedMesh;

		child.AddComponent<MeshRenderer>().sharedMaterial = meshRenderer.sharedMaterial;

		DestroyImmediate( meshFilter );
		DestroyImmediate( meshRenderer );

		child.transform.parent = gameObject.transform;
		child.transform.localRotation = Quaternion.Euler( -90.0f, 0.0f, 0.0f );
		child.transform.localPosition = Vector3.zero;

		EditorUtility.SetDirty( gameObject );
		PrefabUtility.RecordPrefabInstancePropertyModifications( gameObject );
	}

	[MenuItem( "GameObject/Block/Set Dirty", false, 0 )]
	private static void MenuItemSetDirty ()
	{
		EditorUtility.SetDirty( Selection.activeGameObject );
		PrefabUtility.RecordPrefabInstancePropertyModifications( Selection.activeGameObject );
	}

	private static Material material;

	[MenuItem( "Assets/Block Tools/Copy Material", false, 0 )]
	private static void MenuItemCopyMaterial ()
	{
		material = Selection.activeObject as Material;
	}

	[MenuItem( "Assets/Block Tools/Paste Materials", false, 0 )]
	private static void MenuItemPasteMaterials ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			foreach ( var renderer in gameObject.GetComponentsInChildren<MeshRenderer>() )
			{
				renderer.sharedMaterial = material;
			}
		}
	}

	[MenuItem( "GameObject/Block/Quick Setup", false, 0 )]
	private static void MenuItemQuickBlock ()
	{
		QuickBlockStaticPass( Selection.activeGameObject );
	}

	public static void QuickBlockStaticPass ( GameObject gameObject )
	{
		if ( gameObject == null )
		{
			return;
		}

		if ( AssetDatabase.Contains( gameObject ) )
		{
			Debug.LogWarning( "Please move the prefab into the scene hierarchy and try again." );

			return;
		}

		if ( Application.isPlaying )
		{
			Debug.LogWarning( "Please exit play mode and try again." );

			return;
		}

		// Rename the object, if it hasn't been already

		if ( gameObject.name.StartsWith( "mdl" ) )
		{
			gameObject.name = "obj" + gameObject.name.Substring( 3, gameObject.name.Length - 3 );
		}

		// Remove the object's Animator component, if it has one

		var animator = gameObject.GetComponent<Animator>();

		if ( animator != null )
		{
			DestroyImmediate( animator, false );
		}

		// Add a Block component to the object, if it doesn't already have one 

		if ( gameObject.GetComponent<Block>() == null )
		{
			gameObject.AddComponent<Block>();
		}

		// Store a reference to the object, then select it
		Selection.activeGameObject = quickBlock = gameObject;
	}

	private void QuickBlockInstancePass ( GameObject gameObject )
	{
		joins.ClearArray();

		for ( int i = 0, k = 0; i < quickBlock.transform.childCount; i++ )
		{
			var child = quickBlock.transform.GetChild( i );

			bool isJoin = child.GetComponent<MeshRenderer>() == null;

			if ( isJoin )
			{
				//child.hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector;

				child.name = "join";

				joins.InsertArrayElementAtIndex( k );

				joins.GetArrayElementAtIndex( k ).FindPropertyRelative( "join" ).objectReferenceValue = child;

				if ( child.forward == Vector3.up )
				{
					child.rotation = Quaternion.LookRotation( child.up, Vector3.up );
				}
				else if ( child.up == Vector3.up )
				{
					child.rotation = Quaternion.LookRotation( child.up, Vector3.up );
				}

				k++;
			}
			else
			{
				i--;
			}
		}

		serializedObject.ApplyModifiedProperties();

		quickBlock = null;
	}
}
