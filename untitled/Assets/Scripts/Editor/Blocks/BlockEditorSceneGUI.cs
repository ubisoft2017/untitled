﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using Boo.Lang;
using System.Collections;
using System.Collections.Generic;

public partial class BlockEditor : Editor
{
	private const float JOIN_SNAP_TOLERANCE = 2.0f;

	private void OnSceneGUI ()
	{
		// Hide Unity's standard move / rotate / scale gizmos so that they do not obstruct the custom Join GUI
		ToolsSupport.Hidden = true;

		// Terminate any in-progress actions if the user presses a key or finishes a click
		if ( Event.current.type == EventType.KeyDown || Event.current.type == EventType.MouseUp )
		{
			joinIndex = -1;
			EditorPrefs.SetInt( "joinEditIndex", -1 );
		}

		// Get the ray passing through the scene at the user's mouse position
		Ray mouseRay = HandleUtility.GUIPointToWorldRay( Event.current.mousePosition );

		// Create an infinite flat (up-facing normal) plane at the scene's origin
		Plane plane = new Plane( Vector3.up, Vector3.zero );

		// Compute the intersection of the user's mouse ray and the plane

		float mouseAlpha;

		plane.Raycast( mouseRay, out mouseAlpha );

		Vector3 mousePosition = mouseRay.origin + mouseRay.direction * mouseAlpha;

		// Draw interactive GUI for each Join
		for ( int i = 0; i < joins.arraySize; i++ )
		{
			// Get the i'th serialized Join
			var property = joins.GetArrayElementAtIndex( i );

			// Look up its serialized JoinType
			var type = property.FindPropertyRelative( "type" ).objectReferenceValue as JoinType;

			// Look up the Transform it references
			var transform = property.FindPropertyRelative( "join" ).objectReferenceValue as Transform;

			// If the JoinType and Transform are properly defined
			if ( transform != null && type != null )
			{
				// Make the GUI the same color as the Join's gizmo
				Handles.color = type.Color;

				Vector3 position = transform.position - transform.forward * 0.75f;

				// Draw a circle GUI which will behave as a 2-dimensional slider along the ground,
				// and check if the user has dragged it by comparing the slider's input position with its output position
				if ( Handles.Slider2D( position, transform.up, transform.forward, transform.right, 0.25f, Handles.CircleCap, 0.0f ) != position )
				{
					// Store the index of the Join the user has interacted with
					joinIndex = i;

					// Store its JoinType, and Transform

					joinType = type;
					joinTransform = transform;
				}

				// Always reset the color when you're done
				Handles.color = Color.white;
			}
		}

		// If the user is editing a Join's orientation
		if ( EditorPrefs.GetInt( "joinEditIndex", -1 ) > -1 )
		{
			// Get the Transform referenced by the editIndex'th Join
			var editTransform = joins.GetArrayElementAtIndex( EditorPrefs.GetInt( "joinEditIndex" ) ).FindPropertyRelative( "join" ).objectReferenceValue as Transform;

			// Check that it is properly defined
			if ( editTransform != null )
			{
				// Draw a from the Join to the user's mouse, to provide some visual feedback
				Handles.DrawLine( editTransform.position - editTransform.forward * 0.75f, mousePosition );

				// Compute the normalized difference between the Join's position and the user's ground-plane mouse position
				Vector3 hintDirection = (editTransform.position - mousePosition).normalized;

				// Compute the dot products between the hint vector and the Join's local forward / right vectors

				float forwardDot = Vector3.Dot( editTransform.forward, hintDirection );
				float rightDot = Vector3.Dot( editTransform.right, hintDirection );

				// Check if the hint is best aligned to the Join's forward / back axix
				if ( Mathf.Abs( forwardDot ) > Mathf.Abs( rightDot ) )
				{
					if ( forwardDot < 0.0f )
					{
						Undo.RecordObject( (target as Behaviour).transform, "Edit Join Orientation" );
						editTransform.rotation = Quaternion.LookRotation( -editTransform.forward, Vector3.up );
					}
				}
				else // the hint is best aligned to the Join's local right / left axis
				{
					if ( rightDot > 0.0f )
					{
						Undo.RecordObject( (target as Behaviour).transform, "Edit Join Orientation" );
						editTransform.rotation = Quaternion.LookRotation( editTransform.right, Vector3.up );
					}
					else
					{
						Undo.RecordObject( (target as Behaviour).transform, "Edit Join Orientation" );
						editTransform.rotation = Quaternion.LookRotation( -editTransform.right, Vector3.up );
					}
				}

				SceneView.RepaintAll();
			}
		}

		// If the user is interacting with a Join's drag handle
		if ( joinIndex > -1 )
		{
			// Draw a from the Join to the user's mouse, to provide some visual feedback
			Handles.DrawLine( joinTransform.position - joinTransform.forward * 0.75f, mousePosition );

			// Find the nearest matching Join to the user's mouse, within the tolerable distance 

			float minDistance = JOIN_SNAP_TOLERANCE;

			Join foreign = null;

			foreach ( var candidateBlock in FindObjectsOfType<Block>() )
			{
				// Make sure the we do not try and match to a Join on the same Block that we are dragging from
				if ( candidateBlock != target )
				{
					foreach ( var foreignCandidate in candidateBlock.Joins )
					{
						// Make sure that the foreign Join's type matches the Join the user is dragging
						if ( foreignCandidate.Type == joinType )
						{
							float candidateDistance = HandleUtility.DistancePointLine( foreignCandidate.Position, mouseRay.origin, mousePosition );

							if ( candidateDistance < minDistance )
							{
								minDistance = candidateDistance;
								foreign = foreignCandidate;
							}
						}
					}
				}
			}

			// Check that there was a foreign Join found in the scene
			if ( foreign != null )
			{
				// Get the currently selected Block's Transform
				var blockTransform = (target as Behaviour).transform;

				// Make sure the user can undo any changes made
				Undo.RecordObject( blockTransform, "Join" );

				// Reset the Block's rotation
				blockTransform.rotation = Quaternion.identity;

				// Get the rotation offset between the Join being dragged and the Block which owns it
				Quaternion joinCorrection = Quaternion.LookRotation( joinTransform.InverseTransformVector( Vector3.forward ), Vector3.up );

				// Compute rotation specified by the foreign Join's forward vector
				Quaternion lookRotation = Quaternion.LookRotation( -foreign.Forward, foreign.Up );

				// Apply the rotation, corrected for any offset between the Join and the Block
				blockTransform.rotation = joinCorrection * lookRotation;

				// Translate the Block by the difference between the foreign Join and the Join being dragged by the user
				blockTransform.position += foreign.Position - joinTransform.position;
			}
		}

		if ( EditorPrefs.GetBool( "editBlockWalls", false ) )
		{
			serializedObject.Update();

			for ( int i = 0; i < walls.arraySize; i++ )
			{
				var wall = walls.GetArrayElementAtIndex( i );

				var corners = wall.FindPropertyRelative( "corners" );

				// Draw interactive GUI for each of the wall's corner
				for ( int j = 0; j < corners.arraySize; j++ )
				{
					// Get the i'th serialized corner
					var corner = corners.GetArrayElementAtIndex( j );

					Handles.matrix = (target as Block).transform.localToWorldMatrix;

					Vector3 position = new Vector3( corner.vector2Value.x, 0.0f, corner.vector2Value.y );

					position = Handles.DoPositionHandle( position, Quaternion.identity );

					corner.vector2Value = new Vector2( position.x, position.z );

					Handles.matrix = Matrix4x4.identity;
				}
			}
			serializedObject.ApplyModifiedProperties();
		}
	}
}
