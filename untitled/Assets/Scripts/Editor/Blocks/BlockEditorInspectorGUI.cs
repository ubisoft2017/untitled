﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using System.Collections;
using System.Collections.Generic;

public partial class BlockEditor : Editor
{
	private ReorderableList joinList;
	private ReorderableList wallList;

	private void InitializeListGUI ()
	{
		joinList = new ReorderableList( serializedObject, joins );
		wallList = new ReorderableList( serializedObject, walls );

		joinList.elementHeight = EditorGUIUtility.singleLineHeight;
		wallList.elementHeightCallback = WallElementHeight;
		
		joinList.draggable = false;
		wallList.draggable = false;

		joinList.drawHeaderCallback = ( p ) => DrawHeader( p, "Joins" );
		wallList.drawHeaderCallback = ( p ) => DrawHeader( p, "Walls" );

		joinList.drawElementCallback = DrawJoinElement;
		wallList.drawElementCallback = DrawWallElement;
	}

	public override void OnInspectorGUI ()
	{
		// Terminate any in-progress actions if the user presses a key or finishes a click
		if ( Event.current.type == EventType.KeyDown || Event.current.type == EventType.MouseUp )
		{
			joinIndex = -1;
			EditorPrefs.SetInt( "joinEditIndex", -1 );
		}

		serializedObject.Update();

		// Make things a little less cramped
		EditorGUILayout.Space();

		// Draw the ReorderableList of Joins
		joinList.DoLayoutList();

		// Draw the ReorderableList of Walls
		wallList.DoLayoutList();

		// Warn the user if there are any Joins referencing the same Transform
		if ( TargetHasDuplicateJoins() )
		{
			EditorGUILayout.LabelField( "Please remove duplicate joins.", EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "CN EntryWarn" ) );
		}

		// Make enough room after the warning label
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		// Check if the object this Block component is attached to is still an instance of an imported Mesh
		if ( IsStillMeshPrefab() )
		{
			if ( GUILayout.Button( "Create Resource" ) )
			{
				CreateBlockResource();
			}
		}

		// Show widgets for copying / pasting a Block's wall definitions

		EditorGUILayout.PropertyField( linePrefab );

		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.LabelField( "Wall Tools" );

		if ( GUILayout.Button( EditorPrefs.GetBool( "editBlockWalls", false ) ? "Done" : "Edit", EditorStyles.miniButton ) )
		{
			EditorPrefs.SetBool( "editBlockWalls", !EditorPrefs.GetBool( "editBlockWalls", false ) );

			EditorWindow.FocusWindowIfItsOpen<SceneView>();
		}

		if ( GUILayout.Button( "Copy", EditorStyles.miniButtonLeft ) )
		{
			EditorGUIUtility.systemCopyBuffer = target.GetInstanceID().ToString();
		}

		if ( GUILayout.Button( "Paste", EditorStyles.miniButtonRight ) )
		{
			int id;

			if ( int.TryParse( EditorGUIUtility.systemCopyBuffer, out id ) )
			{
				var block = EditorUtility.InstanceIDToObject( id ) as Block;

				walls.ClearArray();

				for ( int i = 0; i < block.Walls.Count; i++ )
				{
					walls.InsertArrayElementAtIndex( i );

					var wall = walls.GetArrayElementAtIndex( i );

					wall.FindPropertyRelative( "height" ).intValue = block.Walls[i].Height;

					var corners = wall.FindPropertyRelative( "corners" );

					corners.ClearArray();

					for ( int j = 0; j < block.Walls[i].Corners.Count; j++ )
					{
						corners.InsertArrayElementAtIndex( j );
						corners.GetArrayElementAtIndex( j ).vector2Value = block.Walls[i].Corners[j];
					}
				}
			}
		}

		EditorGUILayout.EndHorizontal();

		// Show widgets for showing / hiding the Block's child objects

		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.LabelField( "Sub-Objects" );

		if ( GUILayout.Button( "Show", EditorStyles.miniButtonLeft ) )
		{
			ShowChildren();
		}

		if ( GUILayout.Button( "Hide", EditorStyles.miniButtonRight ) )
		{
			HideChildren();
		}

		EditorGUILayout.EndHorizontal();

		// Apply any changes made through the ReorderableList's GUI
		serializedObject.ApplyModifiedProperties();
	}

	private float WallElementHeight ( int index )
	{
		var wall = walls.GetArrayElementAtIndex( index );

		var corners = wall.FindPropertyRelative( "corners" );

		int lines = 2;

		if ( corners.isExpanded )
		{
			lines += corners.arraySize;
		}

		return lines * EditorGUIUtility.singleLineHeight + 6.0f;
	}

	// Callback used by the ReorderableList to fill the list's header
	private void DrawHeader ( Rect position, string text )
	{
		EditorGUI.LabelField( position, text );
	}

	// Callback used by the ReorderableList to draw each Join element
	private void DrawJoinElement ( Rect position, int index, bool isActive, bool isFocused )
	{
		EditorGUI.MultiPropertyField(
			new Rect( position.x, position.y, position.width - 50.0f, position.height ),
			new GUIContent[] { GUIContent.none, GUIContent.none },
			joins.GetArrayElementAtIndex( index ).FindPropertyRelative( "type" ) );

		bool editing = EditorPrefs.GetInt( "joinEditIndex", -1 ) == index;

		if ( !editing )
		{
			if ( GUI.Button( new Rect( position.x + position.width - 50.0f, position.y, 50.0f, position.height ), "Edit", EditorStyles.miniButton ) )
			{
				EditorPrefs.SetInt( "joinEditIndex", index );

				EditorWindow.FocusWindowIfItsOpen<SceneView>();
			}
		}
		else
		{
			if ( GUI.Button( new Rect( position.x + position.width - 50.0f, position.y, 50.0f, position.height ), "Done", EditorStyles.miniButton ) )
			{
				EditorPrefs.SetInt( "joinEditIndex", -1 );
			}
		}
	}

	// Callback used by the ReorderableList to draw each Wall element
	private void DrawWallElement ( Rect position, int index, bool isActive, bool isFocused )
	{
		GUI.backgroundColor = isActive ? Color.white : new Color( 0.75f, 0.75f, 0.75f );

		GUI.Box( new Rect( position.x - 3.0f, position.y - 3.0f, position.width + 6.0f, WallElementHeight( index ) - 2.0f ), GUIContent.none );

		GUI.backgroundColor = Color.white;

		var wall = walls.GetArrayElementAtIndex( index );

		var corners = wall.FindPropertyRelative( "corners" );

		float height = EditorGUIUtility.singleLineHeight;

		Rect rect = new Rect( position.x, position.y, position.width, height );

		EditorGUI.PropertyField( rect, wall.FindPropertyRelative( "height" ) );
		float inset = 10.0f;

		float x = rect.x + inset;
		float width = rect.width - inset;

		inset = 100.0f;

		float y = rect.y + height;

		rect = new Rect( x, y, inset, height );

		corners.isExpanded = EditorGUI.Foldout( rect, corners.isExpanded, new GUIContent( "Corners" ) );

		float third = (width - inset) / 3;

		rect = new Rect( x + inset, rect.y, third, height );

		corners.arraySize = EditorGUI.IntField( rect, corners.arraySize );

		rect = new Rect( x + inset + third, y - 1.0f, third, height );

		if ( GUI.Button( rect, "Prepend", EditorStyles.miniButton ) )
		{
			corners.InsertArrayElementAtIndex( 0 );
			corners.GetArrayElementAtIndex( 0 ).vector2Value = Vector2.zero;
		}

		rect = new Rect( x + inset + third + third, y - 1.0f, third, height );

		if ( GUI.Button( rect, "Append", EditorStyles.miniButton ) )
		{
			index = corners.arraySize;

			corners.InsertArrayElementAtIndex( index );
			corners.GetArrayElementAtIndex( index ).vector2Value = Vector2.zero;
		}

		y += 1;

		if ( corners.isExpanded )
		{
			for ( int i = 0; i < corners.arraySize; i++ )
			{
				inset = 30.0f;

				rect = new Rect( x, y += height, width - inset, height );

				EditorGUI.PropertyField( rect, corners.GetArrayElementAtIndex( i ), GUIContent.none );

				rect = new Rect( x + rect.width, y, inset, height );

				if ( GUI.Button( rect, "-" ) )
				{
					corners.DeleteArrayElementAtIndex( i );
					i--;
				}
			}
		}
	}
}
