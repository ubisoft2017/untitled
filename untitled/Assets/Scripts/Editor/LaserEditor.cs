﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( Laser ), true )]
public class LaserEditor : NodeEditor
{
	[SerializeField] private bool place;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var laser = target as Laser;

		var raycaster = laser.GetComponent<Raycaster>();

		if ( laser != null && PrefabUtility.GetPrefabType( laser.gameObject ) != PrefabType.Prefab )
		{
			if ( Application.isPlaying )
			{
				EditorGUILayout.LabelField( raycaster.Hit ? raycaster.Nearest.transform.name : "(none)" );
			}
			else
			{
				EditorGUILayout.LabelField( "" );
			}

			if ( GUILayout.Button( place ? "Done" : "Reposition" ) )
			{
				place = !place;
			}
		}

		DrawNodeInspector();
	}

	private void OnSceneGUI ()
	{
		

		serializedObject.Update ();

		if ( place )
		{
			RaycastHit hit;

			if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && Physics.Raycast (HandleUtility.GUIPointToWorldRay (Event.current.mousePosition), out hit)) {

				var transform = (target as Laser).transform;

				transform.position = hit.point;

				transform.rotation = Quaternion.LookRotation (hit.normal);

				Event.current.Use ();
			}
		}

		serializedObject.ApplyModifiedProperties ();
		if (Event.current.type == EventType.Layout) {
			HandleUtility.AddDefaultControl (0);
		}
	}
}
