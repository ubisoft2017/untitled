﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using Boo.Lang;
using System.Collections;
using System.Collections.Generic;

[CustomEditor( typeof( Node ), true, isFallback = false )]
public partial class NodeEditor : Editor
{
	private void MenuItemAdd ( object arg )
	{
		NodeGraph.Current.Add( target as Node, arg as Node );
	}

	private void ShowAddMenu ()
	{
		GenericMenu menu = new GenericMenu();

		foreach ( var node in FindObjectsOfType<Node>() )
		{
			if ( AssetDatabase.Contains( node ) || node == target )
			{
				continue;
			}

			menu.AddItem( new GUIContent( string.Format( "{0} ({1})", node.name, node.NodeType ) ), false, MenuItemAdd, node );
		}

		menu.ShowAsContext();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		DrawDefaultInspector();

		DrawNodeInspector();

		serializedObject.ApplyModifiedProperties();
	}

	private int index = -1;

	protected void DrawNodeInspector ()
	{
		EditorGUILayout.Separator();

		if ( PrefabUtility.GetPrefabType( (target as Node).gameObject ) != PrefabType.Prefab )
		{
			EditorGUILayout.LabelField( new GUIContent( "Connected Nodes" ), EditorStyles.boldLabel );

			var connections = NodeGraph.Current.GetConnections( target as Node );

			index = Mathf.Clamp( index, -1, connections.Count - 1 );

			var style = EditorStyles.helpBox;

			GUI.backgroundColor = new Color( 0.5f, 0.5f, 0.5f );

			EditorGUILayout.BeginVertical( style );

			if ( connections.Count == 0 )
			{
				EditorGUILayout.LabelField( "(none)", EditorStyles.centeredGreyMiniLabel );
			}

			for ( int i = 0; i < connections.Count; i++ )
			{
				EditorGUILayout.BeginHorizontal();

				if ( i == index )
				{
					GUI.backgroundColor = Color.white;
					style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "flow node 0" );
				}
				else
				{
					GUI.backgroundColor = new Color( 0.6f, 0.6f, 0.6f );
					style = EditorGUIUtility.GetBuiltinSkin( EditorSkin.Scene ).FindStyle( "flow node 0" );
				}

				GUIContent content;

				if ( connections[i] != null )
				{
					content = new GUIContent( string.Format( "{0} ({1})", connections[i].name, connections[i].NodeType ) );
				}
				else
				{
					content = new GUIContent( "(missing)" );
				}

				string name = "nodeEditorButton" + i;

				GUI.SetNextControlName( name );

				if ( GUILayout.Button( content, style, GUILayout.Height( 26.0f ), GUILayout.ExpandWidth( true ) ) )
				{
					index = i;
				}

				EditorGUILayout.EndHorizontal();

				if ( i != connections.Count - 1 )
				{
					EditorGUILayout.Space();
				}
			}

			EditorGUILayout.EndVertical();

			GUI.backgroundColor = new Color( 0.5f, 0.5f, 0.5f );

			EditorGUILayout.BeginHorizontal();

			if ( GUILayout.Button( "Add", EditorStyles.miniButtonLeft ) )
			{
				ShowAddMenu();
			}

			if ( GUILayout.Button( "Remove", EditorStyles.miniButtonRight ) )
			{
				if ( index > -1 )
				{
					NodeGraph.Current.Remove( target as Node, connections[index] );
				}
			}

			EditorGUILayout.EndHorizontal();
		}

		//if ( NodesSceneGUI.Active )
		//{
		//	EditorGUILayout.LabelField( "(node editor active)", EditorStyles.centeredGreyMiniLabel );
		//}
		//else
		//{
		//	var content = new GUIContent( "Open Visual Editor" );

		//	if ( GUILayout.Button( content ) )
		//	{
		//		NodesSceneGUI.Activate();
		//	}
		//}

		GUI.backgroundColor = Color.white;

		EditorGUILayout.LabelField( "Instance ID", EditorStyles.centeredGreyMiniLabel );

		GUILayout.Space( -12.0f );

		EditorGUILayout.SelectableLabel( target.GetInstanceID().ToString(), EditorStyles.centeredGreyMiniLabel );
	}

	private void OnSceneGUI ()
	{
		Repaint();
	}
}
