﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using LevelDesign;
using Boo.Lang;
using System.Collections;
using System.Collections.Generic;

[CustomEditor( typeof( NodeUI ), true, isFallback = false )]
public partial class NodeUIEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		DrawDefaultInspector();

		DrawNodeInspector();

		serializedObject.ApplyModifiedProperties();
	}

	private int index = -1;

	protected void DrawNodeInspector ()
	{
		if ( Application.isPlaying )
		{
			EditorGUILayout.LabelField( "Target Instance ID", EditorStyles.centeredGreyMiniLabel );

			GUILayout.Space( -12.0f );

			EditorGUILayout.SelectableLabel( (target as NodeUI).TargetInstanceID.ToString(), EditorStyles.centeredGreyMiniLabel );
		}
	}

	private void OnSceneGUI ()
	{
		Repaint();
	}
}
