﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

/// <summary>
/// source: https://gist.github.com/ikriz/b0f9d96205629e19859e (mask)
/// source: http://www.sharkbombs.com/2015/02/17/unity-editor-enum-flags-as-toggle-buttons/ (toggle)
/// </summary>
[CustomPropertyDrawer( typeof( EnumFlagsAttribute ) )]
public class EnumFlagDrawer : PropertyDrawer
{
	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		EnumFlagsAttribute flagSettings = (EnumFlagsAttribute) attribute;

		switch ( flagSettings.style )
		{
		case EnumFlagsStyle.Popup:
			{
				Enum targetEnum = (Enum) Enum.ToObject( fieldInfo.FieldType, property.intValue );

				GUIContent propName = new GUIContent( flagSettings.name );

				if ( string.IsNullOrEmpty( flagSettings.name ) )
					propName = label;

				EditorGUI.BeginProperty( position, label, property );

				Enum enumNew = EditorGUI.EnumPopup( position, propName, targetEnum );

				try
				{
					property.intValue = (int) Convert.ChangeType( enumNew, fieldInfo.FieldType );
				}
				catch ( InvalidCastException )
				{
					try
					{
						property.intValue = (short) Convert.ChangeType( enumNew, fieldInfo.FieldType );
					}
					catch ( InvalidCastException )
					{
						Debug.LogError( "Only enums deriving from int and short are supported." );
					}
				}
			}
			break;

		case EnumFlagsStyle.Mask:
			{
				Enum targetEnum = (Enum) Enum.ToObject( fieldInfo.FieldType, property.intValue );

				GUIContent propName = new GUIContent( flagSettings.name );

				if ( string.IsNullOrEmpty( flagSettings.name ) )
					propName = label;

				EditorGUI.BeginProperty( position, label, property );

				Enum enumNew = EditorGUI.EnumMaskField( position, propName, targetEnum );

				try
				{
					property.intValue = (int) Convert.ChangeType( enumNew, fieldInfo.FieldType );
				}
				catch ( InvalidCastException )
				{
					try
					{
						property.intValue = (short) Convert.ChangeType( enumNew, fieldInfo.FieldType );
					}
					catch ( InvalidCastException )
					{
						Debug.LogError( "Only enums deriving from int and short are supported." );
					}
				}

				EditorGUI.EndProperty();
			}
			break;

		case EnumFlagsStyle.Toggle:
			{
				int buttonsIntValue = 0;
				int enumLength = property.enumNames.Length;
				bool[] buttonPressed = new bool[enumLength];
				float buttonWidth = (position.width - EditorGUIUtility.labelWidth) / enumLength;

				EditorGUI.LabelField( new Rect( position.x, position.y, EditorGUIUtility.labelWidth, position.height ), label );

				EditorGUI.BeginChangeCheck();

				for ( int i = 0; i < enumLength; i++ )
				{
					// Check if the button is/was pressed 
					if ( (property.intValue & (1 << i)) == 1 << i )
					{
						buttonPressed[i] = true;
					}

					Rect buttonPos = new Rect( position.x + EditorGUIUtility.labelWidth + buttonWidth * i, position.y, buttonWidth, position.height );

					buttonPressed[i] = GUI.Toggle( buttonPos, buttonPressed[i], property.enumNames[i], "Button" );

					if ( buttonPressed[i] )
						buttonsIntValue += 1 << i;
				}

				if ( EditorGUI.EndChangeCheck() )
				{
					property.intValue = buttonsIntValue;
				}
			}
			break;
		}
	}
}