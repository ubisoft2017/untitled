﻿using System;
using System.Reflection;
using Tweening;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer( typeof( TweenEventList ) )]
public class TweenEventListDrawer : PropertyDrawer
{
	private void DrawEvent ( Rect position, int index, SerializedProperty array )
	{
		try
		{
			if ( index < array.arraySize )
			{
				EditorGUI.PropertyField( position, array.GetArrayElementAtIndex( index ), GUIContent.none );

				return;

				var objects = Resources.LoadAll( "", typeof( TweenEvent ) );

				var property = array.GetArrayElementAtIndex( index );

				if ( objects.Length > 0 )
				{
					var displayedOptions = new GUIContent[objects.Length];

					int selectedIndex = 0;

					for ( int i = 0; i < objects.Length; i++ )
					{
						displayedOptions[i] = new GUIContent( objects[i].name );

						if ( property.objectReferenceValue == objects[i] )
						{
							selectedIndex = i;
						}
					}

					float reserved = 30.0f;

					Rect rect = new Rect( position.x, position.y, position.width - reserved, position.height );

					property.objectReferenceValue = objects[EditorGUI.Popup( rect, GUIContent.none, selectedIndex, displayedOptions )];

					rect = new Rect( position.x + rect.width, position.y, reserved, position.height - 1.0f );

					if ( GUI.Button( rect, "-", EditorStyles.miniButton ) )
					{
						int size = array.arraySize;

						array.DeleteArrayElementAtIndex( index );

						if ( array.arraySize == size )
						{
							array.DeleteArrayElementAtIndex( index );
						}
					}
				}
				else
				{
					EditorGUI.LabelField( position, new GUIContent( "(no options found)" ) );
				}
			}
		}
		catch ( IndexOutOfRangeException ) { }
	}

	public override float GetPropertyHeight ( SerializedProperty property, GUIContent label )
	{
		var events = property.FindPropertyRelative( "events" );

		return EditorGUIUtility.singleLineHeight * (1 + events.arraySize) + 4.0f;
	}

	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		var events = property.FindPropertyRelative( "events" );

		Rect rect = new Rect( position.x, position.y, position.width * 0.5f, EditorGUIUtility.singleLineHeight );

		EditorGUI.LabelField( rect, property.displayName, EditorStyles.boldLabel );

		rect = new Rect( rect.x + rect.width, rect.y, rect.width, rect.height );

		if ( GUI.Button( rect, "Add Event", EditorStyles.miniButton ) )
		{
			events.InsertArrayElementAtIndex( events.arraySize );
		}

		if ( events.arraySize == 0 )
		{
			return;
		}

		float indent = 10.0f;

		float y = rect.y + rect.height + 2.0f;
		float x = position.x + indent;
		float width = position.width - indent;

		for ( int i = 0; i < events.arraySize; i++, y += rect.height )
		{
			rect = new Rect( x, y, width, EditorGUIUtility.singleLineHeight );

			DrawEvent( rect, i, events );
		}
	}
}