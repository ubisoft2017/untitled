﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( TagReference ) )]
public class TagReferenceDrawer : PropertyDrawer
{
	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		var asset = property.FindPropertyRelative( "tag" );

		var content = new GUIContent( property.displayName );

		asset.stringValue = EditorGUI.TagField( position, content, asset.stringValue );
	}
}