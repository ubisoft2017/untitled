﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.SceneManagement;

using AI;

[CustomEditor( typeof( RouteFollower ), true, isFallback = false )]
public partial class RouteFollowerEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		DrawDefaultInspector();

		EditorGUILayout.Space();

		//EditorGUILayout.LabelField( "Graph Information", EditorStyles.boldLabel );
		int next = 0;

		if ( Application.isPlaying )
		{
			next = serializedObject.FindProperty( "next" ).intValue;
		}
		else
		{
			next = serializedObject.FindProperty( "first" ).intValue;
		}

		EditorGUILayout.LabelField( "Next:", next.ToString(), EditorStyles.miniLabel );

		serializedObject.ApplyModifiedProperties();
	}

	private void OnSceneGUI ()
	{
		Repaint();
	}
}
