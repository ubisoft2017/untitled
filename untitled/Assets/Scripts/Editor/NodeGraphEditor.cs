﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.SceneManagement;

using LevelDesign;

[CustomEditor( typeof( NodeGraph ), true, isFallback = false )]
public partial class NodeGraphEditor : Editor
{
	private static int count;

	static NodeGraphEditor ()
	{
		EditorApplication.update += Count;

		SceneView.onSceneGUIDelegate += DrawCount;
	}

	[InitializeOnLoadMethod]
	private static void Count ()
	{
		var graphs = FindObjectsOfType<NodeGraph>();

		if ( !Application.isPlaying )
		{
			if ( graphs.Length > 1 )
			{
				if ( count <= 1 )
				{
					EditorUtility.DisplayDialog( "Node Graph", "There are multiple scenes loaded which contain Node Graph instances, this may result in unexpected errors.", "Got It" );
				}
			}
			else if ( graphs.Length == 1 && EditorSceneManager.GetActiveScene() != graphs[0].gameObject.scene )
			{
				if ( count == 0 )
				{
					if ( EditorUtility.DisplayDialog( "Node Graph", "There is an open scene which contains a Node Graph, do you want to make it the active scene?", "Yes", "No" ) )
					{
						EditorSceneManager.SetActiveScene( graphs[0].gameObject.scene );
					}
				}
				else if ( count > 1 )
				{
					if ( EditorUtility.DisplayDialog( "Node Graph", "There is now exactly one loaded a scene with a Node Graph, do you want to make it the active scene?", "Yes", "No" ) )
					{
						EditorSceneManager.SetActiveScene( graphs[0].gameObject.scene );
					}
				}
			}
		}

		count = graphs.Length;
	}

	private static void DrawCount ( SceneView view )
	{
		Handles.BeginGUI();

		GUILayout.Space( -18.0f );

		GUILayout.Label( "NodeGraph instances: " + count );

		Handles.EndGUI();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();

		//EditorGUILayout.LabelField( "Graph Information", EditorStyles.boldLabel );
		EditorGUILayout.LabelField( "Connections:", serializedObject.FindProperty( "connections" ).arraySize.ToString(), EditorStyles.miniLabel );

		serializedObject.ApplyModifiedProperties();
	}

	private void OnSceneGUI ()
	{
		Repaint();
	}
}
