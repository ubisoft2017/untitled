﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


[CustomEditor( typeof(PathInterface ), true )]
public class PathEditor : Editor
{
	[SerializeField] private bool place;

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var path = target as PathInterface;

		var raycaster = path.GetComponent<Raycaster>();

		if ( path != null && PrefabUtility.GetPrefabType( path.gameObject ) != PrefabType.Prefab )
			{
				if ( Application.isPlaying )
				{
					EditorGUILayout.LabelField( raycaster.Hit ? raycaster.Nearest.transform.name : "(none)" );
				}
				else
				{
					EditorGUILayout.LabelField( "" );
				}

				if ( GUILayout.Button( place ? "Done" : "Reposition" ) )
				{
					place = !place;
				}
		}
	}

	private void OnSceneGUI ()
	{


		serializedObject.Update ();

		if ( place )
		{
			RaycastHit hit;

			if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && Physics.Raycast (HandleUtility.GUIPointToWorldRay (Event.current.mousePosition), out hit)) {

				var property = serializedObject.FindProperty ("listOfPoints");

				var index = property.arraySize;

				property.InsertArrayElementAtIndex (index);
				property.GetArrayElementAtIndex (index).vector3Value = hit.point;

				Event.current.Use ();
			}

		}


		serializedObject.ApplyModifiedProperties ();
		if (Event.current.type == EventType.Layout) {
			HandleUtility.AddDefaultControl (0);
		}
	}

}
