﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( SceneReference ) )]
public class SceneReferenceDrawer : PropertyDrawer
{
	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label )
	{
		var asset = property.FindPropertyRelative( "asset" );

		var content = new GUIContent( property.displayName );

		asset.objectReferenceValue = EditorGUI.ObjectField( position, content, asset.objectReferenceValue, typeof( UnityEngine.Object ), false );

		if ( !AssetDatabase.GetAssetOrScenePath( asset.objectReferenceValue ).EndsWith( ".unity" ) )
		{
			asset.objectReferenceValue = null;
		}

		property.FindPropertyRelative( "name" ).stringValue = asset.objectReferenceValue == null ? null : asset.objectReferenceValue.name;
	}
}