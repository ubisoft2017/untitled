﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

static class PropHelper
{
	private static int visibility;
	private static int navigation;
	private static int interior;

	[InitializeOnLoadMethod]
	private static void Initialize ()
	{
		visibility = LayerMask.NameToLayer( "Visibility" );
		navigation = LayerMask.NameToLayer( "Navigation" );
		interior = LayerMask.NameToLayer( "Interior" );
	}

	[MenuItem( "Edit/Utilities/Setup Visibility Colliders" )]
	public static void SetupVisibilityColliders ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			foreach ( var meshFilter in gameObject.GetComponentsInChildren<MeshFilter>() )
			{
				var child = new GameObject( "Visibility" );

				var collider = child.AddComponent<MeshCollider>();

				collider.sharedMesh = meshFilter.sharedMesh;

				child.layer = visibility;

				child.transform.SetParent( meshFilter.transform, false );

				Undo.RegisterCreatedObjectUndo( child, "Setup Navigation Colliders" );
			}
		}
	}

	[MenuItem( "Edit/Utilities/Setup Navigation Colliders/Mesh" )]
	public static void SetupNavigationCollidersMesh ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			foreach ( var meshFilter in gameObject.GetComponentsInChildren<MeshFilter>() )
			{
				var child = new GameObject( "Navigation" );

				var collider = child.AddComponent<MeshCollider>();

				collider.sharedMesh = meshFilter.sharedMesh;

				child.layer = navigation;

				child.transform.SetParent( meshFilter.transform, false );

				Undo.RegisterCreatedObjectUndo( child, "Setup Navigation Colliders" );
			}
		}
	}

	[MenuItem( "Edit/Utilities/Setup Navigation Colliders/Box" )]
	public static void SetupNavigationCollidersBox ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			foreach ( var meshFilter in gameObject.GetComponentsInChildren<MeshFilter>() )
			{
				var child = new GameObject( "Navigation" );

				var collider = child.AddComponent<BoxCollider>();

				collider.center = meshFilter.sharedMesh.bounds.center;
				collider.size = meshFilter.sharedMesh.bounds.size;

				child.layer = navigation;

				child.transform.SetParent( meshFilter.transform, false );

				Undo.RegisterCreatedObjectUndo( child, "Setup Navigation Colliders" );
			}
		}
	}

	[MenuItem( "Edit/Utilities/Setup Navigation Colliders/Sphere" )]
	public static void SetupNavigationCollidersShere ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			foreach ( var meshFilter in gameObject.GetComponentsInChildren<MeshFilter>() )
			{
				var child = new GameObject( "Navigation" );

				var collider = child.AddComponent<SphereCollider>();

				collider.center = meshFilter.sharedMesh.bounds.center;
				collider.radius =
					( meshFilter.sharedMesh.bounds.size.x
					+ meshFilter.sharedMesh.bounds.size.y
					+ meshFilter.sharedMesh.bounds.size.z) / 6;

				child.layer = navigation;

				child.transform.SetParent( meshFilter.transform, false );

				Undo.RegisterCreatedObjectUndo( child, "Setup Navigation Colliders" );
			}
		}
	}

	[MenuItem( "Edit/Utilities/Prefabs/Apply" )]
	public static void ApplyPrefabs ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			PrefabUtility.ReplacePrefab( gameObject, PrefabUtility.GetPrefabParent( gameObject ), ReplacePrefabOptions.ConnectToPrefab );
		}
	}

	[MenuItem( "Edit/Utilities/Prefabs/Create" )]
	public static void CreatePrefabs ()
	{
		foreach ( var gameObject in Selection.gameObjects )
		{
			PrefabUtility.CreatePrefab( "Assets/Resources/Prefabs/" + gameObject.name + ".prefab", gameObject, ReplacePrefabOptions.ConnectToPrefab );
		}
	}

	[MenuItem( "Edit/Utilities/Naming/Enumerate" )]
	public static void Enumerate ()
	{
		int i = 0;

		string pad = "0";

		int n = Selection.gameObjects.Length;

		while ( n > 10 )
		{
			n /= 10;
			pad += "0";
		}

		foreach ( var gameObject in Selection.gameObjects )
		{
			gameObject.name = string.Format( "{0}_{1:" + pad + "}", gameObject.name, i++ );
		}
	}
}
