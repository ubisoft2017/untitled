﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( CameraOrbit ), true )]
public class CameraOrbitEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var orbit = target as CameraOrbit;

		serializedObject.Update();
		
		switch ( orbit.type )
		{
		case CameraOrbit.CenterType.Object:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "centerTransform" ) );
			break;

		case CameraOrbit.CenterType.Position:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "centerPosition" ) );
			break;
		}

		switch ( orbit.hint )
		{
		case CameraOrbit.HintType.Follow:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "hintTransform" ) );
			break;

		case CameraOrbit.HintType.Avoid:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "hintTransform" ) );
			break;

		case CameraOrbit.HintType.Direction:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "hintDirection" ) );
			break;
		}

		serializedObject.ApplyModifiedProperties();
	}
}
