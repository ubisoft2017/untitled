﻿using UnityEngine;
using UnityEditor;

using Controls;

[CustomEditor( typeof( CameraLookAt ), true )]
public class CameraLookAtEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		var orbit = target as CameraLookAt;

		serializedObject.Update();

		switch ( orbit.type )
		{
		case CameraLookAt.TargetType.Object:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "targetTransform" ) );
			break;

		case CameraLookAt.TargetType.Position:
			EditorGUILayout.PropertyField( serializedObject.FindProperty( "targetPosition" ) );
			break;
		}

		serializedObject.ApplyModifiedProperties();
	}
}
