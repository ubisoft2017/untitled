﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Laser ) )]
public class LaserTimer : MonoBehaviour {

	[SerializeField] float timeToActivate;
	[SerializeField] Laser myLaser;
	[SerializeField] bool timerOn;
	[SerializeField] Collider laserCollider;
	private bool timerReady = true;

	void OnEnable () {
		myLaser = GetComponent<Laser>();
		laserCollider.enabled = true;
		timerReady = true;
	}
	
	void OnDisable ()
	{
		timerReady = false;
	}

	IEnumerator ActivateLaser()
	{
		yield return new WaitForSeconds (timeToActivate);
		laserCollider.enabled = true;
		myLaser.SetLaserStatus( true );
		StartCoroutine (DeactivateLaser ());
	}

	IEnumerator DeactivateLaser()
	{
		yield return new WaitForSeconds (timeToActivate);
		laserCollider.enabled = false;
		myLaser.SetLaserStatus( false );
		StartCoroutine (ActivateLaser ());
	}

	void Update()
	{
		if (timerOn && timerReady) {
			StartCoroutine (DeactivateLaser ());
			timerReady = false;
		}
	}
}
