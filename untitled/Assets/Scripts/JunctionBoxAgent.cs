﻿using cakeslice;
using Controls;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunctionBoxAgent : JunctionBox, IInteractable
{
    private bool active = false;
    public bool opened = false;

    int nmTry = 0;

    private GameObject currentSelected;

    [SerializeField]
    private Animator doorAnimator;

    [SerializeField]
    private Animator codeAnimator;


    private int currentIndex = 0;

    private int width = 4;

    private bool uplinkInstalled = false;

    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float decryptDurationMax, decryptDurationMin;

    [SerializeField]
    private JunctionBoxNode node;

    [SerializeField]
    private GameObject[] wires;


    [SerializeField]
    private GameObject[] switches;

    [SerializeField]
    private GameObject socket;
    [SerializeField]
    private GameObject uplinkDevice;

    [SerializeField]
    private CameraTransform handCameraTransform;

    [SerializeField]
    private CameraTransform lookCameraTransform;

    [EnumObject(typeof(ButtonAction), PlayerAction.RESOURCE_PATH)]
    [SerializeField]
    private ButtonAction interactAction;

    [SerializeField]
    private ButtonPrompt buttonPrompt;


    [EnumObject(typeof(Tweening.TweenEvent))]
    [SerializeField]
    private Tweening.TweenEvent codeReadyEvent, codeInputEvent;

    bool passed = false;

    protected override void Awake()
    {
        DevTools.Register(string.Format("Place Uplink ({0})", GetType().Name), LoadUplinkVoid);

        base.Awake();
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < wires.Length; i++)
        {
            wires[i].GetComponent<Renderer>().material.color = wireColors.colors[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo animatorInfo = doorAnimator.GetCurrentAnimatorStateInfo(0);
        if (animatorInfo.tagHash == Animator.StringToHash("opened"))
        {
            opened = true;
        }
    }

    public bool isActive()
    {
        return active;
    }

    public void Select(Vector2 dir)
    {
        if (state == State.Wire)
        {
            int index = currentIndex;

            if (dir.Equals(new Vector2(0, 1)))
            {
                if (currentIndex >= width)
                {
                    index -= width;
                }
            }

            else if (dir.Equals(new Vector2(0, -1)))
            {
                if (currentIndex < switches.Length - width)
                {
                    index += width;
                }
            }

            else if (dir.Equals(new Vector2(1, 0)))
            {
                if (currentIndex % width <= width - 1)
                {
                    index += 1;
                }
            }


            else if (dir.Equals(new Vector2(-1, 0)))
            {
                if (currentIndex % width != 0)
                {
                    index -= 1;
                }
            }

            if (index < 0)
            {
                index = 0;
            }
            if (index >= switches.Length)
            {
                index = switches.Length - 1;
            }

            SetCurrentSelected(index);
        }
    }

    public CameraTransform Activate()
    {
        if (!active)
        {
            active = true;
            doorAnimator.SetTrigger("open");
            HidePrompt();
        }

        return handCameraTransform;
    }

    private void SetCurrentSelected(int index)
    {
        if (currentSelected)
        {
            currentSelected.GetComponent<FlickSwitch>().Deselect();
        }
        currentSelected = switches[index];
        currentSelected.GetComponent<FlickSwitch>().Select();
        currentIndex = index;
    }

    public bool Interact()
    {
        if (currentSelected == socket && state == State.WaitForUplink)
        {

        }

        if (state == State.Wire)
        {
            currentSelected.GetComponent<FlickSwitch>().Flick();
            if (currentIndex == rightColor)
            {
                Pass();

                return true;
            }
            else
            {
                AgentToHacker.NotifyCutWrongWire(node);
                Fail();
            }
        }

        return false;
    }

    public override void Pass()
    {
        foreach (GameObject sw in switches)
        {
            sw.GetComponent<FlickSwitch>().Deselect();
        }

        uplinkDevice.GetComponentInChildren<AnimateText>().DisplayText("PASS");
        active = false;

        passed = true;

        AgentToHacker.NotifyBypassJunctionBox(node);
    }

    public override void Fail()
    {

        state = State.Retry;

        foreach (GameObject sw in switches)
        {
            sw.GetComponent<FlickSwitch>().Deselect();
            sw.GetComponent<FlickSwitch>().Close();
        }

        currentSelected = null;
        currentIndex = 0;

        uplinkDevice.GetComponentInChildren<AnimateText>().DisplayText("FAIL");
        nmTry++;

        Invoke("LoadUplinkVoid", 1f);
    }

    void foundCode()
    {
        uplinkDevice.GetComponentInChildren<AnimateText>().DisplayText(rightCode.ToString());
        codeReadyEvent.Invoke();

        codeAnimator.SetTrigger("blink");
    }

    public void SetWireState(int color)
    {
        print("THE COLOR IS " + color);

        codeInputEvent.Invoke();

        state = State.Wire;
        SetCurrentSelected(0);

        rightColor = color;
    }

    private Codes[] GenerateCodes()
    {
        Codes[] chosenCodes = new Codes[3];

        System.Array CodesVals = System.Enum.GetValues(typeof(Codes));
        Codes randomCode = (Codes)CodesVals.GetValue(Random.Range(0, CodesVals.Length));

        for (int i = 0; i < chosenCodes.Length; i++)
        {
            //prevent duplicate
            if (i > 0)
            {
                while (randomCode == chosenCodes[i - 1])
                {
                    randomCode = (Codes)CodesVals.GetValue(Random.Range(0, CodesVals.Length));
                }
            }

            chosenCodes[i] = randomCode;
        }

        return chosenCodes;
    }


    public void Retry()
    {

    }

    private void LoadUplinkVoid()
    {
        LoadUplink();
    }


    public CameraTransform LoadUplink()
    {

        float delay = 0f;

        Codes[] allCodes = GenerateCodes();

        rightCode = allCodes[Random.Range(0, allCodes.Length)];

        if (nmTry == 0)
        {
            uplinkDevice.SetActive(true);
            uplinkDevice.GetComponentInChildren<AnimateText>().StartLoading();
        }
        else
        {
            delay = uplinkDevice.GetComponentInChildren<AnimateText>().StartRetry(nmTry+1);
        }

		//send to hacker
		Debug.Log( "JunctionBoxAgent sending instance ID: " + node.GetInstanceID() );
        AgentToHacker.NotifyDecryptJunctionBox(node, allCodes, rightCode);
        Debug.Log(string.Format("OnDecryptJunctionBox( {0}, {1} )", allCodes, rightCode));

   

        Invoke("foundCode", Random.Range(decryptDurationMin, decryptDurationMax) + delay);

        state = State.Streams;

        return lookCameraTransform;
    }

    public void ShowPrompt(PlayerProfile player)
    {
        if (!active && !promptVisible())
        {
            buttonPrompt.Show(player, interactAction);
        }
    }

    public void HidePrompt()
    {
        buttonPrompt.Hide();
    }

    public bool promptVisible()
    {
        return buttonPrompt.Visible;
    }

    public bool canInteract()
    {
        return !passed;
    }

    public ButtonAction GetAction()
    {
        return interactAction;
    }

}
