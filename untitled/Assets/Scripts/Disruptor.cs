﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class Disruptor : MonoBehaviour
{
	[Header( "Setup" )]

	[SerializeField]
	private Mesh sourceMesh;

	[SerializeField]
	private SkinnedMeshRenderer sourceRenderer;

	[SerializeField]
	private Mesh particleMesh;

	[SerializeField]
	private Mesh particleMaterial;

	[Header( "Parameters" )]

	[SerializeField]
	private AnimationCurve particleDistanceCurve, particleSizeCurve;

	[SerializeField]
	private float minDistance, maxDistance, minSize, maxSize;

	private float[] distances, sizes;

	private Mesh mesh;

	private void Awake ()
	{
		mesh = new Mesh();

		mesh.name = "Disruption";
		mesh.MarkDynamic();

		int n = sourceMesh.vertexCount;

		distances = new float[n];
		sizes = new float[n];

		for ( int i = 0; i < distances.Length; i++ )
		{
			distances[i] = UnityEngine.Random.value;
		}
	}

	private void Update ()
	{
		sourceRenderer.BakeMesh( mesh );
	} 
}
