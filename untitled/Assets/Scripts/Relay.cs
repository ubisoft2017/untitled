﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Relay : Node
{
	public override NodeType NodeType
	{
		get
		{
			return NodeType.Relay;
		}
	}
}
