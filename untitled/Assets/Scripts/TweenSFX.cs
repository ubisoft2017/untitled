﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class TweenSFX : SFX
{
	[EnumObject]
	[SerializeField]
	private Tweening.TweenEvent tweenEvent;

	[SerializeField]
	private AudioClip clip;

	[SerializeField]
	[Range( 0.0f, 1.0f )]
	private float volume = 1.0f;

	protected override void Awake ()
	{
		base.Awake();

		tweenEvent.OnInvoke += ( e ) => PlayClip( clip, volume );
	}
}
