﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    public class Text
    {
        public static string Ordinal(int number)
        {
            string suffix = "";

            int ones = number % 10;
            int tens = (int)Mathf.Floor(number / 10) % 10;

            if (tens == 1)
            {
                suffix = "th";
            }
            else
            {
                switch (ones)
                {
                    case 1:
                        suffix = "st";
                        break;

                    case 2:
                        suffix = "nd";
                        break;

                    case 3:
                        suffix = "rd";
                        break;

                    default:
                        suffix = "th";
                        break;
                }
            }
            return string.Format("{0}{1}", number, suffix);
        }
    }

    public class Math
    {
        public static int mod(int x, int m)
        {
            return (x % m + m) % m;
        }
    }

}