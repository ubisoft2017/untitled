﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;

namespace Localization
{
	[Serializable]
	public abstract class Header : ScriptableObject, IEnumerable<Data>
	{
		[SerializeField]
		protected Table table;

		public Table Table
		{
			get
			{
				return table;
			}
		}

		[SerializeField]
		protected int index;

		public int Index
		{
			get
			{
				return index;
			}
		}

#if UNITY_EDITOR
		public abstract void RefreshIndex ();
#endif

		public abstract Data this[int i]
		{
			get;
		}

		public abstract IEnumerator<Data> GetEnumerator ();

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return GetEnumerator();
		}
	}
}