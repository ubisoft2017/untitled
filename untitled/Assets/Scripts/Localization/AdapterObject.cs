﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Localization;
using System;
using System.Collections.ObjectModel;

[CreateAssetMenu( fileName = "new Adapter.asset", menuName = "Localization/Adapter", order = 51 )]
public class AdapterObject : ScriptableObject, IAdapter
{
	public event Action<IAdapter> OnChangeCulture;

	[SerializeField]
	private Adapter adapter;

	public ReadOnlyCollection<Font> Fonts
	{
		get
		{
			return adapter.Fonts;
		}
	}

	public Table Table
	{
		get
		{
			return adapter.Table;
		}
	}

	public string Query ( RowHeader header )
	{
		return adapter.Query( header );
	}

	public bool TrySetCulture ( ColumnHeader header )
	{
		if ( adapter != null )
		{
			return adapter.TrySetCulture( header );
		}

		return false;
	}

	public bool TrySetCulture ( string name )
	{
		if ( adapter != null )
		{
			return adapter.TrySetCulture( name );
		}

		return false;
	}

	private void OnEnable ()
	{
		if ( Application.isPlaying )
		{
			(adapter as IAdapter).Initialize();

			adapter.OnChangeCulture += HandleChangeCulture;
		}
	}

	private void HandleChangeCulture ( IAdapter adapter )
	{
		if ( OnChangeCulture != null )
		{
			OnChangeCulture( this );
		}
	}

	void IAdapter.Initialize ()
	{
		((IAdapter) adapter).Initialize();
	}
}
