﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using UnityEngine;

namespace Localization
{
	public interface IAdapter
	{
		event Action<IAdapter> OnChangeCulture;
		
		Table Table { get; }
		ReadOnlyCollection<Font> Fonts { get; }

		bool TrySetCulture ( string name );
		bool TrySetCulture ( ColumnHeader header );

		string Query ( RowHeader header );

		void Initialize ();
	}
}
