﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Localization
{
	public interface IProxy
	{
		event Action<IProxy> OnChangeText;

		ReadOnlyCollection<Font> Fonts { get; }

		string Query ();

		void Initialize ();
	}
}
