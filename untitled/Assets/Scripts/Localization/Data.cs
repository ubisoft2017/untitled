﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Localization
{
	[Serializable]
	public class Data
	{
		[Flags]
		public enum MetaFlags : int
		{
			None = 0,
			Remote = 1,
			Violence = 4,
			Sexuality = 8
		}

		[SerializeField]
		private int flags;

		public MetaFlags Flags
		{
			get
			{
				return (MetaFlags) flags;
			}
		}

		[SerializeField]
		private string localString;

		public string LocalString
		{
			get
			{
				return localString;
			}
		}
	}
}