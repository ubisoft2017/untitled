﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;

namespace Localization
{
	[Serializable]
	public class RowHeader : Header
	{
		public static RowHeader CreateInstance ( Table table, int index, string title )
		{
			var header = CreateInstance<RowHeader>();

			header.table = table;
			header.index = index;

			header.title = title;

			return header;
		}

		[SerializeField]
		private string title;

		public string Title
		{
			get
			{
				return title;
			}
		}

		public override Data this[int i]
		{
			get
			{
				return table[index, i];
			}
		}

		public override IEnumerator<Data> GetEnumerator ()
		{
			return new Enumerator( table, index );
		}

#if UNITY_EDITOR
		public override void RefreshIndex ()
		{
			for ( int i = 0; i < table.Rows.Count; i++ )
			{
				if ( table.Rows[i] == this )
				{
					index = i;

					return;
				}
			}
		}
#endif
		
		private class Enumerator : IEnumerator<Data>
		{
			private int row;
			private int col;

			Table table;

			public Data Current
			{
				get
				{
					return table[row, col];
				}
			}

			object IEnumerator.Current
			{
				get
				{
					return Current;
				}
			}

			public Enumerator ( Table table, int row )
			{
				this.table = table;
				this.row = row;

				col = 0;
			}

			public void Dispose () { }

			public bool MoveNext ()
			{
				return ++col < table.Columns.Count;
			}

			public void Reset ()
			{
				col = 0;
			}
		}
	}
}
