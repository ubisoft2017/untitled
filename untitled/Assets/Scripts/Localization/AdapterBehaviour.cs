﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Localization;

public class AdapterBehaviour : MonoBehaviour
{
	[SerializeField]
	private Adapter adapter;

	public Adapter Adapter
	{
		get
		{
			return adapter;
		}
	}

	public void TrySetCulture ( string name )
	{
		if ( adapter != null )
		{
			adapter.TrySetCulture( name );
		}
	}
}
