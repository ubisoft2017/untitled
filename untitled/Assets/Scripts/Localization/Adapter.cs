﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using UnityEngine;

namespace Localization
{
	[Serializable]
	public class Adapter : IAdapter
	{
		public event Action<IAdapter> OnChangeCulture;

		[SerializeField]
		private Table table;

		public Table Table
		{
			get
			{
				return table;
			}
		}

		[SerializeField]
		private ColumnHeader header;

		public ReadOnlyCollection<Font> Fonts
		{
			get
			{
				return header.Fonts;
			}
		}

		void IAdapter.Initialize ()
		{
			
		}

		public bool TrySetCulture ( string name )
		{
			foreach ( var header in table.Columns )
			{
				if ( header.DisplayName == name )
				{
					if ( header != this.header )
					{
						this.header = header;

						if ( OnChangeCulture != null )
						{
							OnChangeCulture( this );
						}
					}

					return true;
				}
			}

			return false;
		}

		public bool TrySetCulture ( ColumnHeader header )
		{
			if ( header == this.header )
			{
				return true;
			}

			if ( header != this.header )
			{
				if ( table.Columns.Contains( header ) )
				{
					this.header = header;

					if ( OnChangeCulture != null )
					{
						OnChangeCulture( this );
					}

					return true;
				}
			}

			return false;
		}

		public string Query ( RowHeader header )
		{
			return table.Query( header, this.header );
		}
	}
}
