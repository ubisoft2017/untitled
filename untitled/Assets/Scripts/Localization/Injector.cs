﻿using UnityEngine;
using UnityEngine.UI;

using Localization;

[RequireComponent( typeof( Text ) )]
public class Injector : MonoBehaviour
{
	[SerializeField]
	private ProxyObject proxy;

	[SerializeField]
	private TextStyle style;

	[SerializeField]
	[HideInInspector]
	private Text text;

	private void OnEnable ()
	{
		proxy.OnChangeText += Inject;

		if ( text != null )
		{
			Inject( proxy );
		}
	}

	private void OnDisable ()
	{
		proxy.OnChangeText -= Inject;
	}

	private void Start ()
	{
		text = GetComponent<Text>();

		Inject( proxy );
	}
	
	private void Inject ( IProxy proxy )
	{
		text.text = proxy.Query();

		if ( proxy.Fonts.Contains( style.Font ) )
		{
			text.font = style.Font;
			text.fontSize = style.Size;
		}
		else
		{
			text.font = style.Fallback.Font;
			text.fontSize = style.Fallback.Size;
		}
	}

	private void Style ( TextStyle style )
	{
		if ( proxy.Fonts.Contains( style.Font ) || style.Fallback == null )
		{
			text.font = style.Font;
			text.fontSize = style.Size;
		}
		else
		{
			Style( style.Fallback );
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		if ( proxy != null )
		{
			text = GetComponent<Text>();

			if ( text != null )
			{
				Inject( proxy );
			}

			text = null;
		}
	}
#endif
}
