﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using UnityEngine;

namespace Localization
{
	[CreateAssetMenu( fileName = "new Table.asset", menuName = "Localization/Table", order = 50 )]
	public partial class Table : ScriptableObject
	{
		public static int FlatIndex ( int row, int col, int rows, int cols )
		{
			return row * cols + col;
		}

		[SerializeField]
		private string title;

		public string Title
		{
			get
			{
				return title;
			}
		}

		[SerializeField]
		private List<Data> cells;

		public Data this[int row, int col]
		{
			get
			{
				return cells[row * columns.Count + col];
			}
		}

		public string Query ( RowHeader row, ColumnHeader column )
		{
			bool remote;

			return Query( row, column, out remote );
		}

		public string Query ( RowHeader row, ColumnHeader column, out bool remote )
		{
			var cell = cells[row.Index * columns.Count + column.Index];

			if ( (cell.Flags & Data.MetaFlags.Remote) == Data.MetaFlags.None || column.Remote == null )
			{
				remote = false;

				return cell.LocalString;
			}

			remote = true;

			return Query( row, columns[column.Index].Remote );
		}

		public string Query ( int row, int column )
		{
			return Query( rows[row], columns[column] );
		}

		[SerializeField]
		private List<ColumnHeader> columns;

		public ReadOnlyCollection<ColumnHeader> Columns
		{
			get
			{
				return columns.AsReadOnly();
			}
		}

		[SerializeField]
		private List<RowHeader> rows;

		public ReadOnlyCollection<RowHeader> Rows
		{
			get
			{
				return rows.AsReadOnly();
			}
		}
	}
}