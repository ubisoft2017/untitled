﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Localization
{
	[CreateAssetMenu( fileName = "new Proxy.asset", menuName = "Localization/Proxy", order = 52 )]
	public class ProxyObject : ScriptableObject, IProxy
	{
		public event Action<IProxy> OnChangeText;

		[SerializeField]
		private Proxy proxy;

		public string Query ()
		{
			return proxy.Query();
		}

		public ReadOnlyCollection<Font> Fonts
		{
			get
			{
				return proxy.Fonts;
			}
		}

		private void HandleCultureChange ( Adapter adapter )
		{
			if ( OnChangeText != null )
			{
				OnChangeText( this );
			}
		}

		private void OnEnable ()
		{
			if ( Application.isPlaying )
			{
				(proxy as IProxy).Initialize();

				proxy.OnChangeText += HandleChangeText;
			}
		}

		private void HandleChangeText ( IProxy proxy )
		{
			if ( OnChangeText != null )
			{
				OnChangeText( this );
			}
		}

		void IProxy.Initialize ()
		{
			((IProxy) proxy).Initialize();
		}
	}
}
