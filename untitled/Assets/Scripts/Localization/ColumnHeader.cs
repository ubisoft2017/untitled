﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;

namespace Localization
{
	[Serializable]
	public class ColumnHeader : Header
	{
		public static ColumnHeader CreateInstance ( Table table, int index, CultureInfo culture )
		{
			var header = CreateInstance<ColumnHeader>();

			header.table = table;
			header.index = index;

			header.threeLetterISOLanguageName = culture.ThreeLetterISOLanguageName;
			header.displayName = culture.DisplayName;

			return header;
		}

		[SerializeField]
		private string threeLetterISOLanguageName;

		public string ThreeLetterISOLanguageName
		{
			get
			{
				return threeLetterISOLanguageName;
			}
		}

		[SerializeField]
		private string displayName;

		public string DisplayName
		{
			get
			{
				return displayName;
			}
		}

		[SerializeField]
		private List<Font> fonts;

		public ReadOnlyCollection<Font> Fonts
		{
			get
			{
				return fonts.AsReadOnly();
			}
		}

		[SerializeField]
		private ColumnHeader remote;

		public ColumnHeader Remote
		{
			get
			{
				return remote;
			}
		}

		public override Data this[int i]
		{
			get
			{
				return table[i, index];
			}
		}

		public override IEnumerator<Data> GetEnumerator ()
		{
			return new Enumerator( table, index );
		}

#if UNITY_EDITOR
		public override void RefreshIndex ()
		{
			for ( int i = 0; i < table.Columns.Count; i++ )
			{
				if ( table.Columns[i] == this )
				{
					index = i;

					return;
				}
			}
		}
#endif

		private class Enumerator : IEnumerator<Data>
		{
			private int col;
			private int row;

			Table table;

			public Data Current
			{
				get
				{
					return table[row, col];
				}
			}

			object IEnumerator.Current
			{
				get
				{
					return Current;
				}
			}

			public Enumerator ( Table table, int col )
			{
				this.table = table;
				this.col = col;

				row = 0;
			}

			public void Dispose () { }

			public bool MoveNext ()
			{
				return ++row < table.Rows.Count;
			}

			public void Reset ()
			{
				row = 0;
			}
		}
	}
}
