﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using UnityEngine;

namespace Localization
{
	[Serializable]
	public class Proxy : IProxy
	{
		public event Action<IProxy> OnChangeText;

		[SerializeField]
		[EnumObject( typeof( IAdapter ) )]
		private UnityEngine.Object adapter;

		[SerializeField]
		private RowHeader header;

		public ReadOnlyCollection<Font> Fonts
		{
			get
			{
				return (adapter as IAdapter).Fonts;
			}
		}

		private bool initialized;

		void IProxy.Initialize ()
		{
			if ( !initialized )
			{
				if ( adapter != null )
				{
					(adapter as IAdapter).OnChangeCulture += HandleCultureChange;

					initialized = true;
				}
			}
		}

		~Proxy ()
		{
			if ( adapter != null )
			{
				(adapter as IAdapter).OnChangeCulture -= HandleCultureChange;
			}
		}

		public string Query ()
		{
			return (adapter as IAdapter).Query( header );
		}

		private void HandleCultureChange ( IAdapter adapter )
		{
			if ( OnChangeText != null )
			{
				OnChangeText( this );
			}
		}
	}
}
