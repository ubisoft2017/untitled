﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public static class GeometricExtensions
{
	public static Vector3[] MakeRegularPolygon ( this Vector3[] arr, float radius )
	{
		int sides = arr.Length - 1;

		float step = (Mathf.PI * 2.0f) / sides;

		for ( int i = 0; i < arr.Length; i++ )
		{
			float theta = step * i;

			float x = Mathf.Cos( theta ) * radius;
			float y = Mathf.Sin( theta ) * radius;

			arr[i] = new Vector3( x, y, 0.0f );
		}

		return arr;
	}

	public static Vector3[] MakePlot ( this Vector3[] arr, float a, float b, Func<float,float> function )
	{
		int segments = arr.Length - 1;

		float delta = a - b;
		float dx = delta / segments;

		float x = a;

		for ( int i = 0; i < arr.Length; i++ )
		{
			arr[i] = new Vector3( x, function( x ), 0.0f );

			x += dx;
		}

		return arr;
	}
}
