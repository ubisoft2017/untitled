﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using System;

public class PathInterface : MonoBehaviour
{
	[SerializeField] private List<Vector3> listOfPoints;

	[SerializeField] public float speed;

	public ReadOnlyCollection<Vector3> Points
	{
		get
		{
			return listOfPoints.AsReadOnly();
		}
	}

	private float length;

	public float Length
	{
		get
		{
			return length;
		}
	}

	private float[] lengths;
	private float[] times;

	protected virtual void Awake ()
	{
		lengths = new float[Points.Count];
		times = new float[Points.Count];

		length = 0.0f;

		for ( int i = 1; i < Points.Count; i++ )
		{
			length += lengths[i] = Vector3.Distance( Points[i - 1], Points[i] );
		}
	}

	public Vector3 PositionAt( float time )
	{
		float total = (length / speed);

		time %= total * 2.0f;

		/// Check if we are going "backwards" along the path
		if ( time > total )
		{
			time = (total - (time - total));
		}

		for ( int i = 1; i < lengths.Length; i++ )
		{
			times[i] = lengths[i] / speed;

			if ( time <= times[i] )
			{
				return Vector3.Lerp( Points[i - 1], Points[i], (time - times[i - 1]) / times[i] );
			}
		}

		Debug.LogError( "error with laser movement", this );
		return new Vector3( 0, 0, 0 );
	}

	#if UNITY_EDITOR
	private void OnDrawGizmosSelected ()
	{
		if (listOfPoints != null)
		{
			Gizmos.color = Color.red;

			for (int i = 1; i < listOfPoints.Count; i++) {
				Gizmos.DrawLine( listOfPoints[i], listOfPoints[i - 1] );
			}	

			Gizmos.color = Color.white;
		}
	}
	#endif
}
