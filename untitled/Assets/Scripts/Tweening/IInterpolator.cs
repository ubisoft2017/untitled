﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	public interface IInterpolator
	{
		void Interpolate ( float t );
	}
}