﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	[Serializable]
	public class Listener
	{
		[SerializeField]
		[Tooltip( "The curve and duration used to drive this response." )]
		private TweenParameters parameters;

		public TweenParameters Parameters
		{
			get
			{
				return parameters;
			}

			set
			{
				parameters = value;
			}
		}

		[SerializeField]
		[EnumFlags( EnumFlagsStyle.Mask )]
		[Tooltip( "If true, curve will be evaluated in reverse." )]
		private ModifierFlags modifiers;

		public ModifierFlags Modifiers
		{
			get
			{
				return modifiers;
			}

			set
			{
				modifiers = value;
			}
		}

		[SerializeField]
		//[EnumObject( typeof( TweenEvent ) )]
		private TweenEvent trigger;

		public TweenEvent Trigger
		{
			get
			{
				return trigger;
			}
		}

		[SerializeField]
		private float multiplier = 1.0f;

		public float Multiplier
		{
			get
			{
				return multiplier;
			}

			set
			{
				multiplier = Mathf.Clamp( value, float.Epsilon, float.MaxValue );
			}
		}
	}
}