﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Tweening
{
	public enum TimeType
	{
		Unscaled,
		Scaled
	}

	public class Tweener : MonoBehaviour, IInterpolator
	{
		public event Action<Tweener> OnFinishTween;

		[SerializeField]
		private List<UnityEngine.Object> targets = new List<UnityEngine.Object>();

		public ReadOnlyCollection<UnityEngine.Object> Targets
		{
			get
			{
				return targets.AsReadOnly();
			}
		}

		[SerializeField]
		private List<Listener> listeners = new List<Listener>();

		public ReadOnlyCollection<Listener> Listeners
		{
			get
			{
				return listeners.AsReadOnly();
			}
		}

		[SerializeField]
		[Tooltip( "If true, tweens will run regardless of whether or not this behaviour is active and enabled." )]
		private bool runInBackground = true;

		[SerializeField]
		[Tooltip( "If true, sibling Tweeners will have their tweens cancelled any time this Tweener starts a new tween." )]
		private bool stopSiblings;

		[SerializeField]
		[Range( 0.0f, 1.0f )]
		private float startPosition = 0.0f;

		[SerializeField]
		private TimeType timeType = TimeType.Unscaled;

		[SerializeField]
		[HideInInspector]
		private Coroutine coroutine;

		[SerializeField]
		[HideInInspector]
		private float position;

		public float Position
		{
			get
			{
				return position;
			}
		}

		private Tweener[] siblings;

		public void InvokeLocal ( string @event )
		{
			foreach ( var listener in listeners )
			{
				if ( listener.Trigger.name == @event )
				{
					Tween( listener.Parameters, listener.Modifiers );
				}
			}
		}

		public void InvokeLocal ( TweenEvent @event )
		{
			HandleInvoke( @event );
		}

        public void InvokeAll()
        {
            foreach(Listener listener in listeners)
            {
                listener.Trigger.Invoke();
            }
        }

        public void InvokeAllLocal()
        {
            foreach (Listener listener in listeners)
            {
                InvokeLocal(listener.Trigger);
            }
        }


        private void HandleInvoke ( TweenEvent @event )
		{
			foreach ( var listener in listeners )
			{
				if ( listener.Trigger == @event )
				{
					Tween( listener.Parameters, listener.Modifiers, listener.Multiplier );

					return;
				}
			}
		}

		public void Interpolate ( float t )
		{
			position = t;

			foreach ( var target in targets )
			{
				if ( target != null )
				{
					(target as IInterpolator).Interpolate( t );
				}
			}
		}

		public void TweenForward ( TweenParameters parameters )
		{
			Tween( parameters, 0 );
		}

		public void TweenReverse ( TweenParameters parameters )
		{
			Tween( parameters, ModifierFlags.Reverse );
		}

		public void TweenForwardInverted ( TweenParameters parameters )
		{
			Tween( parameters, ModifierFlags.Invert );
		}

		public void TweenReverseInverted ( TweenParameters parameters )
		{
			Tween( parameters, ModifierFlags.Reverse | ModifierFlags.Invert );
		}

		public void Stop ()
		{
			if ( coroutine != null )
			{
				StopCoroutine( coroutine );
			}
		}

		public void Tween ( TweenParameters parameters, ModifierFlags modifiers = 0, float multiplier = 1.0f )
		{
			Stop();

			if ( stopSiblings )
			{
				foreach ( var sibling in siblings )
				{
					//Debug.Log( string.Format( "{0} stopping {1}", name, sibling.name ) );

					sibling.Stop();
				}
			}

			coroutine = StartCoroutine( Coroutine( parameters, modifiers, multiplier ) );

			if ( !gameObject.GetActive() )
			{
				Debug.LogError( "bad door", this );
			}
		}

		private void OnEnable ()
		{
			foreach ( var listener in listeners )
			{
				if ( listener != null )
				{
					listener.Trigger.OnInvoke += HandleInvoke;
				}
			}
		}

		private void Awake ()
		{
			var list = new List<Tweener>();

			foreach ( var tweener in GetComponents<Tweener>() )
			{
				if ( tweener != this )
				{
					list.Add( tweener );
				}
			}

			siblings = list.ToArray();
		} 

		private void Start ()
		{
			Interpolate( startPosition );
		}

		private float DeltaTime
		{
			get
			{
				return timeType == TimeType.Unscaled ? Time.unscaledDeltaTime : Time.deltaTime;
			}
		}

		private IEnumerator Coroutine ( TweenParameters parameters, ModifierFlags modifiers, float multiplier )
		{
			if ( parameters == null )
			{
				yield break;
			}

			float start = Time.time, time = start;
			float duration = parameters.Duration * multiplier;

			bool reverse = (modifiers & ModifierFlags.Reverse) != 0;
			bool invert = (modifiers & ModifierFlags.Invert) != 0;
			bool min = (modifiers & ModifierFlags.Min) != 0;
			bool max = (modifiers & ModifierFlags.Max) != 0;
			bool debug = (modifiers & ModifierFlags.Debug) != 0;

			while ( DeltaTime == 0.0f )
			{
				yield return null;
			}

			if ( min )
			{
				bool done = false;

				float t = Calculate( 0.0f, parameters.Curve, reverse, invert, min, max, ref done );

				while ( t > position && !done )
				{
					time += DeltaTime;
					t = Calculate( (time - start) / duration, parameters.Curve, reverse, invert, min, max, ref done );
				}
			}

			if ( max )
			{
				bool done = false;

				float t = Calculate( 0.0f, parameters.Curve, reverse, invert, false, false, ref done );

				while ( t < position && !done )
				{
					time += DeltaTime;
					t = Calculate( (time - start) / duration, parameters.Curve, reverse, invert, false, false, ref done );
				}
			}

			time -= DeltaTime;

			{
				bool done = false;

				do
				{
					time += DeltaTime;

					float t = (time - start) / duration;

					t = Calculate( t, parameters.Curve, reverse, invert, min, max, ref done );

					Interpolate( t );

					if ( debug )
					{
						Debug.Log( string.Format( "{0}.Interpolate( {1} )", name, t ) );
					}

					yield return null;
				}
				while ( !done );
			}

			coroutine = null;

			if ( OnFinishTween != null )
			{
				OnFinishTween( this );
			}
		}

		private float Calculate ( float t, AnimationCurve curve, bool reverse, bool invert, bool min, bool max, ref bool done )
		{
			if ( t >= 1.0f )
			{
				t = 1.0f;

				done = true;
			}

			if ( reverse )
			{
				t = 1.0f - t;
			}

			t = curve.Evaluate( t );

			if ( invert )
			{
				t = 1.0f - t;
			}

			if ( min )
			{
				t = Mathf.Min( position, t );
			}

			if ( max )
			{
				t = Mathf.Max( position, t );
			}

			return t;
		}

		private void OnDisable ()
		{
			foreach ( var listener in listeners )
			{
				if ( listener != null )
				{
					listener.Trigger.OnInvoke -= HandleInvoke;
				}
			}

			if ( !runInBackground )
			{
				if ( coroutine != null )
				{
					StopCoroutine( coroutine );

					coroutine = null;
				}
			}
		}
	}
}