﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;
using UnityStandardAssets.ImageEffects;

public class TwirlInterpolator : Interpolator<float>, IInterpolator
{
	[SerializeField]
	private Twirl twirl;

	public override void Interpolate ( float t )
	{
		twirl.angle = Mathf.Lerp( a, b, t );
	}
}
