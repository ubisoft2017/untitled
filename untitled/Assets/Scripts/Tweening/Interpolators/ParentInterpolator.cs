﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

public class ParentInterpolator : Interpolator<Transform>, IInterpolator
{

    [SerializeField] private bool keepLocalTransform;

    private void Start()
    {
    }

    public override void Interpolate(float t)
    {
        Vector3 pos = transform.localPosition;
        Vector3 scale = transform.localScale;
        Quaternion rotation = transform.localRotation;

        if (t < 0.5f)
        {
            transform.parent = a;
        }
        else
        {
            transform.parent = b;
        }

        //if changed parent, keep local pos;
        if (keepLocalTransform)
        {


            if (!transform.localPosition.Equals(pos))
            {
                transform.localPosition = pos;
            }

            if (!transform.localScale.Equals(scale))
            {
                transform.localScale = scale;
            }

            if (!transform.localRotation.Equals(rotation))
            {
                transform.localRotation = rotation;
            }
        }
    }
}
