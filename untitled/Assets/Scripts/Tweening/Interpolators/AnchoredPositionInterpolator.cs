﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

[RequireComponent( typeof( RectTransform ) )]
public class AnchoredPositionInterpolator : Interpolator<Vector2>, IInterpolator
{
	[SerializeField]
	[HideInInspector]
	private RectTransform rect;

	private void Start ()
	{
		rect = GetComponent<RectTransform>();
	}

	public override void Interpolate ( float t )
	{
		rect.anchoredPosition = Vector2.Lerp( a, b, t );
	}
}
