﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

[RequireComponent( typeof( RectTransform ) )]
public class TwistInterpolator : Interpolator<float>, IInterpolator
{
	[SerializeField]
	private RectTransform rect;

	private void Start ()
	{
		rect = GetComponent<RectTransform>();
	}

	public override void Interpolate ( float t )
	{
		rect.localRotation = Quaternion.Euler( 0.0f, 0.0f, Mathf.Lerp( a, b, t ) );
	}
}
