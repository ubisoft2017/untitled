﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

[RequireComponent( typeof( CanvasGroup ) )]
public class AlphaInterpolator : MonoBehaviour, IInterpolator
{
	[SerializeField]
	[HideInInspector]
	private CanvasGroup group;

	private void Awake ()
	{
		group = GetComponent<CanvasGroup>();
	}

	public void Interpolate ( float t )
	{
		group.alpha = t;
	}
}
