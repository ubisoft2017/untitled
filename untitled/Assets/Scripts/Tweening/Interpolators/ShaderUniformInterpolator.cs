﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

public class ShaderUniformInterpolator : MonoBehaviour, IInterpolator
{

    [System.Serializable]
    enum Type
    {
        Float,
        Int
    }

    [SerializeField]
    private Type type;

    [SerializeField]
    private float multiplier;

    [SerializeField]
    [HideInInspector]
    public Material mat;

    [SerializeField] private string property;

    [SerializeField] private bool global = false;


    private void Awake()
    {
        if (!global)
        {
            mat = GetComponent<Renderer>().material;
        }
    }

    public void Interpolate(float t)
    {
        if (global)
        {
            if (type == Type.Float)
            {
                Shader.SetGlobalFloat(property, t * multiplier);
            }
            else if (type == Type.Int)
            {
                Shader.SetGlobalInt(property, (int)(t * multiplier));
            }
        }
        else
        {
            if (type == Type.Float)
            {
                mat.SetFloat(property, t * multiplier);
            }
            else if (type == Type.Int)
            {
                mat.SetInt(property, (int)(t * multiplier));
            }
        }
    }
}
