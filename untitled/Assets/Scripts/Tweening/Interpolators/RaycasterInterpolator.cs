﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

[RequireComponent( typeof( Graphic ) )]
public class RaycasterInterpolator : MonoBehaviour, IInterpolator
{
	[SerializeField]
	[HideInInspector]
	private Graphic graphic;

	private void Awake ()
	{
		graphic = GetComponent<Graphic>();
	}

	public void Interpolate ( float t )
	{
		graphic.raycastTarget = t == 1.0f;
	}
}
