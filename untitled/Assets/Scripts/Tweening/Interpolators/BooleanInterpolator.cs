﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

public enum Comparison
{
	Equal,
	NotEqual,
	Greater,
	Less,
	EqualOrGreater,
	EqualOrLess
}

public abstract class BooleanInterpolator : MonoBehaviour, IInterpolator
{
	[Range( 0.0f, 1.0f )]
	[SerializeField]
	private float threshold;

	[SerializeField]
	private Comparison comparison;

	public void Interpolate ( float t )
	{
		switch ( comparison )
		{
		case Comparison.Equal:
			Set( t == threshold );
			break;
		case Comparison.NotEqual:
			Set( t != threshold );
			break;
		case Comparison.Greater:
			Set( t > threshold );
			break;
		case Comparison.Less:
			Set( t < threshold );
			break;
		case Comparison.EqualOrGreater:
			Set( t >= threshold );
			break;
		case Comparison.EqualOrLess:
			Set( t <= threshold );
			break;
		default:
			break;
		}
	}

	protected abstract void Set ( bool value );
}
