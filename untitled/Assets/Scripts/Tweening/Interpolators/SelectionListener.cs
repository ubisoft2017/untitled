﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;
using UnityEngine.EventSystems;

public class SelectionListener : MonoBehaviour
{
	[SerializeField]
	public GameObject selection;

	[SerializeField]
	private TweenEventList triggers;

	private EventSystem es;

	private void Awake ()
	{
		es = FindObjectOfType<EventSystem>();

		foreach ( var trigger in triggers )
		{
			trigger.OnInvoke += ( e ) =>
			{
				es.SetSelectedGameObject( selection );
				//Debug.Log( GetType().Name, this );
			};
		}

		enabled = false;
	} 
}
