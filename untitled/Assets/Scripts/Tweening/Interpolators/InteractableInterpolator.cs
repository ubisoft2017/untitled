﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

public enum TargetMode
{
	Local,
	Children,
	LocalWithChildren
}

public class InteractableInterpolator : BooleanInterpolator, IInterpolator
{
	[SerializeField]
	private TargetMode mode;

	private bool init;
	private bool track;

	protected override void Set ( bool value )
	{
		if ( track != value || !init )
		{
			switch ( mode )
			{
			case TargetMode.Local:
				foreach ( var selectable in GetComponents<Selectable>() )
				{
					selectable.interactable = value;
				}
				break;
			case TargetMode.Children:
				foreach ( var selectable in GetComponentsInChildren<Selectable>() )
				{
					if ( selectable.gameObject != gameObject )
					{
						selectable.interactable = value;
					}
				}
				break;
			case TargetMode.LocalWithChildren:
				foreach ( var selectable in GetComponentsInChildren<Selectable>() )
				{
					Debug.Log( string.Format( "{0}.interactable = {1}", selectable.name, value ) );
					selectable.interactable = value;
				}
				break;
			default:
				break;
			}

			track = value;
			init = true;
		}
	}
}
