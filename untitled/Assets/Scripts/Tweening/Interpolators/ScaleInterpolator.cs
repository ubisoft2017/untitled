﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;

[RequireComponent( typeof( RectTransform ) )]
public class ScaleInterpolator : Interpolator<Vector2>, IInterpolator
{
	[SerializeField]
	[HideInInspector]
	private RectTransform rect;

	private void Start ()
	{
		rect = GetComponent<RectTransform>();
	}

	public override void Interpolate ( float t )
	{
		Vector2 scale = Vector2.Lerp( a, b, t );

		if ( rect != null )
		{
			rect.localScale = new Vector3( scale.x, scale.y, 1.0f );
		}
	}
}
