﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tweening;
using UnityEngine;


[Serializable]
public class TweenEventList : IList<TweenEvent>
{
	[SerializeField]
	private List<TweenEvent> events = new List<TweenEvent>();

	public TweenEvent this[int index]
	{
		get
		{
			return ((IList<TweenEvent>) events)[index];
		}

		set
		{
			((IList<TweenEvent>) events)[index] = value;
		}
	}

	public int Count
	{
		get
		{
			return ((IList<TweenEvent>) events).Count;
		}
	}

	public bool IsReadOnly
	{
		get
		{
			return ((IList<TweenEvent>) events).IsReadOnly;
		}
	}

	public void Add ( TweenEvent item )
	{
		((IList<TweenEvent>) events).Add( item );
	}

	public void Clear ()
	{
		((IList<TweenEvent>) events).Clear();
	}

	public bool Contains ( TweenEvent item )
	{
		return ((IList<TweenEvent>) events).Contains( item );
	}

	public void CopyTo ( TweenEvent[] array, int arrayIndex )
	{
		((IList<TweenEvent>) events).CopyTo( array, arrayIndex );
	}

	public IEnumerator<TweenEvent> GetEnumerator ()
	{
		return ((IList<TweenEvent>) events).GetEnumerator();
	}

	public int IndexOf ( TweenEvent item )
	{
		return ((IList<TweenEvent>) events).IndexOf( item );
	}

	public void Insert ( int index, TweenEvent item )
	{
		((IList<TweenEvent>) events).Insert( index, item );
	}

	public void InvokeAll ()
	{
		foreach ( var @event in events )
		{
			@event.Invoke();
		}
	}

	public void InvokeAllLocal ( Tweener tweener )
	{
		foreach ( var @event in events )
		{
			tweener.InvokeLocal( @event );
		}
	}

	public bool Remove ( TweenEvent item )
	{
		return ((IList<TweenEvent>) events).Remove( item );
	}

	public void RemoveAt ( int index )
	{
		((IList<TweenEvent>) events).RemoveAt( index );
	}

	IEnumerator IEnumerable.GetEnumerator ()
	{
		return ((IList<TweenEvent>) events).GetEnumerator();
	}
}
