﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;
using UnityEngine.EventSystems;

public class ColorInterpolator : Interpolator<Color>, IInterpolator
{
	[SerializeField]
	private Graphic graphic;

	public override void Interpolate ( float t )
	{
		EventSystem sys;

		if ( graphic != null )
		{
			graphic.color = Color.Lerp( a, b, t );
		}
	}
}
