﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	public class KinoAnalogInterpolator : Interpolator<KinoAnalogInterpolator.AnalogGlitchSettings>, IInterpolator
	{
		[Serializable]
		public struct AnalogGlitchSettings
		{
			[SerializeField, Range(0, 1)] public float scanLineJitter;
			[SerializeField, Range(0, 1)] public float verticalJump;
			[SerializeField, Range(0, 1)] public float horizontalShake;
			[SerializeField, Range(0, 1)] public float colorDrift;
		}

		[SerializeField]
		private Kino.AnalogGlitch effect;

		public override void Interpolate ( float t )
		{
			effect.scanLineJitter = Mathf.Lerp( a.scanLineJitter, b.scanLineJitter, t );
			effect.verticalJump = Mathf.Lerp( a.verticalJump, b.verticalJump, t );
			effect.horizontalShake = Mathf.Lerp( a.horizontalShake, b.horizontalShake, t );
			effect.colorDrift = Mathf.Lerp( a.colorDrift, b.colorDrift, t );
		}
	}
}
