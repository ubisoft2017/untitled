﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Tweening;
using System;

public class ActiveInterpolator : BooleanInterpolator
{
	[SerializeField]
	private List<GameObject> targets;

	protected override void Set ( bool value )
	{
		foreach ( var target in targets )
		{
			if ( target.GetActive() != value )
			{
				target.SetActive( value );
			}
		}
	}
}
