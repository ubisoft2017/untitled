﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	[Serializable]
	[CreateAssetMenu( fileName = "new TweenEvent.asset", menuName = "Tween Event", order = 410 )]
	public class TweenEvent : ScriptableObject
	{
		public event Action<TweenEvent> OnInvoke;

		[SerializeField]
		private string label;

		public string Label
		{
			get
			{
				return label;
			}
		}

		public void Invoke ()
		{
			if ( OnInvoke != null )
			{
				OnInvoke( this );
			}
        }

        public void Clear()
        {
            OnInvoke = delegate{};
        }
	}
}