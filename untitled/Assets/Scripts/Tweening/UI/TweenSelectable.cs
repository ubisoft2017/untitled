﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

namespace Tweening
{
	public class TweenSelectable : Selectable
	{
		public event Action<TweenSelectable,BaseEventData> OnSelected;
		public event Action<TweenSelectable,BaseEventData> OnDeselected;

		protected Tweener[] tweeners;

		[SerializeField]
		private TweenEventList onSelect, onSelectLocal;

		[SerializeField]
		private TweenEventList onDeselect, onDeselectLocal;

		protected override void Awake ()
		{
			base.Awake();

			tweeners = GetComponents<Tweener>();
		}

		public override void OnSelect ( BaseEventData eventData )
		{
			base.OnSelect( eventData );

			if ( onSelectLocal != null )
			{
				foreach ( var tweener in tweeners )
				{
					onSelectLocal.InvokeAllLocal( tweener );
				}
			}

			if ( onSelect != null )
			{
				onSelect.InvokeAll();
			}

			if ( OnSelected != null )
			{
				OnSelected( this, eventData );
			}

			//Debug.Log( "select" );
		}

		public override void OnDeselect ( BaseEventData eventData )
		{
			base.OnDeselect( eventData );

			if ( onDeselectLocal != null )
			{
				foreach ( var tweener in tweeners )
				{
					onDeselectLocal.InvokeAllLocal( tweener );
				}
			}

			if ( onDeselect != null )
			{
				onDeselect.InvokeAll();
			}

			if ( OnDeselected != null )
			{
				OnDeselected( this, eventData );
			}

			//Debug.Log( "deselect" );
		}
	}
}