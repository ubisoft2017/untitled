﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

namespace Tweening
{
	public class TweenButton : TweenSelectable, ISubmitHandler, ICancelHandler
	{
		public event Action<TweenButton,BaseEventData> OnSubmit; 
		public event Action<TweenButton,BaseEventData> OnCancel; 

		[SerializeField]
		private TweenEventList onSubmit, onSubmitLocal;

		[SerializeField]
		private TweenEventList onCancel, onCancelLocal;

		public override void OnPointerDown ( PointerEventData eventData )
		{
			base.OnPointerDown( eventData );

			(this as ISubmitHandler).OnSubmit( eventData );
		}

		void ISubmitHandler.OnSubmit ( BaseEventData eventData )
		{
			if ( onSubmitLocal != null )
			{
				foreach ( var tweener in tweeners )
				{
					onSubmitLocal.InvokeAllLocal( tweener );
				}
			}

			if ( onSubmit != null )
			{
				onSubmit.InvokeAll();
			}

			if ( OnSubmit != null )
			{
				OnSubmit( this, eventData );
			}

			//Debug.Log( string.Format( "{0}.OnSubmit", name ) );
		}

		void ICancelHandler.OnCancel ( BaseEventData eventData )
		{
			if ( onCancelLocal != null )
			{
				foreach ( var tweener in tweeners )
				{
					onCancelLocal.InvokeAllLocal( tweener );
				}
			}

			if ( onCancel != null )
			{
				onCancel.InvokeAll();
			}

			if ( OnCancel != null )
			{
				OnCancel( this, eventData );
			}
		}
	}
}