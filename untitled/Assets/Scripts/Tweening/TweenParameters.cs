﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	[CreateAssetMenu( fileName = "new TweenParameters.asset", menuName = "Tween Parameters", order = 411 )]
	public class TweenParameters : ScriptableObject
	{
		[SerializeField]
		private float duration = 0.5f;

		public float Duration
		{
			get
			{
				return duration;
			}
		}

		[SerializeField]
		private AnimationCurve curve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );

		public AnimationCurve Curve
		{
			get
			{
				return curve;
			}
		}
	}
}