﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	public abstract class Interpolator<T> : MonoBehaviour, IInterpolator
	{
		[SerializeField]
		protected T a;

		public T A
		{
			get
			{
				return a;
			}
		}

		[SerializeField]
		protected T b;

		public T B
		{
			get
			{
				return b;
			}
		}

		public abstract void Interpolate ( float t );
	}
}