﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tweening
{
	[Flags]
	public enum ModifierFlags
	{
		Reverse = 1,
		Invert = 2,
		Min = 4,
		Max = 8,
		Debug = 16
	}
}