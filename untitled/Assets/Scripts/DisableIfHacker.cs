﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableIfHacker : MonoBehaviour
{


    bool wasDisabled = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!wasDisabled && FindObjectOfType<SessionController>().LocalSelection == PlayerSelection.Hacker)
        {
            for (int i = 0; i < transform.childCount; i++)
            {

                transform.GetChild(i).gameObject.SetActive(false);
            }
            wasDisabled = true;
        }
    }
}
