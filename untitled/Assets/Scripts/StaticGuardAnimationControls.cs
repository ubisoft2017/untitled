﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticGuardAnimationControls : StateMachineBehaviour {


	//anim info
	//1 = Handheld on hip
	//2 = lookAroundLoop
	//3 = handOnHipLoop
	//4 = le_but_stratch
	//5 = cross arms look around

	private int choice;
	private int size = 7;

	private int makeChoice ()
	{
		choice = choice + Random.Range( 1, (size -1) );
		choice %= size;
		return choice;
	}


	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		makeChoice();
		animator.SetInteger( "chooseAnim", choice  );
	}	
}
