﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSystem : Node {
	

	[SerializeField]
	private GameObject[] laserConfigurations;
	[SerializeField]
	private bool isTutorial;


	private int currentLaserConfiguration = 0;

	public override NodeType NodeType
	{
		get
		{
			return NodeType.LaserSystem;
		}
	}



	public void NextLaserConfiguration ()
	{
		if ( isTutorial )
		{
			FindObjectOfType<TutorialLevel2>().DisableLaserText();
		}

		laserConfigurations[currentLaserConfiguration].SetActive( false );
		currentLaserConfiguration++;

		currentLaserConfiguration %= laserConfigurations.Length;

		laserConfigurations[currentLaserConfiguration].SetActive( true );

	}
}
