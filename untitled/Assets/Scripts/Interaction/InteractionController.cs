﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent(typeof(InteractionRaycaster))]
public abstract class InteractionController<T> : ControllerBase where T : IInteractable
{
    [SerializeField] [HideInInspector] private InteractionRaycaster raycaster;

    [Tooltip("Whether or not to search the entire hierarchy of a raycast result.")]
    [SerializeField]
    private bool includeChildren;

    [SerializeField] [HideInInspector] private T result;

    private bool lastHasResult = false;

    private T lastResult;

    public T Result
    {
        get
        {
            return result;
        }
    }

    public bool HasResult
    {
        get
        {
            return result != null && result.canInteract();
        }
    }

    [HideInInspector] public bool canNewlyInteract = false;
    [HideInInspector] public bool canNoLongerInteract = false;

    protected override void ControllerStart()
    {
        base.ControllerStart();

        raycaster = GetComponent<InteractionRaycaster>();
    }

    protected override void ControllerUpdate()
    {
        base.ControllerUpdate();

        if (raycaster.Hit)
        {
            result = includeChildren ? raycaster.Nearest.collider.GetComponentInChildren<T>() : raycaster.Nearest.collider.GetComponent<T>();
        }
        else
        {
            result = default(T);
        }

        if (HasResult != lastHasResult)
        {
            if (HasResult)
            {
                canNewlyInteract = true;
                if (result.canInteract())
                {
                    result.ShowPrompt(Player.Profile);
                }
            }
            else
            {
                canNoLongerInteract = true;
                if (lastResult.promptVisible())
                {
                    lastResult.HidePrompt();
                }
            }
        }
        else
        {
            canNewlyInteract = false;
            canNoLongerInteract = false;
        }

        lastHasResult = HasResult;
        lastResult = result;


    }
}
