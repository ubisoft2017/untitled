﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controls;

public interface IInteractable
{
    void ShowPrompt(PlayerProfile player);
    void HidePrompt();

    //add at some point to unify interaction system
    //void Interact();

    bool canInteract();
    bool promptVisible();
    ButtonAction GetAction();
}
