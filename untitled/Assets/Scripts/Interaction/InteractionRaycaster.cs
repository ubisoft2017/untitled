﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides a shared raycasting service for a collection of InteractionControllers.
/// </summary>
[RequireComponent( typeof( Eye ) )]
public class InteractionRaycaster : Raycaster
{
	private void Start ()
	{
		caster = GetComponent<Eye>().EyeTransform;
	}
}
