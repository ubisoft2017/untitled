﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides a raycasting service.
/// </summary>
public class Raycaster : MonoBehaviour
{
	[SerializeField] private LayerMask layerMask;

	[SerializeField] private float range;

	[SerializeField][HideInInspector] private bool hit;

	public bool Hit
	{
		get
		{
			return hit;
		}
	}

	[SerializeField][HideInInspector] private RaycastHit nearest;

	public RaycastHit Nearest
	{
		get
		{
			return nearest;
		}
	}

	[SerializeField][HideInInspector] public Transform caster;

	private static Ray ray;

	private static int n, i;

	private static float minimum;

	private static RaycastHit[] results;

	static Raycaster ()
	{
		// Initialize RaycastHit buffer, required to use Physics.RaycastNonAlloc
		results = new RaycastHit[256];
	}

	public void Clear ()
	{
		hit = false;
		nearest = default( RaycastHit );
	}

	private void Start ()
	{
		caster = transform;

	} 

	private void Update ()
	{
		Raycast();
	}

	public bool Raycast ()
	{
		// Setup ray using eye position / direction

		ray.origin = caster.position;
		ray.direction = caster.forward;

		// Raycast along ray
		hit = Physics.RaycastNonAlloc( ray, results, range, layerMask ) > 0;

		minimum = float.PositiveInfinity;

		// Find the nearest point of contact along ray

		if ( hit )
		{
			nearest = results[0];

			for ( i = 0; i < n; i++ )
			{
				if ( results[i].distance < minimum )
				{
					minimum = results[i].distance;
					nearest = results[i];
				}
			}

			return true;
		}

		return false;
	}
}
