﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides a raycasting service.
/// </summary>
public class Raycaster2D : MonoBehaviour
{
	[SerializeField] public LayerMask layerMask;

	[SerializeField] public float range;

	[SerializeField][HideInInspector] private bool hit;

	public bool Hit
	{
		get
		{
			return hit;
		}
	}

	[SerializeField][HideInInspector] private RaycastHit2D nearest;

	public RaycastHit2D Nearest
	{
		get
		{
			return nearest;
		}
	}

	[SerializeField][HideInInspector] public Transform caster;

	private static Vector2 origin;
	private static Vector2 direction;

	private static int n, i;

	private static float minimum;

	private static RaycastHit2D[] results;

	static Raycaster2D ()
	{
		// Initialize RaycastHit buffer, required to use Physics.RaycastNonAlloc
		results = new RaycastHit2D[256];
	}

	public void Clear ()
	{
		hit = false;
		nearest = default( RaycastHit2D );
	}

	private void Start ()
	{
		caster = transform;
	} 

	private void Update ()
	{
		Raycast();
	}

	public bool Raycast ()
	{
		// Setup ray using eye position / direction

		origin = caster.position;
		direction = caster.forward;

		// Raycast along ray
		hit = Physics2D.RaycastNonAlloc( origin, direction, results, range, layerMask ) > 0;

		minimum = float.PositiveInfinity;

		// Find the nearest point of contact along ray

		if ( hit )
		{
			nearest = results[0];

			for ( i = 0; i < n; i++ )
			{
				if ( results[i].distance < minimum )
				{
					minimum = results[i].distance;
					nearest = results[i];
				}
			}

			return true;
		}

		return false;
	}
}
