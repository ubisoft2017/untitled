﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public enum FireTweenEventBehaviour
{
	Awake,
	Start,
	Manual
}

public class FireTweenEvent : MonoBehaviour
{
	[SerializeField]
	private Tweening.TweenEvent tweenEvent;

	[SerializeField]
	private FireTweenEventBehaviour behaviour = FireTweenEventBehaviour.Awake;

	[SerializeField]
	private bool local;

	[SerializeField]
	private float delay;

	public void Fire ()
	{
		if ( local )
		{
			foreach ( var tweener in GetComponents<Tweener>() )
			{
				tweener.InvokeLocal( tweenEvent );
			}
		}
		else
		{
			if ( tweenEvent != null )
			{
				tweenEvent.Invoke();
			}
		}
	}

	private void Awake ()
	{
		if ( behaviour == FireTweenEventBehaviour.Awake )
		{
			if ( delay > 0.0f )
			{
				Invoke( "Fire", delay );
			}
			else
			{
				Fire();
			}
		}
	}
	
	private void Start ()
	{
		if ( behaviour == FireTweenEventBehaviour.Start )
		{
			if ( delay > 0.0f )
			{
				Invoke( "Fire", delay );
			}
			else
			{
				Fire();
			}
		}
	}
}
