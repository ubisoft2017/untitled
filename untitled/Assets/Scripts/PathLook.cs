﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathLook : PathInterface
{
	void Update ()
	{
		transform.rotation = Quaternion.LookRotation( PositionAt( Time.time ) - transform.position );
	}
}
