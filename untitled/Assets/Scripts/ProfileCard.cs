﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProfileCard : MonoBehaviour
{
	public RectTransform rect;

	public Button button;
	public Image avatar;
	public Text label;

	public EventTrigger eventTrigger;
}
