﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProfileController : MonoBehaviour
{
	[SerializeField]
	private RectTransform profileCardContainer;

	[SerializeField]
	private GameObject profileCardPrefab;

	[SerializeField]
	private ProfileCard newProfileCard;

	[SerializeField]
	private TweenEvent showProfilesEvent, hideProfilesEvent, newProfileEvent, ubiProfileEvent;

	[SerializeField]
	private TweenEventList onJoin;

	private List<ProfileCard> cards;

	private Player player;

	public Player Player
	{
		get
		{
			return player;
		}
	}

	private EventSystem es;

	private GameObject history;

	private GameObject active;

	[SerializeField]
	private List<string> names;

	[SerializeField]
	private List<Sprite> sprites;

	public string RandomName ()
	{
		return string.Format( "Agent {0}", names[Random.Range( 0, names.Count )] );
	}

	private void Awake ()
	{
		/// Inject Player into all controllers in newly loaded scenes
		SceneManager.sceneLoaded += HandleSceneLoad;

		es = FindObjectOfType<EventSystem>();

		showProfilesEvent.OnInvoke += ( e ) =>
		{
			history = es.currentSelectedGameObject;

			if ( active != null )
			{
				es.SetSelectedGameObject( active );
				Debug.Log( GetType().Name, this );
				CenterViewOn( active );
			}
			else if ( cards.Count > 0 )
			{
				es.SetSelectedGameObject( cards[0].gameObject );
				Debug.Log( GetType().Name, this );
				CenterViewOn( cards[0].gameObject );
			}
			else
			{
				es.SetSelectedGameObject( newProfileCard.gameObject );
				Debug.Log( GetType().Name, this );
				CenterViewOn( newProfileCard.gameObject );
			}
		};

		hideProfilesEvent.OnInvoke += ( e ) =>
		{
			es.SetSelectedGameObject( history );
		};

		newProfileEvent.OnInvoke += ( e ) =>
		{
			var profile = new PlayerProfile( RandomName() );

			PlayerProfiles.Write( profile );

			/// Instantiate card UI prefab for new profile
			var card = CreateCardFor( profile );

			/// Setup explicit navigation
			SetupNavigation();

			/// Select new profile
			es.SetSelectedGameObject( card.gameObject );
			Debug.Log( GetType().Name, this );

			StartCoroutine( CenterDelayed( card.gameObject ) );
		};

		/// Temp stuff for Ubi presentation day
		ubiProfileEvent.OnInvoke += ( e ) =>
		{
			var profile = new PlayerProfile( "" );

			PlayerProfiles.Write( profile );

			player.TryJoinAs( profile );

			onJoin.InvokeAll();
		};

		SetupSelectHandler( newProfileCard, newProfileCard.gameObject );

		if ( names == null )
		{
			names = new List<string>();
		}

		names.Add( "Collins" );
		names.Add( "Viggo" );
		names.Add( "Mathis" );
		names.Add( "Stella" );
		names.Add( "Greggor" );
		names.Add( "Chase" );
		names.Add( "Olsen" );
		names.Add( "Flint" );
		names.Add( "Stone" );
		names.Add( "Number 9" );
		names.Add( "Number 6" );
		names.Add( "Smart" );
		names.Add( "Walker" );
		names.Add( "Clover" );
		names.Add( "Lynd" );
		names.Add( "Archer" );
		names.Add( "Bauer" );
		names.Add( "Smith" );
		names.Add( "Palmer" );
		names.Add( "Smiley" );
		names.Add( "Hunt" );
		names.Add( "Banks" );
		names.Add( "Snake" );
		names.Add( "Dark" );
		names.Add( "English" );
		names.Add( "Powers" );
		names.Add( "Evil" );
	}

	private IEnumerator CenterDelayed ( GameObject instance )
	{
		yield return null;

		CenterViewOn( instance );

		yield break;
	}

	private void HandleSceneLoad ( Scene scene, LoadSceneMode mode )
	{
		foreach ( var controller in FindObjectsOfType<ControllerBase>() )
		{
			controller.Player = player;
		}
	}

	/// This must be in Start. In Awake, profiles may not have been read from the disk, yet.
	private void Start ()
	{
		player = new Player();

		cards = new List<ProfileCard>( PlayerProfiles.AvailableProfiles.Count );

		/// Instantiate card UI prefabs for each profile
		foreach ( var available in PlayerProfiles.AvailableProfiles )
		{
			CreateCardFor( available );
		}

		/// Add "new profile" card
		cards.Add( newProfileCard );

		/// Setup explicit navigation
		SetupNavigation();
	}

	private ProfileCard CreateCardFor ( PlayerProfile profile )
	{
		var instance = Instantiate( profileCardPrefab, profileCardContainer, false );

		var card = instance.GetComponent<ProfileCard>();

		card.label.text = profile.name;

		cards.Add( card );

		/// Hook up events
		card.button.onClick.AddListener( () =>
		{
			JoinAs( profile, card );
		} );

		SetupSelectHandler( card, instance );

		return card;
	}

	private void SetupSelectHandler( ProfileCard card, GameObject instance )
	{
		var entry = new EventTrigger.Entry();

		var callback = new EventTrigger.TriggerEvent();

		callback.AddListener( ( e ) =>
		{
			CenterViewOn( instance );
		} );

		entry.eventID = EventTriggerType.Select;

		entry.callback = callback;

		card.eventTrigger.triggers.Add( entry );
	}

	private void CenterViewOn( GameObject instance )
	{
		var index = instance.transform.GetSiblingIndex();

		var width = profileCardContainer.rect.width;

		var n = profileCardContainer.childCount;

		var spacing = profileCardContainer.GetComponent<HorizontalLayoutGroup>().spacing;

		width -= spacing * (n - 1);

		width /= n;

		profileCardContainer.anchoredPosition = new Vector2( - index * (width + spacing ) - width * 0.5f, 30.0f );
	}

	private void JoinAs ( PlayerProfile profile, ProfileCard card )
	{
		if ( player.TryJoinAs( profile ) )
		{
			if ( onJoin != null )
			{
				onJoin.InvokeAll();
			}

			Debug.Log( "joined as " + profile.name );

			//es.SetSelectedGameObject( history );

			active = card.gameObject;
		}
	}

	private void SetupNavigation ()
	{
		newProfileCard.transform.SetAsLastSibling();

		for ( int i = 0; i < profileCardContainer.childCount; i++ )
		{
			var nav = profileCardContainer.GetChild( i ).GetComponent<ProfileCard>().button.navigation;

			nav.mode = Navigation.Mode.Explicit;

			if ( i > 0 )
			{
				nav.selectOnLeft = profileCardContainer.GetChild( i - 1 ).GetComponent<ProfileCard>().button;
			}

			if ( i < cards.Count - 1 )
			{
				nav.selectOnRight = profileCardContainer.GetChild( i + 1 ).GetComponent<ProfileCard>().button;
			}

			profileCardContainer.GetChild( i ).GetComponent<ProfileCard>().button.navigation = nav;
		}
	}
}
