﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tweening;
using System;

[RequireComponent( typeof( Tweener ) )]
[RequireComponent( typeof( LineRenderer ) )]
public class Edge : MonoBehaviour, IInterpolator
{
	private new LineRenderer renderer;
	private Tweener tweener;

	[SerializeField]
	private float speed;

	//[SerializeField]
	//private float padding;

	[SerializeField]
	private float height;

	[SerializeField]
	[HideInInspector]
	private NodeUI a, b;

	public NodeUI A
	{
		get
		{
			return a;
		}
	}

	public NodeUI B
	{
		get
		{
			return b;
		}
	}

	private NodeUI source;

	[SerializeField]
	private Tweening.TweenEvent discoverEvent;

	private void Awake ()
	{
		tweener = GetComponent<Tweener>();
		renderer = GetComponent<LineRenderer>();
		renderer.enabled = false;
	}

	private Vector3 delta, direction, start, end;

	public void Discover ( NodeUI a, NodeUI b )
	{
		this.a = a;
		this.b = b;

		delta = a.transform.position - b.transform.position;
		direction = delta.normalized;

		auto = a is GuardUI || b is GuardUI;

		Refresh();

		renderer.numPositions = 2;
		renderer.SetPosition( 0, start );
		renderer.SetPosition( 1, start );
		renderer.enabled = true;

		tweener.OnFinishTween += HandleFinishTween;

		tweener.Listeners[0].Multiplier = Vector3.Distance( start, end ) / speed;

		tweener.InvokeLocal( discoverEvent );

		Debug.Log( string.Format( "discovering edge from {0} to {1}", A.TargetInstanceID, B.TargetInstanceID ), this );
	}

	private bool auto, done;

	private void HandleFinishTween ( Tweener tweener )
	{
		tweener.OnFinishTween -= HandleFinishTween;

		B.Discover();
	}

	private void Update ()
	{
		if ( done && auto )
		{
			Interpolate( 1.0f );
		}
	} 

	private void Refresh ()
	{
		start = a.transform.position + direction * -a.Padding + Vector3.back * height;
		end = b.transform.position - direction * -b.Padding + Vector3.back * height;
	}

	public void Interpolate ( float t )
	{
		if ( auto )
		{
			Refresh();
		}

		renderer.SetPosition( 1, Vector3.Lerp( start, end, t ) );
	}
}