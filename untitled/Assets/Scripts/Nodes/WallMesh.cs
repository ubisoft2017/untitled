﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class WallMesh : MonoBehaviour
{
	[SerializeField]
	private GameObject linePrefab;

	[SerializeField]
	private MeshFilter filter;

	[SerializeField]
	private new MeshRenderer renderer;

	private List<Vector3> corners;
	private List<int> offsets;
	private List<int> lengths;
	private List<float> heights;

	private int triangleCount;

	public bool debug;

	private void Update ()
	{
		if ( debug )
		{
			debug = false;

			Initialize( 1, transform.childCount );

			var positions = new Vector3[transform.childCount];

			for ( int i = 0; i < transform.childCount; i++ )
			{
				positions[i] = transform.GetChild( i ).position;
			}

			Add( positions, 2.0f );

			Finish( Vector3.up );
		}
	} 

	public void Initialize ( int wallCount, int cornerCount )
	{
		corners = new List<Vector3>( cornerCount );
		offsets = new List<int>( wallCount );
		lengths = new List<int>( wallCount );
		heights = new List<float>( wallCount );
	} 

	public GameObject Add ( Vector3[] wall, float height )
	{
		offsets.Add( corners.Count );
		corners.AddRange( wall );
		lengths.Add( wall.Length );
		heights.Add( height );

		triangleCount += (wall.Length - 1) * 4;

		var gameObject = Instantiate( linePrefab, transform, false );

		return gameObject;
	}

	public void Finish ( Vector3 up )
	{
		var mesh = new Mesh();

		mesh.name = "Virtual Walls";

		mesh.MarkDynamic();

		var vertices = new Vector3[corners.Count * 2];
		var triangles = new int[triangleCount * 3];

		int v = 0;
		int t = 0;

		for ( int i = 0; i < offsets.Count; i++ )
		{
			for ( int j = offsets[i]; j < offsets[i] + lengths[i]; j++ )
			{
				vertices[v++] = corners[j];
				vertices[v++] = corners[j] + up * heights[i];

				if ( j > offsets[i] )
				{
					triangles[t++] = v - 1;
					triangles[t++] = v - 2;
					triangles[t++] = v - 4;

					triangles[t++] = v - 1;
					triangles[t++] = v - 4;
					triangles[t++] = v - 2;

					triangles[t++] = v - 1;
					triangles[t++] = v - 3;
					triangles[t++] = v - 4;

					triangles[t++] = v - 1;
					triangles[t++] = v - 4;
					triangles[t++] = v - 3;
				}
			}
		}

		mesh.vertices = vertices;
		mesh.triangles = triangles;

		/// Calculate the mesh's bounding box
		mesh.RecalculateBounds();

		/// We do not need to calculate the normals since the walls do not use a lit shader
		mesh.normals = new Vector3[vertices.Length];

		/// We do not need to calculate the real tangents since we are not normal mapping
		mesh.tangents = new Vector4[vertices.Length];

		filter.sharedMesh = mesh;
	}

	public void CopyFrom ( WallMesh wallMesh, bool copyMaterial = true )
	{
		filter.sharedMesh = wallMesh.filter.sharedMesh;

		if ( copyMaterial )
		{
			renderer.sharedMaterial = wallMesh.renderer.sharedMaterial;
		}
	}
}
