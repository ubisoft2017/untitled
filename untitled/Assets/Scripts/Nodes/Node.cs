﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum NodeType : short
{
	JunctionBox,
	Door,
	Clock,
	Route,
	Valuable,
	LaserSystem,
	Guard,
	Relay,
	Exit
}

public abstract class Node : MonoBehaviour
{
	private static Dictionary<int,Node> registry;

	protected Phase owner;

    public int preHackTutorialTextIndex = -1;
    public int postHackTutorialTextIndex = -1;

    public Phase Owner
	{
		get
		{
			return owner;
		}

		set
		{
			if ( value != owner )
			{
				owner = value;
			}
		}
	}

	static Node ()
	{
		registry = new Dictionary<int, Node>();
	}

	public static T Get<T>( int instanceID ) where T : Node
	{
		return registry[instanceID] as T;
	}

	public static void ClearRegistry ()
	{
		registry.Clear();
	}

	public abstract NodeType NodeType { get; }

	protected void OnEnable ()
	{
		registry.Add( GetInstanceID(), this );
	}

	protected virtual void Awake ()
	{
		AgentToHacker.Enqueue( this );
	}
}
