﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "new LineRendererParameters", menuName = "LineRenderer Parameters" )]
public class LineRendererParameters : ScriptableObject
{
	[SerializeField]
	UnityEngine.Rendering.ShadowCastingMode castShadows;

	[SerializeField]
	private bool receiveShadows;

	[SerializeField]
	private Material material;

	[SerializeField]
	private Color color = Color.white;

	[SerializeField]
	private int numCapVertices = 4;

	[SerializeField]
	private float width = 0.1f;

	public void CopyTo ( LineRenderer renderer )
	{
		renderer.shadowCastingMode = castShadows;
		renderer.receiveShadows = receiveShadows;

		renderer.material = material;

		renderer.startColor = color;
		renderer.endColor = color;

		renderer.startWidth = width;
		renderer.endWidth = width;

		renderer.numCapVertices = numCapVertices;
	}
}