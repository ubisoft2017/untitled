﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct NodeInfo
{
    public int instanceID;
    public Vector2 position;
    public float rotation;
    public NodeType type;
    public int[] connections;
    public Vector2[] patrolRoute;
    public int phaseID;
    public bool open;
    public int preHackTutorialTextIndex;
    public int postHackTutorialTextIndex;

    public NodeInfo(Node node, NodeGraph graph)
    {
        instanceID = node.GetInstanceID();

        phaseID = node.Owner == null ? -1 : node.Owner.transform.GetSiblingIndex();

        position = new Vector2(node.transform.position.x, node.transform.position.z);
        rotation = node.transform.rotation.eulerAngles.y;

        type = node.NodeType;

        preHackTutorialTextIndex = node.preHackTutorialTextIndex;
        postHackTutorialTextIndex = node.postHackTutorialTextIndex;

        var nodes = graph.GetConnections(node);

        connections = new int[nodes.Count];

        for (int i = 0; i < nodes.Count; i++)
        {
            connections[i] = nodes[i].GetInstanceID();
        }

        if (node is PatrolRoute)
        {
            var route = node as PatrolRoute;

            bool closed = route.Type == PatrolRouteType.Wrap;

            patrolRoute = new Vector2[route.transform.childCount + (closed ? 1 : 0)];

            for (int i = 0; i < route.transform.childCount + (closed ? 1 : 0); i++)
            {
                int j = i % route.transform.childCount;
                Vector2 tempPosition = new Vector2(route.transform.GetChild(j).position.x, route.transform.GetChild(j).position.z);
                patrolRoute[i] = tempPosition;
            }
        }
        else
        {
            patrolRoute = new Vector2[0];
        }

        if (node is Door)
        {
            open = (node as Door).State > 0.0f;
        }
        else
        {
            open = false;
        }
    }

    public byte[] ToBytes()
    {

        var bytes = new byte[39 + (connections.Length * 4) + (patrolRoute.Length * 8)];

        BitConverter.GetBytes(instanceID).CopyTo(bytes, 0);

        BitConverter.GetBytes(position.x).CopyTo(bytes, 4);
        BitConverter.GetBytes(position.y).CopyTo(bytes, 8);

        BitConverter.GetBytes(rotation).CopyTo(bytes, 12);

        BitConverter.GetBytes((short)type).CopyTo(bytes, 16);

        BitConverter.GetBytes(connections.Length).CopyTo(bytes, 18);
        BitConverter.GetBytes(patrolRoute.Length).CopyTo(bytes, 22);

        BitConverter.GetBytes(phaseID).CopyTo(bytes, 26);
        BitConverter.GetBytes(open).CopyTo(bytes, 30);

        BitConverter.GetBytes(preHackTutorialTextIndex).CopyTo(bytes, 31);
        BitConverter.GetBytes(postHackTutorialTextIndex).CopyTo(bytes, 35);

        int j = 39;

        for (int i = 0; i < connections.Length; i++, j += 4)
        {
            BitConverter.GetBytes(connections[i]).CopyTo(bytes, j);
        }


        for (int i = 0; i < patrolRoute.Length; i++, j += 4)
        {
            BitConverter.GetBytes(patrolRoute[i].x).CopyTo(bytes, j);
            BitConverter.GetBytes(patrolRoute[i].y).CopyTo(bytes, j += 4);
        }

        return bytes;

    }

    public NodeInfo(byte[] bytes)
    {

        instanceID = BitConverter.ToInt32(bytes, 0);

        float x = BitConverter.ToSingle(bytes, 4);
        float y = BitConverter.ToSingle(bytes, 8);

        position = new Vector2(x, y);

        rotation = BitConverter.ToSingle(bytes, 12);

        type = (NodeType)BitConverter.ToInt16(bytes, 16);

        var connectionsLength = BitConverter.ToInt32(bytes, 18);
        var patrolLength = BitConverter.ToInt32(bytes, 22);

        phaseID = BitConverter.ToInt32(bytes, 26);
        open = BitConverter.ToBoolean(bytes, 30);

        preHackTutorialTextIndex = BitConverter.ToInt32(bytes, 31);
        postHackTutorialTextIndex = BitConverter.ToInt32(bytes, 35);

        if(type == NodeType.JunctionBox)
        {        Debug.Log("HGJG " + postHackTutorialTextIndex);


        }
        connections = new int[connectionsLength];
        patrolRoute = new Vector2[patrolLength];

        int j = 39;

        for (int i = 0; i < connectionsLength; i++, j += 4)
        {
            connections[i] = BitConverter.ToInt32(bytes, j);
        }

        for (int i = 0; i < patrolLength; i++, j += 4)
        {
            float patrolX = BitConverter.ToSingle(bytes, j);
            float patrolY = BitConverter.ToSingle(bytes, j += 4);

            patrolRoute[i].x = patrolX;
            patrolRoute[i].y = patrolY;
        }


    }
}