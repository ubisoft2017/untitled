﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( NodeView ) )]
public class NodeViewRotateController : ControllerBase
{
	[EnumObject][SerializeField] private JoystickAction rotateAction;

	[SerializeField] private Vector2 velocity;

	[SerializeField][HideInInspector] private NodeView view;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		view = GetComponent<NodeView>();
	}

	protected override void ControllerFixedUpdate ()
	{
		base.ControllerUpdate();

		Vector2 vector = GamePad.Query( rotateAction, Player.Profile, Player.State );

		if ( Mathf.Sign( view.angularVelocity ) != Mathf.Sign( vector.x ) )
		{
			view.angularVelocity = Mathf.Lerp( view.angularVelocity, 0.0f, Time.deltaTime );
		}

		if ( Mathf.Sign( view.verticalVelocity ) != Mathf.Sign( vector.y ) )
		{
			view.verticalVelocity = Mathf.Lerp( view.verticalVelocity, 0.0f, Time.deltaTime );
		}

		view.angularVelocity += vector.x * velocity.x * Time.fixedDeltaTime;
		view.verticalVelocity += vector.y * velocity.y * Time.fixedDeltaTime;
	}
}
