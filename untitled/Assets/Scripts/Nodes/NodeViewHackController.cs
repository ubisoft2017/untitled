﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( NodeView ) )]
public class NodeViewHackController : ControllerBase
{
	[EnumObject][SerializeField] private ButtonAction hackAction;

	[SerializeField][HideInInspector] private NodeView view;

	private bool hold;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		view = GetComponent<NodeView>();

		Register( hackAction, () => hold = true );
	}

	protected override void ControllerUpdate ()
	{
		if ( hold )
		{
			view.StartHacking();
		}
		else
		{
			view.StopHacking();
		}
	} 

	private void LateUpdate ()
	{
		hold = false;
	}
}
