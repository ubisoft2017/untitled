﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( MeshFilter), typeof( MeshRenderer ) )]
[RequireComponent( typeof( EdgeCollider2D ) )]
public class WallUI : MonoBehaviour
{
	[SerializeField]
	private new EdgeCollider2D collider;
	
	//private new LineRenderer renderer;

	private void Awake ()
	{
		//renderer = GetComponent<LineRenderer>();
		//collider = GetComponent<EdgeCollider2D>();

		enabled = false;
	}

	public Vector3[] SetCorners ( Vector2[] positions )
	{
		Vector3[] positions3 = new Vector3[positions.Length];

		for ( int i = 0; i < positions.Length; i++ )
		{
			positions3[i] = positions[i];
		}

		//renderer.numPositions = positions.Length;
		//renderer.SetPositions( positions3 );

		collider.points = positions;

		return positions3;
	}
}
