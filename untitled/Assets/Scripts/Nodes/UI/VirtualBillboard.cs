﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class VirtualBillboard : MonoBehaviour
{
	private new Camera camera;

	private void Awake ()
	{
		camera = GameObject.FindGameObjectWithTag( "Virtual Camera" ).GetComponent<Camera>();
	}

	private void Update ()
	{
		var delta = camera.transform.position - transform.position;

		var direction = delta.normalized;

		transform.rotation = Quaternion.LookRotation( Vector3.forward, direction );
	} 
}
