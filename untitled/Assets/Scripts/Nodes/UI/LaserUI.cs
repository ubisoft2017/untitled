﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Tweening;

public class LaserUI : NodeUI
{
    [SerializeField]
    private TweenEvent cycle, cycleBack;

    private bool hasCycled = false;

    public override bool CanBeInteractedWith
    {
        get
        {
            return HasBeenHacked;
        }
    }

    public override string ViewLabel
    {
        get
        {
            return "Laser Array";
        }
    }

    public override string InteractionPrompt
    {
        get
        {
            return "Cycle Configurations";
        }
    }

    protected override void OnStartInteract()
    {
        system.Add(Cost);

        HackerToAgent.NotifyCycleLaserSystem(this);


        if (hasCycled)
        {
            GetComponent<Tweener>().InvokeLocal(cycleBack);
        }
        else
        {
            GetComponent<Tweener>().InvokeLocal(cycle);
        }

        hasCycled = !hasCycled;
    }
}
