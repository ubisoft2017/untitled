﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValuableUI : NodeUI
{
	[SerializeField]
	private Transform model;

	[SerializeField]
	private AnimationCurve curve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 0.0f );

	[SerializeField]
	private float speed;

	[SerializeField]
	private GameObject checkmark;

	public override bool CanBeInteractedWith
	{
		get
		{
			return false;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Valuable";
		}
	}

	private float ts;
	private float td;

	private Vector3 zero;

	private bool stolen;

	protected new void Awake ()
	{
		base.Awake();

		zero = model.localPosition;
	} 

	public void Steal ()
	{
		stolen = true;
		
		if ( HasBeenHacked )
		{
			ShowStolen();
		}
	}

	private void ShowStolen ()
	{
		checkmark.SetActive( true );

		foreach ( var renderer in renderers )
		{
			renderer.enabled = false;
		}
	}

	protected override void DoSelectedIdle ()
	{
		base.DoSelectedIdle();

		ts += Time.deltaTime * speed;

		float eval = curve.Evaluate( ts );

		//model.localScale = Vector3.one * (1.0f + eval);
		model.localPosition = zero + Vector3.back * eval * 0.5f;

		DoDeselectedIdle();
	}

	protected override void DoDeselectedIdle ()
	{
		base.DoDeselectedIdle();

		td += Time.deltaTime * speed;

		model.localScale = Vector3.one;
		model.localPosition = zero;

		model.localRotation = Quaternion.Euler( -180.0f, 0.0f, td * 90.0f );
	}

	protected override void OnHack ()
	{
		base.OnHack();

		if ( stolen )
		{
			ShowStolen();
		}
	}
}
