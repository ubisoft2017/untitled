﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class SFX : MonoBehaviour
{
	private AudioSourcePool pool;

	protected virtual void Awake ()
	{
		pool = FindObjectOfType<AudioSourcePool>();
	}

	private event Action clear;

	public void PlayClip ( AudioClip clip, float volume = 1.0f, bool interrupt = true )
	{
		if ( clip != null )
		{
			if ( interrupt )
			{
				if ( clear != null )
				{
					clear();

					foreach ( Delegate d in clear.GetInvocationList() )
					{
						clear -= (Action) d;
					}
				}

				StopAllCoroutines();
			}

			StartCoroutine( PlayClip( clip, volume ) );
		}
	}

	private IEnumerator PlayClip ( AudioClip clip, float volume )
	{
		var source = pool.Get();

		Action release = () => pool.Release( source );

		clear += release;

		source.clip = clip;
		
		source.volume = volume;

		source.Play();

		float end = Time.time + clip.length;

		int id = UnityEngine.Random.Range( 0, int.MaxValue );

		yield return null;

		while ( Time.time <= end )
		{
			yield return null;
		}

		clear -= release;

		release();

		yield break;
	}
}
