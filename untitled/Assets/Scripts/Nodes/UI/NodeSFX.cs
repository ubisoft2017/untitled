﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

[RequireComponent( typeof( NodeUI ) )]
public class NodeSFX : SFX
{
	[SerializeField]
	private AudioClip select, discover, hack, startInteract, stopInteract;

	protected override void Awake ()
	{
		base.Awake();

		var node = GetComponent<NodeUI>();

		node.OnSelected += ( n ) => PlayClip( select );
		node.OnDiscovered += ( n ) => PlayClip( discover );
		node.OnHacked += ( n ) => PlayClip( hack );
		node.OnStartedInteract += ( n ) => PlayClip( startInteract );
		node.OnStoppedInteract += ( n ) => PlayClip( stopInteract );
	}
}
