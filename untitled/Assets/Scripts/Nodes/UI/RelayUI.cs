﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;

using UnityEngine;

public class RelayUI : NodeUI
{
	public override bool CanBeInteractedWith
	{
		get
		{
			return false;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Relay";
		}
	}

	protected override void OnStartInteract ()
	{
		Debug.LogError( "Relay node interaction is not supported.", this );
	}
}
