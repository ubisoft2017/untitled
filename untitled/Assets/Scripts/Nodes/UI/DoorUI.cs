﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class DoorUI : NodeUI
{
	public override string InteractionPrompt
	{
		get
		{
			return "Open";
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Door";
		}
	}

	[SerializeField]
	private GameObject openObject, closedObject;

	[SerializeField]
	private Tweener tweener;

	[SerializeField]
	private TweenEvent openEvent, closeEvent;

	public bool isOpen;

	protected override void OnStartInteract ()
	{
		Open();
	}

	protected override void OnHoldInteract ()
	{
		system.Add( Cost * Time.deltaTime );
	}

	protected override void OnStopInteract ()
	{
		Close();
	}

	public override void Initialize ( int instanceID, NodeType type, int preHackTutorialTextIndex, int postHackTutorialTextIndex, int n, Vector2[] patrolRoute, bool open, Queue<Edge> edges )
	{
		base.Initialize( instanceID, type, preHackTutorialTextIndex, postHackTutorialTextIndex, n, patrolRoute, open, edges );

		if ( open )
		{
			OnOpen();
		}
		else
		{
			OnClose();
		}
	}

	public void Open ()
	{
		OnOpen();

		HackerToAgent.NotifyOpenDoor( this );
	}

	private void OnOpen ()
	{
		isOpen = true;

		openObject.SetActive( true );
		closedObject.SetActive( false );

		tweener.InvokeLocal( openEvent );
	}

	public void Close ()
	{
		OnClose();

		HackerToAgent.NotifyCloseDoor( this );
	}

	private void OnClose ()
	{
		isOpen = false;

		openObject.SetActive( false );
		closedObject.SetActive( true );

		tweener.InvokeLocal( closeEvent );
	}
}
