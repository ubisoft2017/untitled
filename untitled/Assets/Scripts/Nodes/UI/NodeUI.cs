﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class NodeUI : MonoBehaviour
{
	private static List<NodeUI> registry;

	public static NodeUI Get( int instanceID )
	{
		for ( int i = 0; i < registry.Count; i++ )
		{
			if ( registry[i].targetInstanceID == instanceID )
			{
				return registry[i];
			}
		}

		return null;
	}

	static NodeUI ()
	{
		registry = new List<NodeUI>();
	}

	public event Action<NodeUI> OnDiscovered;
	public event Action<NodeUI> OnSelected;
	public event Action<NodeUI> OnDeselected;
	public event Action<NodeUI> OnHacked, OnStartedInteract, OnStoppedInteract, OnHoldingInteract;

	protected SecuritySystem system;

	[SerializeField]
	private GameObject unknownPrefab;

	[SerializeField]
	private float padding = 0.5f;

	public float Padding
	{
		get
		{
			return padding;
		}
	}

	private List<NodeUI> connections;

	public ReadOnlyCollection<NodeUI> Connections
	{
		get
		{
			return connections.AsReadOnly();
		}
	}

	private NodeType targetType;

	public NodeType TargetType
	{
		get
		{
			return targetType;
		}
	}


    private int preHackTutorialTextIndex;

    public int PreHackTutorialTextIndex
    {
        get
        {
            return preHackTutorialTextIndex;
        }
    }


    private int postHackTutorialTextIndex;

    public int PostHackTutorialTextIndex
    {
        get
        {
            return postHackTutorialTextIndex;
        }
    }

    private int targetInstanceID;

	public int TargetInstanceID
	{
		get
		{
			return targetInstanceID;
		}
	}

	public bool IsConnectedTo ( Node node )
	{
		foreach ( var connection in connections )
		{
			if ( connection == node )
			{
				return true;
			}
		}

		return false;
	}

	[SerializeField]
	private float hackDuration;

	public float HackDuration
	{
		get
		{
			return hackDuration;
		}
	}

	private bool discovered;

	public bool HasBeenDiscovered
	{
		get
		{
			return discovered;
		}
	}

	private bool hacked;

	public bool HasBeenHacked
	{
		get
		{
			return hacked;
		}
	}

	public virtual bool CanBeDiscovered
	{
		get
		{
			return !discovered;
		}
	}

	public virtual bool CanBeHacked
	{
		get
		{
			return discovered && !hacked;
		}
	}

	public virtual bool CanBeInteractedWith
	{
		get
		{
			return hacked;
		}
	}

	public virtual string InteractionPrompt
	{
		get
		{
			return "Interact";
		}
	}

	public virtual string ViewLabel
	{
		get
		{
			return "Node";
		}
	}

	public float HackProgress
	{
		get
		{
			return progress / hackDuration;
		}
	}

	[SerializeField]
	private float cost = 1.0f;

	public float Cost
	{
		get
		{
			return cost;
		}
	}

	[SerializeField]
	protected Renderer[] renderers;

	[SerializeField]
	private SpriteRenderer edgeRing;

	private bool hacking;
	private float progress;
	private bool selected;
	private Queue<Edge> edges;

	public virtual void Initialize ( int instanceID, NodeType type, int preHackTutorialTextIndex, int postHackTutorialTextIndex, int n, Vector2[] patrolRoute, bool open, Queue<Edge> edges )
	{
		targetInstanceID = instanceID;
		targetType = type;
        this.preHackTutorialTextIndex = preHackTutorialTextIndex;

        this.postHackTutorialTextIndex = postHackTutorialTextIndex;
        registry.Add( this );

		connections = new List<NodeUI> ( n );

		this.edges = edges;

		system = FindObjectOfType<SecuritySystem>();
	}

	public void Connect ( NodeUI ui )
	{
		connections.Add( ui );

		Debug.Log( string.Format( "connection {0} to {1}", this.targetInstanceID, ui.targetInstanceID ), this );
	} 

	public void Discover ()
	{
		if ( !discovered )
		{
			discovered = true;

			OnDiscover();

			//Debug.LogFormat( "discovered {0} ({1})", targetInstanceID, targetType );

			if ( OnDiscovered != null )
			{
				OnDiscovered( this );
			}
		}
	}

	public void Select ()
	{
		selected = true;

		OnSelect();

		if ( OnSelected != null )
		{
			OnSelected( this );
		}
	} 

	public void Deselect ()
	{
		selected = false;

		OnDeselect();

		if ( OnDeselected != null )
		{
			OnDeselected( this );
		}
	}

	public void Hack ( bool discover = false )
	{
		if ( discover )
		{
			Discover();
		}

		if ( discovered && !hacked )
		{
			hacked = true;

			OnHack();

			//Debug.LogFormat( "hacked {0} ({1})", targetInstanceID, targetType );

			if ( OnHacked != null )
			{
				OnHacked( this );
			}
		}
	}

	public void StartHacking ()
	{
		hacking = true;
	}

	public void StopHacking ()
	{
		hacking = false;
	}

	protected Unknown unknown;

	protected virtual void Awake ()
	{
		DevTools.Register( string.Format( "Discover {0} ({1})", targetInstanceID, targetType ), Discover );
		DevTools.Register( string.Format( "Hack {0} ({1})", targetInstanceID, targetType ), () => Hack() );

		unknown = Instantiate( unknownPrefab, transform, false ).GetComponent<Unknown>();

		unknown.transform.localPosition = Vector3.zero;
		unknown.transform.localRotation = Quaternion.identity;

		foreach ( var renderer in renderers )
		{
			renderer.enabled = false;
		}

		edgeRing.enabled = false;

		unknown.Hide();
	}
	
	protected virtual void Update ()
	{
		if ( hacking )
		{
			progress += Time.deltaTime;

			if ( progress >= hackDuration )
			{
				Hack();
			}
		}
		else
		{
			progress = 0.0f;
		}

		if ( selected )
		{
			DoSelectedIdle();
		}
		else
		{
			DoDeselectedIdle();
		}
	}

	public void StartInteract ()
	{
		OnStartInteract();

		if ( OnStartedInteract != null )
		{
			OnStartedInteract( this );
		}
	}

	public void HoldInteract ()
	{
		OnHoldInteract();

		if ( OnHoldingInteract != null )
		{
			OnHoldingInteract( this );
		}
	}

	public void StopInteract ()
	{
		OnStopInteract();

		if ( OnStoppedInteract != null )
		{
			OnStoppedInteract( this );
		}
	}

	protected virtual void OnStartInteract () { }
	protected virtual void OnHoldInteract () { }
	protected virtual void OnStopInteract () { }

	protected virtual void OnDiscover ()
	{
		unknown.Show();

		edgeRing.enabled = true;
	}

	protected virtual void OnHack ()
	{
		foreach ( var renderer in renderers )
		{
			renderer.enabled = true;
		}

		unknown.Hide();

		for ( int i = 0; i < connections.Count; i++ )
		{
			if ( !connections[i].HasBeenDiscovered )
			{
				var edge = edges.Dequeue();

				edge.Discover( this, connections[i] );
			}
		}
	}

	protected virtual void OnSelect () { }
	protected virtual void OnDeselect () { }

	protected virtual void DoSelectedIdle ()
	{
		unknown.DoSelectedIdle();
	}

	protected virtual void DoDeselectedIdle ()
	{
		unknown.DoDeselectedIdle();
	}

#if UNITY_EDITOR
	private void OnDrawGizmos ()
	{
		Gizmos.DrawRay( transform.position + transform.forward * -0.5f, transform.right * padding );
	}
#endif
}
