﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunctionBoxUI : NodeUI
{
	public override bool CanBeInteractedWith
	{
		get
		{
			return false;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "System Access Point";
		}
	}
}
