﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using AI;
using Tweening;

public class GuardUI : NodeUI
{
	[EnumObject]
	[SerializeField]
	private VisionSettings settings;

	[SerializeField]
	private Raycaster2D raycaster;

	[SerializeField]
	private Transform eye;

	[SerializeField]
	private MeshFilter filter;

	[SerializeField]
	private new MeshRenderer renderer;

	[SerializeField]
	private AnimationCurve tagCurve;

	[SerializeField]
	private SFX sfx;

	[SerializeField]
	private AudioClip tagClip;

	[SerializeField]
	private GameObject tagVisuals;

	[SerializeField]
	private float volume = 0.8f;

	[SerializeField]
	private TweenEvent startEvent, stopEvent;

	private Mesh mesh;

	[SerializeField]
	[Range( 2, 16 )]
	private int rays = 8;

	private bool isTagged;

	public bool IsTagged
	{
		get
		{
			return isTagged;
		}
	}

	public override bool CanBeInteractedWith
	{
		get
		{
			return isTagged;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Security Guard";
		}
	}

	public override string InteractionPrompt
	{
		get
		{
			return "Hold To Disrupt Phase";
		}
	}

	public void Tag ()
	{
		isTagged = true;

		Debug.Log( "Tagged!", this );

		sfx.PlayClip( tagClip, volume, true );

		tagVisuals.SetActive( true );
	}

	protected override void OnStartInteract ()
	{
		HackerToAgent.NotifyStartDisruptingGuard( this );

		startEvent.Invoke();

		/// TODO: fx
	}

	protected override void OnHoldInteract ()
	{
		system.Add( Cost * Time.deltaTime );
	}

	protected override void OnStopInteract ()
	{
		HackerToAgent.NotifyStopDisruptingGuard( this );

		stopEvent.Invoke();

		/// TODO: fx
		/// 
		isTagged = false;
	}

	protected override void Awake ()
	{
		base.Awake();

		mesh = new Mesh();

		mesh.name = "Guard Vision " + TargetInstanceID;

		mesh.MarkDynamic();

		mesh.vertices = vertices = new Vector3[rays + 1];
		mesh.uv = uvs = new Vector2[rays + 1];

		mesh.normals = new Vector3[rays + 1];
		mesh.tangents = new Vector4[rays + 1];

		vertices[0] = Vector3.zero;

		var triangles = new int[(rays - 1) * 3];

		for ( int i = 1, j = 0; i < rays; i++ )
		{
			triangles[j++] = 0;
			triangles[j++] = i;
			triangles[j++] = i + 1;
		}

		mesh.triangles = triangles;

		filter.sharedMesh = mesh;

		tagVisuals.SetActive( false );
	}

	protected override void Update ()
	{
		base.Update();

		tagVisuals.transform.localPosition = Vector3.back * tagCurve.Evaluate( Time.time );
	} 

	private void Start ()
	{
		foreach ( var renderer in renderers )
		{
			renderer.enabled = true;
		}

		unknown.Hide();
	} 

	protected override void OnDiscover ()
	{
		base.OnDiscover();

		Hack();
	}

	protected override void DoSelectedIdle ()
	{
		base.DoSelectedIdle();

		DoMesh();
	}

	protected override void DoDeselectedIdle ()
	{
		base.DoDeselectedIdle();

		DoMesh();
	}

	private Transform[] casters;
	private Vector3[] vertices;
	private Vector2[] uvs;

	private void DoMesh ()
	{
		float step = (settings.FieldOfView * 2.0f) / (rays - 1);

		float theta = -settings.FieldOfView;

		raycaster.caster = eye;
		raycaster.range = settings.Range;

		for ( int i = 0; i < rays; i++ )
		{
			eye.localRotation = Quaternion.Euler( -90.0f, 0.0f, 0.0f );

			eye.Rotate( 0.0f, 0.0f, theta, Space.World );

			float distance = settings.Range;

			if ( raycaster.Raycast() )
			{
				distance = raycaster.Nearest.distance;
			}

			vertices[i + 1] = renderer.transform.worldToLocalMatrix.MultiplyPoint3x4( eye.position + eye.forward * distance );

			uvs[i + 1] = new Vector2( distance / settings.Range, i / (rays - 1) );

			Debug.DrawRay( eye.position, eye.forward * distance, Color.magenta );

			theta += step;
		}

		mesh.vertices = vertices;
		mesh.uv = uvs;

		mesh.RecalculateBounds();
	}
}
