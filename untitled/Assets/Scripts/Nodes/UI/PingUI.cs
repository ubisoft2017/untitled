﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;

using UnityEngine;
using UnityEngine.UI;

public class PingUI : SFX
{
	[SerializeField]
	private AnimationCurve scaleCurve;

	[SerializeField]
	private float duration = 0.5f, volume = 1.0f;

	[SerializeField]
	private AudioClip clip;

	[SerializeField]
	private Gradient gradient;

	[SerializeField]
	private SpriteRenderer graphic;

	public static PingUI ping;

    [SerializeField]
    private Instructions tutorial;


    private float time;

	protected override void Awake ()
	{
		base.Awake();

		ping = this;
	}

	public void Ping ( Vector3 agentPosition, bool showHint)
	{
		transform.position = new Vector3( agentPosition.x, agentPosition.z, 0.0f );

		time = Time.time;

		PlayClip( clip, volume, true );

        if (showHint)
        {
            tutorial.SetInstruction("AGENT JUST PINGED HIS POSITION", 0.035f, 4f);
        }
	}

	private void Update ()
	{
		var t = (Time.time - time) / duration;

		transform.localScale = Vector3.one * scaleCurve.Evaluate( t );
		graphic.color = gradient.Evaluate( t );
	}
}
