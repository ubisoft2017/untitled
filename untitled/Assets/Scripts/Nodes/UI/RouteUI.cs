﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteUI : NodeUI
{
	[SerializeField]
	private LineRenderer line;

	public override bool CanBeInteractedWith
	{
		get
		{
			return false;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Patrol Route";
		}
	}

	protected override void OnStartInteract ()
	{
		Debug.LogError( "Route node interactions have not been implemented.", this );
	}

	public override void Initialize ( int instanceID, NodeType type, int preHackTutorialTextIndex, int postHackTutorialTextIndex, int n, Vector2[] patrolRoute, bool open, Queue<Edge> edges )
	{
		base.Initialize( instanceID, type, preHackTutorialTextIndex, postHackTutorialTextIndex, n, patrolRoute, open, edges );

		line.numPositions = patrolRoute.Length;

		for ( int i = 0; i < patrolRoute.Length; i++ )
		{
			line.SetPosition( i, patrolRoute[i] );
		}
	}
}
