﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;

using UnityEngine;

public class ExitUI : NodeUI
{
	public override bool CanBeInteractedWith
	{
		get
		{
			return false;
		}
	}

	public override string ViewLabel
	{
		get
		{
			return "Exit";
		}
	}
}
