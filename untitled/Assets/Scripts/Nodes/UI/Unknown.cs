﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class Unknown : MonoBehaviour
{
	[SerializeField]
	private LineRenderer[] renderers;

	private int[] rotations;

	private void Awake ()
	{
		rotations = new int[3];
	}
	
	public void Show ()
	{
		for ( int i = 0; i < renderers.Length; i++ )
		{
			renderers[i].enabled = true;
		}
	}

	public void Hide ()
	{
		for ( int i = 0; i < renderers.Length; i++ )
		{
			renderers[i].enabled = false;
		}
	}

	private void Update ()
	{
		DoDeselectedIdle();
	}

	public void DoSelectedIdle ()
	{
		DoDeselectedIdle();
	}

	public void DoDeselectedIdle ()
	{
		for ( int i = 1; i < renderers.Length; i++ )
		{
			rotations[i - 1] = Mathf.FloorToInt( (Time.time + i * 0.2f) / 2 ) % 4;

			Quaternion rotation = Quaternion.Euler( rotations[i - 1] * 90.0f, -90.0f, 0.0f );

			renderers[i].transform.localRotation = Quaternion.Lerp( renderers[i].transform.localRotation, rotation, Mathf.SmoothStep( 0.0f, 1.0f, (Time.time + i * 0.2f) % 2 ) );
		}
	}
}
