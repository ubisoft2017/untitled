﻿using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;
using UnityEngine.UI;

public class NodeView : MonoBehaviour
{
	[Header( "Setup" )]

	[SerializeField] private CameraLookAt cameraLookAt;
	[SerializeField] private CameraTransform cameraTransform;
	[SerializeField] private Transform pivot;
	[SerializeField] private Transform joint;
	[SerializeField] private Text details;
	[SerializeField] private Text interact;
	[SerializeField] private Instructions tutorial;
    [SerializeField] private ButtonAction hackAction, interactAction;
	[SerializeField] private HackerSFX sfx;
	[SerializeField] private Image hackProgress;
	[SerializeField] private SecuritySystem system;
	[SerializeField] private Transform highlight;
	[SerializeField] private Transform highlightBlink;

	[Header( "Control Parameters" )]

	[SerializeField][Range( -1.0f, 1.0f )] private float tolerance = 0.0f;

	[Header( "Camera Parameters" )]

	[SerializeField] private float followHeight;
	[SerializeField] private float followRadius;
	[SerializeField] private float followLerp;
	[SerializeField] private float minJointAngle;
	[SerializeField] private float maxJointAngle;
	[SerializeField] private AnimationCurve followHeightCurve = AnimationCurve.Linear( 0.0f, 0.5f, 1.0f, 1.0f );
	[SerializeField] private float interactPromptBlinkRate;
	[SerializeField] private float highlightSpinRate;
	[SerializeField] private AnimationCurve selectionBlinkCurve = AnimationCurve.Linear( 0.0f, 0.5f, 1.0f, 1.0f );

	[SerializeField][HideInInspector] private NodeUI selection;

	public float angularVelocity;
	public float verticalVelocity;

	private float jointAngleDelta;

	[SerializeField] private float jointAngle;
	[SerializeField] private Vector2 friction;

	private ProfileController profileController;

	public NodeUI Selection
	{
		get
		{
			return selection;
		}

		set
		{
			selection = value;
		}
	}

	private void Awake ()
	{
		if ( selection != null )
		{
			Select( selection, true );
		}

		jointAngleDelta = maxJointAngle - minJointAngle;

		profileController = FindObjectOfType<ProfileController>();

		details.text = "";
	}

	/// <summary>
	/// Push the view along an edge.
	/// </summary>
	/// <param name="vector">A normalized 2D vector representing the push direction in camera space.</param>
	public void Push ( Vector2 vector )
	{
		if ( selection != null )
		{
			//string log = "";

			//log += "raw vector: " + vector + "\n";

			vector = pivot.localToWorldMatrix.MultiplyVector( vector );

			Debug.DrawRay( selection.transform.position, vector, Color.blue );

			// Find the neighbour whose delta from the current target is closest to the push vector

			NodeUI candidate = null;

			float max = float.NegativeInfinity;

			//log += "local vector: " + vector + "\n";

			for ( int i = 0; i < selection.Connections.Count; i++ )
			{
				if ( selection.Connections[i].HasBeenDiscovered )
				{
					// Figure out which side of the edge the current target falls on, if it is part of the edge at all

					//log += string.Format( "evaluating {0} ({1})\n", selection.Connections[i].TargetInstanceID, selection.Connections[i].TargetType );

					// Get the camera space direction vector from the current target to the neighbour
					Vector3 delta = selection.Connections[i].transform.position - selection.transform.position;

					Vector2 direction = new Vector2( delta.x, delta.y );

					direction = direction.normalized;

					//log += "\tdirection: " + direction + "\n";

					// Take the dot product between the direction vector and the push vector, as a measure of their similarity
					float dot = Vector2.Dot( vector, direction );

					//log += "\tdot: " + dot + "\n";

					// If the dot product is within the tolerance, and is the highest thus far
					if ( dot >= tolerance && dot > max )
					{
						// Select the neighbour as the candidate
						candidate = selection.Connections[i];

						// Record the new highest dot product
						max = dot;

						//log += "\tnew max candidate\n";
					}
				}
			}

			// If we found a candidate neighbour to go to
			if ( candidate != null )
			{
				// Make the neighbour the current target
				Select( candidate );

				//log += "selecting " + candidate;

				//Debug.Log( log, candidate );
			}
			else
			{
				//log += "no selection";

				//Debug.Log( log );
			}
		}
	}

	public void Select ( NodeUI node, bool snap = false )
	{
		Deselect();

		selection = node;

		if ( selection != null )
		{
			selection.Select();

			//Debug.LogFormat( "select {0} ({1})", node.TargetInstanceID, node.TargetType );

			SetDetails( selection );

			selection.OnHacked += SetDetails;

			if ( snap )
			{
				pivot.position = new Vector3( selection.transform.position.x, selection.transform.position.z, 0.0f );
			}
		}
	}

	public void StartHacking ()
	{
		if ( selection != null && !selection.HasBeenHacked )
		{
			if ( selection.HackProgress == 0.0f )
			{
				selection.StartHacking();

				sfx.Type( Random.Range( 12, 15 ) );
			}
		}
	}

	public void StopHacking ()
	{
		if ( selection != null )
		{
			selection.StopHacking();

			sfx.StopAllCoroutines();
		}
	}

	private bool check;

	public void StartInteract ()
	{
		if ( selection != null && selection.CanBeInteractedWith )
		{
			check = true;

			selection.StartInteract();

			sfx.Type( 1 );
		}
	}

	public void HoldInteract ()
	{
		if ( selection != null && selection.CanBeInteractedWith )
		{
			selection.HoldInteract();
		}
	}

	public void StopInteract ()
	{
		if ( selection != null && selection.CanBeInteractedWith )
		{
			check = false;

			selection.StopInteract();
		}
	}

	public void Deselect ()
	{
		if ( selection != null )
		{
			if ( check )
			{
				selection.StopInteract();
			}

			selection.Deselect();

			selection.OnHacked -= SetDetails;
		}

		selection = null;

		details.text = "";
	}

	private void SetDetails ( NodeUI node )
	{
        if (!node.HasBeenHacked)
        {
            if (node.PreHackTutorialTextIndex != -1)
            {
                string tut = HackerTutorialStrings.preHacks[node.PreHackTutorialTextIndex];
                if (tut.Length > 0)
                {
                    tutorial.SetInstruction(tut, 0.035f);
                }
                else
                {
                    tutorial.SetInstruction("");
                }
            }
            else
            {
                tutorial.SetInstruction("");
            }
        }
        else
        {
            if (node.PostHackTutorialTextIndex != -1)
            {
                string tut = HackerTutorialStrings.postHacks[node.PostHackTutorialTextIndex];

                //messy, but will do for now
                bool guardCond = node.TargetType == NodeType.Guard ? node.CanBeInteractedWith : true;


                if (tut.Length > 0 && guardCond)
                {
                    tutorial.SetInstruction(tut, 0.035f);
                }
                else
                {
                    tutorial.SetInstruction("");
                }
            }
            else
            {
                tutorial.SetInstruction("");
            }
        }

        details.text = string.Format( "[{0}]", node.HasBeenHacked ? node.ViewLabel.ToLower() : "?" );

		interact.text = "";

		if ( node.CanBeHacked || node.CanBeInteractedWith )
		{
			if ( node.CanBeHacked )
			{
				interact.text += string.Format( "({0}) hold to {1}, ", profileController.Player.Profile[hackAction].mapping.ToString().ToUpper(), hackAction.Prompt.ToLower() );
			}

			if ( node.CanBeInteractedWith )
			{
				interact.text += string.Format( "({0}) {1}, ", profileController.Player.Profile[interactAction].mapping.ToString().ToUpper(), node.InteractionPrompt.ToLower() );
			}

			interact.text = interact.text.TrimEnd( ',', ' ' );

			interact.text = interact.text.Replace("(X)", "<color=#008BFFFF>(X)</color>");
			interact.text = interact.text.Replace("(Y)", "<color=#E3C04CFF>(Y)</color>");
			interact.text = interact.text.Replace("(B)", "<color=#DB2041FF>(B)</color>");
			interact.text = interact.text.Replace("(A)", "<color=#70B573FF>(A)</color>");
		}
	}

	private void FixedUpdate ()
	{
		if ( selection != null )
		{
			hackProgress.transform.position = selection.transform.position + Vector3.back * 0.5f;

			hackProgress.fillAmount = selection.HasBeenHacked ? 0.0f : selection.HackProgress;

			highlight.position = selection.transform.position;
			highlight.Rotate( Vector3.back, highlightSpinRate * Time.fixedDeltaTime );
			highlightBlink.localScale = Vector3.one * selectionBlinkCurve.Evaluate( (Time.time % interactPromptBlinkRate) / interactPromptBlinkRate );

			SetDetails( selection );

			// Override CameraTransform's settings
			if ( cameraTransform != null )
			{
				cameraTransform.remote = null;
			}

			// Smoothly shift the view to be centered around the current Node
			pivot.position = Vector3.Lerp( pivot.position, new Vector3( selection.transform.position.x, selection.transform.position.y, 0.0f ), followLerp * Time.fixedDeltaTime );

			float height = followHeightCurve.Evaluate( (jointAngle - minJointAngle) / jointAngleDelta ) * followHeight;

			cameraTransform.transform.localPosition = new Vector3( 0.0f, 0.0f, -height );
		}

		pivot.Rotate( Vector3.forward, angularVelocity * Time.fixedDeltaTime );

		jointAngle -= verticalVelocity * Time.fixedDeltaTime;

		if ( jointAngle <= minJointAngle )
		{
			jointAngle = minJointAngle;
			verticalVelocity = 0.0f;
		}

		if ( jointAngle >= maxJointAngle )
		{
			jointAngle = maxJointAngle;
			verticalVelocity = 0.0f;
		}

		joint.localRotation = Quaternion.identity;

		joint.Rotate( Vector3.right, jointAngle, Space.Self );

		angularVelocity = Mathf.Lerp( angularVelocity, 0.0f, Time.fixedDeltaTime * friction.x );
		verticalVelocity = Mathf.Lerp( verticalVelocity, 0.0f, Time.fixedDeltaTime * friction.y );
	}
}
