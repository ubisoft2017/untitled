﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( NodeView ) )]
public class NodeSelectController : ControllerBase
{
	[EnumObject][SerializeField] private JoystickAction selectAction;

	[SerializeField][Range(  0.0f, 1.0f )] private float threshold;

	[SerializeField][Range( -1.0f, 1.0f )] private float tolerance;

	[SerializeField][HideInInspector] private NodeView view;

	[SerializeField][HideInInspector] private bool reset;

	[SerializeField][HideInInspector] private Vector2 last;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		view = GetComponent<NodeView>();

		reset = true;
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		Vector2 vector = GamePad.Query( selectAction, Player.Profile, Player.State );

		float magnitude = vector.magnitude;

		if ( magnitude >= threshold )
		{
			vector /= magnitude;

			if ( reset || Vector2.Dot( last, vector ) <= tolerance )
			{
				view.Push( vector.normalized );

				reset = false;

				last = vector;
			}
		}
		else
		{
			reset = true;
		}
	}
}
