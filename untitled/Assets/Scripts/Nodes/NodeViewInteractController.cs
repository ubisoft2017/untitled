﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;

[RequireComponent( typeof( NodeView ) )]
public class NodeViewInteractController : ControllerBase
{
	[EnumObject][SerializeField] private ButtonAction pressAction, holdAction, releaseAction;

	[SerializeField][HideInInspector] private NodeView view;

	protected override void ControllerStart ()
	{
		base.ControllerStart();

		view = GetComponent<NodeView>();

		Register( pressAction, view.StartInteract );
		Register( releaseAction, view.StopInteract );
	}

	protected override void ControllerUpdate ()
	{
		base.ControllerUpdate();

		if ( GamePad.Query( holdAction, Player.Profile, Player.State ) )
		{
			view.HoldInteract();
		}
	}
}
