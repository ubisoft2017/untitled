﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class Connection
{
	public Connection ( Node a, Node b )
	{
		this.a = a;
		this.b = b;
	}

	[SerializeField]
	private Node a, b;

	public Node A
	{
		get
		{
			return a;
		}
	}

	public Node B
	{
		get
		{
			return b;
		}
	}
}

public class NodeGraph : MonoBehaviour
{
	private static NodeGraph instance;

	public static NodeGraph Current
	{
		get
		{
			if ( instance == null )
			{
				instance = FindObjectOfType<NodeGraph>();

#if UNITY_EDITOR
				if ( instance == null )
				{
					if ( EditorUtility.DisplayDialog( "NodeGraph", "No NodeGraph found, create one in " + SceneManager.GetActiveScene().name + "?", "Yes", "No" ) )
					{
						instance = new GameObject( "Node Graph" ).AddComponent<NodeGraph>();

						EditorGUIUtility.PingObject( instance );
					}
				}
#endif
			}

			return instance;
		}
	}

	[SerializeField]
	[HideInInspector]
	private List<Connection> connections = new List<Connection>();

	public ReadOnlyCollection<Connection> Connections
	{
		get
		{
			return connections.AsReadOnly();
		}
	}

	public List<Node> GetConnections ( Node node )
	{
		var list = new List<Node>();

		foreach ( var connection in connections )
		{
			if ( connection.A == node )
			{
				list.Add( connection.B );
			}
			else if ( connection.B == node )
			{
				list.Add( connection.A );
			}
		}

		return list;
	}

	public bool Contains ( Node a, Node b )
	{
		for ( int i = 0; i < connections.Count; i++ )
		{
			if ( (connections[i].A == a && connections[i].B == b) || (connections[i].A == b && connections[i].B == a) )
			{
				return true;
			}
		}

		return false;
	}

#if UNITY_EDITOR
	public void Add ( Node a, Node b )
	{
		Undo.RecordObject( this, "Create Connection" );

		connections.Add( new Connection( a, b ) );
	}

	public void Remove ( Node a, Node b )
	{
		Undo.RecordObject( this, "Remove Connection" );

		for ( int i = 0; i < connections.Count; i++ )
		{
			if ( (connections[i].A == a && connections[i].B == b) || (connections[i].A == b && connections[i].B == a) )
			{
				connections.RemoveAt( i );

				return;
			}
		}
	}

	private void Awake ()
	{
		for ( int i = 0; i < connections.Count; i++ )
		{
			if ( connections[i].A == null ||connections[i].B == null )
			{
				connections.RemoveAt( i-- );
			}
		}
	}

	private bool warn;

	private void OnDrawGizmos ()
	{
		Gizmos.color = Color.yellow;

		for ( int i = 0; i < connections.Count; i++ )
		{
			if ( connections[i].A == null || connections[i].B == null )
			{
				connections.RemoveAt( i-- );

				continue;
			}

			Gizmos.DrawLine( connections[i].A.transform.position, connections[i].B.transform.position );
		}

		Gizmos.color = Color.white;
	}
#endif
}
