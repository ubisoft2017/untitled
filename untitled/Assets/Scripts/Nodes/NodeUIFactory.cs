﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class NodeUIFactory : MonoBehaviour
{
	[SerializeField]
	private GameObject clockPrefab, routePrefab, guardPrefab, laserSystemPrefab, junctionBoxPrefab, doorPrefab, valuablePrefab, relayPrefab, exitPrefab, edgePrefab, wallPrefab;

	private Transform parent;

	private void Awake ()
	{
		if ( FindObjectOfType<SessionController>().LocalSelection == PlayerSelection.Hacker )
		{
			parent = GameObject.FindGameObjectWithTag( "Virtual Parent" ).transform;
		}
	}

	public NodeUI CreateUI ( NodeInfo info, Queue<Edge> edges )
	{
		GameObject prefab;

		switch ( info.type )
		{
		case NodeType.Clock:
			prefab = clockPrefab;
			break;

		case NodeType.Route:
			prefab = routePrefab;
			break;

		case NodeType.Guard:
			prefab = guardPrefab;
			break;

		case NodeType.LaserSystem:
			prefab = laserSystemPrefab;
			break;

		case NodeType.JunctionBox:
			prefab = junctionBoxPrefab;
			break;

		case NodeType.Door:
			prefab = doorPrefab;
			break;

		case NodeType.Valuable:
			prefab = valuablePrefab;
			break;

		case NodeType.Relay:
			prefab = relayPrefab;
			break;

		case NodeType.Exit:
			prefab = exitPrefab;
			break;

		default:
			throw new NotImplementedException( string.Format( "A prefab does not exist for the NodeType {0}", info.type ) );
		}

		var gameObject = Instantiate( prefab, parent, false );

		gameObject.name = info.type + " Node";

		gameObject.transform.position = info.position;
		gameObject.transform.rotation = Quaternion.Euler( 0.0f, 0.0f, info.rotation );

		var ui = gameObject.GetComponent<NodeUI>();

		Debug.Log( "initializing " + info.instanceID + " (" + info.type + ") as " + gameObject.name + " (originally " + prefab.name + ")" );

		ui.Initialize( info.instanceID, info.type, info.preHackTutorialTextIndex, info.postHackTutorialTextIndex, info.connections.Length, info.patrolRoute, info.open, edges );

		return ui;
	}

	public Edge CreateEdge ()
	{
		var gameObject = Instantiate( edgePrefab, parent, false );

		gameObject.name = "Edge";

		var edge = gameObject.GetComponent<Edge>();

		return edge;
	}

	public WallMesh CreateWall ( WallInfo[] walls )
	{
		var gameObject = Instantiate( wallPrefab, parent, false );

		gameObject.name = "Walls";

		var wallMesh = gameObject.GetComponent<WallMesh>();

		int cornerCount = 0;

		foreach ( var wall in walls )
		{
			cornerCount += wall.corners.Length;
		}

		wallMesh.Initialize( walls.Length, cornerCount );

		foreach ( var wall in walls )
		{
			Vector3[] positions = new Vector3[wall.corners.Length];

			for ( int i = 0; i < wall.corners.Length; i++ )
			{
				positions[i] = (Quaternion.Euler( 0.0f, 0.0f, - wall.rotation ) * wall.corners[i]) + (Vector3) wall.position;
			}

			Vector3[] corners = new Vector3[wall.corners.Length];

			for ( int i = 0; i < wall.corners.Length; i++ )
			{
				corners[i] = wall.corners[i];
			}

			var wallObject = wallMesh.Add( positions, 1.0f );

			var line = wallObject.GetComponent<LineRenderer>();

			line.numPositions = corners.Length;
			line.SetPositions( corners );

			wallObject.GetComponent<EdgeCollider2D>().points = wall.corners;

			wallObject.transform.localPosition = wall.position;
			wallObject.transform.localRotation = Quaternion.Euler( 0.0f, 0.0f, -wall.rotation );
		}

		wallMesh.Finish( Vector3.back );

		return wallMesh;
	}
}
