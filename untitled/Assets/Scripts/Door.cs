﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Door : Node
{
	[SerializeField]
	private bool locked;

	public bool Locked
	{
		get
		{
			return locked;
		}
	}

	[SerializeField]
	private float speed;

	/// <summary>
	/// How open this Door is. 0 = completely closed, 1 = wide open.
	/// </summary>
	public abstract float State { get; }

	public override NodeType NodeType
	{
		get
		{
			return NodeType.Door;
		}
	}

	public void Open ()
	{
		if ( !locked )
		{
			OnOpen();
		}
	}

	public void Close ()
	{
		if ( !locked )
		{
			OnClose();
		}
	}

	public void Lock ()
	{
		locked = true;
	}

	public void Unlock ()
	{
		locked = false;
	}

	protected abstract void OnOpen ();

	protected abstract void OnClose ();

	protected abstract void OnLock ();

	protected abstract void OnUnlock ();
}
