﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExitGames.Client.Photon;

/// <summary>
/// Utility methods for converting networked data types to and from bytes.
/// </summary>
public static class Serializers
{
	#region GuardTracker
	private static readonly byte[] guardTrackerMem = new byte[20];

	public static short SerializeGuardTracker ( StreamBuffer outStream, object customObject )
	{
		GuardTracker gT = (GuardTracker) customObject;
		lock ( guardTrackerMem )
		{
			byte[] bytes = guardTrackerMem;
			int index = 0;
			Protocol.Serialize( gT.position.x, bytes, ref index );
			Protocol.Serialize( gT.position.y, bytes, ref index );
			Protocol.Serialize( gT.velocity.x, bytes, ref index );
			Protocol.Serialize( gT.velocity.y, bytes, ref index );
			Protocol.Serialize( gT.rotation, bytes, ref index );
			outStream.Write( bytes, 0, 20 );
		}
		return 20;
	}

	public static object DeserializeGuardTracker ( StreamBuffer inStream, short length )
	{
		GuardTracker tracker = new GuardTracker();

		lock ( guardTrackerMem )
		{
			inStream.Read( guardTrackerMem, 0, 20 );
			int index = 0;

			Protocol.Deserialize( out tracker.position.x, guardTrackerMem, ref index );
			Protocol.Deserialize( out tracker.position.y, guardTrackerMem, ref index );
			Protocol.Deserialize( out tracker.velocity.x, guardTrackerMem, ref index );
			Protocol.Deserialize( out tracker.velocity.y, guardTrackerMem, ref index );
			Protocol.Deserialize( out tracker.rotation, guardTrackerMem, ref index );
		}

		return tracker;
	}
	#endregion

	#region GuardInfo
	public static byte[] SerializeGuardInfo ( object arg )
	{
		var guardInfo = (GuardInfo) arg;

		var bytes = new byte[8];

		BitConverter.GetBytes( guardInfo.guardInstanceID ).CopyTo( bytes, 0 );
		BitConverter.GetBytes( guardInfo.phaseInstanceID ).CopyTo( bytes, 4 );

		return bytes;
	}

	public static object DeserializeGuardInfo ( byte[] bytes )
	{
		return new GuardInfo( BitConverter.ToInt32( bytes, 0 ), BitConverter.ToInt32( bytes, 4 ) );
	}
	#endregion

	#region NodeInfo
	public static byte[] SerializeNodeInfo ( object arg )
	{
		return ((NodeInfo) arg).ToBytes();
	}

	public static object DeserializeNodeInfo ( byte[] bytes )
	{
		return new NodeInfo( bytes );
	}
	#endregion

	#region WallInfo
	public static byte[] SerializeWallInfo ( object arg )
	{
		return ((WallInfo) arg).ToBytes();
	}

	public static object DeserializeWallInfo ( byte[] bytes )
	{
		return new WallInfo( bytes );
	}
	#endregion
}
