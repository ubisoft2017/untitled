﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controls;
using UnityEngine.SceneManagement;
using Tweening;
using System;
using System.IO;

public enum ConnectionType
{
    Online,
    Local,
    Offline
}

public class ConnectionController : Photon.PunBehaviour
{
    #region Public Variables
    /// <summary>
    /// The PUN loglevel. 
    /// </summary>
    public PhotonLogLevel Loglevel = PhotonLogLevel.Informational;

    public GameObject voiceHub;

    #endregion

    #region Private Variables

    /// <summary>
    /// This client's version number. Users are separated from each other by gameversion (which allows you to make breaking changes).
    /// </summary>
    string _gameVersion = "1";

    /// <summary>
    /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
    /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
    /// Typically this is used for the OnConnectedToMaster() callback.
    /// </summary>
    bool isConnecting;

    string hostIP;

    //the game room we know our players will be connecting to
    string gameRoom;
    RoomOptions roomOptions;

    [SerializeField]
    private SceneReference lobbyScene;

    [SerializeField]
    [EnumObject(typeof(Tweening.TweenEvent))]
    private Tweening.TweenEvent connectEvent, hostLanEvent, joinLanEvent, disconnectEvent;

	[SerializeField]
	private GameObject dev;

#if UNITY_EDITOR
    [SerializeField]
    private bool offlineMode = false;

    [SerializeField]
    private GameObject offlineButton;
#endif

    [SerializeField]
    private TweenEventList onBusy, onConnect, onFailConnect, onDisconnect;

    #endregion

    #region MonoBehaviour CallBacks

    private bool isLanServer = false;

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    /// </summary>
    void Awake()
    {
		if ( !Application.isEditor )
		{
			dev.SetActive( false );
		}

#if UNITY_EDITOR
        PhotonNetwork.offlineMode = offlineMode;

        if (offlineMode)
        {
            offlineButton.SetActive(true);
        }
#endif

        //get game room info from text file
        string path = Application.dataPath + "/GameRoomInfo.txt";

        // This text is added only once to the file.

        if (!File.Exists(path))
        {
            // Create a file to write to.

            string createText = "gameRoom1" + Environment.NewLine;

            File.WriteAllText(path, createText);
        }

        // Open the file to read from.

        gameRoom = File.ReadAllText(path).Trim(' ', '\t', '\r', '\n');

        Debug.Log(gameRoom);

        // #Critical
        // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
        PhotonNetwork.autoJoinLobby = false;

        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;

        // #NotImportant
        // Force LogLevel
        PhotonNetwork.logLevel = Loglevel;

        //create room options
        roomOptions = new RoomOptions() { IsVisible = false, MaxPlayers = 2 };

        //when the connect event fires, connect
        connectEvent.OnInvoke += (e) =>
        {
            Connect();
        };

        hostLanEvent.OnInvoke += (e) =>
        {
            HostLAN();
            isLanServer = true;
        };

        joinLanEvent.OnInvoke += (e) =>
        {
            StartCoroutine(FindLAN());
            onBusy.InvokeAll();
        };

        disconnectEvent.OnInvoke += (e) => Disconnect();
    }

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    void Start()
    {
        Debug.Log(PhotonVoiceNetwork.instance);
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Start the connection process. 
    /// - If already connected, we attempt joining a random room
    /// - if not yet connected, Connect this application instance to Photon Cloud Network
    /// </summary>
    public void Connect()
    {
        isConnecting = true;

        if (onBusy != null)
        {
            onBusy.InvokeAll();
        }

        //set up cloud server settings
        PhotonNetwork.PhotonServerSettings.HostType = ServerSettings.HostingOption.PhotonCloud;
        PhotonNetwork.PhotonServerSettings.PreferredRegion = CloudRegionCode.us;
        PhotonNetwork.PhotonServerSettings.AppID = "2649bc97-a41f-44f4-8fb4-ce6d840bdf4b";

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.connected)
        {
            //Here we join a specific room, if that fails we will create the specific room, if we are creating the room, then the OnConnectedToMaster will be called hence why we have to have the join method in two spots
            PhotonNetwork.JoinOrCreateRoom(gameRoom, roomOptions, TypedLobby.Default);
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }

    public void HostLAN()
    {

        Tweening.TweenEvent launchedServerEvent = FindObjectOfType<LANServerLauncher>().RunLan();


        launchedServerEvent.OnInvoke += LaunchedServerEvent_OnInvoke;

        isConnecting = true;

        if (onBusy != null)
        {
            onBusy.InvokeAll();
        }

        //set lan server settings
        PhotonNetwork.PhotonServerSettings.HostType = ServerSettings.HostingOption.SelfHosted;
        PhotonNetwork.PhotonServerSettings.ServerAddress = Network.player.ipAddress;
        PhotonNetwork.PhotonServerSettings.ServerPort = 5055;


    }

    private void LaunchedServerEvent_OnInvoke(TweenEvent obj)
    {
        obj.OnInvoke -= LaunchedServerEvent_OnInvoke;
        ConnectLocal();
    }

    void ConnectLocal()
    {
        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.connected)
        {
            //Here we join a specific room, if that fails we will create the specific room, if we are creating the room, then the OnConnectedToMaster will be called hence why we have to have the join method in two spots
            PhotonNetwork.JoinOrCreateRoom(gameRoom, roomOptions, TypedLobby.Default);
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }

    IEnumerator FindLAN()
    {
        LANServerLauncher lanserverlauncher = FindObjectOfType<LANServerLauncher>();
        lanserverlauncher.StartLanClient();

        while (true)
        {
            hostIP = lanserverlauncher.GetIPOfHost();
            //s  print("FindLAN " + hostIP);
            if (hostIP != null)
            {
                break;
            }
            yield return null;
        }
        if (hostIP != null)
        {
            JoinLAN();
        }
    }

    public void JoinLAN()
    {
        isConnecting = true;

        if (onBusy != null)
        {
            onBusy.InvokeAll();
        }

        //set lan server settings
        PhotonNetwork.PhotonServerSettings.HostType = ServerSettings.HostingOption.SelfHosted;
        PhotonNetwork.PhotonServerSettings.ServerAddress = hostIP;
        PhotonNetwork.PhotonServerSettings.ServerPort = 5055;

        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.connected)
        {
            //Here we join a specific room, if that fails we will create the specific room, if we are creating the room, then the OnConnectedToMaster will be called hence why we have to have the join method in two spots
            PhotonNetwork.JoinOrCreateRoom(gameRoom, roomOptions, TypedLobby.Default);
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            PhotonNetwork.ConnectUsingSettings(_gameVersion);
        }
    }

    public void UnpauseGame()
    {
        //photonView.RPC( "UnpauseGameRPC", PhotonTargets.All );
        Time.timeScale = 1;
        print("unpause");
    }


    public void PauseGame()
    {
        //photonView.RPC( "PauseGameRPC", PhotonTargets.All );
        Time.timeScale = 0;
        print("pause");
    }

    [PunRPC]
    private void UnpauseGameRPC()
    {
        Time.timeScale = 1;
    }

    [PunRPC]
    private void PauseGameRPC()
    {
        Time.timeScale = 0;
    }

    public void Disconnect()
    {
        if (PhotonNetwork.connected)
        {
            if (onBusy != null)
            {
                onBusy.InvokeAll();
            }

            PhotonNetwork.Disconnect();
        }
        else
        {
            if (onDisconnect != null)
            {
                onDisconnect.InvokeAll();
            }
        }
    }

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            Debug.Log("connected");
            //Here we join a specific room, if that fails we will create the specific room.
            PhotonNetwork.JoinOrCreateRoom(gameRoom, roomOptions, TypedLobby.Default);
        }
    }

    public override void OnDisconnectedFromPhoton()
    {
        Debug.Log("disconnected");

        joined = false;

        if (onDisconnect != null)
        {
            onDisconnect.InvokeAll();
        }

        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.

        Debug.Log("Failed to join room :(");

        if (onFailConnect != null)
        {
            onFailConnect.InvokeAll();
        }

        joined = false;
    }

    private bool joined;

    public override void OnJoinedRoom()
    {
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        // #Critical
        Debug.Log("Room created and / or joined succesfully ! ");
        // Load the  Level. 
        PhotonNetwork.LoadLevelAdditive("sce_lobby");
		FindObjectOfType<MainMenuMusic>().fadeOutAndStop();
        PhotonNetwork.Instantiate(voiceHub.name, new Vector3(0, 100, 0), Quaternion.identity, 0);

        if (onConnect != null)
        {
            onConnect.InvokeAll();
        }

        if (isLanServer)
        {
            FindObjectOfType<LANServerLauncher>().StartAsServer();
        }

        joined = true;
    }

    #endregion
}
