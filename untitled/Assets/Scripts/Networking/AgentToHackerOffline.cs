﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Photon;
using LevelDesign;

[RequireComponent( typeof( AgentToHacker ) )]
public class AgentToHackerOffline : UnityEngine.MonoBehaviour, IAgentToHacker
{
	private AgentToHacker @base;

	private void Awake ()
	{
		@base = GetComponent<AgentToHacker>();

		AgentToHacker.Instance = this;
	}

	private void Start ()
	{
		@base.OnStart();
	} 

	public void Initialize ( GuardInfo[] guards, NodeInfo[] nodes, WallInfo[] walls, int edgeCount, float localTime )
	{
		@base.OnInitialize( guards, nodes, walls, edgeCount, localTime );
	}

	public void DecryptJunctionBox ( int instanceID, JunctionBox.Codes[] codes, JunctionBox.Codes code )
	{
		@base.OnDecryptJunctionBox( instanceID, codes, code );
	}

	public void BypassJunctionBox ( int instanceID )
	{
		@base.OnBypassJunctionBox( instanceID );
	}

	public void CutWrongWire ( int instanceID )
	{
		@base.OnCutWrongWire( instanceID );
	}

	public void SwipeValuable ( int instanceID, string name )
	{
		@base.OnSwipeValuable( instanceID, name );
	}

	public void UseClock ( int instanceID, float localTime )
	{
		@base.OnUseClock( instanceID, localTime );
	}

	public void TagGuard ( int instanceID )
	{
		@base.OnTagGuard( instanceID );
	}

	public void Alarm ()
	{
		@base.OnAlarm();
	}

	public void Done ()
	{
		@base.OnDone();
	}

	public void Ping(Vector3 agentPosition, bool showHint)
	{
		@base.OnPing( agentPosition, showHint );
	}
}
