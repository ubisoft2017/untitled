﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Photon;
using LevelDesign;

using Tweening;

[RequireComponent( typeof( NodeUIFactory ) )]
public class AgentToHacker : PunBehaviour
{
	#region Singleton
	private static IAgentToHacker @interface;

	public static IAgentToHacker Instance
	{
		get
		{
			return @interface;
		}

		set
		{
			@interface = value;
		}
	}
	#endregion

	public static void Reset ()
	{
		guardInstances = new List<Guard>();
		nodeInstances = new List<Node>();
		blockInstances = new List<Block>();

		stop = true;

		var instance = FindObjectOfType<AgentToHacker>().gameObject;

		if ( instance != null )
		{
			Destroy( instance );
		}

		@interface = null;
	}

	private static bool stop;

	#region Lists
	private static List<Guard> guardInstances = new List<Guard>();
	private static List<Node> nodeInstances = new List<Node>();
	private static List<Block> blockInstances = new List<Block>();

	public static void Enqueue ( Node node )
	{
		nodeInstances.Add( node );

		if ( node is Guard )
		{
			guardInstances.Add( node as Guard );
		}
	}

	public static void Enqueue ( Block block )
	{
		blockInstances.Add( block );
	}
	#endregion


	public TweenEvent doneEvent;
	#region Fields
	private NodeUIFactory factory;

	private NetworkingSettings settings;

	// Agent side
	private List<CharacterController> agentSideGuards;

	// Hacker side
	private List<GuardState> hackerSideGuards;

	private bool initGuardTransforms = false;

	private bool hasGuardData = false;

	private int activePhaseInstanceID;

	[SerializeField]
	private float rotateSpeed = 80.0f;

	[SerializeField]
	private TweenEvent glitchEvent, resumeEvent;

	private float localTime;

	public float LocalTime
	{
		get
		{
			return localTime + Time.time;
		}
	}

	// Photon
	private double lastNetworkDataReceivedTime;
	#endregion

	#region MonoBehaviour Callbacks
	private void Awake ()
	{
		factory = GetComponent<NodeUIFactory>();

		settings = FindObjectOfType<NetworkingSettings>();

		PhotonNetwork.sendRate = settings.SendRate;
		PhotonNetwork.sendRateOnSerialize = settings.SendRateOnSerialize;

		stop = false;
	}

	// Use this for initialization
	public void OnStart ()
	{
		/// Cache references to guards' CharacterControllers, used in serialization loop

		agentSideGuards = new List<CharacterController>( guardInstances.Count );

		foreach ( Node guard in guardInstances )
		{
			agentSideGuards.Add( guard.gameObject.GetComponent<CharacterController>() );
		}

		/// Create level info arrays

		var guardInfo = new GuardInfo[guardInstances.Count];
		var nodeInfo = new NodeInfo[nodeInstances.Count];
		var wallInfo = new List<WallInfo>();

		/// Fill guard info array

		for ( int i = 0; i < guardInfo.Length; i++ )
		{
			guardInfo[i] = new GuardInfo( guardInstances[i].GetInstanceID(), 0 );

			/// Old version, when guards belonged to a phase

			//var phase = guardInstances[i].transform.parent.GetComponent<Phase>();

			//guardInfo[i] = new GuardInfo( guardInstances[i].GetInstanceID(), phase.GetInstanceID() );
		}

		/// Fill node info array

		for ( int i = 0; i < nodeInfo.Length; i++ )
		{
			nodeInfo[i] = new NodeInfo( nodeInstances[i], NodeGraph.Current );
		}

		/// Fill wall info array

		foreach ( var block in blockInstances )
		{
			for ( int i = 0; i < block.Walls.Count; i++ )
			{
				wallInfo.Add( new WallInfo( block, i ) );
			}
		}

		int edgeCount = FindObjectOfType<NodeGraph>().Connections.Count;

		print( string.Format( "# guards: {0}, # nodes: {1}, # walls: {2}, # edges: {3}", guardInfo.Length, nodeInfo.Length, wallInfo.Count, edgeCount ) );

		@interface.Initialize( guardInfo, nodeInfo, wallInfo.ToArray(), edgeCount, Phase.Active.LocalTime );
	}

	void Update ()
	{
		if ( !stop )
		{
			if ( !photonView.isMine )
			{
				if ( hackerSideGuards != null && hasGuardData )
				{
					if ( !initGuardTransforms )
					{
						foreach ( var ui in hackerSideGuards )
						{
							ui.transform.position = ui.tracker.position;
							ui.transform.rotation = Quaternion.Euler( 0.0f, 0.0f, -ui.tracker.rotation );
						}

						initGuardTransforms = true;
					}
					else
					{
						foreach ( var ui in hackerSideGuards )
						{
							ui.transform.position = Vector2.MoveTowards(
								new Vector2(
									ui.transform.position.x,
									ui.transform.position.y ),
								ui.tracker.position,
								ui.tracker.velocity.magnitude * Time.deltaTime );

							Quaternion newRotation = Quaternion.Euler( 0, 0, ui.tracker.rotation );

							ui.transform.rotation = Quaternion.RotateTowards(
								ui.transform.rotation,
								newRotation, Time.deltaTime * rotateSpeed );
						}
					}
				}
			}
			else
			{
				//possible Agent will need to do stuff in update as well
			}
		}
	}
	#endregion

	#region RPCs
	[PunRPC]
	public void OnInitialize ( GuardInfo[] guards, NodeInfo[] nodes, WallInfo[] walls, int edgeCount, float localTime )
	{
		print( string.Format( "# guards: {0}, # nodes: {1}, # walls: {2}, # edges: {3}", guards.Length, nodes.Length, walls.Length, edgeCount ) );

		/// Create edge objects
		
		Queue<Edge> edges = new Queue<Edge>( edgeCount );

		for ( int i = 0; i < edgeCount; i++ )
		{
			edges.Enqueue( factory.CreateEdge() );
		}

		/// Create node interface objects (inlcuding guards)

		NodeUI[] ui = new NodeUI[nodes.Length];

		for ( int i = 0; i < nodes.Length; i++ )
		{
			ui[i] = factory.CreateUI( nodes[i], edges );
		}

		/// Create guard state structs

		hackerSideGuards = new List<GuardState>( guards.Length );

		for ( int i = 0; i < guards.Length; i++ )
		{
			hackerSideGuards.Add( new GuardState( guards[i], NodeUI.Get( guards[i].guardInstanceID ).transform ) );
		}

		/// Link connected nodes

		for ( int i = 0; i < nodes.Length; i++ )
		{
			for ( int j = 0; j < nodes[i].connections.Length; j++ )
			{
				ui[i].Connect( NodeUI.Get( nodes[i].connections[j] ) );
			}
		}

		/// Create wall renderers

		// TODO: for ( each phase )
		factory.CreateWall( walls );

		/// Setup local time
		this.localTime = localTime - Time.time;
	}

	/// <summary>
	/// Begin the hacker side of the junction box handshake.
	/// </summary>
	/// <param name="codes">Face buttons to be pressed by hacker.</param>
	public static void NotifyDecryptJunctionBox ( JunctionBoxNode node, JunctionBox.Codes[] codes, JunctionBox.Codes code )
	{
		@interface.DecryptJunctionBox( node.GetInstanceID(), codes, code );
	}

	[PunRPC]
	public void OnDecryptJunctionBox ( int instanceID, JunctionBox.Codes[] codes, JunctionBox.Codes rightCode )
	{
		var box = FindObjectOfType<JunctionBoxHacker>();
		
		box.ui = NodeUI.Get( instanceID );

		if ( box.ui == null )
		{
			Debug.LogError( "junction box ui was never initialized, instance ID: " + instanceID );
		}

		box.Initialize( codes, rightCode );

		Debug.Log( string.Format( "OnDecryptJunctionBox( {0}, {1} )", codes, rightCode ) );
	}

	/// <summary>
	/// Indicate that a bypass has been successfully integrated at a junction box.
	/// </summary>
	/// <param name="junctionBox">JunctionBox at which bypass was integrated.</param>
	public static void NotifyBypassJunctionBox ( JunctionBoxNode junctionBox )
	{
		@interface.BypassJunctionBox( junctionBox.GetInstanceID() );
	}

	[PunRPC]
	public void OnBypassJunctionBox ( int instanceID )
	{
		var ui = NodeUI.Get( instanceID );

		Debug.Log( "looking for " + instanceID + " junction box" );

		FindObjectOfType<NodeView>().Select( ui, snap: true );

		ui.Discover();

		FindObjectOfType<JunctionBoxHacker>().Pass();
	}

	/// <summary>
	/// Indicate that the agent has cut the wrong wire on the junction box.
	/// </summary>
	/// <param name="junctionBox">JunctionBox at which bypass was failed.</param>
	public static void NotifyCutWrongWire ( JunctionBoxNode junctionBox )
	{
		@interface.CutWrongWire( junctionBox.GetInstanceID() );
	}

	[PunRPC]
	public void OnCutWrongWire ( int instanceID )
	{
		FindObjectOfType<JunctionBoxHacker>().Fail();
	}

	/// <summary>
	/// Indicate that the agent has just swiped a valuable.
	/// </summary>
	/// <param name="valuable">Valuable that was swiped.</param>
	public static void NotifySwipeValuable ( Valuable valuable )
	{
		@interface.SwipeValuable( valuable.GetInstanceID(), valuable.Type.name );
	}

	[PunRPC]
	public void OnSwipeValuable ( int instanceID, string name )
	{
		//TODO: feedback
		//display valauble in hacker room

		var valuable = ValuableType.Get( name );

		FindObjectOfType<SessionController>().OnSteal( valuable );

		(NodeUI.Get( instanceID ) as ValuableUI).Steal();

		Debug.Log( "You've stolen " + valuable.Determiner + " " + valuable.Label + "!" );
	}

	/// <summary>
	/// Indicate that the agent has jumped to a new phase by touching a clock face.
	/// </summary>
	/// <param name="clock">Clock that was touched.</param>
	public static void NotifyUseClock ( Clock clock )
	{
		@interface.UseClock( clock.GetInstanceID(), clock.Phase.LocalTime );
	}

	[PunRPC]
	public void OnUseClock ( int instanceID, float localTime )
	{
		activePhaseInstanceID = instanceID;

		glitchEvent.Invoke();

		this.localTime = localTime - Time.time;
	}


	/// <summary>
	/// Indicate that a guard has been tagged for tracking.
	/// </summary>
	/// <param name="instanceID">Instance ID of tagged guard.</param>
	public static void NotifyTagGuard ( Guard guard )
	{
		@interface.TagGuard( guard.GetInstanceID() );
	}

	[PunRPC]
	public void OnTagGuard ( int instanceID )
	{
		var ui = NodeUI.Get( instanceID ) as GuardUI;

		ui.Tag();
	}

	public static void NotifyPing ( Vector3 agentPosition, bool showHint)
	{
		@interface.Ping(agentPosition, showHint);
	}

	[PunRPC]
	public void OnPing(Vector3 agentPosition, bool showHint )
	{
		PingUI.ping.Ping( agentPosition, showHint);
	}

	/// <summary>
	/// Indicate that the agent has triggered an alarm.
	/// </summary>
	public static void NotifyAlarm ()
	{
		if ( @interface != null )
		{
			@interface.Alarm();
		}
	}

	[PunRPC]
	public void OnAlarm ()
	{
		FindObjectOfType<SecuritySystem>().Fail( "the agent was discovered" );
	}

	/// <summary>
	/// Indicate that the agent has finished the game.
	/// </summary>
	public static void NotifyDone ()
	{
		if ( @interface != null )
		{
			@interface.Alarm();
		}
	}

	[PunRPC]
	public void OnDone ()
	{
		doneEvent.Invoke();
	}

	#endregion

	#region Photon Callbacks
	//Serialize Info
	//We dont have to check for ownership here since photon knows that if a client owns the photonView then it writes else if its not owned by the client it reads, this method is also only called if the data its sending has changed aka the none owner client is not always calling this method
	void OnPhotonSerializeView ( PhotonStream stream, PhotonMessageInfo info )
	{
		if ( !stop )
		{
			// Agent's client writes guard info to stream
			if ( stream.isWriting )
			{
				stream.SendNext( Phase.Active.LocalTime );

				for ( int i = 0; i < agentSideGuards.Count; i++ )
				{
					stream.SendNext( new GuardTracker(
						new Vector2(
							agentSideGuards[i].transform.position.x,
							agentSideGuards[i].transform.position.z ),
						new Vector3(
							agentSideGuards[i].velocity.x,
							agentSideGuards[i].velocity.z ),
						-agentSideGuards[i].transform.eulerAngles.y ) );    // must be reversed
				}
			}

			// Hacker's client reads it back out
			else
			{
				lastNetworkDataReceivedTime = info.timestamp;

				localTime = (float) stream.ReceiveNext();

				if ( hackerSideGuards != null )
				{
					for ( int i = 0; i < hackerSideGuards.Count; i++ )
					{
						hackerSideGuards[i].tracker = (GuardTracker) stream.ReceiveNext();
					}

					hasGuardData = true;
				}
			}
		}
	}
	#endregion
}
