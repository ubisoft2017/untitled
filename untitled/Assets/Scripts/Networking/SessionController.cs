﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Controls;
using UnityEngine.SceneManagement;
using Tweening;
using System;
using System.Collections.ObjectModel;

public static class ScoreFormatter
{
	public static string Format ( int score, bool decimals )
	{
		string scr = score.ToString();

		string str = "";

		for ( int i = scr.Length - 1, j = 0; i >= 0; i--, j++ )
		{
			if ( j > 0 && j % 3 == 0 )
			{
				str = ',' + str;
			}

			str = scr[i] + str;
		}

		str = "$" + str;

		if ( decimals )
		{
			str += ".00";
		}

		return str;
	}
}

public enum PlayerSelection
{
	None,
	Agent,
	Hacker
}

public class SessionController : Photon.PunBehaviour
{
	public static Action<SessionController> onScoreChange;

	public void Bump ()
	{
		if ( onScoreChange !=null)
		{
			onScoreChange( this );
		}
	}

	private static SessionController activeSession;

	public static SessionController ActiveSession
	{
		get
		{
			return activeSession;
		}
	}

	[Header( "Setup" )]

	[SerializeField]
	private GameObject agentFacade;

	[SerializeField]
	private GameObject hackerFacade;

	[SerializeField]
	private SceneReference hackerScene;

	[EnumObject( typeof( PlayerType ) )]
	[SerializeField]
	private PlayerType agent, hacker;

	[Header( "Events" )]

	[SerializeField]
	private TweenEvent unloadLevelEvent;

	[SerializeField]
	private TweenEventList onStartLoadingGame, onStartUnloadingGame, onBothClientsLoaded, eventsToClear;

	[SerializeField]
	private bool masterAuthority;

	private bool isLoaded;

	private bool isBusy;

	private ProfileController profileController;

	private LevelList levelList;

	private GameObject agentFacadeInstance, hackerFacadeInstance;

	private PlayerSelection localSelection;
	private PlayerSelection remoteSelection;

	public PlayerSelection LocalSelection
	{
		get
		{
			return localSelection;
		}
	}

	public PlayerSelection RemoteSelection
	{
		get
		{
			return remoteSelection;
		}
	}

	private bool remoteIsLoaded = false;
	private bool localIsLoaded = false;

	[HideInInspector]
	public string agentName, hackerName;

	private int buffer, bufferedScore;

	public int BufferedScore
	{
		get
		{
			return bufferedScore;
		}
	}

	[HideInInspector]
	private int score;

	public int Score
	{
		get
		{
			return score;
		}
	}

	[HideInInspector]
	public float time;

	private int loadedIndex, nextIndex;

	public int LoadedIndex
	{
		get
		{
			return loadedIndex;
		}
	}

	private List<ValuableType> valuables;

	/// <summary>
	/// List of valuable stolen to date.
	/// </summary>
	public ReadOnlyCollection<ValuableType> Valuables
	{
		get
		{
			return valuables.AsReadOnly();
		}
	}

	private void OnEnable ()
	{
		localSelection = PlayerSelection.None;
		remoteSelection = PlayerSelection.None;
	}

	private void Awake ()
	{
		valuables = new List<ValuableType>();

		profileController = FindObjectOfType<ProfileController>();
		levelList = FindObjectOfType<LevelList>();

		activeSession = this;

		StartCoroutine( Bufferer() );

        DevTools.Register("Restart level", RestartLevel);
        DevTools.Register("Skip level", NextLevel);

    }

    private void RestartLevel()
    {
        UnloadLevel(LoadedIndex);
    }

    private void NextLevel()
    {
        UnloadLevel(LoadedIndex + 1);
    }

    public void OnSteal ( ValuableType valuable )
	{
		score += valuable.Value;
		buffer += valuable.Value;

		if ( onScoreChange != null )
		{
			onScoreChange( this );
		}

		valuables.Add( valuable );
	}

	private IEnumerator Bufferer ()
	{
		while ( true )
		{
			if ( buffer > 0 )
			{
				int delta = Mathf.CeilToInt( buffer / 2.0f );

				buffer -= delta;
				bufferedScore += delta;

				if ( onScoreChange != null )
				{
					onScoreChange( this );
				}
			}

			yield return new WaitForSeconds( 0.1f );
		}
	}

	public void LoadLevel ( int index, PlayerSelection local, PlayerSelection remote )
	{
		LoadGame( index, local, remote );
		
		photonView.RPC( "LoadGame", PhotonTargets.OthersBuffered, index, remote, local );
	}

	public void UnloadLevel ( int nextLevelIndex = -1 )
	{
		UnloadGame( nextLevelIndex );

		photonView.RPC( "UnloadGame", PhotonTargets.OthersBuffered, nextLevelIndex );
	}

	private void HandleSceneLoaded ( Scene scene, LoadSceneMode mode )
	{
		SceneManager.sceneLoaded -= HandleSceneLoaded;

		isBusy = false;
		isLoaded = true;

		Time.timeScale = 1.0f;

		//if ( ! SceneManager.SetActiveScene( scene ) )
		//{
		//	Debug.LogError( "SetActiveScene failed." );
		//}

		if ( localSelection == PlayerSelection.Agent )
		{
			agentFacadeInstance = PhotonNetwork.Instantiate( agentFacade.name, new Vector3( 0, 50, 0 ), Quaternion.identity, 0 );

			GameObject.FindGameObjectWithTag( "Agent" ).GetComponentInChildren<CameraAnchor>().priority = 10;
		}
		else if ( localSelection == PlayerSelection.Hacker )
		{
			hackerFacadeInstance = PhotonNetwork.Instantiate( hackerFacade.name, new Vector3( 0, 50, 0 ), Quaternion.identity, 0 );

			GameObject.FindGameObjectWithTag( "Hacker" ).GetComponentInChildren<CameraAnchor>().priority = 10;
		}
		else
		{
			return;
		}

		localIsLoaded = true;

		Debug.Log( "local is loaded" );

		photonView.RPC( "SetRemoteLoaded", PhotonTargets.OthersBuffered, true );

		CheckIfBothClientsLoaded();
	}

	[PunRPC]
	private void SetRemoteLoaded ( bool loaded )
	{
		remoteIsLoaded = loaded;

		Debug.Log( "remote is loaded" );

		CheckIfBothClientsLoaded();
	}

	private void CheckIfBothClientsLoaded ()
	{
		if ( remoteIsLoaded && localIsLoaded )
		{
			Debug.Log( "both clients loaded" );

			if ( onBothClientsLoaded != null )
			{
				onBothClientsLoaded.InvokeAll();
			}
		}
	}

	[PunRPC]
	private void LoadGame ( int levelIndex, PlayerSelection local, PlayerSelection remote )
	{
		if ( !isBusy )
		{
			Debug.Log( "!!!!!!!! LOAD GAME", this );

			localSelection = local;
			remoteSelection = remote;

			loadedIndex = levelIndex;

			if ( local == PlayerSelection.Agent )
			{
				isBusy = true;

				profileController.Player.State = agent.DefaultState;

				if ( onStartLoadingGame != null )
				{
					onStartLoadingGame.InvokeAll();
				}

				SceneManager.sceneLoaded += HandleSceneLoaded;

				PhotonNetwork.LoadLevelAdditive( levelList[levelIndex].SceneReference.GetSceneName() );
			}
			else if ( local == PlayerSelection.Hacker )
			{
				isBusy = true;

				profileController.Player.State = hacker.DefaultState;

				if ( onStartLoadingGame != null )
				{
					onStartLoadingGame.InvokeAll();
				}

				SceneManager.sceneLoaded += HandleSceneLoaded;

				PhotonNetwork.LoadLevelAdditive( hackerScene.GetSceneName() );
			}
			else
			{
				Debug.LogError( "Game cannot be loaded before a player type is selected." );
			}
		}
	}

	[PunRPC]
	private void UnloadGame ( int nextLevelIndex )
	{
		if ( isLoaded && !isBusy )
		{
			AgentToHacker.Reset();
			HackerToAgent.Reset();

			GamePad.Clear();

			GamePadInputModule.Bind();

			onScoreChange = delegate{};

			if ( eventsToClear != null )
			{
				foreach ( var @event in eventsToClear )
				{
					if ( @event != null )
					{
						@event.Clear();
					}
				}
			}

			if ( localSelection == PlayerSelection.Agent )
			{
				Destroy( agentFacadeInstance );
			}
			else if ( localSelection == PlayerSelection.Hacker )
			{
				Destroy( hackerFacadeInstance );
			}

			Debug.Log( "!!!!!!!! UNLOAD GAME", this );

			isBusy = true;

			onStartUnloadingGame.InvokeAll();

			StartCoroutine( UnloadCoroutine( nextLevelIndex ) );
		}
		else
		{
			Debug.Log( "!!!!!!!!! REFUSE TO UNLOAD" );
		}
	}

	private IEnumerator UnloadCoroutine ( int nextLevelIndex )
	{
		yield return new WaitForSeconds( 2.0f );

		SceneManager.sceneUnloaded += HandleSceneUnloaded;

		unloadLevelEvent.Invoke();

		nextIndex = nextLevelIndex;
	}

	private void HandleSceneUnloaded ( Scene scene )
	{
		SceneManager.sceneUnloaded -= HandleSceneUnloaded;

		isLoaded = false;

		Invoke( "LoadNext", 0.5f );
	}

	private void LoadNext ()
	{
		isBusy = false;

		if ( nextIndex > -1 )
		{
			LoadGame( nextIndex, localSelection, remoteSelection );
		}
	}
}
