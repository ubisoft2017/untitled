﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

/// <summary>
/// Contains serialized guard data broadcasted at regular intervals.
/// </summary>
public class GuardState
{
	public readonly GuardInfo info;
	public GuardTracker tracker;
	public Transform transform;
	public bool tagged;

	public GuardState ( GuardInfo info, Transform transform )
	{
		this.info = info;
		this.transform = transform;
	}
}
