﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExitGames.Client.Photon;
using UnityEngine;

public class NetworkingSettings : MonoBehaviour
{
	[SerializeField]
	private int sendRate = 20;

	public int SendRate
	{
		get
		{
			return sendRate;
		}
	}

	[SerializeField]
	private int sendRateOnSerialize = 10;

	public int SendRateOnSerialize
	{
		get
		{
			return sendRateOnSerialize;
		}
	}

	private void Awake ()
	{
		PhotonPeer.RegisterType( typeof( NodeInfo ), (byte) 'N', Serializers.SerializeNodeInfo, Serializers.DeserializeNodeInfo );
		PhotonPeer.RegisterType( typeof( WallInfo ), (byte) 'w', Serializers.SerializeWallInfo, Serializers.DeserializeWallInfo );
		PhotonPeer.RegisterType( typeof( GuardInfo ), (byte) 'G', Serializers.SerializeGuardInfo, Serializers.DeserializeGuardInfo );
		PhotonPeer.RegisterType( typeof( GuardTracker ), (byte) 'X', Serializers.SerializeGuardTracker, Serializers.DeserializeGuardTracker );
	}
}
