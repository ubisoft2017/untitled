﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Photon;
using LevelDesign;

[RequireComponent( typeof( HackerToAgent ) )]
public class HackerToAgentPhoton : UnityEngine.MonoBehaviour, IHackerToAgent
{
	private HackerToAgent @base;

	private void Awake ()
	{
		@base = GetComponent<HackerToAgent>();

		HackerToAgent.Instance = this;
	}

	public void FoundStreamJunctionBox ( int color, int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnFoundStreamJunctionBox", PhotonTargets.Others, color, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void FailJunctionBox ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnFailJunctionBox", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void OpenDoor ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnOpenDoor", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void CloseDoor ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnCloseDoor", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void StartDisruptingGuard ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnStartDisruptingGuard", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void StopDisruptingGuard ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnStopDisruptingGuard", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void CycleLaserSystem ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnCycleLaserSystem", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}

	public void Alarm ()
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnAlarm", PhotonTargets.Others );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not hacker.", @base );
		}
	}
}
