﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Photon;
using LevelDesign;

[RequireComponent( typeof( HackerToAgent ) )]
public class HackerToAgentOffline : UnityEngine.MonoBehaviour, IHackerToAgent
{
	private HackerToAgent @base;

	private void Awake ()
	{
		@base = GetComponent<HackerToAgent>();

		HackerToAgent.Instance = this;
	}

	public void FoundStreamJunctionBox ( int color, int instanceID )
	{
		@base.OnFoundStreamJunctionBox( color, instanceID );
	}

	public void FailJunctionBox ( int instanceID )
	{
		@base.OnFailJunctionBox( instanceID );
	}

	public void OpenDoor ( int instanceID )
	{
		@base.OnOpenDoor( instanceID );
	}

	public void CloseDoor ( int instanceID )
	{
		@base.OnCloseDoor( instanceID );
	}

	public void StartDisruptingGuard ( int instanceID )
	{
		@base.OnStartDisruptingGuard( instanceID );
	}

	public void StopDisruptingGuard ( int instanceID )
	{
		@base.OnStopDisruptingGuard( instanceID );
	}

	public void CycleLaserSystem( int instanceID )
	{
		@base.OnCycleLaserSystem( instanceID );
	}

	public void Alarm ()
	{
		@base.OnAlarm();
	}
}
