﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

/// <summary>
/// Tracks guard data on the hacker side.
/// </summary>
[Serializable]
public struct GuardTracker
{
	public Vector2 position, velocity;
	public float rotation;

	public GuardTracker ( Vector2 position, Vector2 velocity, float rotation )
	{
		this.position = position;
		this.velocity = velocity;
		this.rotation = rotation;
	}
}
