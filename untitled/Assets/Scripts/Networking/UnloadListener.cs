﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains initialization data for each guard in a level.
/// </summary>
[Serializable]
public class UnloadListener : MonoBehaviour
{
	[SerializeField]
	private TweenEvent @event;

	private void Awake ()
	{
		@event.OnInvoke += Unload;

		enabled = false;
	}

	private void Unload ( TweenEvent e )
	{
		@event.OnInvoke -= Unload;

		if ( this != null && gameObject != null )
		{
			SceneManager.UnloadSceneAsync( gameObject.scene );
		}
	}
}
