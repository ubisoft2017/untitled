﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IHackerToAgent
{
	void FoundStreamJunctionBox ( int color, int instanceID );

	void FailJunctionBox ( int instanceID );

	void OpenDoor ( int instanceID );

	void CloseDoor ( int instanceID );

	void StartDisruptingGuard ( int instanceID );

	void StopDisruptingGuard ( int instanceID );

	void CycleLaserSystem ( int instanceID );

	void Alarm ();
}