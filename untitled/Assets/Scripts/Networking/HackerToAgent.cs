﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class HackerToAgent : Photon.PunBehaviour
{
	#region Singleton
	private static IHackerToAgent @interface;

	public static IHackerToAgent Instance
	{
		get
		{
			return @interface;
		}

		set
		{
			@interface = value;
		}
	}
	#endregion

	private static HackerToAgent instance;

	public static void Reset ()
	{
		Destroy( instance.gameObject );

		instance = null;

		@interface = null;
	}

	private void Awake ()
	{
		instance = this;
	}

	#region RPCs
	//TODO: any hacker actions which impact the agent's world

	/// <summary>
	/// Indicate that the color of the critical wire has been found.
	/// </summary>
	/// <param name="codes">Color index.</param>
	public static void NotifyFoundStreamJunctionBox( int color, NodeUI ui )
    {
		@interface.FoundStreamJunctionBox( color, ui.TargetInstanceID );
	}

    [PunRPC]
	public void OnFoundStreamJunctionBox( int color, int instanceID )
	{
		Node.Get<JunctionBoxNode>( instanceID ).Box.SetWireState( color );
	}

	/// <summary>
	/// Indicate that the code was not properly entered.
	/// </summary>
	/// <param name="junctionBox">JunctionBox at which bypass was failed.</param>
	public static void NotifyFailJunctionBox( NodeUI ui )
    {
		@interface.FailJunctionBox( ui.TargetInstanceID );
	}

    [PunRPC]
	public void OnFailJunctionBox( int instanceID )
	{
		Node.Get<JunctionBoxNode>( instanceID ).Box.Fail();
	}
   
	public static void NotifyOpenDoor (NodeUI ui)
	{
		@interface.OpenDoor( ui.TargetInstanceID );
	}

	[PunRPC]
	public void OnOpenDoor ( int instanceID )
	{
		Node.Get<Door>( instanceID ).Open();
	}

	public static void NotifyCloseDoor ( NodeUI ui )
	{
		@interface.CloseDoor( ui.TargetInstanceID );
	}

	[PunRPC]
	public void OnCloseDoor ( int instanceID )
	{
		Node.Get<Door>( instanceID ).Close();
	}

	public static void NotifyStopDisruptingGuard ( GuardUI ui )
	{
		@interface.StopDisruptingGuard( ui.TargetInstanceID );
	}

	[PunRPC]
	public void OnStopDisruptingGuard ( int instanceID )
	{
		Node.Get<Guard>( instanceID ).StopDisrupting();
	}

	public static void NotifyStartDisruptingGuard ( GuardUI ui )
	{
		@interface.StartDisruptingGuard( ui.TargetInstanceID );
	}

	[PunRPC]
	public void OnStartDisruptingGuard ( int instanceID )
	{
		Node.Get<Guard>( instanceID ).StartDisrupting();
	}

	public static void NotifyCycleLaserSystem ( LaserUI ui )
	{
		@interface.CycleLaserSystem( ui.TargetInstanceID );
	}

	[PunRPC]
	public void OnCycleLaserSystem ( int instanceID )
	{
		Node.Get<LaserSystem>( instanceID ).NextLaserConfiguration();
		Debug.Log( "Laser Cycle" );
	}

	public static void NotifyAlarm()
	{
		@interface.Alarm();
	}

	[PunRPC]
	public void OnAlarm ()
	{
		FindObjectOfType<GameOverSystem>().HackerDetected();
	}
	#endregion
}
