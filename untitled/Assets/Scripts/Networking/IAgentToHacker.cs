﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IAgentToHacker
{
	void Initialize ( GuardInfo[] guards, NodeInfo[] nodes, WallInfo[] walls, int edgeCount, float localTime );

	void DecryptJunctionBox ( int instanceID, JunctionBox.Codes[] codes, JunctionBox.Codes rightCode );

	void BypassJunctionBox ( int instanceID );

	void CutWrongWire ( int instanceID );

	void SwipeValuable ( int instanceID, string name );

	void UseClock ( int instanceID, float localTime );

	void TagGuard ( int instanceID );

	void Ping( Vector3 agentPosition, bool showHint);

	void Alarm ();

	void Done ();
}