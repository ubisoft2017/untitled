﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Controls;
using UnityEngine.SceneManagement;
using Tweening;
using System;

public class LobbyController : Photon.PunBehaviour
{
	[Header( "Setup" )]

	[SerializeField]
	private SceneReference sessionScene;

	[EnumObject( typeof( PlayerType ) )]
	[SerializeField]
	private PlayerType agent, hacker;

	[Header( "Events" )]
	[EnumObject( typeof( TweenEvent ) )]
	[SerializeField]
	private TweenEvent localSelectHackerEvent;

	[EnumObject( typeof( TweenEvent ) )]
	[SerializeField]
	private TweenEvent localSelectAgentEvent, remoteSelectHackerEvent, remoteSelectAgentEvent, toggleReadyEvent;

	[EnumObject( typeof( TweenEvent ) )]
	[SerializeField]
	private TweenEvent agentReadyEvent;

    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent hackerReadyEvent;

    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent agentUnreadyEvent;

    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent hackerUnreadyEvent;

    [EnumObject(typeof(TweenEvent))]
    [SerializeField]
    private TweenEvent lobbyDisconnectEvent;

	private PlayerSelection localSelection;
	private PlayerSelection remoteSelection;

	[SerializeField]
	private bool masterAuthority;

	private bool isGameLoadedOrLoading;

	public bool IsGameLoadedOrLoading
	{
		get
		{
			return isGameLoadedOrLoading;
		}
	}

	private ProfileController profileController;

	private LevelList levelList;

	private bool localPlayerIsReady = false;
	private bool remotePlayerIsReady = false;

	private Text ready;

	private void OnEnable ()
	{
		localSelection = PlayerSelection.None;
		remoteSelection = PlayerSelection.None;
	}

	private void Awake ()
	{
		threshold = 0.5f;

		profileController = FindObjectOfType<ProfileController>();

		levelList = FindObjectOfType<LevelList>();

		ready = GameObject.FindGameObjectWithTag( "Ready" ).GetComponent<Text>();

		ready.text = "Ready";

		PhotonNetwork.LoadLevelAdditive( sessionScene.GetSceneName() );
	}

	void Start ()
	{
		toggleReadyEvent.OnInvoke += ToggleReady;

		lobbyDisconnectEvent.OnInvoke += ( e ) =>
		{
			toggleReadyEvent.OnInvoke -= ToggleReady;
		};
	}

	private void ToggleReady ( TweenEvent e )
	{
		if ( !localPlayerIsReady || !remotePlayerIsReady )
		{
			localPlayerIsReady = !localPlayerIsReady;

			ShowReadyStatus( localSelection, localPlayerIsReady );

			photonView.RPC( "SetRemoteReady", PhotonTargets.OthersBuffered, localPlayerIsReady );

			print( localPlayerIsReady ? "local ready" : "local unready" );

			ready.text = localPlayerIsReady ? "Unready" : "Ready";

			TryLoad();
		}
    }

    private void TryLoad()
    {
        if (localPlayerIsReady && remotePlayerIsReady)
        {
            if (PhotonNetwork.isMasterClient)
            {
                FindObjectOfType<SessionController>().LoadLevel(levelList.Index, localSelection, remoteSelection);
            }
        }
    }

	private void ShowReadyStatus ( PlayerSelection selection, bool ready )
	{
		if ( ready )
		{
			if ( selection == PlayerSelection.Agent )
			{
				agentReadyEvent.Invoke();
			}
			else
			{
				hackerReadyEvent.Invoke();
			}
		}
		else
		{
			if ( selection == PlayerSelection.Agent )
			{
				agentUnreadyEvent.Invoke();
			}
			else
			{
				hackerUnreadyEvent.Invoke();
			}
		}
	}

	private float threshold = 0.9f;

	// Update is called once per frame
	void Update()
    {
        if (!localPlayerIsReady && !remotePlayerIsReady)
        {
            if (GamePad.Query(DeviceButton.Left).WasJustPressed || GamePad.Query(DeviceJoystick.Left).Position.x < -threshold || GamePad.Query(DeviceJoystick.Right).Position.x < -threshold)
            {
                if (PhotonNetwork.isMasterClient || !masterAuthority)
                {
                    SelectLocalPlayer(PlayerSelection.Agent);
                }
            }


            //GamePad.Query(DeviceJoystick.Left).Position;
            if (GamePad.Query(DeviceButton.Right).WasJustPressed || GamePad.Query(DeviceJoystick.Left).Position.x > threshold || GamePad.Query(DeviceJoystick.Right).Position.x > threshold )
            {
                if (PhotonNetwork.isMasterClient || !masterAuthority)
                {
                    SelectLocalPlayer(PlayerSelection.Hacker);
                }
            }
        }
    }

	private void SelectLocalPlayer ( PlayerSelection selection )
	{
		if ( selection != localSelection )
		{
			localSelection = selection;

			if ( selection == PlayerSelection.Agent )
			{
				localSelectAgentEvent.Invoke();
			}
			else if ( selection == PlayerSelection.Hacker )
			{
				localSelectHackerEvent.Invoke();
			}

			photonView.RPC( "SelectRemotePlayer", PhotonTargets.OthersBuffered, selection );
			
			//Debug.Log( "local is " + selection );
		}
	}

	//syncs character select screen
	[PunRPC]
	private void SelectRemotePlayer ( PlayerSelection selection )
	{
		remoteSelection = selection;

		if ( selection == PlayerSelection.Agent )
		{
			remoteSelectAgentEvent.Invoke();
		}
		else if ( selection == PlayerSelection.Hacker )
		{
			remoteSelectHackerEvent.Invoke();
		}

		Debug.Log( "remote is " + selection );

		if ( masterAuthority && !PhotonNetwork.isMasterClient )
		{
			if ( selection == PlayerSelection.Agent )
			{
				SelectLocalPlayer( PlayerSelection.Hacker );
			}
			else if ( selection == PlayerSelection.Hacker )
			{
				SelectLocalPlayer( PlayerSelection.Agent );
			}
		}
	}

	[PunRPC]
	private void SetRemoteReady ( bool ready )
	{
		remotePlayerIsReady = ready;

		ShowReadyStatus( remoteSelection, remotePlayerIsReady );

		print( remotePlayerIsReady ? "remote ready" : "remote unready" );

        TryLoad();
	}
}
