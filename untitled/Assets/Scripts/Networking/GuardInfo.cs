﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Contains initialization data for each guard in a level.
/// </summary>
[Serializable]
public struct GuardInfo
{
	public int guardInstanceID, phaseInstanceID;

	public GuardInfo ( int guardInstanceID, int phaseInstanceID )
	{
		this.guardInstanceID = guardInstanceID;
		this.phaseInstanceID = phaseInstanceID;
	}
}
