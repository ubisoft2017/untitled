﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Photon;
using LevelDesign;

[RequireComponent( typeof( AgentToHacker ) )]
public class AgentToHackerPhoton : UnityEngine.MonoBehaviour, IAgentToHacker
{
	private AgentToHacker @base;

	private void Awake ()
	{
		@base = GetComponent<AgentToHacker>();

		AgentToHacker.Instance = this;
	}

	private void Start ()
	{
		if ( @base.photonView.isMine )
		{
			@base.OnStart();
		}
	}

	public void Initialize ( GuardInfo[] guards, NodeInfo[] nodes, WallInfo[] walls, int edgeCount, float localTime )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnInitialize", PhotonTargets.OthersBuffered, guards, nodes, walls, edgeCount, localTime );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void DecryptJunctionBox ( int instanceID, JunctionBox.Codes[] codes, JunctionBox.Codes code )
	{
		if ( @base.photonView.isMine )
		{
			Debug.Log( "AgentToHackerPhoton RPC instance ID: " + instanceID );
			@base.photonView.RPC( "OnDecryptJunctionBox", PhotonTargets.Others, instanceID, codes, code );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void BypassJunctionBox ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnBypassJunctionBox", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void CutWrongWire ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnCutWrongWire", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void SwipeValuable ( int instanceID, string name )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnSwipeValuable", PhotonTargets.Others, instanceID, name );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void UseClock ( int instanceID, float localTime )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnUseClock", PhotonTargets.Others, instanceID, localTime );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void TagGuard ( int instanceID )
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnTagGuard", PhotonTargets.Others, instanceID );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void Alarm ()
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnAlarm", PhotonTargets.Others );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}


	public void Ping ( Vector3 agentPosition, bool showHint)
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnPing", PhotonTargets.Others,  agentPosition, showHint);
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}

	public void Done ()
	{
		if ( @base.photonView.isMine )
		{
			@base.photonView.RPC( "OnDone", PhotonTargets.Others );
		}
		else
		{
			Debug.LogError( "Could not notify, local client is not agent.", this );
		}
	}
}
