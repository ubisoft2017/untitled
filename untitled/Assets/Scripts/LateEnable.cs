﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LateEnable : MonoBehaviour
{
	[SerializeField][HideInInspector] private bool done;
	[SerializeField] private float delay;
	[SerializeField] private MonoBehaviour behaviour;

	// Update is called once per frame
	void Update ()
	{
		if ( !done && Time.unscaledTime >= delay )
		{
			if ( behaviour != null )
			{
				behaviour.enabled = true;
			}

			done = true;
		}
	}
}
