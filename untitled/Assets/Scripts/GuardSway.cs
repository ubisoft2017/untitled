﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardSway : MonoBehaviour {

	[SerializeField] private Transform guardPivot;
	[SerializeField] private float leftRotation;
	[SerializeField] private float rightRotation;
	[SerializeField] private float leanDuration;

	private bool leanLeft;
	private bool leanRight;
	private float time;
	private Quaternion leftSwayPos;
	private Quaternion rightSwayPos;
	private Quaternion currentRot;

	// Use this for initialization
	void Start () {
		leanLeft = true;
		leftSwayPos = Quaternion.Euler( 0, 0, leftRotation );
		rightSwayPos = Quaternion.Euler( 0, 0, rightRotation );
		currentRot = guardPivot.localRotation;

	}
	
	// Update is called once per frame
	void Update () {

		if ( leanLeft )
		{
			time += Time.deltaTime / leanDuration;
			guardPivot.localRotation = Quaternion.Lerp( currentRot, leftSwayPos, time );

			if (time >=1 )
			{
				leanLeft = false;
				leanRight = true;
				currentRot = guardPivot.localRotation;
				time = 0;
			}
		}

		if ( leanRight )
		{
			time += Time.deltaTime / leanDuration;
			guardPivot.localRotation = Quaternion.Lerp( currentRot, rightSwayPos, time );

			if ( time >= 1 )
			{
				leanLeft = true;
				leanRight = false;
				currentRot = guardPivot.localRotation;
				time = 0;
			}
		}
	}
}
