﻿using System;

using UnityEngine;

[Serializable]
[CreateAssetMenu( fileName = "new Level.asset", menuName = "Level", order = 500 )]
public class Level : ScriptableObject
{
	[SerializeField]
	private SceneReference sceneReference;

	public SceneReference SceneReference
	{
		get
		{
			return sceneReference;
		}
	}

	[SerializeField]
	private string title;

	public string Title
	{
		get
		{
			return title;
		}
	}

	[SerializeField]
	private Sprite thumbnail;

	public Sprite Thumbnail
	{
		get
		{
			return thumbnail;
		}
	}

	[SerializeField]
	private bool isTutorial;

	public bool IsTutorial
	{
		get
		{
			return isTutorial;
		}
	}
}