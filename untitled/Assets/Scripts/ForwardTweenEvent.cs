﻿using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;

public class ForwardTweenEvent : MonoBehaviour
{
	[SerializeField]
	private Tweening.TweenEvent source;

	[SerializeField]
	private List<Tweening.TweenEvent> destinations = new List<TweenEvent>( new TweenEvent[1] );

	[SerializeField]
	private bool local;

	[SerializeField]
	private float delay;

	[SerializeField]
	private bool runInBackground;

	public void Fire ()
	{
		if ( local )
		{
			foreach ( var tweener in GetComponents<Tweener>() )
			{
				foreach ( var @event in destinations )
				{
					if ( @event != null )
					{
						@event.Invoke();
					}
				}
			}
		}
		else
		{
			foreach ( var @event in destinations )
			{
				if ( @event != null )
				{
					@event.Invoke();
				}
			}
		}
	}

	private void Awake ()
	{
		source.OnInvoke += HandleInvoke;
	}

	private void HandleInvoke ( TweenEvent @event )
	{
		if ( runInBackground || isActiveAndEnabled )
		{
			if ( delay > 0.0f )
			{
				Invoke( "Fire", delay );
			}
			else
			{
				Fire();
			}
		}
	}
}
