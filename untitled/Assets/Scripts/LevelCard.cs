﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelCard : MonoBehaviour
{
	public RectTransform rect;

	public Button button;
	public Image thumbnail;
	public Text label;

	public EventTrigger eventTrigger;

	public GameObject tutorial;
}
