﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderThisOnTop : MonoBehaviour {

    void Awake() {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraRenderOnTop>().addRenderer(GetComponent<Renderer>());
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraRenderOnTop>().Activate();
    }

}
