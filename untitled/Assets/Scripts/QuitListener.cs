﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tweening;
using UnityEngine;

public class QuitListener : MonoBehaviour
{
	public TweenEvent quitEvent;

	private void Awake ()
	{
		quitEvent.OnInvoke += ( e ) => Application.Quit();
	}
}
