﻿/// <summary>
/// Used by patroling AI to track which direction they are going along a patrol route.
/// </summary>
public enum Traversal
{
	Forward,
	Reverse
}

public static class TraversalExtensions
{
	public static Traversal Flip ( this Traversal traversal )
	{
		switch ( traversal )
		{
		case Traversal.Forward:
			return Traversal.Reverse;

		case Traversal.Reverse:
			return Traversal.Forward;

		default:
			throw new System.Exception();
		}
	}
}