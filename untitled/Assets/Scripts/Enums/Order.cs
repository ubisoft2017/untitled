﻿/// <summary>
/// Used by patroling AI to track which direction they are going along a patrol route.
/// </summary>
public enum Order
{
	Before,
	After
}

public static class OrderExtensions
{
	public static Order Reverse ( this Order traversal )
	{
		switch ( traversal )
		{
		case Order.Before:
			return Order.After;

		case Order.After:
			return Order.Before;

		default:
			throw new System.Exception();
		}
	}
}