﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class SceneReference
{
	[SerializeField]
	private UnityEngine.Object asset;

	[SerializeField]
	[HideInInspector]
	private string name;

	[SerializeField]
	[HideInInspector]
	private Scene scene;

	public UnityEngine.Object GetSceneAsset ()
	{
		if ( Application.isPlaying )
		{
			throw new InvalidOperationException( "Scene assets do not exist at runtime." );
		}

		return asset;
	}

	public string GetSceneName ()
	{
		return name;
	}
}
