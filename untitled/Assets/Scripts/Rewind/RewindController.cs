﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RewindController : MonoBehaviour {
    
    bool isRecording;
    bool isRewinding;
    public bool stackBehavior;
    public int bufferSize;
    public bool isActive;
    public int index = 0;
    private int startIndex;
    [Tooltip("How many seconds must past before a frame is saved")]
    public float recordingSpeed;

    [Tooltip("Ratio between time between saved frames and unloading (ex 2 means we load frames tiwce as fast as we saved them)")]
    public float playbackRatio;

    private float playbackSpeed;
    private float recordTimer = 0,rewindTimer = 0;

    public  List<AbstractRecording> recordings;
    public float timeToLoad, timeToSave;
    public float timeToLoadRatio, timeToSaveRatio;
    
    void Awake() {
        recordings = new List<AbstractRecording>();
        playbackSpeed = recordingSpeed / playbackRatio;
        startIndex = 0;
        startRecording();

    }

    void Update() {
        if (isActive)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                if (!isRecording)
                {
                    startRecording();
                }
                else
                {
                    startRewind();
                }
            }
        }
    }
    void FixedUpdate() {
        if (isActive)
        {
            if (isRecording)
            {
                recordTimer += Time.fixedDeltaTime;
                timeToSave = Mathf.Clamp(recordingSpeed - recordTimer, 0, recordTimer);
                timeToSaveRatio = Mathf.Clamp01(recordTimer / recordingSpeed);
                if (recordTimer >= recordingSpeed)
                {
                    recordTimer =0;
                    saveStates();
                }
            }
            else if (isRewinding)
            {
                rewindTimer += Time.fixedDeltaTime;
                timeToLoad = Mathf.Clamp(playbackSpeed - rewindTimer, 0, rewindTimer);
                timeToLoadRatio = Mathf.Clamp01(rewindTimer / playbackSpeed);
                while (rewindTimer >= playbackSpeed)
                {
                    rewindTimer -=playbackSpeed;
                    loadStates();
                }
            }
        }
    }
    public void addRecording(AbstractRecording rec) {
        recordings.Add(rec);
    }
    
    public void startRecording() {
        stopRewind();
        foreach (AbstractRecording rec in recordings)
        {
            rec.recordingStart();
        }
        isRecording = true;
        recordTimer = 0;
        Debug.Log("RECORDING");
    }
    public void stopRecording()
    {
        foreach (AbstractRecording rec in recordings)
        {
            rec.recordingStop();
        }
        isRecording = false;
    }

    public void startRewind() {
        stopRecording();
        foreach (AbstractRecording rec in recordings)
        {
            rec.rewindingStart();
        }
        isRewinding = true;
        rewindTimer = 0;
        Debug.Log("REWINDING");
    }

    public void stopRewind() {
        foreach (AbstractRecording rec in recordings)
        {
            rec.rewindingStop();
        }
        isRewinding = false;

    }
    private void saveStates() {
        foreach(AbstractRecording rec in recordings) {
            rec.saveState(index);
        }
        incrementIndex();
    }
    private void loadStates()
    {
        decrementIndex();
        if (isRewinding)
        {
            foreach (AbstractRecording rec in recordings)
            {
                rec.loadState(index);
            }
        }
    }
    public void resetRecordings() {
        foreach (AbstractRecording rec in recordings)
        {
            rec.reset();
        }
    }

    public void deleteRecordings() {

        recordings.Clear();
    }
    private void incrementIndex() {
        index = (index + 1) % bufferSize;
        if (startIndex == index)
            startIndex = index;
        
    }
    private void decrementIndex()
    {
        index = (index - 1);
        if (!stackBehavior)
        {
            if (index == startIndex)
            {
                endOfRecording();
            }
            if (index < 0)
                index = bufferSize - 1;

        }
        else {
            if (index == 0)
            {
                endOfRecording();
            }
        }
    }
    public void endOfRecording()
    {
        index = 0;
        startIndex = 0;
        resetRecordings();
        startRecording();

    }
}
