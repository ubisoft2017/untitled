﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tweening;

public class TransformRecording : AbstractRecording
{

    private Vector3 endPosition, startPosition;
    private Quaternion endRotation, startRotation;

    public Transform positionTransform;
    public Transform rotationTransform;


    void Update() {
        if (rewinding) {
            positionTransform.position = Vector3.Lerp(positionTransform.position, endPosition, rewindController.timeToLoadRatio );
            rotationTransform.rotation = Quaternion.Slerp(rotationTransform.rotation, endRotation, rewindController.timeToLoadRatio);
        }
    }

    public override void recordingStart()
    {
        rewinding = false;
    }
    public override void rewindingStart()
    {
        rewinding = true;
    }
    public override void rewindingStop()
    {
        positionTransform.position = endPosition;
        rotationTransform.rotation = endRotation;
    }
    public override void saveState(int index) {
        TransformData data = new TransformData(transform);
        saveStateToBuffer(data, index);

    }

    public override void loadState(int index) {
        TransformData data = (TransformData)loadStateFromBuffer(index);
        positionTransform.position = endPosition;
        rotationTransform.rotation = endRotation;
        startPosition = endPosition;
        startRotation = endRotation;

        endPosition = DataTranslator.toVector3(data.data.position);
        endRotation = DataTranslator.toQuaternion(data.data.rotation);


    }



}

