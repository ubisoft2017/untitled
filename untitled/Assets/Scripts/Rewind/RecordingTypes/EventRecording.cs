﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class EventRecording: AbstractRecording 
{

    public override void saveState(int position)
    {

    }
    public override void loadState(int position)
    {

    }


    public override void recordingStart()
    {

    }
    public override void recordingStop()
    {
    }
    public override void rewindingStart()
    {
    }
    public override void rewindingStop()
    {
    }

}
