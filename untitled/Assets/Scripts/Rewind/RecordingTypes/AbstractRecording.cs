﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class AbstractRecording: MonoBehaviour 
{
    public List<AbstractData> recordingBuffer;
    protected RewindController rewindController;
    private int bufferSize;
    protected bool rewinding = false;

    void Awake() {
        rewindController = FindObjectOfType<RewindController>();
        setbufferSize(rewindController.bufferSize);
        if (rewindController != null) {
            rewindController.addRecording(this);
        }
        
    }

    public virtual void setbufferSize(int size) {
        bufferSize = size;
        recordingBuffer = new List<AbstractData>();
        Debug.Log("created array of size: " + recordingBuffer.Count + "size shoud be  : " + size);
    }


    public void reset() {
        recordingBuffer.Clear();
    }
    public void saveStateToBuffer(AbstractData data,int index) {

        if (index >= recordingBuffer.Count)
            recordingBuffer.Add(data);
        else
            recordingBuffer[index] = data;
        
    }
    public AbstractData loadStateFromBuffer(int index) {
        return recordingBuffer[index];

    }

    public abstract void saveState(int position);
    public abstract void loadState(int position);

    public virtual void recordingStart()
    {

    }
    public virtual void recordingStop()
    {
    }
    public virtual void rewindingStart()
    {
    }
    public virtual void rewindingStop()
    {
    }

}
