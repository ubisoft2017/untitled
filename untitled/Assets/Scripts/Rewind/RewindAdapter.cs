﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RewindAdapter : MonoBehaviour {

    public abstract void startRecording();
    public abstract void stopRecording();
    public abstract void startRewind();
    public abstract void stopRewind();

}
