﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformData : AbstractData {

    public TransformDataType data;
    
    public TransformData(Transform t) {
        data = DataTranslator.toTransformData(t);
    }
}
