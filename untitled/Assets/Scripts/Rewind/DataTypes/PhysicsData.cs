﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Vec3
{
    public float x;
    public float y;
    public float z;
}
public struct Vec4
{
    public float x;
    public float y;
    public float z;
    public float w;

}

public struct TransformDataType
{
    public Vec3 position;
    public Vec4 rotation;
}

public struct RigidBodyDataType {
    public Vec3 velocity;
    public Vec3 angularVelocity;
}

public struct PhysicsData {
    public TransformDataType transformData;
    public RigidBodyDataType rigidBodyData;
}

class DataTranslator {
    public static TransformDataType toTransformData(Transform t) {
        TransformDataType data;

        data.position.x = t.position.x;
        data.position.y = t.position.y;
        data.position.z = t.position.z;

        data.rotation.x = t.rotation.x;
        data.rotation.y = t.rotation.y;
        data.rotation.z = t.rotation.z;
        data.rotation.w = t.rotation.w;


        return data;
    }

    public static RigidBodyDataType toRigidBodyData(Rigidbody rb)
    {
        RigidBodyDataType data;

        data.velocity.x = rb.velocity.x;
        data.velocity.y = rb.velocity.y;
        data.velocity.z = rb.velocity.z;

        data.angularVelocity.x = rb.angularVelocity.x;
        data.angularVelocity.y = rb.angularVelocity.y;
        data.angularVelocity.z = rb.angularVelocity.z;


        return data;
    }


    public static Vector3 toVector3(Vec3 vector)
    {
        return new Vector3(vector.x, vector.y, vector.z);
    }

    public static Quaternion toQuaternion(Vec4 vector)
    {
        return new Quaternion(vector.x, vector.y, vector.z, vector.w);
    }
    public static void setTransform(TransformDataType data,Transform t) {

        t.position = toVector3(data.position);
        t.rotation = toQuaternion(data.rotation);
    }

    public static void setRigidBody(RigidBodyDataType data, Rigidbody rb)
    {
        rb.velocity = toVector3(data.velocity);
        rb.angularVelocity = toVector3(data.angularVelocity);
    }
    public static RigidBodyDataType rigidBodyDefault() {
        RigidBodyDataType rb;
        rb.velocity.x = 0;
        rb.velocity.y = 0;
        rb.velocity.z = 0;

        rb.angularVelocity.x = 0;
        rb.angularVelocity.y = 0;
        rb.angularVelocity.z = 0;

        return rb;
    }
}
