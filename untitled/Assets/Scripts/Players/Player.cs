﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using Controls;

/// <summary>
/// Abstraction for a connected player.
/// It follows that there should be a 1:1 correspondence from connected input devices (i.e game pads) to Player instances.
/// </summary>
[Serializable]
public class Player
{
	private PlayerProfile profile;

	public PlayerProfile Profile
	{
		get
		{
			return profile;
		}
	}

	private PlayerState state;

	public PlayerState State
	{
		get
		{
			return state;
		}

		set
		{
			state = value;

			Debug.Log( "state is " + (value == null ? "null" : value.name ) );
            GamePad.instance.ReadStates();
        }
	}

	public bool HasJoined
	{
		get
		{
			return profile != null;
		}
	}

	public bool TryJoinAs ( PlayerProfile profile )
	{
		if ( this.profile != null )
		{
			PlayerProfiles.Release( this.profile );
		}

		if ( PlayerProfiles.AvailableProfiles.Contains( profile ) )
		{
			this.profile = profile;

			if ( this.profile != null )
			{
				PlayerProfiles.Consume( this.profile );
			}

			return true;
		}
		else
		{
			this.profile = null;
		}

		return false;
	}
}
