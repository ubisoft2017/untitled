﻿using Controls;
using UnityEngine;

[CreateAssetMenu( fileName = "New PlayerType.asset", menuName = "Input/Player Type", order = 120 )]
public class PlayerType : ScriptableObject
{
	[Header( "Info" )]

	[Tooltip( "Short label used in options menu, etc." )]
	[SerializeField] private string label;

	public string Label
	{
		get
		{
			return label;
		}
	}

	[Tooltip( "Description of this player type. May be appear on help screens, options menu, etc." )]
	[SerializeField] private string description;

	public string Description
	{
		get
		{
			return description;
		}
	}

	[SerializeField]
	[EnumObject( typeof( PlayerState ), PlayerState.RESOURCE_PATH )]
	private PlayerState defaultState;

	public PlayerState DefaultState
	{
		get
		{
			return defaultState;
		}
	}
}
