﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

using UnityEngine;

public static class PlayerProfiles
{
	private static string path;

	// List of unclaimed profiles
	private static List<PlayerProfile> availableProfiles;

	public static ReadOnlyCollection<PlayerProfile> AvailableProfiles
	{
		get
		{
			return availableProfiles.AsReadOnly();
		}
	}

	private static bool ready;

	public static bool Ready
	{
		get
		{
			return ready;
		}
	}

	static PlayerProfiles ()
	{
		availableProfiles = new List<PlayerProfile>();
	}

	[RuntimeInitializeOnLoadMethod]
	private static void Initialize ()
	{
		path = Application.persistentDataPath + "/profiles/";

		if ( !Directory.Exists( path ) )
		{
			Directory.CreateDirectory( path );
		}

		ready = true;

		ReadAll();
	}

	public static void Consume ( PlayerProfile profile )
	{
		availableProfiles.Remove( profile );
	}

	public static void Release ( PlayerProfile profile )
	{
		if ( ready && !availableProfiles.Contains( profile ) )
		{
			availableProfiles.Add( profile );
		}
	}

	/// <summary>
	/// Write the PlayerProfile to the disk.
	/// </summary>
	/// <param name="profile"></param>
	public static void Write ( PlayerProfile profile )
	{
		/// Open a stream to a file in the player profiles directory, using the profile's GUID as the file name
		using ( var stream = new StreamWriter( new FileStream( string.Format( "{0}{1}.dat", path, profile.GUID.ToString() ), FileMode.OpenOrCreate ) ) )
		{
			/// Stringify mappings and GUID
			profile.PreSerialize();

			/// Write profile to stream as JSON
			stream.Write( JsonUtility.ToJson( profile ) );

			/// Flush stream
			stream.Flush();
		}
	}

	/// <summary>
	/// Read all PlayerProfile data from the disk.
	/// </summary>
	public static void ReadAll ()
	{
		/// Iterate over files in the player profiles directory
		foreach ( var file in Directory.GetFiles( path ) )
		{
			/// Create profile from JSON in file
			var profile = JsonUtility.FromJson<PlayerProfile>( File.ReadAllText( file ) );

			/// Destringify mappings and GUID
			profile.PostSerialize();
		}
	}
}
