﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Controls;
using System.Collections.ObjectModel;

/// <summary>
/// Represents a player's profile. Stores name, mappings, and any other information we might need.
/// TODO: use total order of PlayerAction instances to implement O(1) mapping lookups.
/// </summary>
[Serializable]
public class PlayerProfile
{
	private Guid guid;

	public Guid GUID
	{
		get
		{
			return guid;
		}
	}

	public string name;

	private List<ButtonMapping> buttonMappings;
	private List<TriggerMapping> triggerMappings;
	private List<JoystickMapping> joystickMappings;

	public ButtonMapping this[ButtonAction a]
	{
		get
		{
			return buttonMappings[a.Number];
		}
	}

	public TriggerMapping this[TriggerAction a]
	{
		get
		{
			return triggerMappings[a.Number];
		}
	}

	public JoystickMapping this[JoystickAction a]
	{
		get
		{
			return joystickMappings[a.Number];
		}
	}

	public ReadOnlyCollection<ButtonMapping> ButtonMappings
	{
		get
		{
			return buttonMappings.AsReadOnly();
		}
	}

	public ReadOnlyCollection<TriggerMapping> TriggerMappings
	{
		get
		{
			return triggerMappings.AsReadOnly();
		}
	}

	public ReadOnlyCollection<JoystickMapping> JoystickMappings
	{
		get
		{
			return joystickMappings.AsReadOnly();
		}
	}

	public PlayerProfile ()
	{
		buttonMappings = new List<ButtonMapping>( ButtonAction.Count );
		triggerMappings = new List<TriggerMapping>( TriggerAction.Count );
		joystickMappings = new List<JoystickMapping>( JoystickAction.Count );

		for ( int i = 0; i < ButtonAction.Count; i++ )
		{
			buttonMappings.Add( new ButtonMapping( ButtonAction.GetAction( i ) ) );
		}

		for ( int i = 0; i < TriggerAction.Count; i++ )
		{
			triggerMappings.Add( new TriggerMapping( TriggerAction.GetAction( i ) ) );
		}

		for ( int i = 0; i < JoystickAction.Count; i++ )
		{
			joystickMappings.Add( new JoystickMapping( JoystickAction.GetAction( i ) ) );
		}

		init = true;
	}

	public PlayerProfile ( string name ) : this()
	{
		guid = Guid.NewGuid();

		this.name = name;

		PlayerProfiles.Release( this );
	}

	[SerializeField]
	private List<string> serializedButtonActionNames, serializedTriggerActionNames, serializedJoystickActionNames;

	[SerializeField]
	private List<string> serializedButtonNames, serializedTriggerNames, serializedJoystickNames;

	[SerializeField]
	private string serializedGUID;

	[SerializeField]
	private bool init = false;

	public void PreSerialize ()
	{
		if ( !PlayerProfiles.Ready )
		{
			return;
		}

		if ( init )
		{
			serializedGUID = guid.ToString();

			serializedButtonActionNames = new List<string>( buttonMappings.Count );
			serializedButtonNames = new List<string>( buttonMappings.Count );

			foreach ( var mapping in buttonMappings )
			{
				serializedButtonActionNames.Add( mapping.Action.name );
				serializedButtonNames.Add( mapping.mapping.ToString() );
			}

			serializedTriggerActionNames = new List<string>( triggerMappings.Count );
			serializedTriggerNames = new List<string>( buttonMappings.Count );

			foreach ( var mapping in triggerMappings )
			{
				serializedTriggerActionNames.Add( mapping.Action.name );
				serializedTriggerNames.Add( mapping.mapping.ToString() );
			}

			serializedJoystickActionNames = new List<string>( joystickMappings.Count );
			serializedJoystickNames = new List<string>( buttonMappings.Count );

			foreach ( var mapping in joystickMappings )
			{
				serializedJoystickActionNames.Add( mapping.Action.name );
				serializedJoystickNames.Add( mapping.mapping.ToString() );
			}
		}
	}

	public void PostSerialize ()
	{
		if ( !PlayerProfiles.Ready )
		{
			return;
		}

		if ( init )
		{
			guid = new Guid( serializedGUID );

			for ( int i = 0; i < serializedButtonNames.Count; i++ )
			{
				foreach ( var mapping in buttonMappings )
				{
					if ( mapping.Action.name == serializedButtonActionNames[i] )
					{
						mapping.mapping = (DeviceButton) Enum.Parse( typeof( DeviceButton ), serializedButtonNames[i], true );
					}
				}
			}

			for ( int i = 0; i < serializedTriggerNames.Count; i++ )
			{
				foreach ( var mapping in triggerMappings )
				{
					if ( mapping.Action.name == serializedTriggerActionNames[i] )
					{
						mapping.mapping = (DeviceTrigger) Enum.Parse( typeof( DeviceTrigger ), serializedTriggerNames[i], true );
					}
				}
			}

			for ( int i = 0; i < serializedJoystickNames.Count; i++ )
			{
				foreach ( var mapping in joystickMappings )
				{
					if ( mapping.Action.name == serializedJoystickActionNames[i] )
					{
						mapping.mapping = (DeviceJoystick) Enum.Parse( typeof( DeviceJoystick ), serializedJoystickNames[i], true );
					}
				}
			}

			PlayerProfiles.Release( this );
		}
	}
}

