﻿using System.Collections;
using System.Collections.Generic;
using Process = System.Diagnostics.Process;
using System.IO;
using Tweening;
using UnityEngine;

public class PhotonServerLauncher : MonoBehaviour
{

    private string punPath = "/PhotonServer/Photon-OnPremise-Server-SDK_v4-0-29-11263/deploy/bin_Win64/";
    private string folder;

    private string fileName;

    private string logFilePath;

    public TweenEvent serverReadyEvent;

    Process server = null;


    public void Start()
    {
        string rootFolderPath = Path.GetDirectoryName(Application.dataPath);
        folder = rootFolderPath + punPath;
        fileName = folder + "PhotonSocketServer.exe";

    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.E)){
            Resources.Load<TweenEvent>("Tweening/Events/twe_network_local").Invoke();
        }
    }

    public void StartServer()
    {
        StopPrevious();

        Directory.Delete(folder + "log/", true);

        Process app = GetApp();

        app.StartInfo.Arguments = "/run LoadBalancing";

        app.Start();

        server = app;

        System.DateTime now = System.DateTime.Now;
        string date = "" + now.Year.ToString("0000") + now.Month.ToString("00") + now.Day.ToString("00");

        logFilePath = folder + "log/Photon-LoadBalancing-" + date + ".log";

        StartCoroutine(testYet());
    }


    private void StopPrevious()
    {
        Process app = GetApp();

        app.StartInfo.Arguments = "/stop";
        app.Start();

        app.WaitForExit();

        //wait until no PhotonSocketServer are running
        Process[] photons = Process.GetProcessesByName("PhotonSocketServer");

        foreach(Process proc in photons)
        {
            proc.WaitForExit();
        }

        server = null;
    }


    Process GetApp()
    {
        Process app = new Process();


        app.StartInfo.FileName = fileName;
        app.StartInfo.WorkingDirectory = folder;
        return app;

    }


    IEnumerator testYet()
    {
        while (true)
        {
            try
            {
                File.Copy(logFilePath, logFilePath + "2", true);

                string s = File.ReadAllText(logFilePath + "2");


                if (s.Contains("Service is running"))
                {
                    Debug.Log("YEEEEEEEEEEEEEESSSSSSSSSSSSS");
                    //serverStarted.Invoke();
                    //serverReady = true;
                    serverReadyEvent.Invoke();
                    StopAllCoroutines();
                }

            }
            catch (IOException e)
            {
                ;
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    void OnApplicationQuit()
    {
        if (server != null)
        {
            StopPrevious();
        }
    }
}